import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('MealClassification e2e test', () => {

    let navBarPage: NavBarPage;
    let mealClassificationDialogPage: MealClassificationDialogPage;
    let mealClassificationComponentsPage: MealClassificationComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load MealClassifications', () => {
        navBarPage.goToEntity('meal-classification');
        mealClassificationComponentsPage = new MealClassificationComponentsPage();
        expect(mealClassificationComponentsPage.getTitle())
            .toMatch(/ichooseApp.mealClassification.home.title/);

    });

    it('should load create MealClassification dialog', () => {
        mealClassificationComponentsPage.clickOnCreateButton();
        mealClassificationDialogPage = new MealClassificationDialogPage();
        expect(mealClassificationDialogPage.getModalTitle())
            .toMatch(/ichooseApp.mealClassification.home.createOrEditLabel/);
        mealClassificationDialogPage.close();
    });

    it('should create and save MealClassifications', () => {
        mealClassificationComponentsPage.clickOnCreateButton();
        mealClassificationDialogPage.setNameInput('name');
        expect(mealClassificationDialogPage.getNameInput()).toMatch('name');
        mealClassificationDialogPage.save();
        expect(mealClassificationDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class MealClassificationComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-meal-classification div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class MealClassificationDialogPage {
    modalTitle = element(by.css('h4#myMealClassificationLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    };

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
