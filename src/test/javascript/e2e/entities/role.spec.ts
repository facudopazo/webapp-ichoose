import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Role e2e test', () => {

    let navBarPage: NavBarPage;
    let roleDialogPage: RoleDialogPage;
    let roleComponentsPage: RoleComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Roles', () => {
        navBarPage.goToEntity('role');
        roleComponentsPage = new RoleComponentsPage();
        expect(roleComponentsPage.getTitle())
            .toMatch(/ichooseApp.role.home.title/);

    });

    it('should load create Role dialog', () => {
        roleComponentsPage.clickOnCreateButton();
        roleDialogPage = new RoleDialogPage();
        expect(roleDialogPage.getModalTitle())
            .toMatch(/ichooseApp.role.home.createOrEditLabel/);
        roleDialogPage.close();
    });

    it('should create and save Roles', () => {
        roleComponentsPage.clickOnCreateButton();
        roleDialogPage.setTypeInput('type');
        expect(roleDialogPage.getTypeInput()).toMatch('type');
        roleDialogPage.setLoginInput('login');
        expect(roleDialogPage.getLoginInput()).toMatch('login');
        roleDialogPage.restaurantSelectLastOption();
        roleDialogPage.save();
        expect(roleDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class RoleComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-role div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class RoleDialogPage {
    modalTitle = element(by.css('h4#myRoleLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    typeInput = element(by.css('input#field_type'));
    loginInput = element(by.css('input#field_login'));
    restaurantSelect = element(by.css('select#field_restaurant'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setTypeInput = function(type) {
        this.typeInput.sendKeys(type);
    };

    getTypeInput = function() {
        return this.typeInput.getAttribute('value');
    };

    setLoginInput = function(login) {
        this.loginInput.sendKeys(login);
    };

    getLoginInput = function() {
        return this.loginInput.getAttribute('value');
    };

    restaurantSelectLastOption = function() {
        this.restaurantSelect.all(by.tagName('option')).last().click();
    };

    restaurantSelectOption = function(option) {
        this.restaurantSelect.sendKeys(option);
    };

    getRestaurantSelect = function() {
        return this.restaurantSelect;
    };

    getRestaurantSelectedOption = function() {
        return this.restaurantSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
