import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Shift e2e test', () => {

    let navBarPage: NavBarPage;
    let shiftDialogPage: ShiftDialogPage;
    let shiftComponentsPage: ShiftComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Shifts', () => {
        navBarPage.goToEntity('shift');
        shiftComponentsPage = new ShiftComponentsPage();
        expect(shiftComponentsPage.getTitle())
            .toMatch(/ichooseApp.shift.home.title/);

    });

    it('should load create Shift dialog', () => {
        shiftComponentsPage.clickOnCreateButton();
        shiftDialogPage = new ShiftDialogPage();
        expect(shiftDialogPage.getModalTitle())
            .toMatch(/ichooseApp.shift.home.createOrEditLabel/);
        shiftDialogPage.close();
    });

    it('should create and save Shifts', () => {
        shiftComponentsPage.clickOnCreateButton();
        shiftDialogPage.setStartInput('start');
        expect(shiftDialogPage.getStartInput()).toMatch('start');
        shiftDialogPage.setEndInput('end');
        expect(shiftDialogPage.getEndInput()).toMatch('end');
        shiftDialogPage.setNameInput('name');
        expect(shiftDialogPage.getNameInput()).toMatch('name');
        shiftDialogPage.restaurantSelectLastOption();
        shiftDialogPage.save();
        expect(shiftDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ShiftComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-shift div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ShiftDialogPage {
    modalTitle = element(by.css('h4#myShiftLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    startInput = element(by.css('input#field_start'));
    endInput = element(by.css('input#field_end'));
    nameInput = element(by.css('input#field_name'));
    restaurantSelect = element(by.css('select#field_restaurant'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setStartInput = function(start) {
        this.startInput.sendKeys(start);
    };

    getStartInput = function() {
        return this.startInput.getAttribute('value');
    };

    setEndInput = function(end) {
        this.endInput.sendKeys(end);
    };

    getEndInput = function() {
        return this.endInput.getAttribute('value');
    };

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    };

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    };

    restaurantSelectLastOption = function() {
        this.restaurantSelect.all(by.tagName('option')).last().click();
    };

    restaurantSelectOption = function(option) {
        this.restaurantSelect.sendKeys(option);
    };

    getRestaurantSelect = function() {
        return this.restaurantSelect;
    };

    getRestaurantSelectedOption = function() {
        return this.restaurantSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
