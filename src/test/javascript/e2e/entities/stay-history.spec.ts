import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('StayHistory e2e test', () => {

    let navBarPage: NavBarPage;
    let stayHistoryDialogPage: StayHistoryDialogPage;
    let stayHistoryComponentsPage: StayHistoryComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load StayHistories', () => {
        navBarPage.goToEntity('stay-history');
        stayHistoryComponentsPage = new StayHistoryComponentsPage();
        expect(stayHistoryComponentsPage.getTitle())
            .toMatch(/ichooseApp.stayHistory.home.title/);

    });

    it('should load create StayHistory dialog', () => {
        stayHistoryComponentsPage.clickOnCreateButton();
        stayHistoryDialogPage = new StayHistoryDialogPage();
        expect(stayHistoryDialogPage.getModalTitle())
            .toMatch(/ichooseApp.stayHistory.home.createOrEditLabel/);
        stayHistoryDialogPage.close();
    });

    it('should create and save StayHistories', () => {
        stayHistoryComponentsPage.clickOnCreateButton();
        stayHistoryDialogPage.setNumberOfStaysInput('5');
        expect(stayHistoryDialogPage.getNumberOfStaysInput()).toMatch('5');
        stayHistoryDialogPage.setAverageAmountInput('5');
        expect(stayHistoryDialogPage.getAverageAmountInput()).toMatch('5');
        stayHistoryDialogPage.setLastStayInput(12310020012301);
        expect(stayHistoryDialogPage.getLastStayInput()).toMatch('2001-12-31T02:30');
        stayHistoryDialogPage.restaurantSelectLastOption();
        stayHistoryDialogPage.clientSelectLastOption();
        stayHistoryDialogPage.save();
        expect(stayHistoryDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class StayHistoryComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-stay-history div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class StayHistoryDialogPage {
    modalTitle = element(by.css('h4#myStayHistoryLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    numberOfStaysInput = element(by.css('input#field_numberOfStays'));
    averageAmountInput = element(by.css('input#field_averageAmount'));
    lastStayInput = element(by.css('input#field_lastStay'));
    restaurantSelect = element(by.css('select#field_restaurant'));
    clientSelect = element(by.css('select#field_client'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNumberOfStaysInput = function(numberOfStays) {
        this.numberOfStaysInput.sendKeys(numberOfStays);
    };

    getNumberOfStaysInput = function() {
        return this.numberOfStaysInput.getAttribute('value');
    };

    setAverageAmountInput = function(averageAmount) {
        this.averageAmountInput.sendKeys(averageAmount);
    };

    getAverageAmountInput = function() {
        return this.averageAmountInput.getAttribute('value');
    };

    setLastStayInput = function(lastStay) {
        this.lastStayInput.sendKeys(lastStay);
    };

    getLastStayInput = function() {
        return this.lastStayInput.getAttribute('value');
    };

    restaurantSelectLastOption = function() {
        this.restaurantSelect.all(by.tagName('option')).last().click();
    };

    restaurantSelectOption = function(option) {
        this.restaurantSelect.sendKeys(option);
    };

    getRestaurantSelect = function() {
        return this.restaurantSelect;
    };

    getRestaurantSelectedOption = function() {
        return this.restaurantSelect.element(by.css('option:checked')).getText();
    };

    clientSelectLastOption = function() {
        this.clientSelect.all(by.tagName('option')).last().click();
    };

    clientSelectOption = function(option) {
        this.clientSelect.sendKeys(option);
    };

    getClientSelect = function() {
        return this.clientSelect;
    };

    getClientSelectedOption = function() {
        return this.clientSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
