import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Booking e2e test', () => {

    let navBarPage: NavBarPage;
    let bookingDialogPage: BookingDialogPage;
    let bookingComponentsPage: BookingComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Bookings', () => {
        navBarPage.goToEntity('booking');
        bookingComponentsPage = new BookingComponentsPage();
        expect(bookingComponentsPage.getTitle())
            .toMatch(/ichooseApp.booking.home.title/);

    });

    it('should load create Booking dialog', () => {
        bookingComponentsPage.clickOnCreateButton();
        bookingDialogPage = new BookingDialogPage();
        expect(bookingDialogPage.getModalTitle())
            .toMatch(/ichooseApp.booking.home.createOrEditLabel/);
        bookingDialogPage.close();
    });

    it('should create and save Bookings', () => {
        bookingComponentsPage.clickOnCreateButton();
        bookingDialogPage.setDateTimeInput(12310020012301);
        expect(bookingDialogPage.getDateTimeInput()).toMatch('2001-12-31T02:30');
        bookingDialogPage.setPeopleInput('5');
        expect(bookingDialogPage.getPeopleInput()).toMatch('5');
        bookingDialogPage.setClientNameInput('clientName');
        expect(bookingDialogPage.getClientNameInput()).toMatch('clientName');
        bookingDialogPage.setClientTelephoneNumberInput('clientTelephoneNumber');
        expect(bookingDialogPage.getClientTelephoneNumberInput()).toMatch('clientTelephoneNumber');
        bookingDialogPage.getActiveInput().isSelected().then((selected) => {
            if (selected) {
                bookingDialogPage.getActiveInput().click();
                expect(bookingDialogPage.getActiveInput().isSelected()).toBeFalsy();
            } else {
                bookingDialogPage.getActiveInput().click();
                expect(bookingDialogPage.getActiveInput().isSelected()).toBeTruthy();
            }
        });
        bookingDialogPage.setCommentsInput('comments');
        expect(bookingDialogPage.getCommentsInput()).toMatch('comments');
        bookingDialogPage.tableSelectLastOption();
        bookingDialogPage.save();
        expect(bookingDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class BookingComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-booking div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class BookingDialogPage {
    modalTitle = element(by.css('h4#myBookingLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    dateTimeInput = element(by.css('input#field_dateTime'));
    peopleInput = element(by.css('input#field_people'));
    clientNameInput = element(by.css('input#field_clientName'));
    clientTelephoneNumberInput = element(by.css('input#field_clientTelephoneNumber'));
    activeInput = element(by.css('input#field_active'));
    commentsInput = element(by.css('input#field_comments'));
    tableSelect = element(by.css('select#field_table'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setDateTimeInput = function(dateTime) {
        this.dateTimeInput.sendKeys(dateTime);
    };

    getDateTimeInput = function() {
        return this.dateTimeInput.getAttribute('value');
    };

    setPeopleInput = function(people) {
        this.peopleInput.sendKeys(people);
    };

    getPeopleInput = function() {
        return this.peopleInput.getAttribute('value');
    };

    setClientNameInput = function(clientName) {
        this.clientNameInput.sendKeys(clientName);
    };

    getClientNameInput = function() {
        return this.clientNameInput.getAttribute('value');
    };

    setClientTelephoneNumberInput = function(clientTelephoneNumber) {
        this.clientTelephoneNumberInput.sendKeys(clientTelephoneNumber);
    };

    getClientTelephoneNumberInput = function() {
        return this.clientTelephoneNumberInput.getAttribute('value');
    };

    getActiveInput = function() {
        return this.activeInput;
    };
    setCommentsInput = function(comments) {
        this.commentsInput.sendKeys(comments);
    };

    getCommentsInput = function() {
        return this.commentsInput.getAttribute('value');
    };

    tableSelectLastOption = function() {
        this.tableSelect.all(by.tagName('option')).last().click();
    };

    tableSelectOption = function(option) {
        this.tableSelect.sendKeys(option);
    };

    getTableSelect = function() {
        return this.tableSelect;
    };

    getTableSelectedOption = function() {
        return this.tableSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
