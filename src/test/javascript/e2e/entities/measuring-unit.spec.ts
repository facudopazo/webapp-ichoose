import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('MeasuringUnit e2e test', () => {

    let navBarPage: NavBarPage;
    let measuringUnitDialogPage: MeasuringUnitDialogPage;
    let measuringUnitComponentsPage: MeasuringUnitComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load MeasuringUnits', () => {
        navBarPage.goToEntity('measuring-unit');
        measuringUnitComponentsPage = new MeasuringUnitComponentsPage();
        expect(measuringUnitComponentsPage.getTitle())
            .toMatch(/ichooseApp.measuringUnit.home.title/);

    });

    it('should load create MeasuringUnit dialog', () => {
        measuringUnitComponentsPage.clickOnCreateButton();
        measuringUnitDialogPage = new MeasuringUnitDialogPage();
        expect(measuringUnitDialogPage.getModalTitle())
            .toMatch(/ichooseApp.measuringUnit.home.createOrEditLabel/);
        measuringUnitDialogPage.close();
    });

    it('should create and save MeasuringUnits', () => {
        measuringUnitComponentsPage.clickOnCreateButton();
        measuringUnitDialogPage.setNameInput('name');
        expect(measuringUnitDialogPage.getNameInput()).toMatch('name');
        measuringUnitDialogPage.setCodeInput('code');
        expect(measuringUnitDialogPage.getCodeInput()).toMatch('code');
        measuringUnitDialogPage.save();
        expect(measuringUnitDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class MeasuringUnitComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-measuring-unit div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class MeasuringUnitDialogPage {
    modalTitle = element(by.css('h4#myMeasuringUnitLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));
    codeInput = element(by.css('input#field_code'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    };

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    };

    setCodeInput = function(code) {
        this.codeInput.sendKeys(code);
    };

    getCodeInput = function() {
        return this.codeInput.getAttribute('value');
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
