import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('ClientTableStatus e2e test', () => {

    let navBarPage: NavBarPage;
    let clientTableStatusDialogPage: ClientTableStatusDialogPage;
    let clientTableStatusComponentsPage: ClientTableStatusComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load ClientTableStatuses', () => {
        navBarPage.goToEntity('client-table-status');
        clientTableStatusComponentsPage = new ClientTableStatusComponentsPage();
        expect(clientTableStatusComponentsPage.getTitle())
            .toMatch(/ichooseApp.clientTableStatus.home.title/);

    });

    it('should load create ClientTableStatus dialog', () => {
        clientTableStatusComponentsPage.clickOnCreateButton();
        clientTableStatusDialogPage = new ClientTableStatusDialogPage();
        expect(clientTableStatusDialogPage.getModalTitle())
            .toMatch(/ichooseApp.clientTableStatus.home.createOrEditLabel/);
        clientTableStatusDialogPage.close();
    });

    it('should create and save ClientTableStatuses', () => {
        clientTableStatusComponentsPage.clickOnCreateButton();
        clientTableStatusDialogPage.setNameInput('name');
        expect(clientTableStatusDialogPage.getNameInput()).toMatch('name');
        clientTableStatusDialogPage.save();
        expect(clientTableStatusDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ClientTableStatusComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-client-table-status div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ClientTableStatusDialogPage {
    modalTitle = element(by.css('h4#myClientTableStatusLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    };

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
