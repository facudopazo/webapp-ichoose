import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Nationality e2e test', () => {

    let navBarPage: NavBarPage;
    let nationalityDialogPage: NationalityDialogPage;
    let nationalityComponentsPage: NationalityComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Nationalities', () => {
        navBarPage.goToEntity('nationality');
        nationalityComponentsPage = new NationalityComponentsPage();
        expect(nationalityComponentsPage.getTitle())
            .toMatch(/ichooseApp.nationality.home.title/);

    });

    it('should load create Nationality dialog', () => {
        nationalityComponentsPage.clickOnCreateButton();
        nationalityDialogPage = new NationalityDialogPage();
        expect(nationalityDialogPage.getModalTitle())
            .toMatch(/ichooseApp.nationality.home.createOrEditLabel/);
        nationalityDialogPage.close();
    });

    it('should create and save Nationalities', () => {
        nationalityComponentsPage.clickOnCreateButton();
        nationalityDialogPage.setNameSpInput('nameSp');
        expect(nationalityDialogPage.getNameSpInput()).toMatch('nameSp');
        nationalityDialogPage.setNameEnInput('nameEn');
        expect(nationalityDialogPage.getNameEnInput()).toMatch('nameEn');
        nationalityDialogPage.setNameInput('name');
        expect(nationalityDialogPage.getNameInput()).toMatch('name');
        nationalityDialogPage.setIso2Input('iso2');
        expect(nationalityDialogPage.getIso2Input()).toMatch('iso2');
        nationalityDialogPage.setIso3Input('iso3');
        expect(nationalityDialogPage.getIso3Input()).toMatch('iso3');
        nationalityDialogPage.setPhoneCodeInput('phoneCode');
        expect(nationalityDialogPage.getPhoneCodeInput()).toMatch('phoneCode');
        nationalityDialogPage.imageSelectLastOption();
        nationalityDialogPage.save();
        expect(nationalityDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class NationalityComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-nationality div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class NationalityDialogPage {
    modalTitle = element(by.css('h4#myNationalityLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameSpInput = element(by.css('input#field_nameSp'));
    nameEnInput = element(by.css('input#field_nameEn'));
    nameInput = element(by.css('input#field_name'));
    iso2Input = element(by.css('input#field_iso2'));
    iso3Input = element(by.css('input#field_iso3'));
    phoneCodeInput = element(by.css('input#field_phoneCode'));
    imageSelect = element(by.css('select#field_image'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameSpInput = function(nameSp) {
        this.nameSpInput.sendKeys(nameSp);
    };

    getNameSpInput = function() {
        return this.nameSpInput.getAttribute('value');
    };

    setNameEnInput = function(nameEn) {
        this.nameEnInput.sendKeys(nameEn);
    };

    getNameEnInput = function() {
        return this.nameEnInput.getAttribute('value');
    };

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    };

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    };

    setIso2Input = function(iso2) {
        this.iso2Input.sendKeys(iso2);
    };

    getIso2Input = function() {
        return this.iso2Input.getAttribute('value');
    };

    setIso3Input = function(iso3) {
        this.iso3Input.sendKeys(iso3);
    };

    getIso3Input = function() {
        return this.iso3Input.getAttribute('value');
    };

    setPhoneCodeInput = function(phoneCode) {
        this.phoneCodeInput.sendKeys(phoneCode);
    };

    getPhoneCodeInput = function() {
        return this.phoneCodeInput.getAttribute('value');
    };

    imageSelectLastOption = function() {
        this.imageSelect.all(by.tagName('option')).last().click();
    };

    imageSelectOption = function(option) {
        this.imageSelect.sendKeys(option);
    };

    getImageSelect = function() {
        return this.imageSelect;
    };

    getImageSelectedOption = function() {
        return this.imageSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
