import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Menu e2e test', () => {

    let navBarPage: NavBarPage;
    let menuDialogPage: MenuDialogPage;
    let menuComponentsPage: MenuComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Menus', () => {
        navBarPage.goToEntity('menu');
        menuComponentsPage = new MenuComponentsPage();
        expect(menuComponentsPage.getTitle())
            .toMatch(/ichooseApp.menu.home.title/);

    });

    it('should load create Menu dialog', () => {
        menuComponentsPage.clickOnCreateButton();
        menuDialogPage = new MenuDialogPage();
        expect(menuDialogPage.getModalTitle())
            .toMatch(/ichooseApp.menu.home.createOrEditLabel/);
        menuDialogPage.close();
    });

    it('should create and save Menus', () => {
        menuComponentsPage.clickOnCreateButton();
        menuDialogPage.setNameInput('name');
        expect(menuDialogPage.getNameInput()).toMatch('name');
        menuDialogPage.setDescriptionInput('description');
        expect(menuDialogPage.getDescriptionInput()).toMatch('description');
        menuDialogPage.setPriceInput('5');
        expect(menuDialogPage.getPriceInput()).toMatch('5');
        menuDialogPage.setQuantityInput('5');
        expect(menuDialogPage.getQuantityInput()).toMatch('5');
        menuDialogPage.setDestinationInput('destination');
        expect(menuDialogPage.getDestinationInput()).toMatch('destination');
        menuDialogPage.getDisabledInput().isSelected().then((selected) => {
            if (selected) {
                menuDialogPage.getDisabledInput().click();
                expect(menuDialogPage.getDisabledInput().isSelected()).toBeFalsy();
            } else {
                menuDialogPage.getDisabledInput().click();
                expect(menuDialogPage.getDisabledInput().isSelected()).toBeTruthy();
            }
        });
        menuDialogPage.setDeleteDateInput(12310020012301);
        expect(menuDialogPage.getDeleteDateInput()).toMatch('2001-12-31T02:30');
        menuDialogPage.measuringUnitSelectLastOption();
        menuDialogPage.classificationSelectLastOption();
        menuDialogPage.restaurantSelectLastOption();
        // menuDialogPage.picturesSelectLastOption();
        // menuDialogPage.subclassificationsSelectLastOption();
        menuDialogPage.save();
        expect(menuDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class MenuComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-menu div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class MenuDialogPage {
    modalTitle = element(by.css('h4#myMenuLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));
    descriptionInput = element(by.css('input#field_description'));
    priceInput = element(by.css('input#field_price'));
    quantityInput = element(by.css('input#field_quantity'));
    destinationInput = element(by.css('input#field_destination'));
    disabledInput = element(by.css('input#field_disabled'));
    deleteDateInput = element(by.css('input#field_deleteDate'));
    measuringUnitSelect = element(by.css('select#field_measuringUnit'));
    classificationSelect = element(by.css('select#field_classification'));
    restaurantSelect = element(by.css('select#field_restaurant'));
    picturesSelect = element(by.css('select#field_pictures'));
    subclassificationsSelect = element(by.css('select#field_subclassifications'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    };

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    };

    setDescriptionInput = function(description) {
        this.descriptionInput.sendKeys(description);
    };

    getDescriptionInput = function() {
        return this.descriptionInput.getAttribute('value');
    };

    setPriceInput = function(price) {
        this.priceInput.sendKeys(price);
    };

    getPriceInput = function() {
        return this.priceInput.getAttribute('value');
    };

    setQuantityInput = function(quantity) {
        this.quantityInput.sendKeys(quantity);
    };

    getQuantityInput = function() {
        return this.quantityInput.getAttribute('value');
    };

    setDestinationInput = function(destination) {
        this.destinationInput.sendKeys(destination);
    };

    getDestinationInput = function() {
        return this.destinationInput.getAttribute('value');
    };

    getDisabledInput = function() {
        return this.disabledInput;
    };
    setDeleteDateInput = function(deleteDate) {
        this.deleteDateInput.sendKeys(deleteDate);
    };

    getDeleteDateInput = function() {
        return this.deleteDateInput.getAttribute('value');
    };

    measuringUnitSelectLastOption = function() {
        this.measuringUnitSelect.all(by.tagName('option')).last().click();
    };

    measuringUnitSelectOption = function(option) {
        this.measuringUnitSelect.sendKeys(option);
    };

    getMeasuringUnitSelect = function() {
        return this.measuringUnitSelect;
    };

    getMeasuringUnitSelectedOption = function() {
        return this.measuringUnitSelect.element(by.css('option:checked')).getText();
    };

    classificationSelectLastOption = function() {
        this.classificationSelect.all(by.tagName('option')).last().click();
    };

    classificationSelectOption = function(option) {
        this.classificationSelect.sendKeys(option);
    };

    getClassificationSelect = function() {
        return this.classificationSelect;
    };

    getClassificationSelectedOption = function() {
        return this.classificationSelect.element(by.css('option:checked')).getText();
    };

    restaurantSelectLastOption = function() {
        this.restaurantSelect.all(by.tagName('option')).last().click();
    };

    restaurantSelectOption = function(option) {
        this.restaurantSelect.sendKeys(option);
    };

    getRestaurantSelect = function() {
        return this.restaurantSelect;
    };

    getRestaurantSelectedOption = function() {
        return this.restaurantSelect.element(by.css('option:checked')).getText();
    };

    picturesSelectLastOption = function() {
        this.picturesSelect.all(by.tagName('option')).last().click();
    };

    picturesSelectOption = function(option) {
        this.picturesSelect.sendKeys(option);
    };

    getPicturesSelect = function() {
        return this.picturesSelect;
    };

    getPicturesSelectedOption = function() {
        return this.picturesSelect.element(by.css('option:checked')).getText();
    };

    subclassificationsSelectLastOption = function() {
        this.subclassificationsSelect.all(by.tagName('option')).last().click();
    };

    subclassificationsSelectOption = function(option) {
        this.subclassificationsSelect.sendKeys(option);
    };

    getSubclassificationsSelect = function() {
        return this.subclassificationsSelect;
    };

    getSubclassificationsSelectedOption = function() {
        return this.subclassificationsSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
