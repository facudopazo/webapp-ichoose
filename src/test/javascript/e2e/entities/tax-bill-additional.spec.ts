import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('TaxBillAdditional e2e test', () => {

    let navBarPage: NavBarPage;
    let taxBillAdditionalDialogPage: TaxBillAdditionalDialogPage;
    let taxBillAdditionalComponentsPage: TaxBillAdditionalComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load TaxBillAdditionals', () => {
        navBarPage.goToEntity('tax-bill-additional');
        taxBillAdditionalComponentsPage = new TaxBillAdditionalComponentsPage();
        expect(taxBillAdditionalComponentsPage.getTitle())
            .toMatch(/ichooseApp.taxBillAdditional.home.title/);

    });

    it('should load create TaxBillAdditional dialog', () => {
        taxBillAdditionalComponentsPage.clickOnCreateButton();
        taxBillAdditionalDialogPage = new TaxBillAdditionalDialogPage();
        expect(taxBillAdditionalDialogPage.getModalTitle())
            .toMatch(/ichooseApp.taxBillAdditional.home.createOrEditLabel/);
        taxBillAdditionalDialogPage.close();
    });

    it('should create and save TaxBillAdditionals', () => {
        taxBillAdditionalComponentsPage.clickOnCreateButton();
        taxBillAdditionalDialogPage.setDescriptionInput('description');
        expect(taxBillAdditionalDialogPage.getDescriptionInput()).toMatch('description');
        taxBillAdditionalDialogPage.setAmountInput('5');
        expect(taxBillAdditionalDialogPage.getAmountInput()).toMatch('5');
        taxBillAdditionalDialogPage.setIvaInput('5');
        expect(taxBillAdditionalDialogPage.getIvaInput()).toMatch('5');
        taxBillAdditionalDialogPage.getNegativeInput().isSelected().then((selected) => {
            if (selected) {
                taxBillAdditionalDialogPage.getNegativeInput().click();
                expect(taxBillAdditionalDialogPage.getNegativeInput().isSelected()).toBeFalsy();
            } else {
                taxBillAdditionalDialogPage.getNegativeInput().click();
                expect(taxBillAdditionalDialogPage.getNegativeInput().isSelected()).toBeTruthy();
            }
        });
        taxBillAdditionalDialogPage.taxBillSelectLastOption();
        taxBillAdditionalDialogPage.save();
        expect(taxBillAdditionalDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class TaxBillAdditionalComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-tax-bill-additional div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class TaxBillAdditionalDialogPage {
    modalTitle = element(by.css('h4#myTaxBillAdditionalLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    descriptionInput = element(by.css('input#field_description'));
    amountInput = element(by.css('input#field_amount'));
    ivaInput = element(by.css('input#field_iva'));
    negativeInput = element(by.css('input#field_negative'));
    taxBillSelect = element(by.css('select#field_taxBill'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setDescriptionInput = function(description) {
        this.descriptionInput.sendKeys(description);
    };

    getDescriptionInput = function() {
        return this.descriptionInput.getAttribute('value');
    };

    setAmountInput = function(amount) {
        this.amountInput.sendKeys(amount);
    };

    getAmountInput = function() {
        return this.amountInput.getAttribute('value');
    };

    setIvaInput = function(iva) {
        this.ivaInput.sendKeys(iva);
    };

    getIvaInput = function() {
        return this.ivaInput.getAttribute('value');
    };

    getNegativeInput = function() {
        return this.negativeInput;
    };
    taxBillSelectLastOption = function() {
        this.taxBillSelect.all(by.tagName('option')).last().click();
    };

    taxBillSelectOption = function(option) {
        this.taxBillSelect.sendKeys(option);
    };

    getTaxBillSelect = function() {
        return this.taxBillSelect;
    };

    getTaxBillSelectedOption = function() {
        return this.taxBillSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
