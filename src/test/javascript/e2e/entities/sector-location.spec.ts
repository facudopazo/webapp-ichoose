import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('SectorLocation e2e test', () => {

    let navBarPage: NavBarPage;
    let sectorLocationDialogPage: SectorLocationDialogPage;
    let sectorLocationComponentsPage: SectorLocationComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load SectorLocations', () => {
        navBarPage.goToEntity('sector-location');
        sectorLocationComponentsPage = new SectorLocationComponentsPage();
        expect(sectorLocationComponentsPage.getTitle())
            .toMatch(/ichooseApp.sectorLocation.home.title/);

    });

    it('should load create SectorLocation dialog', () => {
        sectorLocationComponentsPage.clickOnCreateButton();
        sectorLocationDialogPage = new SectorLocationDialogPage();
        expect(sectorLocationDialogPage.getModalTitle())
            .toMatch(/ichooseApp.sectorLocation.home.createOrEditLabel/);
        sectorLocationDialogPage.close();
    });

    it('should create and save SectorLocations', () => {
        sectorLocationComponentsPage.clickOnCreateButton();
        sectorLocationDialogPage.setNameInput('name');
        expect(sectorLocationDialogPage.getNameInput()).toMatch('name');
        sectorLocationDialogPage.save();
        expect(sectorLocationDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class SectorLocationComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-sector-location div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class SectorLocationDialogPage {
    modalTitle = element(by.css('h4#mySectorLocationLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    };

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
