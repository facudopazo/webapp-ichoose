import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('TaxBill e2e test', () => {

    let navBarPage: NavBarPage;
    let taxBillDialogPage: TaxBillDialogPage;
    let taxBillComponentsPage: TaxBillComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load TaxBills', () => {
        navBarPage.goToEntity('tax-bill');
        taxBillComponentsPage = new TaxBillComponentsPage();
        expect(taxBillComponentsPage.getTitle())
            .toMatch(/ichooseApp.taxBill.home.title/);

    });

    it('should load create TaxBill dialog', () => {
        taxBillComponentsPage.clickOnCreateButton();
        taxBillDialogPage = new TaxBillDialogPage();
        expect(taxBillDialogPage.getModalTitle())
            .toMatch(/ichooseApp.taxBill.home.createOrEditLabel/);
        taxBillDialogPage.close();
    });

    it('should create and save TaxBills', () => {
        taxBillComponentsPage.clickOnCreateButton();
        taxBillDialogPage.setTaxbillnumberInput('taxbillnumber');
        expect(taxBillDialogPage.getTaxbillnumberInput()).toMatch('taxbillnumber');
        taxBillDialogPage.setDomicilioclienteInput('domiciliocliente');
        expect(taxBillDialogPage.getDomicilioclienteInput()).toMatch('domiciliocliente');
        taxBillDialogPage.setTipodocInput('tipodoc');
        expect(taxBillDialogPage.getTipodocInput()).toMatch('tipodoc');
        taxBillDialogPage.setNombreclienteInput('nombrecliente');
        expect(taxBillDialogPage.getNombreclienteInput()).toMatch('nombrecliente');
        taxBillDialogPage.setTipocbteInput('tipocbte');
        expect(taxBillDialogPage.getTipocbteInput()).toMatch('tipocbte');
        taxBillDialogPage.setTiporesponsableInput('tiporesponsable');
        expect(taxBillDialogPage.getTiporesponsableInput()).toMatch('tiporesponsable');
        taxBillDialogPage.save();
        expect(taxBillDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class TaxBillComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-tax-bill div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class TaxBillDialogPage {
    modalTitle = element(by.css('h4#myTaxBillLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    taxbillnumberInput = element(by.css('input#field_taxbillnumber'));
    domicilioclienteInput = element(by.css('input#field_domiciliocliente'));
    tipodocInput = element(by.css('input#field_tipodoc'));
    nombreclienteInput = element(by.css('input#field_nombrecliente'));
    tipocbteInput = element(by.css('input#field_tipocbte'));
    tiporesponsableInput = element(by.css('input#field_tiporesponsable'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setTaxbillnumberInput = function(taxbillnumber) {
        this.taxbillnumberInput.sendKeys(taxbillnumber);
    };

    getTaxbillnumberInput = function() {
        return this.taxbillnumberInput.getAttribute('value');
    };

    setDomicilioclienteInput = function(domiciliocliente) {
        this.domicilioclienteInput.sendKeys(domiciliocliente);
    };

    getDomicilioclienteInput = function() {
        return this.domicilioclienteInput.getAttribute('value');
    };

    setTipodocInput = function(tipodoc) {
        this.tipodocInput.sendKeys(tipodoc);
    };

    getTipodocInput = function() {
        return this.tipodocInput.getAttribute('value');
    };

    setNombreclienteInput = function(nombrecliente) {
        this.nombreclienteInput.sendKeys(nombrecliente);
    };

    getNombreclienteInput = function() {
        return this.nombreclienteInput.getAttribute('value');
    };

    setTipocbteInput = function(tipocbte) {
        this.tipocbteInput.sendKeys(tipocbte);
    };

    getTipocbteInput = function() {
        return this.tipocbteInput.getAttribute('value');
    };

    setTiporesponsableInput = function(tiporesponsable) {
        this.tiporesponsableInput.sendKeys(tiporesponsable);
    };

    getTiporesponsableInput = function() {
        return this.tiporesponsableInput.getAttribute('value');
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
