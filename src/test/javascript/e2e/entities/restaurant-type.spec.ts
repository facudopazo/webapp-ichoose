import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('RestaurantType e2e test', () => {

    let navBarPage: NavBarPage;
    let restaurantTypeDialogPage: RestaurantTypeDialogPage;
    let restaurantTypeComponentsPage: RestaurantTypeComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load RestaurantTypes', () => {
        navBarPage.goToEntity('restaurant-type');
        restaurantTypeComponentsPage = new RestaurantTypeComponentsPage();
        expect(restaurantTypeComponentsPage.getTitle())
            .toMatch(/ichooseApp.restaurantType.home.title/);

    });

    it('should load create RestaurantType dialog', () => {
        restaurantTypeComponentsPage.clickOnCreateButton();
        restaurantTypeDialogPage = new RestaurantTypeDialogPage();
        expect(restaurantTypeDialogPage.getModalTitle())
            .toMatch(/ichooseApp.restaurantType.home.createOrEditLabel/);
        restaurantTypeDialogPage.close();
    });

    it('should create and save RestaurantTypes', () => {
        restaurantTypeComponentsPage.clickOnCreateButton();
        restaurantTypeDialogPage.setNameInput('name');
        expect(restaurantTypeDialogPage.getNameInput()).toMatch('name');
        restaurantTypeDialogPage.save();
        expect(restaurantTypeDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class RestaurantTypeComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-restaurant-type div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class RestaurantTypeDialogPage {
    modalTitle = element(by.css('h4#myRestaurantTypeLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    };

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
