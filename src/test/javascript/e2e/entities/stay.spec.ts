import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Stay e2e test', () => {

    let navBarPage: NavBarPage;
    let stayDialogPage: StayDialogPage;
    let stayComponentsPage: StayComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Stays', () => {
        navBarPage.goToEntity('stay');
        stayComponentsPage = new StayComponentsPage();
        expect(stayComponentsPage.getTitle())
            .toMatch(/ichooseApp.stay.home.title/);

    });

    it('should load create Stay dialog', () => {
        stayComponentsPage.clickOnCreateButton();
        stayDialogPage = new StayDialogPage();
        expect(stayDialogPage.getModalTitle())
            .toMatch(/ichooseApp.stay.home.createOrEditLabel/);
        stayDialogPage.close();
    });

    it('should create and save Stays', () => {
        stayComponentsPage.clickOnCreateButton();
        stayDialogPage.setAmountInput('5');
        expect(stayDialogPage.getAmountInput()).toMatch('5');
        stayDialogPage.setPeopleInput('5');
        expect(stayDialogPage.getPeopleInput()).toMatch('5');
        stayDialogPage.setInitInput(12310020012301);
        expect(stayDialogPage.getInitInput()).toMatch('2001-12-31T02:30');
        stayDialogPage.setEndInput(12310020012301);
        expect(stayDialogPage.getEndInput()).toMatch('2001-12-31T02:30');
        stayDialogPage.setDiscountInput('5');
        expect(stayDialogPage.getDiscountInput()).toMatch('5');
        stayDialogPage.clientTableSelectLastOption();
        stayDialogPage.save();
        expect(stayDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class StayComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-stay div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class StayDialogPage {
    modalTitle = element(by.css('h4#myStayLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    amountInput = element(by.css('input#field_amount'));
    peopleInput = element(by.css('input#field_people'));
    initInput = element(by.css('input#field_init'));
    endInput = element(by.css('input#field_end'));
    discountInput = element(by.css('input#field_discount'));
    clientTableSelect = element(by.css('select#field_clientTable'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setAmountInput = function(amount) {
        this.amountInput.sendKeys(amount);
    };

    getAmountInput = function() {
        return this.amountInput.getAttribute('value');
    };

    setPeopleInput = function(people) {
        this.peopleInput.sendKeys(people);
    };

    getPeopleInput = function() {
        return this.peopleInput.getAttribute('value');
    };

    setInitInput = function(init) {
        this.initInput.sendKeys(init);
    };

    getInitInput = function() {
        return this.initInput.getAttribute('value');
    };

    setEndInput = function(end) {
        this.endInput.sendKeys(end);
    };

    getEndInput = function() {
        return this.endInput.getAttribute('value');
    };

    setDiscountInput = function(discount) {
        this.discountInput.sendKeys(discount);
    };

    getDiscountInput = function() {
        return this.discountInput.getAttribute('value');
    };

    clientTableSelectLastOption = function() {
        this.clientTableSelect.all(by.tagName('option')).last().click();
    };

    clientTableSelectOption = function(option) {
        this.clientTableSelect.sendKeys(option);
    };

    getClientTableSelect = function() {
        return this.clientTableSelect;
    };

    getClientTableSelectedOption = function() {
        return this.clientTableSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
