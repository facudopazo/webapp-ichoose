import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
import * as path from 'path';
describe('Picture e2e test', () => {

    let navBarPage: NavBarPage;
    let pictureDialogPage: PictureDialogPage;
    let pictureComponentsPage: PictureComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Pictures', () => {
        navBarPage.goToEntity('picture');
        pictureComponentsPage = new PictureComponentsPage();
        expect(pictureComponentsPage.getTitle())
            .toMatch(/ichooseApp.picture.home.title/);

    });

    it('should load create Picture dialog', () => {
        pictureComponentsPage.clickOnCreateButton();
        pictureDialogPage = new PictureDialogPage();
        expect(pictureDialogPage.getModalTitle())
            .toMatch(/ichooseApp.picture.home.createOrEditLabel/);
        pictureDialogPage.close();
    });

    it('should create and save Pictures', () => {
        pictureComponentsPage.clickOnCreateButton();
        pictureDialogPage.setContentInput(absolutePath);
        pictureDialogPage.setContentTypeInput('contentType');
        expect(pictureDialogPage.getContentTypeInput()).toMatch('contentType');
        pictureDialogPage.restaurantSelectLastOption();
        pictureDialogPage.save();
        expect(pictureDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class PictureComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-picture div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class PictureDialogPage {
    modalTitle = element(by.css('h4#myPictureLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    contentInput = element(by.css('input#file_content'));
    contentTypeInput = element(by.css('input#field_contentType'));
    restaurantSelect = element(by.css('select#field_restaurant'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setContentInput = function(content) {
        this.contentInput.sendKeys(content);
    };

    getContentInput = function() {
        return this.contentInput.getAttribute('value');
    };

    setContentTypeInput = function(contentType) {
        this.contentTypeInput.sendKeys(contentType);
    };

    getContentTypeInput = function() {
        return this.contentTypeInput.getAttribute('value');
    };

    restaurantSelectLastOption = function() {
        this.restaurantSelect.all(by.tagName('option')).last().click();
    };

    restaurantSelectOption = function(option) {
        this.restaurantSelect.sendKeys(option);
    };

    getRestaurantSelect = function() {
        return this.restaurantSelect;
    };

    getRestaurantSelectedOption = function() {
        return this.restaurantSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
