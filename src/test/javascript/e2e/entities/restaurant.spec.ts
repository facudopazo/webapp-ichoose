import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Restaurant e2e test', () => {

    let navBarPage: NavBarPage;
    let restaurantDialogPage: RestaurantDialogPage;
    let restaurantComponentsPage: RestaurantComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Restaurants', () => {
        navBarPage.goToEntity('restaurant');
        restaurantComponentsPage = new RestaurantComponentsPage();
        expect(restaurantComponentsPage.getTitle())
            .toMatch(/ichooseApp.restaurant.home.title/);

    });

    it('should load create Restaurant dialog', () => {
        restaurantComponentsPage.clickOnCreateButton();
        restaurantDialogPage = new RestaurantDialogPage();
        expect(restaurantDialogPage.getModalTitle())
            .toMatch(/ichooseApp.restaurant.home.createOrEditLabel/);
        restaurantDialogPage.close();
    });

    it('should create and save Restaurants', () => {
        restaurantComponentsPage.clickOnCreateButton();
        restaurantDialogPage.setNameInput('name');
        expect(restaurantDialogPage.getNameInput()).toMatch('name');
        restaurantDialogPage.setDescriptionInput('description');
        expect(restaurantDialogPage.getDescriptionInput()).toMatch('description');
        restaurantDialogPage.setCuitInput('cuit');
        expect(restaurantDialogPage.getCuitInput()).toMatch('cuit');
        restaurantDialogPage.setCbuInput('cbu');
        expect(restaurantDialogPage.getCbuInput()).toMatch('cbu');
        restaurantDialogPage.locationSelectLastOption();
        restaurantDialogPage.restaurantTypeSelectLastOption();
        // restaurantDialogPage.mealClassificationSelectLastOption();
        restaurantDialogPage.save();
        expect(restaurantDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class RestaurantComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-restaurant div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class RestaurantDialogPage {
    modalTitle = element(by.css('h4#myRestaurantLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));
    descriptionInput = element(by.css('input#field_description'));
    cuitInput = element(by.css('input#field_cuit'));
    cbuInput = element(by.css('input#field_cbu'));
    locationSelect = element(by.css('select#field_location'));
    restaurantTypeSelect = element(by.css('select#field_restaurantType'));
    mealClassificationSelect = element(by.css('select#field_mealClassification'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    };

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    };

    setDescriptionInput = function(description) {
        this.descriptionInput.sendKeys(description);
    };

    getDescriptionInput = function() {
        return this.descriptionInput.getAttribute('value');
    };

    setCuitInput = function(cuit) {
        this.cuitInput.sendKeys(cuit);
    };

    getCuitInput = function() {
        return this.cuitInput.getAttribute('value');
    };

    setCbuInput = function(cbu) {
        this.cbuInput.sendKeys(cbu);
    };

    getCbuInput = function() {
        return this.cbuInput.getAttribute('value');
    };

    locationSelectLastOption = function() {
        this.locationSelect.all(by.tagName('option')).last().click();
    };

    locationSelectOption = function(option) {
        this.locationSelect.sendKeys(option);
    };

    getLocationSelect = function() {
        return this.locationSelect;
    };

    getLocationSelectedOption = function() {
        return this.locationSelect.element(by.css('option:checked')).getText();
    };

    restaurantTypeSelectLastOption = function() {
        this.restaurantTypeSelect.all(by.tagName('option')).last().click();
    };

    restaurantTypeSelectOption = function(option) {
        this.restaurantTypeSelect.sendKeys(option);
    };

    getRestaurantTypeSelect = function() {
        return this.restaurantTypeSelect;
    };

    getRestaurantTypeSelectedOption = function() {
        return this.restaurantTypeSelect.element(by.css('option:checked')).getText();
    };

    mealClassificationSelectLastOption = function() {
        this.mealClassificationSelect.all(by.tagName('option')).last().click();
    };

    mealClassificationSelectOption = function(option) {
        this.mealClassificationSelect.sendKeys(option);
    };

    getMealClassificationSelect = function() {
        return this.mealClassificationSelect;
    };

    getMealClassificationSelectedOption = function() {
        return this.mealClassificationSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
