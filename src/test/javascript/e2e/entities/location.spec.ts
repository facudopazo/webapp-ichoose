import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Location e2e test', () => {

    let navBarPage: NavBarPage;
    let locationDialogPage: LocationDialogPage;
    let locationComponentsPage: LocationComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Locations', () => {
        navBarPage.goToEntity('location');
        locationComponentsPage = new LocationComponentsPage();
        expect(locationComponentsPage.getTitle())
            .toMatch(/ichooseApp.location.home.title/);

    });

    it('should load create Location dialog', () => {
        locationComponentsPage.clickOnCreateButton();
        locationDialogPage = new LocationDialogPage();
        expect(locationDialogPage.getModalTitle())
            .toMatch(/ichooseApp.location.home.createOrEditLabel/);
        locationDialogPage.close();
    });

    it('should create and save Locations', () => {
        locationComponentsPage.clickOnCreateButton();
        locationDialogPage.setLatitudeInput('5');
        expect(locationDialogPage.getLatitudeInput()).toMatch('5');
        locationDialogPage.setLogitudeInput('5');
        expect(locationDialogPage.getLogitudeInput()).toMatch('5');
        locationDialogPage.setStreetNumberInput('streetNumber');
        expect(locationDialogPage.getStreetNumberInput()).toMatch('streetNumber');
        locationDialogPage.setRouteInput('route');
        expect(locationDialogPage.getRouteInput()).toMatch('route');
        locationDialogPage.setPostalCodeInput('postalCode');
        expect(locationDialogPage.getPostalCodeInput()).toMatch('postalCode');
        locationDialogPage.setCountryInput('country');
        expect(locationDialogPage.getCountryInput()).toMatch('country');
        locationDialogPage.setFormattedAddressInput('formattedAddress');
        expect(locationDialogPage.getFormattedAddressInput()).toMatch('formattedAddress');
        locationDialogPage.save();
        expect(locationDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class LocationComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-location div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class LocationDialogPage {
    modalTitle = element(by.css('h4#myLocationLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    latitudeInput = element(by.css('input#field_latitude'));
    logitudeInput = element(by.css('input#field_logitude'));
    streetNumberInput = element(by.css('input#field_streetNumber'));
    routeInput = element(by.css('input#field_route'));
    postalCodeInput = element(by.css('input#field_postalCode'));
    countryInput = element(by.css('input#field_country'));
    formattedAddressInput = element(by.css('input#field_formattedAddress'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setLatitudeInput = function(latitude) {
        this.latitudeInput.sendKeys(latitude);
    };

    getLatitudeInput = function() {
        return this.latitudeInput.getAttribute('value');
    };

    setLogitudeInput = function(logitude) {
        this.logitudeInput.sendKeys(logitude);
    };

    getLogitudeInput = function() {
        return this.logitudeInput.getAttribute('value');
    };

    setStreetNumberInput = function(streetNumber) {
        this.streetNumberInput.sendKeys(streetNumber);
    };

    getStreetNumberInput = function() {
        return this.streetNumberInput.getAttribute('value');
    };

    setRouteInput = function(route) {
        this.routeInput.sendKeys(route);
    };

    getRouteInput = function() {
        return this.routeInput.getAttribute('value');
    };

    setPostalCodeInput = function(postalCode) {
        this.postalCodeInput.sendKeys(postalCode);
    };

    getPostalCodeInput = function() {
        return this.postalCodeInput.getAttribute('value');
    };

    setCountryInput = function(country) {
        this.countryInput.sendKeys(country);
    };

    getCountryInput = function() {
        return this.countryInput.getAttribute('value');
    };

    setFormattedAddressInput = function(formattedAddress) {
        this.formattedAddressInput.sendKeys(formattedAddress);
    };

    getFormattedAddressInput = function() {
        return this.formattedAddressInput.getAttribute('value');
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
