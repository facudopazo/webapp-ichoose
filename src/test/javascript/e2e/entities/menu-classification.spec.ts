import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('MenuClassification e2e test', () => {

    let navBarPage: NavBarPage;
    let menuClassificationDialogPage: MenuClassificationDialogPage;
    let menuClassificationComponentsPage: MenuClassificationComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load MenuClassifications', () => {
        navBarPage.goToEntity('menu-classification');
        menuClassificationComponentsPage = new MenuClassificationComponentsPage();
        expect(menuClassificationComponentsPage.getTitle())
            .toMatch(/ichooseApp.menuClassification.home.title/);

    });

    it('should load create MenuClassification dialog', () => {
        menuClassificationComponentsPage.clickOnCreateButton();
        menuClassificationDialogPage = new MenuClassificationDialogPage();
        expect(menuClassificationDialogPage.getModalTitle())
            .toMatch(/ichooseApp.menuClassification.home.createOrEditLabel/);
        menuClassificationDialogPage.close();
    });

    it('should create and save MenuClassifications', () => {
        menuClassificationComponentsPage.clickOnCreateButton();
        menuClassificationDialogPage.setNameInput('name');
        expect(menuClassificationDialogPage.getNameInput()).toMatch('name');
        // menuClassificationDialogPage.subclassificationsSelectLastOption();
        menuClassificationDialogPage.save();
        expect(menuClassificationDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class MenuClassificationComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-menu-classification div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class MenuClassificationDialogPage {
    modalTitle = element(by.css('h4#myMenuClassificationLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));
    subclassificationsSelect = element(by.css('select#field_subclassifications'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    };

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    };

    subclassificationsSelectLastOption = function() {
        this.subclassificationsSelect.all(by.tagName('option')).last().click();
    };

    subclassificationsSelectOption = function(option) {
        this.subclassificationsSelect.sendKeys(option);
    };

    getSubclassificationsSelect = function() {
        return this.subclassificationsSelect;
    };

    getSubclassificationsSelectedOption = function() {
        return this.subclassificationsSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
