import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('PromotionShipping e2e test', () => {

    let navBarPage: NavBarPage;
    let promotionShippingDialogPage: PromotionShippingDialogPage;
    let promotionShippingComponentsPage: PromotionShippingComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load PromotionShippings', () => {
        navBarPage.goToEntity('promotion-shipping');
        promotionShippingComponentsPage = new PromotionShippingComponentsPage();
        expect(promotionShippingComponentsPage.getTitle())
            .toMatch(/ichooseApp.promotionShipping.home.title/);

    });

    it('should load create PromotionShipping dialog', () => {
        promotionShippingComponentsPage.clickOnCreateButton();
        promotionShippingDialogPage = new PromotionShippingDialogPage();
        expect(promotionShippingDialogPage.getModalTitle())
            .toMatch(/ichooseApp.promotionShipping.home.createOrEditLabel/);
        promotionShippingDialogPage.close();
    });

    it('should create and save PromotionShippings', () => {
        promotionShippingComponentsPage.clickOnCreateButton();
        promotionShippingDialogPage.setDateTimeShippingInput(12310020012301);
        expect(promotionShippingDialogPage.getDateTimeShippingInput()).toMatch('2001-12-31T02:30');
        promotionShippingDialogPage.promotionSelectLastOption();
        promotionShippingDialogPage.groupSelectLastOption();
        promotionShippingDialogPage.save();
        expect(promotionShippingDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class PromotionShippingComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-promotion-shipping div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class PromotionShippingDialogPage {
    modalTitle = element(by.css('h4#myPromotionShippingLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    dateTimeShippingInput = element(by.css('input#field_dateTimeShipping'));
    promotionSelect = element(by.css('select#field_promotion'));
    groupSelect = element(by.css('select#field_group'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setDateTimeShippingInput = function(dateTimeShipping) {
        this.dateTimeShippingInput.sendKeys(dateTimeShipping);
    };

    getDateTimeShippingInput = function() {
        return this.dateTimeShippingInput.getAttribute('value');
    };

    promotionSelectLastOption = function() {
        this.promotionSelect.all(by.tagName('option')).last().click();
    };

    promotionSelectOption = function(option) {
        this.promotionSelect.sendKeys(option);
    };

    getPromotionSelect = function() {
        return this.promotionSelect;
    };

    getPromotionSelectedOption = function() {
        return this.promotionSelect.element(by.css('option:checked')).getText();
    };

    groupSelectLastOption = function() {
        this.groupSelect.all(by.tagName('option')).last().click();
    };

    groupSelectOption = function(option) {
        this.groupSelect.sendKeys(option);
    };

    getGroupSelect = function() {
        return this.groupSelect;
    };

    getGroupSelectedOption = function() {
        return this.groupSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
