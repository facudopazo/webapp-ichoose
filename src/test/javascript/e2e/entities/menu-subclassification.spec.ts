import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('MenuSubclassification e2e test', () => {

    let navBarPage: NavBarPage;
    let menuSubclassificationDialogPage: MenuSubclassificationDialogPage;
    let menuSubclassificationComponentsPage: MenuSubclassificationComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load MenuSubclassifications', () => {
        navBarPage.goToEntity('menu-subclassification');
        menuSubclassificationComponentsPage = new MenuSubclassificationComponentsPage();
        expect(menuSubclassificationComponentsPage.getTitle())
            .toMatch(/ichooseApp.menuSubclassification.home.title/);

    });

    it('should load create MenuSubclassification dialog', () => {
        menuSubclassificationComponentsPage.clickOnCreateButton();
        menuSubclassificationDialogPage = new MenuSubclassificationDialogPage();
        expect(menuSubclassificationDialogPage.getModalTitle())
            .toMatch(/ichooseApp.menuSubclassification.home.createOrEditLabel/);
        menuSubclassificationDialogPage.close();
    });

    it('should create and save MenuSubclassifications', () => {
        menuSubclassificationComponentsPage.clickOnCreateButton();
        menuSubclassificationDialogPage.setNameInput('name');
        expect(menuSubclassificationDialogPage.getNameInput()).toMatch('name');
        menuSubclassificationDialogPage.save();
        expect(menuSubclassificationDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class MenuSubclassificationComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-menu-subclassification div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class MenuSubclassificationDialogPage {
    modalTitle = element(by.css('h4#myMenuSubclassificationLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    };

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
