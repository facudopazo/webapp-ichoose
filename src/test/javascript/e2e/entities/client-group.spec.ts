import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('ClientGroup e2e test', () => {

    let navBarPage: NavBarPage;
    let clientGroupDialogPage: ClientGroupDialogPage;
    let clientGroupComponentsPage: ClientGroupComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load ClientGroups', () => {
        navBarPage.goToEntity('client-group');
        clientGroupComponentsPage = new ClientGroupComponentsPage();
        expect(clientGroupComponentsPage.getTitle())
            .toMatch(/ichooseApp.clientGroup.home.title/);

    });

    it('should load create ClientGroup dialog', () => {
        clientGroupComponentsPage.clickOnCreateButton();
        clientGroupDialogPage = new ClientGroupDialogPage();
        expect(clientGroupDialogPage.getModalTitle())
            .toMatch(/ichooseApp.clientGroup.home.createOrEditLabel/);
        clientGroupDialogPage.close();
    });

    it('should create and save ClientGroups', () => {
        clientGroupComponentsPage.clickOnCreateButton();
        clientGroupDialogPage.setNameInput('name');
        expect(clientGroupDialogPage.getNameInput()).toMatch('name');
        clientGroupDialogPage.save();
        expect(clientGroupDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ClientGroupComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-client-group div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ClientGroupDialogPage {
    modalTitle = element(by.css('h4#myClientGroupLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    };

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
