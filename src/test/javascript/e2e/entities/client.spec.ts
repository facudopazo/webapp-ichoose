import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Client e2e test', () => {

    let navBarPage: NavBarPage;
    let clientDialogPage: ClientDialogPage;
    let clientComponentsPage: ClientComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Clients', () => {
        navBarPage.goToEntity('client');
        clientComponentsPage = new ClientComponentsPage();
        expect(clientComponentsPage.getTitle())
            .toMatch(/ichooseApp.client.home.title/);

    });

    it('should load create Client dialog', () => {
        clientComponentsPage.clickOnCreateButton();
        clientDialogPage = new ClientDialogPage();
        expect(clientDialogPage.getModalTitle())
            .toMatch(/ichooseApp.client.home.createOrEditLabel/);
        clientDialogPage.close();
    });

    it('should create and save Clients', () => {
        clientComponentsPage.clickOnCreateButton();
        clientDialogPage.setTelephoneNumberInput('telephoneNumber');
        expect(clientDialogPage.getTelephoneNumberInput()).toMatch('telephoneNumber');
        clientDialogPage.setBirthdateInput(12310020012301);
        expect(clientDialogPage.getBirthdateInput()).toMatch('2001-12-31T02:30');
        clientDialogPage.setFirstNameInput('firstName');
        expect(clientDialogPage.getFirstNameInput()).toMatch('firstName');
        clientDialogPage.setLastNameInput('lastName');
        expect(clientDialogPage.getLastNameInput()).toMatch('lastName');
        clientDialogPage.setEmailInput('email');
        expect(clientDialogPage.getEmailInput()).toMatch('email');
        clientDialogPage.setUserIdInput('5');
        expect(clientDialogPage.getUserIdInput()).toMatch('5');
        clientDialogPage.locationSelectLastOption();
        clientDialogPage.pictureSelectLastOption();
        clientDialogPage.nationalitySelectLastOption();
        clientDialogPage.save();
        expect(clientDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ClientComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-client div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ClientDialogPage {
    modalTitle = element(by.css('h4#myClientLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    telephoneNumberInput = element(by.css('input#field_telephoneNumber'));
    birthdateInput = element(by.css('input#field_birthdate'));
    firstNameInput = element(by.css('input#field_firstName'));
    lastNameInput = element(by.css('input#field_lastName'));
    emailInput = element(by.css('input#field_email'));
    userIdInput = element(by.css('input#field_userId'));
    locationSelect = element(by.css('select#field_location'));
    pictureSelect = element(by.css('select#field_picture'));
    nationalitySelect = element(by.css('select#field_nationality'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setTelephoneNumberInput = function(telephoneNumber) {
        this.telephoneNumberInput.sendKeys(telephoneNumber);
    };

    getTelephoneNumberInput = function() {
        return this.telephoneNumberInput.getAttribute('value');
    };

    setBirthdateInput = function(birthdate) {
        this.birthdateInput.sendKeys(birthdate);
    };

    getBirthdateInput = function() {
        return this.birthdateInput.getAttribute('value');
    };

    setFirstNameInput = function(firstName) {
        this.firstNameInput.sendKeys(firstName);
    };

    getFirstNameInput = function() {
        return this.firstNameInput.getAttribute('value');
    };

    setLastNameInput = function(lastName) {
        this.lastNameInput.sendKeys(lastName);
    };

    getLastNameInput = function() {
        return this.lastNameInput.getAttribute('value');
    };

    setEmailInput = function(email) {
        this.emailInput.sendKeys(email);
    };

    getEmailInput = function() {
        return this.emailInput.getAttribute('value');
    };

    setUserIdInput = function(userId) {
        this.userIdInput.sendKeys(userId);
    };

    getUserIdInput = function() {
        return this.userIdInput.getAttribute('value');
    };

    locationSelectLastOption = function() {
        this.locationSelect.all(by.tagName('option')).last().click();
    };

    locationSelectOption = function(option) {
        this.locationSelect.sendKeys(option);
    };

    getLocationSelect = function() {
        return this.locationSelect;
    };

    getLocationSelectedOption = function() {
        return this.locationSelect.element(by.css('option:checked')).getText();
    };

    pictureSelectLastOption = function() {
        this.pictureSelect.all(by.tagName('option')).last().click();
    };

    pictureSelectOption = function(option) {
        this.pictureSelect.sendKeys(option);
    };

    getPictureSelect = function() {
        return this.pictureSelect;
    };

    getPictureSelectedOption = function() {
        return this.pictureSelect.element(by.css('option:checked')).getText();
    };

    nationalitySelectLastOption = function() {
        this.nationalitySelect.all(by.tagName('option')).last().click();
    };

    nationalitySelectOption = function(option) {
        this.nationalitySelect.sendKeys(option);
    };

    getNationalitySelect = function() {
        return this.nationalitySelect;
    };

    getNationalitySelectedOption = function() {
        return this.nationalitySelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
