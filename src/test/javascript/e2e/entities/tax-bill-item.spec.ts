import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('TaxBillItem e2e test', () => {

    let navBarPage: NavBarPage;
    let taxBillItemDialogPage: TaxBillItemDialogPage;
    let taxBillItemComponentsPage: TaxBillItemComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load TaxBillItems', () => {
        navBarPage.goToEntity('tax-bill-item');
        taxBillItemComponentsPage = new TaxBillItemComponentsPage();
        expect(taxBillItemComponentsPage.getTitle())
            .toMatch(/ichooseApp.taxBillItem.home.title/);

    });

    it('should load create TaxBillItem dialog', () => {
        taxBillItemComponentsPage.clickOnCreateButton();
        taxBillItemDialogPage = new TaxBillItemDialogPage();
        expect(taxBillItemDialogPage.getModalTitle())
            .toMatch(/ichooseApp.taxBillItem.home.createOrEditLabel/);
        taxBillItemDialogPage.close();
    });

    it('should create and save TaxBillItems', () => {
        taxBillItemComponentsPage.clickOnCreateButton();
        taxBillItemDialogPage.setAlicivaInput('5');
        expect(taxBillItemDialogPage.getAlicivaInput()).toMatch('5');
        taxBillItemDialogPage.setImporteInput('5');
        expect(taxBillItemDialogPage.getImporteInput()).toMatch('5');
        taxBillItemDialogPage.setDsInput('ds');
        expect(taxBillItemDialogPage.getDsInput()).toMatch('ds');
        taxBillItemDialogPage.setQtyInput('5');
        expect(taxBillItemDialogPage.getQtyInput()).toMatch('5');
        taxBillItemDialogPage.staySelectLastOption();
        taxBillItemDialogPage.taxBillSelectLastOption();
        taxBillItemDialogPage.save();
        expect(taxBillItemDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class TaxBillItemComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-tax-bill-item div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class TaxBillItemDialogPage {
    modalTitle = element(by.css('h4#myTaxBillItemLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    alicivaInput = element(by.css('input#field_aliciva'));
    importeInput = element(by.css('input#field_importe'));
    dsInput = element(by.css('input#field_ds'));
    qtyInput = element(by.css('input#field_qty'));
    staySelect = element(by.css('select#field_stay'));
    taxBillSelect = element(by.css('select#field_taxBill'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setAlicivaInput = function(aliciva) {
        this.alicivaInput.sendKeys(aliciva);
    };

    getAlicivaInput = function() {
        return this.alicivaInput.getAttribute('value');
    };

    setImporteInput = function(importe) {
        this.importeInput.sendKeys(importe);
    };

    getImporteInput = function() {
        return this.importeInput.getAttribute('value');
    };

    setDsInput = function(ds) {
        this.dsInput.sendKeys(ds);
    };

    getDsInput = function() {
        return this.dsInput.getAttribute('value');
    };

    setQtyInput = function(qty) {
        this.qtyInput.sendKeys(qty);
    };

    getQtyInput = function() {
        return this.qtyInput.getAttribute('value');
    };

    staySelectLastOption = function() {
        this.staySelect.all(by.tagName('option')).last().click();
    };

    staySelectOption = function(option) {
        this.staySelect.sendKeys(option);
    };

    getStaySelect = function() {
        return this.staySelect;
    };

    getStaySelectedOption = function() {
        return this.staySelect.element(by.css('option:checked')).getText();
    };

    taxBillSelectLastOption = function() {
        this.taxBillSelect.all(by.tagName('option')).last().click();
    };

    taxBillSelectOption = function(option) {
        this.taxBillSelect.sendKeys(option);
    };

    getTaxBillSelect = function() {
        return this.taxBillSelect;
    };

    getTaxBillSelectedOption = function() {
        return this.taxBillSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
