import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Notification e2e test', () => {

    let navBarPage: NavBarPage;
    let notificationDialogPage: NotificationDialogPage;
    let notificationComponentsPage: NotificationComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Notifications', () => {
        navBarPage.goToEntity('notification');
        notificationComponentsPage = new NotificationComponentsPage();
        expect(notificationComponentsPage.getTitle())
            .toMatch(/ichooseApp.notification.home.title/);

    });

    it('should load create Notification dialog', () => {
        notificationComponentsPage.clickOnCreateButton();
        notificationDialogPage = new NotificationDialogPage();
        expect(notificationDialogPage.getModalTitle())
            .toMatch(/ichooseApp.notification.home.createOrEditLabel/);
        notificationDialogPage.close();
    });

    it('should create and save Notifications', () => {
        notificationComponentsPage.clickOnCreateButton();
        notificationDialogPage.setParametersInput('parameters');
        expect(notificationDialogPage.getParametersInput()).toMatch('parameters');
        notificationDialogPage.setCreationDateInput(12310020012301);
        expect(notificationDialogPage.getCreationDateInput()).toMatch('2001-12-31T02:30');
        notificationDialogPage.setReadDateInput(12310020012301);
        expect(notificationDialogPage.getReadDateInput()).toMatch('2001-12-31T02:30');
        notificationDialogPage.setTypeInput('type');
        expect(notificationDialogPage.getTypeInput()).toMatch('type');
        notificationDialogPage.restaurantSelectLastOption();
        notificationDialogPage.clientSelectLastOption();
        notificationDialogPage.save();
        expect(notificationDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class NotificationComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-notification div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class NotificationDialogPage {
    modalTitle = element(by.css('h4#myNotificationLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    parametersInput = element(by.css('input#field_parameters'));
    creationDateInput = element(by.css('input#field_creationDate'));
    readDateInput = element(by.css('input#field_readDate'));
    typeInput = element(by.css('input#field_type'));
    restaurantSelect = element(by.css('select#field_restaurant'));
    clientSelect = element(by.css('select#field_client'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setParametersInput = function(parameters) {
        this.parametersInput.sendKeys(parameters);
    };

    getParametersInput = function() {
        return this.parametersInput.getAttribute('value');
    };

    setCreationDateInput = function(creationDate) {
        this.creationDateInput.sendKeys(creationDate);
    };

    getCreationDateInput = function() {
        return this.creationDateInput.getAttribute('value');
    };

    setReadDateInput = function(readDate) {
        this.readDateInput.sendKeys(readDate);
    };

    getReadDateInput = function() {
        return this.readDateInput.getAttribute('value');
    };

    setTypeInput = function(type) {
        this.typeInput.sendKeys(type);
    };

    getTypeInput = function() {
        return this.typeInput.getAttribute('value');
    };

    restaurantSelectLastOption = function() {
        this.restaurantSelect.all(by.tagName('option')).last().click();
    };

    restaurantSelectOption = function(option) {
        this.restaurantSelect.sendKeys(option);
    };

    getRestaurantSelect = function() {
        return this.restaurantSelect;
    };

    getRestaurantSelectedOption = function() {
        return this.restaurantSelect.element(by.css('option:checked')).getText();
    };

    clientSelectLastOption = function() {
        this.clientSelect.all(by.tagName('option')).last().click();
    };

    clientSelectOption = function(option) {
        this.clientSelect.sendKeys(option);
    };

    getClientSelect = function() {
        return this.clientSelect;
    };

    getClientSelectedOption = function() {
        return this.clientSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
