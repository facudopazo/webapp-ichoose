import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Promotion e2e test', () => {

    let navBarPage: NavBarPage;
    let promotionDialogPage: PromotionDialogPage;
    let promotionComponentsPage: PromotionComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Promotions', () => {
        navBarPage.goToEntity('promotion');
        promotionComponentsPage = new PromotionComponentsPage();
        expect(promotionComponentsPage.getTitle())
            .toMatch(/ichooseApp.promotion.home.title/);

    });

    it('should load create Promotion dialog', () => {
        promotionComponentsPage.clickOnCreateButton();
        promotionDialogPage = new PromotionDialogPage();
        expect(promotionDialogPage.getModalTitle())
            .toMatch(/ichooseApp.promotion.home.createOrEditLabel/);
        promotionDialogPage.close();
    });

    it('should create and save Promotions', () => {
        promotionComponentsPage.clickOnCreateButton();
        promotionDialogPage.setNameInput('name');
        expect(promotionDialogPage.getNameInput()).toMatch('name');
        promotionDialogPage.setInitInput(12310020012301);
        expect(promotionDialogPage.getInitInput()).toMatch('2001-12-31T02:30');
        promotionDialogPage.setEndInput(12310020012301);
        expect(promotionDialogPage.getEndInput()).toMatch('2001-12-31T02:30');
        promotionDialogPage.setFinalPriceInput('5');
        expect(promotionDialogPage.getFinalPriceInput()).toMatch('5');
        promotionDialogPage.getOpenInput().isSelected().then((selected) => {
            if (selected) {
                promotionDialogPage.getOpenInput().click();
                expect(promotionDialogPage.getOpenInput().isSelected()).toBeFalsy();
            } else {
                promotionDialogPage.getOpenInput().click();
                expect(promotionDialogPage.getOpenInput().isSelected()).toBeTruthy();
            }
        });
        promotionDialogPage.restaurantSelectLastOption();
        // promotionDialogPage.groupsSelectLastOption();
        promotionDialogPage.save();
        expect(promotionDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class PromotionComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-promotion div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class PromotionDialogPage {
    modalTitle = element(by.css('h4#myPromotionLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));
    initInput = element(by.css('input#field_init'));
    endInput = element(by.css('input#field_end'));
    finalPriceInput = element(by.css('input#field_finalPrice'));
    openInput = element(by.css('input#field_open'));
    restaurantSelect = element(by.css('select#field_restaurant'));
    groupsSelect = element(by.css('select#field_groups'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    };

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    };

    setInitInput = function(init) {
        this.initInput.sendKeys(init);
    };

    getInitInput = function() {
        return this.initInput.getAttribute('value');
    };

    setEndInput = function(end) {
        this.endInput.sendKeys(end);
    };

    getEndInput = function() {
        return this.endInput.getAttribute('value');
    };

    setFinalPriceInput = function(finalPrice) {
        this.finalPriceInput.sendKeys(finalPrice);
    };

    getFinalPriceInput = function() {
        return this.finalPriceInput.getAttribute('value');
    };

    getOpenInput = function() {
        return this.openInput;
    };
    restaurantSelectLastOption = function() {
        this.restaurantSelect.all(by.tagName('option')).last().click();
    };

    restaurantSelectOption = function(option) {
        this.restaurantSelect.sendKeys(option);
    };

    getRestaurantSelect = function() {
        return this.restaurantSelect;
    };

    getRestaurantSelectedOption = function() {
        return this.restaurantSelect.element(by.css('option:checked')).getText();
    };

    groupsSelectLastOption = function() {
        this.groupsSelect.all(by.tagName('option')).last().click();
    };

    groupsSelectOption = function(option) {
        this.groupsSelect.sendKeys(option);
    };

    getGroupsSelect = function() {
        return this.groupsSelect;
    };

    getGroupsSelectedOption = function() {
        return this.groupsSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
