import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Sector e2e test', () => {

    let navBarPage: NavBarPage;
    let sectorDialogPage: SectorDialogPage;
    let sectorComponentsPage: SectorComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Sectors', () => {
        navBarPage.goToEntity('sector');
        sectorComponentsPage = new SectorComponentsPage();
        expect(sectorComponentsPage.getTitle())
            .toMatch(/ichooseApp.sector.home.title/);

    });

    it('should load create Sector dialog', () => {
        sectorComponentsPage.clickOnCreateButton();
        sectorDialogPage = new SectorDialogPage();
        expect(sectorDialogPage.getModalTitle())
            .toMatch(/ichooseApp.sector.home.createOrEditLabel/);
        sectorDialogPage.close();
    });

    it('should create and save Sectors', () => {
        sectorComponentsPage.clickOnCreateButton();
        sectorDialogPage.setNameInput('name');
        expect(sectorDialogPage.getNameInput()).toMatch('name');
        sectorDialogPage.getAccesibilityInput().isSelected().then((selected) => {
            if (selected) {
                sectorDialogPage.getAccesibilityInput().click();
                expect(sectorDialogPage.getAccesibilityInput().isSelected()).toBeFalsy();
            } else {
                sectorDialogPage.getAccesibilityInput().click();
                expect(sectorDialogPage.getAccesibilityInput().isSelected()).toBeTruthy();
            }
        });
        sectorDialogPage.setDeleteDateInput(12310020012301);
        expect(sectorDialogPage.getDeleteDateInput()).toMatch('2001-12-31T02:30');
        sectorDialogPage.restaurantSelectLastOption();
        sectorDialogPage.locationSelectLastOption();
        sectorDialogPage.save();
        expect(sectorDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class SectorComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-sector div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class SectorDialogPage {
    modalTitle = element(by.css('h4#mySectorLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));
    accesibilityInput = element(by.css('input#field_accesibility'));
    deleteDateInput = element(by.css('input#field_deleteDate'));
    restaurantSelect = element(by.css('select#field_restaurant'));
    locationSelect = element(by.css('select#field_location'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    };

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    };

    getAccesibilityInput = function() {
        return this.accesibilityInput;
    };
    setDeleteDateInput = function(deleteDate) {
        this.deleteDateInput.sendKeys(deleteDate);
    };

    getDeleteDateInput = function() {
        return this.deleteDateInput.getAttribute('value');
    };

    restaurantSelectLastOption = function() {
        this.restaurantSelect.all(by.tagName('option')).last().click();
    };

    restaurantSelectOption = function(option) {
        this.restaurantSelect.sendKeys(option);
    };

    getRestaurantSelect = function() {
        return this.restaurantSelect;
    };

    getRestaurantSelectedOption = function() {
        return this.restaurantSelect.element(by.css('option:checked')).getText();
    };

    locationSelectLastOption = function() {
        this.locationSelect.all(by.tagName('option')).last().click();
    };

    locationSelectOption = function(option) {
        this.locationSelect.sendKeys(option);
    };

    getLocationSelect = function() {
        return this.locationSelect;
    };

    getLocationSelectedOption = function() {
        return this.locationSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
