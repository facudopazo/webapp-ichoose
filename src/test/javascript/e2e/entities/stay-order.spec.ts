import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('StayOrder e2e test', () => {

    let navBarPage: NavBarPage;
    let stayOrderDialogPage: StayOrderDialogPage;
    let stayOrderComponentsPage: StayOrderComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load StayOrders', () => {
        navBarPage.goToEntity('stay-order');
        stayOrderComponentsPage = new StayOrderComponentsPage();
        expect(stayOrderComponentsPage.getTitle())
            .toMatch(/ichooseApp.stayOrder.home.title/);

    });

    it('should load create StayOrder dialog', () => {
        stayOrderComponentsPage.clickOnCreateButton();
        stayOrderDialogPage = new StayOrderDialogPage();
        expect(stayOrderDialogPage.getModalTitle())
            .toMatch(/ichooseApp.stayOrder.home.createOrEditLabel/);
        stayOrderDialogPage.close();
    });

    it('should create and save StayOrders', () => {
        stayOrderComponentsPage.clickOnCreateButton();
        stayOrderDialogPage.setQuantityInput('5');
        expect(stayOrderDialogPage.getQuantityInput()).toMatch('5');
        stayOrderDialogPage.setReleasedInput(12310020012301);
        expect(stayOrderDialogPage.getReleasedInput()).toMatch('2001-12-31T02:30');
        stayOrderDialogPage.setPriceInput('5');
        expect(stayOrderDialogPage.getPriceInput()).toMatch('5');
        stayOrderDialogPage.staySelectLastOption();
        stayOrderDialogPage.menuSelectLastOption();
        stayOrderDialogPage.save();
        expect(stayOrderDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class StayOrderComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-stay-order div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class StayOrderDialogPage {
    modalTitle = element(by.css('h4#myStayOrderLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    quantityInput = element(by.css('input#field_quantity'));
    releasedInput = element(by.css('input#field_released'));
    priceInput = element(by.css('input#field_price'));
    staySelect = element(by.css('select#field_stay'));
    menuSelect = element(by.css('select#field_menu'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setQuantityInput = function(quantity) {
        this.quantityInput.sendKeys(quantity);
    };

    getQuantityInput = function() {
        return this.quantityInput.getAttribute('value');
    };

    setReleasedInput = function(released) {
        this.releasedInput.sendKeys(released);
    };

    getReleasedInput = function() {
        return this.releasedInput.getAttribute('value');
    };

    setPriceInput = function(price) {
        this.priceInput.sendKeys(price);
    };

    getPriceInput = function() {
        return this.priceInput.getAttribute('value');
    };

    staySelectLastOption = function() {
        this.staySelect.all(by.tagName('option')).last().click();
    };

    staySelectOption = function(option) {
        this.staySelect.sendKeys(option);
    };

    getStaySelect = function() {
        return this.staySelect;
    };

    getStaySelectedOption = function() {
        return this.staySelect.element(by.css('option:checked')).getText();
    };

    menuSelectLastOption = function() {
        this.menuSelect.all(by.tagName('option')).last().click();
    };

    menuSelectOption = function(option) {
        this.menuSelect.sendKeys(option);
    };

    getMenuSelect = function() {
        return this.menuSelect;
    };

    getMenuSelectedOption = function() {
        return this.menuSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
