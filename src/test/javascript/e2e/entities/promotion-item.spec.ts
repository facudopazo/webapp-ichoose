import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('PromotionItem e2e test', () => {

    let navBarPage: NavBarPage;
    let promotionItemDialogPage: PromotionItemDialogPage;
    let promotionItemComponentsPage: PromotionItemComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load PromotionItems', () => {
        navBarPage.goToEntity('promotion-item');
        promotionItemComponentsPage = new PromotionItemComponentsPage();
        expect(promotionItemComponentsPage.getTitle())
            .toMatch(/ichooseApp.promotionItem.home.title/);

    });

    it('should load create PromotionItem dialog', () => {
        promotionItemComponentsPage.clickOnCreateButton();
        promotionItemDialogPage = new PromotionItemDialogPage();
        expect(promotionItemDialogPage.getModalTitle())
            .toMatch(/ichooseApp.promotionItem.home.createOrEditLabel/);
        promotionItemDialogPage.close();
    });

    it('should create and save PromotionItems', () => {
        promotionItemComponentsPage.clickOnCreateButton();
        promotionItemDialogPage.setQuantityInput('5');
        expect(promotionItemDialogPage.getQuantityInput()).toMatch('5');
        promotionItemDialogPage.promotionSelectLastOption();
        promotionItemDialogPage.menuSelectLastOption();
        promotionItemDialogPage.save();
        expect(promotionItemDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class PromotionItemComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-promotion-item div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class PromotionItemDialogPage {
    modalTitle = element(by.css('h4#myPromotionItemLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    quantityInput = element(by.css('input#field_quantity'));
    promotionSelect = element(by.css('select#field_promotion'));
    menuSelect = element(by.css('select#field_menu'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setQuantityInput = function(quantity) {
        this.quantityInput.sendKeys(quantity);
    };

    getQuantityInput = function() {
        return this.quantityInput.getAttribute('value');
    };

    promotionSelectLastOption = function() {
        this.promotionSelect.all(by.tagName('option')).last().click();
    };

    promotionSelectOption = function(option) {
        this.promotionSelect.sendKeys(option);
    };

    getPromotionSelect = function() {
        return this.promotionSelect;
    };

    getPromotionSelectedOption = function() {
        return this.promotionSelect.element(by.css('option:checked')).getText();
    };

    menuSelectLastOption = function() {
        this.menuSelect.all(by.tagName('option')).last().click();
    };

    menuSelectOption = function(option) {
        this.menuSelect.sendKeys(option);
    };

    getMenuSelect = function() {
        return this.menuSelect;
    };

    getMenuSelectedOption = function() {
        return this.menuSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
