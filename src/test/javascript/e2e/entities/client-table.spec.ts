import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('ClientTable e2e test', () => {

    let navBarPage: NavBarPage;
    let clientTableDialogPage: ClientTableDialogPage;
    let clientTableComponentsPage: ClientTableComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load ClientTables', () => {
        navBarPage.goToEntity('client-table');
        clientTableComponentsPage = new ClientTableComponentsPage();
        expect(clientTableComponentsPage.getTitle())
            .toMatch(/ichooseApp.clientTable.home.title/);

    });

    it('should load create ClientTable dialog', () => {
        clientTableComponentsPage.clickOnCreateButton();
        clientTableDialogPage = new ClientTableDialogPage();
        expect(clientTableDialogPage.getModalTitle())
            .toMatch(/ichooseApp.clientTable.home.createOrEditLabel/);
        clientTableDialogPage.close();
    });

    it('should create and save ClientTables', () => {
        clientTableComponentsPage.clickOnCreateButton();
        clientTableDialogPage.setNumberInput('number');
        expect(clientTableDialogPage.getNumberInput()).toMatch('number');
        clientTableDialogPage.setChairsInput('5');
        expect(clientTableDialogPage.getChairsInput()).toMatch('5');
        clientTableDialogPage.getModifiableInput().isSelected().then((selected) => {
            if (selected) {
                clientTableDialogPage.getModifiableInput().click();
                expect(clientTableDialogPage.getModifiableInput().isSelected()).toBeFalsy();
            } else {
                clientTableDialogPage.getModifiableInput().click();
                expect(clientTableDialogPage.getModifiableInput().isSelected()).toBeTruthy();
            }
        });
        clientTableDialogPage.setDeleteDateInput(12310020012301);
        expect(clientTableDialogPage.getDeleteDateInput()).toMatch('2001-12-31T02:30');
        clientTableDialogPage.sectorSelectLastOption();
        clientTableDialogPage.statusSelectLastOption();
        clientTableDialogPage.save();
        expect(clientTableDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ClientTableComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-client-table div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ClientTableDialogPage {
    modalTitle = element(by.css('h4#myClientTableLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    numberInput = element(by.css('input#field_number'));
    chairsInput = element(by.css('input#field_chairs'));
    modifiableInput = element(by.css('input#field_modifiable'));
    deleteDateInput = element(by.css('input#field_deleteDate'));
    sectorSelect = element(by.css('select#field_sector'));
    statusSelect = element(by.css('select#field_status'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNumberInput = function(number) {
        this.numberInput.sendKeys(number);
    };

    getNumberInput = function() {
        return this.numberInput.getAttribute('value');
    };

    setChairsInput = function(chairs) {
        this.chairsInput.sendKeys(chairs);
    };

    getChairsInput = function() {
        return this.chairsInput.getAttribute('value');
    };

    getModifiableInput = function() {
        return this.modifiableInput;
    };
    setDeleteDateInput = function(deleteDate) {
        this.deleteDateInput.sendKeys(deleteDate);
    };

    getDeleteDateInput = function() {
        return this.deleteDateInput.getAttribute('value');
    };

    sectorSelectLastOption = function() {
        this.sectorSelect.all(by.tagName('option')).last().click();
    };

    sectorSelectOption = function(option) {
        this.sectorSelect.sendKeys(option);
    };

    getSectorSelect = function() {
        return this.sectorSelect;
    };

    getSectorSelectedOption = function() {
        return this.sectorSelect.element(by.css('option:checked')).getText();
    };

    statusSelectLastOption = function() {
        this.statusSelect.all(by.tagName('option')).last().click();
    };

    statusSelectOption = function(option) {
        this.statusSelect.sendKeys(option);
    };

    getStatusSelect = function() {
        return this.statusSelect;
    };

    getStatusSelectedOption = function() {
        return this.statusSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
