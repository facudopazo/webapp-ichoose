
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IchooseTestModule } from '../../../test.module';
import { ClientTableDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/client-table/client-table-delete-dialog.component';
import { ClientTableService } from '../../../../../../main/webapp/app/entities/client-table/client-table.service';

describe('Component Tests', () => {

    describe('ClientTable Management Delete Component', () => {
        let comp: ClientTableDeleteDialogComponent;
        let fixture: ComponentFixture<ClientTableDeleteDialogComponent>;
        let service: ClientTableService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [ClientTableDeleteDialogComponent],
                providers: [
                    ClientTableService
                ]
            })
            .overrideTemplate(ClientTableDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ClientTableDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ClientTableService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(observableOf({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
