
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';

import { IchooseTestModule } from '../../../test.module';
import { ClientTableDetailComponent } from '../../../../../../main/webapp/app/entities/client-table/client-table-detail.component';
import { ClientTableService } from '../../../../../../main/webapp/app/entities/client-table/client-table.service';
import { ClientTable } from '../../../../../../main/webapp/app/entities/client-table/client-table.model';

describe('Component Tests', () => {

    describe('ClientTable Management Detail Component', () => {
        let comp: ClientTableDetailComponent;
        let fixture: ComponentFixture<ClientTableDetailComponent>;
        let service: ClientTableService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [ClientTableDetailComponent],
                providers: [
                    ClientTableService
                ]
            })
            .overrideTemplate(ClientTableDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ClientTableDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ClientTableService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(observableOf(new HttpResponse({
                    body: new ClientTable(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.clientTable).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
