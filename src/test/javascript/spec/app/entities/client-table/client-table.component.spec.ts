
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IchooseTestModule } from '../../../test.module';
import { ClientTableComponent } from '../../../../../../main/webapp/app/entities/client-table/client-table.component';
import { ClientTableService } from '../../../../../../main/webapp/app/entities/client-table/client-table.service';
import { ClientTable } from '../../../../../../main/webapp/app/entities/client-table/client-table.model';

describe('Component Tests', () => {

    describe('ClientTable Management Component', () => {
        let comp: ClientTableComponent;
        let fixture: ComponentFixture<ClientTableComponent>;
        let service: ClientTableService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [ClientTableComponent],
                providers: [
                    ClientTableService
                ]
            })
            .overrideTemplate(ClientTableComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ClientTableComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ClientTableService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(observableOf(new HttpResponse({
                    body: [new ClientTable(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.clientTables[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
