/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IchooseTestModule } from '../../../test.module';
import { StayComponent } from '../../../../../../main/webapp/app/entities/stay/stay.component';
import { StayService } from '../../../../../../main/webapp/app/entities/stay/stay.service';
import { Stay } from '../../../../../../main/webapp/app/entities/stay/stay.model';

describe('Component Tests', () => {

    describe('Stay Management Component', () => {
        let comp: StayComponent;
        let fixture: ComponentFixture<StayComponent>;
        let service: StayService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [StayComponent],
                providers: [
                    StayService
                ]
            })
            .overrideTemplate(StayComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(StayComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(StayService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Stay(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.stays[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
