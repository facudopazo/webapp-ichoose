/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { IchooseTestModule } from '../../../test.module';
import { StayDetailComponent } from '../../../../../../main/webapp/app/entities/stay/stay-detail.component';
import { StayService } from '../../../../../../main/webapp/app/entities/stay/stay.service';
import { Stay } from '../../../../../../main/webapp/app/entities/stay/stay.model';

describe('Component Tests', () => {

    describe('Stay Management Detail Component', () => {
        let comp: StayDetailComponent;
        let fixture: ComponentFixture<StayDetailComponent>;
        let service: StayService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [StayDetailComponent],
                providers: [
                    StayService
                ]
            })
            .overrideTemplate(StayDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(StayDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(StayService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Stay(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.stay).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
