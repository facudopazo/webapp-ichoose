/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { IchooseTestModule } from '../../../test.module';
import { TaxBillItemDialogComponent } from '../../../../../../main/webapp/app/entities/tax-bill-item/tax-bill-item-dialog.component';
import { TaxBillItemService } from '../../../../../../main/webapp/app/entities/tax-bill-item/tax-bill-item.service';
import { TaxBillItem } from '../../../../../../main/webapp/app/entities/tax-bill-item/tax-bill-item.model';
import { StayService } from '../../../../../../main/webapp/app/entities/stay';
import { TaxBillService } from '../../../../../../main/webapp/app/entities/tax-bill';

describe('Component Tests', () => {

    describe('TaxBillItem Management Dialog Component', () => {
        let comp: TaxBillItemDialogComponent;
        let fixture: ComponentFixture<TaxBillItemDialogComponent>;
        let service: TaxBillItemService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [TaxBillItemDialogComponent],
                providers: [
                    StayService,
                    TaxBillService,
                    TaxBillItemService
                ]
            })
            .overrideTemplate(TaxBillItemDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TaxBillItemDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TaxBillItemService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new TaxBillItem(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.taxBillItem = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'taxBillItemListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new TaxBillItem();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.taxBillItem = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'taxBillItemListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
