/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IchooseTestModule } from '../../../test.module';
import { TaxBillItemComponent } from '../../../../../../main/webapp/app/entities/tax-bill-item/tax-bill-item.component';
import { TaxBillItemService } from '../../../../../../main/webapp/app/entities/tax-bill-item/tax-bill-item.service';
import { TaxBillItem } from '../../../../../../main/webapp/app/entities/tax-bill-item/tax-bill-item.model';

describe('Component Tests', () => {

    describe('TaxBillItem Management Component', () => {
        let comp: TaxBillItemComponent;
        let fixture: ComponentFixture<TaxBillItemComponent>;
        let service: TaxBillItemService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [TaxBillItemComponent],
                providers: [
                    TaxBillItemService
                ]
            })
            .overrideTemplate(TaxBillItemComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TaxBillItemComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TaxBillItemService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new TaxBillItem(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.taxBillItems[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
