/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { IchooseTestModule } from '../../../test.module';
import { TaxBillItemDetailComponent } from '../../../../../../main/webapp/app/entities/tax-bill-item/tax-bill-item-detail.component';
import { TaxBillItemService } from '../../../../../../main/webapp/app/entities/tax-bill-item/tax-bill-item.service';
import { TaxBillItem } from '../../../../../../main/webapp/app/entities/tax-bill-item/tax-bill-item.model';

describe('Component Tests', () => {

    describe('TaxBillItem Management Detail Component', () => {
        let comp: TaxBillItemDetailComponent;
        let fixture: ComponentFixture<TaxBillItemDetailComponent>;
        let service: TaxBillItemService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [TaxBillItemDetailComponent],
                providers: [
                    TaxBillItemService
                ]
            })
            .overrideTemplate(TaxBillItemDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TaxBillItemDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TaxBillItemService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new TaxBillItem(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.taxBillItem).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
