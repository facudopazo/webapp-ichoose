/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IchooseTestModule } from '../../../test.module';
import { StayOrderComponent } from '../../../../../../main/webapp/app/entities/stay-order/stay-order.component';
import { StayOrderService } from '../../../../../../main/webapp/app/entities/stay-order/stay-order.service';
import { StayOrder } from '../../../../../../main/webapp/app/entities/stay-order/stay-order.model';

describe('Component Tests', () => {

    describe('StayOrder Management Component', () => {
        let comp: StayOrderComponent;
        let fixture: ComponentFixture<StayOrderComponent>;
        let service: StayOrderService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [StayOrderComponent],
                providers: [
                    StayOrderService
                ]
            })
            .overrideTemplate(StayOrderComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(StayOrderComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(StayOrderService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new StayOrder(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.stayOrders[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
