/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { IchooseTestModule } from '../../../test.module';
import { StayOrderDetailComponent } from '../../../../../../main/webapp/app/entities/stay-order/stay-order-detail.component';
import { StayOrderService } from '../../../../../../main/webapp/app/entities/stay-order/stay-order.service';
import { StayOrder } from '../../../../../../main/webapp/app/entities/stay-order/stay-order.model';

describe('Component Tests', () => {

    describe('StayOrder Management Detail Component', () => {
        let comp: StayOrderDetailComponent;
        let fixture: ComponentFixture<StayOrderDetailComponent>;
        let service: StayOrderService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [StayOrderDetailComponent],
                providers: [
                    StayOrderService
                ]
            })
            .overrideTemplate(StayOrderDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(StayOrderDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(StayOrderService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new StayOrder(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.stayOrder).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
