
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';

import { IchooseTestModule } from '../../../test.module';
import { ClientTableStatusDetailComponent } from '../../../../../../main/webapp/app/entities/client-table-status/client-table-status-detail.component';
import { ClientTableStatusService } from '../../../../../../main/webapp/app/entities/client-table-status/client-table-status.service';
import { ClientTableStatus } from '../../../../../../main/webapp/app/entities/client-table-status/client-table-status.model';

describe('Component Tests', () => {

    describe('ClientTableStatus Management Detail Component', () => {
        let comp: ClientTableStatusDetailComponent;
        let fixture: ComponentFixture<ClientTableStatusDetailComponent>;
        let service: ClientTableStatusService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [ClientTableStatusDetailComponent],
                providers: [
                    ClientTableStatusService
                ]
            })
            .overrideTemplate(ClientTableStatusDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ClientTableStatusDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ClientTableStatusService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(observableOf(new HttpResponse({
                    body: new ClientTableStatus(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.clientTableStatus).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
