
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IchooseTestModule } from '../../../test.module';
import { ClientTableStatusComponent } from '../../../../../../main/webapp/app/entities/client-table-status/client-table-status.component';
import { ClientTableStatusService } from '../../../../../../main/webapp/app/entities/client-table-status/client-table-status.service';
import { ClientTableStatus } from '../../../../../../main/webapp/app/entities/client-table-status/client-table-status.model';

describe('Component Tests', () => {

    describe('ClientTableStatus Management Component', () => {
        let comp: ClientTableStatusComponent;
        let fixture: ComponentFixture<ClientTableStatusComponent>;
        let service: ClientTableStatusService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [ClientTableStatusComponent],
                providers: [
                    ClientTableStatusService
                ]
            })
            .overrideTemplate(ClientTableStatusComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ClientTableStatusComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ClientTableStatusService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(observableOf(new HttpResponse({
                    body: [new ClientTableStatus(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.clientTableStatuses[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
