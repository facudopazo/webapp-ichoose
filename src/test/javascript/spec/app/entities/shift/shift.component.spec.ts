/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IchooseTestModule } from '../../../test.module';
import { ShiftComponent } from '../../../../../../main/webapp/app/entities/shift/shift.component';
import { ShiftService } from '../../../../../../main/webapp/app/entities/shift/shift.service';
import { Shift } from '../../../../../../main/webapp/app/entities/shift/shift.model';

describe('Component Tests', () => {

    describe('Shift Management Component', () => {
        let comp: ShiftComponent;
        let fixture: ComponentFixture<ShiftComponent>;
        let service: ShiftService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [ShiftComponent],
                providers: [
                    ShiftService
                ]
            })
            .overrideTemplate(ShiftComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ShiftComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ShiftService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Shift(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.shifts[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
