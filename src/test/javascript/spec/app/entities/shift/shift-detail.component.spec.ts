/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { IchooseTestModule } from '../../../test.module';
import { ShiftDetailComponent } from '../../../../../../main/webapp/app/entities/shift/shift-detail.component';
import { ShiftService } from '../../../../../../main/webapp/app/entities/shift/shift.service';
import { Shift } from '../../../../../../main/webapp/app/entities/shift/shift.model';

describe('Component Tests', () => {

    describe('Shift Management Detail Component', () => {
        let comp: ShiftDetailComponent;
        let fixture: ComponentFixture<ShiftDetailComponent>;
        let service: ShiftService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [ShiftDetailComponent],
                providers: [
                    ShiftService
                ]
            })
            .overrideTemplate(ShiftDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ShiftDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ShiftService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Shift(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.shift).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
