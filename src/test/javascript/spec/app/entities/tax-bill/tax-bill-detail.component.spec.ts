/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { IchooseTestModule } from '../../../test.module';
import { TaxBillDetailComponent } from '../../../../../../main/webapp/app/entities/tax-bill/tax-bill-detail.component';
import { TaxBillService } from '../../../../../../main/webapp/app/entities/tax-bill/tax-bill.service';
import { TaxBill } from '../../../../../../main/webapp/app/entities/tax-bill/tax-bill.model';

describe('Component Tests', () => {

    describe('TaxBill Management Detail Component', () => {
        let comp: TaxBillDetailComponent;
        let fixture: ComponentFixture<TaxBillDetailComponent>;
        let service: TaxBillService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [TaxBillDetailComponent],
                providers: [
                    TaxBillService
                ]
            })
            .overrideTemplate(TaxBillDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TaxBillDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TaxBillService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new TaxBill(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.taxBill).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
