/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IchooseTestModule } from '../../../test.module';
import { TaxBillComponent } from '../../../../../../main/webapp/app/entities/tax-bill/tax-bill.component';
import { TaxBillService } from '../../../../../../main/webapp/app/entities/tax-bill/tax-bill.service';
import { TaxBill } from '../../../../../../main/webapp/app/entities/tax-bill/tax-bill.model';

describe('Component Tests', () => {

    describe('TaxBill Management Component', () => {
        let comp: TaxBillComponent;
        let fixture: ComponentFixture<TaxBillComponent>;
        let service: TaxBillService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [TaxBillComponent],
                providers: [
                    TaxBillService
                ]
            })
            .overrideTemplate(TaxBillComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TaxBillComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TaxBillService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new TaxBill(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.taxBills[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
