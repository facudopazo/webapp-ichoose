
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IchooseTestModule } from '../../../test.module';
import { MealClassificationComponent } from '../../../../../../main/webapp/app/entities/meal-classification/meal-classification.component';
import { MealClassificationService } from '../../../../../../main/webapp/app/entities/meal-classification/meal-classification.service';
import { MealClassification } from '../../../../../../main/webapp/app/entities/meal-classification/meal-classification.model';

describe('Component Tests', () => {

    describe('MealClassification Management Component', () => {
        let comp: MealClassificationComponent;
        let fixture: ComponentFixture<MealClassificationComponent>;
        let service: MealClassificationService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [MealClassificationComponent],
                providers: [
                    MealClassificationService
                ]
            })
            .overrideTemplate(MealClassificationComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(MealClassificationComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MealClassificationService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(observableOf(new HttpResponse({
                    body: [new MealClassification(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.mealClassifications[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
