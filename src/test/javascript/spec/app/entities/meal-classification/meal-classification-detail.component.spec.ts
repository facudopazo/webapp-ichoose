
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';

import { IchooseTestModule } from '../../../test.module';
import { MealClassificationDetailComponent } from '../../../../../../main/webapp/app/entities/meal-classification/meal-classification-detail.component';
import { MealClassificationService } from '../../../../../../main/webapp/app/entities/meal-classification/meal-classification.service';
import { MealClassification } from '../../../../../../main/webapp/app/entities/meal-classification/meal-classification.model';

describe('Component Tests', () => {

    describe('MealClassification Management Detail Component', () => {
        let comp: MealClassificationDetailComponent;
        let fixture: ComponentFixture<MealClassificationDetailComponent>;
        let service: MealClassificationService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [MealClassificationDetailComponent],
                providers: [
                    MealClassificationService
                ]
            })
            .overrideTemplate(MealClassificationDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(MealClassificationDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MealClassificationService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(observableOf(new HttpResponse({
                    body: new MealClassification(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.mealClassification).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
