/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { IchooseTestModule } from '../../../test.module';
import { StayHistoryDetailComponent } from '../../../../../../main/webapp/app/entities/stay-history/stay-history-detail.component';
import { StayHistoryService } from '../../../../../../main/webapp/app/entities/stay-history/stay-history.service';
import { StayHistory } from '../../../../../../main/webapp/app/entities/stay-history/stay-history.model';

describe('Component Tests', () => {

    describe('StayHistory Management Detail Component', () => {
        let comp: StayHistoryDetailComponent;
        let fixture: ComponentFixture<StayHistoryDetailComponent>;
        let service: StayHistoryService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [StayHistoryDetailComponent],
                providers: [
                    StayHistoryService
                ]
            })
            .overrideTemplate(StayHistoryDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(StayHistoryDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(StayHistoryService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new StayHistory(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.stayHistory).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
