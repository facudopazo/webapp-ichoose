/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { IchooseTestModule } from '../../../test.module';
import { StayHistoryDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/stay-history/stay-history-delete-dialog.component';
import { StayHistoryService } from '../../../../../../main/webapp/app/entities/stay-history/stay-history.service';

describe('Component Tests', () => {

    describe('StayHistory Management Delete Component', () => {
        let comp: StayHistoryDeleteDialogComponent;
        let fixture: ComponentFixture<StayHistoryDeleteDialogComponent>;
        let service: StayHistoryService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [StayHistoryDeleteDialogComponent],
                providers: [
                    StayHistoryService
                ]
            })
            .overrideTemplate(StayHistoryDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(StayHistoryDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(StayHistoryService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
