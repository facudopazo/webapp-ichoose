
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';

import { IchooseTestModule } from '../../../test.module';
import { MenuSubclassificationDetailComponent } from '../../../../../../main/webapp/app/entities/menu-subclassification/menu-subclassification-detail.component';
import { MenuSubclassificationService } from '../../../../../../main/webapp/app/entities/menu-subclassification/menu-subclassification.service';
import { MenuSubclassification } from '../../../../../../main/webapp/app/entities/menu-subclassification/menu-subclassification.model';

describe('Component Tests', () => {

    describe('MenuSubclassification Management Detail Component', () => {
        let comp: MenuSubclassificationDetailComponent;
        let fixture: ComponentFixture<MenuSubclassificationDetailComponent>;
        let service: MenuSubclassificationService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [MenuSubclassificationDetailComponent],
                providers: [
                    MenuSubclassificationService
                ]
            })
            .overrideTemplate(MenuSubclassificationDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(MenuSubclassificationDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MenuSubclassificationService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(observableOf(new HttpResponse({
                    body: new MenuSubclassification(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.menuSubclassification).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
