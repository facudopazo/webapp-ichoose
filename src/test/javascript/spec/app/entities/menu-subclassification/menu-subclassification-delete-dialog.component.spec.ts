
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IchooseTestModule } from '../../../test.module';
import { MenuSubclassificationDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/menu-subclassification/menu-subclassification-delete-dialog.component';
import { MenuSubclassificationService } from '../../../../../../main/webapp/app/entities/menu-subclassification/menu-subclassification.service';

describe('Component Tests', () => {

    describe('MenuSubclassification Management Delete Component', () => {
        let comp: MenuSubclassificationDeleteDialogComponent;
        let fixture: ComponentFixture<MenuSubclassificationDeleteDialogComponent>;
        let service: MenuSubclassificationService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [MenuSubclassificationDeleteDialogComponent],
                providers: [
                    MenuSubclassificationService
                ]
            })
            .overrideTemplate(MenuSubclassificationDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(MenuSubclassificationDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MenuSubclassificationService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(observableOf({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
