
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IchooseTestModule } from '../../../test.module';
import { MenuSubclassificationComponent } from '../../../../../../main/webapp/app/entities/menu-subclassification/menu-subclassification.component';
import { MenuSubclassificationService } from '../../../../../../main/webapp/app/entities/menu-subclassification/menu-subclassification.service';
import { MenuSubclassification } from '../../../../../../main/webapp/app/entities/menu-subclassification/menu-subclassification.model';

describe('Component Tests', () => {

    describe('MenuSubclassification Management Component', () => {
        let comp: MenuSubclassificationComponent;
        let fixture: ComponentFixture<MenuSubclassificationComponent>;
        let service: MenuSubclassificationService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [MenuSubclassificationComponent],
                providers: [
                    MenuSubclassificationService
                ]
            })
            .overrideTemplate(MenuSubclassificationComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(MenuSubclassificationComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MenuSubclassificationService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(observableOf(new HttpResponse({
                    body: [new MenuSubclassification(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.menuSubclassifications[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
