/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { IchooseTestModule } from '../../../test.module';
import { TaxBillAdditionalDetailComponent } from '../../../../../../main/webapp/app/entities/tax-bill-additional/tax-bill-additional-detail.component';
import { TaxBillAdditionalService } from '../../../../../../main/webapp/app/entities/tax-bill-additional/tax-bill-additional.service';
import { TaxBillAdditional } from '../../../../../../main/webapp/app/entities/tax-bill-additional/tax-bill-additional.model';

describe('Component Tests', () => {

    describe('TaxBillAdditional Management Detail Component', () => {
        let comp: TaxBillAdditionalDetailComponent;
        let fixture: ComponentFixture<TaxBillAdditionalDetailComponent>;
        let service: TaxBillAdditionalService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [TaxBillAdditionalDetailComponent],
                providers: [
                    TaxBillAdditionalService
                ]
            })
            .overrideTemplate(TaxBillAdditionalDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TaxBillAdditionalDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TaxBillAdditionalService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new TaxBillAdditional(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.taxBillAdditional).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
