/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IchooseTestModule } from '../../../test.module';
import { TaxBillAdditionalComponent } from '../../../../../../main/webapp/app/entities/tax-bill-additional/tax-bill-additional.component';
import { TaxBillAdditionalService } from '../../../../../../main/webapp/app/entities/tax-bill-additional/tax-bill-additional.service';
import { TaxBillAdditional } from '../../../../../../main/webapp/app/entities/tax-bill-additional/tax-bill-additional.model';

describe('Component Tests', () => {

    describe('TaxBillAdditional Management Component', () => {
        let comp: TaxBillAdditionalComponent;
        let fixture: ComponentFixture<TaxBillAdditionalComponent>;
        let service: TaxBillAdditionalService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [TaxBillAdditionalComponent],
                providers: [
                    TaxBillAdditionalService
                ]
            })
            .overrideTemplate(TaxBillAdditionalComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TaxBillAdditionalComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TaxBillAdditionalService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new TaxBillAdditional(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.taxBillAdditionals[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
