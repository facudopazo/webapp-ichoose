/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { IchooseTestModule } from '../../../test.module';
import { TaxBillAdditionalDialogComponent } from '../../../../../../main/webapp/app/entities/tax-bill-additional/tax-bill-additional-dialog.component';
import { TaxBillAdditionalService } from '../../../../../../main/webapp/app/entities/tax-bill-additional/tax-bill-additional.service';
import { TaxBillAdditional } from '../../../../../../main/webapp/app/entities/tax-bill-additional/tax-bill-additional.model';
import { TaxBillService } from '../../../../../../main/webapp/app/entities/tax-bill';

describe('Component Tests', () => {

    describe('TaxBillAdditional Management Dialog Component', () => {
        let comp: TaxBillAdditionalDialogComponent;
        let fixture: ComponentFixture<TaxBillAdditionalDialogComponent>;
        let service: TaxBillAdditionalService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [TaxBillAdditionalDialogComponent],
                providers: [
                    TaxBillService,
                    TaxBillAdditionalService
                ]
            })
            .overrideTemplate(TaxBillAdditionalDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TaxBillAdditionalDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TaxBillAdditionalService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new TaxBillAdditional(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.taxBillAdditional = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'taxBillAdditionalListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new TaxBillAdditional();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.taxBillAdditional = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'taxBillAdditionalListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
