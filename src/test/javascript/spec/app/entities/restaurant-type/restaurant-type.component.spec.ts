
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IchooseTestModule } from '../../../test.module';
import { RestaurantTypeComponent } from '../../../../../../main/webapp/app/entities/restaurant-type/restaurant-type.component';
import { RestaurantTypeService } from '../../../../../../main/webapp/app/entities/restaurant-type/restaurant-type.service';
import { RestaurantType } from '../../../../../../main/webapp/app/entities/restaurant-type/restaurant-type.model';

describe('Component Tests', () => {

    describe('RestaurantType Management Component', () => {
        let comp: RestaurantTypeComponent;
        let fixture: ComponentFixture<RestaurantTypeComponent>;
        let service: RestaurantTypeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [RestaurantTypeComponent],
                providers: [
                    RestaurantTypeService
                ]
            })
            .overrideTemplate(RestaurantTypeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RestaurantTypeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RestaurantTypeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(observableOf(new HttpResponse({
                    body: [new RestaurantType(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.restaurantTypes[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
