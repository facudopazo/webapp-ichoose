
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';

import { IchooseTestModule } from '../../../test.module';
import { RestaurantTypeDetailComponent } from '../../../../../../main/webapp/app/entities/restaurant-type/restaurant-type-detail.component';
import { RestaurantTypeService } from '../../../../../../main/webapp/app/entities/restaurant-type/restaurant-type.service';
import { RestaurantType } from '../../../../../../main/webapp/app/entities/restaurant-type/restaurant-type.model';

describe('Component Tests', () => {

    describe('RestaurantType Management Detail Component', () => {
        let comp: RestaurantTypeDetailComponent;
        let fixture: ComponentFixture<RestaurantTypeDetailComponent>;
        let service: RestaurantTypeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [RestaurantTypeDetailComponent],
                providers: [
                    RestaurantTypeService
                ]
            })
            .overrideTemplate(RestaurantTypeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RestaurantTypeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RestaurantTypeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(observableOf(new HttpResponse({
                    body: new RestaurantType(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.restaurantType).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
