
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IchooseTestModule } from '../../../test.module';
import { PictureDialogComponent } from '../../../../../../main/webapp/app/entities/picture/picture-dialog.component';
import { PictureService } from '../../../../../../main/webapp/app/entities/picture/picture.service';
import { Picture } from '../../../../../../main/webapp/app/entities/picture/picture.model';
import { RestaurantService } from '../../../../../../main/webapp/app/entities/restaurant';

describe('Component Tests', () => {

    describe('Picture Management Dialog Component', () => {
        let comp: PictureDialogComponent;
        let fixture: ComponentFixture<PictureDialogComponent>;
        let service: PictureService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [PictureDialogComponent],
                providers: [
                    RestaurantService,
                    PictureService
                ]
            })
            .overrideTemplate(PictureDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PictureDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PictureService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Picture(123);
                        spyOn(service, 'update').and.returnValue(observableOf(new HttpResponse({body: entity})));
                        comp.picture = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'pictureListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Picture();
                        spyOn(service, 'create').and.returnValue(observableOf(new HttpResponse({body: entity})));
                        comp.picture = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'pictureListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
