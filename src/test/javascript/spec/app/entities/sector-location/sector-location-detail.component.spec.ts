
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';

import { IchooseTestModule } from '../../../test.module';
import { SectorLocationDetailComponent } from '../../../../../../main/webapp/app/entities/sector-location/sector-location-detail.component';
import { SectorLocationService } from '../../../../../../main/webapp/app/entities/sector-location/sector-location.service';
import { SectorLocation } from '../../../../../../main/webapp/app/entities/sector-location/sector-location.model';

describe('Component Tests', () => {

    describe('SectorLocation Management Detail Component', () => {
        let comp: SectorLocationDetailComponent;
        let fixture: ComponentFixture<SectorLocationDetailComponent>;
        let service: SectorLocationService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [SectorLocationDetailComponent],
                providers: [
                    SectorLocationService
                ]
            })
            .overrideTemplate(SectorLocationDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SectorLocationDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SectorLocationService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(observableOf(new HttpResponse({
                    body: new SectorLocation(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.sectorLocation).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
