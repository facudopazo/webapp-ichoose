
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IchooseTestModule } from '../../../test.module';
import { SectorLocationDialogComponent } from '../../../../../../main/webapp/app/entities/sector-location/sector-location-dialog.component';
import { SectorLocationService } from '../../../../../../main/webapp/app/entities/sector-location/sector-location.service';
import { SectorLocation } from '../../../../../../main/webapp/app/entities/sector-location/sector-location.model';

describe('Component Tests', () => {

    describe('SectorLocation Management Dialog Component', () => {
        let comp: SectorLocationDialogComponent;
        let fixture: ComponentFixture<SectorLocationDialogComponent>;
        let service: SectorLocationService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [SectorLocationDialogComponent],
                providers: [
                    SectorLocationService
                ]
            })
            .overrideTemplate(SectorLocationDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SectorLocationDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SectorLocationService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new SectorLocation(123);
                        spyOn(service, 'update').and.returnValue(observableOf(new HttpResponse({body: entity})));
                        comp.sectorLocation = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'sectorLocationListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new SectorLocation();
                        spyOn(service, 'create').and.returnValue(observableOf(new HttpResponse({body: entity})));
                        comp.sectorLocation = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'sectorLocationListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
