
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IchooseTestModule } from '../../../test.module';
import { SectorLocationComponent } from '../../../../../../main/webapp/app/entities/sector-location/sector-location.component';
import { SectorLocationService } from '../../../../../../main/webapp/app/entities/sector-location/sector-location.service';
import { SectorLocation } from '../../../../../../main/webapp/app/entities/sector-location/sector-location.model';

describe('Component Tests', () => {

    describe('SectorLocation Management Component', () => {
        let comp: SectorLocationComponent;
        let fixture: ComponentFixture<SectorLocationComponent>;
        let service: SectorLocationService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [SectorLocationComponent],
                providers: [
                    SectorLocationService
                ]
            })
            .overrideTemplate(SectorLocationComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SectorLocationComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SectorLocationService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(observableOf(new HttpResponse({
                    body: [new SectorLocation(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.sectorLocations[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
