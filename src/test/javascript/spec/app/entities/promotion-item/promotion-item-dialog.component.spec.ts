/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { IchooseTestModule } from '../../../test.module';
import { PromotionItemDialogComponent } from '../../../../../../main/webapp/app/entities/promotion-item/promotion-item-dialog.component';
import { PromotionItemService } from '../../../../../../main/webapp/app/entities/promotion-item/promotion-item.service';
import { PromotionItem } from '../../../../../../main/webapp/app/entities/promotion-item/promotion-item.model';
import { PromotionService } from '../../../../../../main/webapp/app/entities/promotion';
import { MenuService } from '../../../../../../main/webapp/app/entities/menu';

describe('Component Tests', () => {

    describe('PromotionItem Management Dialog Component', () => {
        let comp: PromotionItemDialogComponent;
        let fixture: ComponentFixture<PromotionItemDialogComponent>;
        let service: PromotionItemService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [PromotionItemDialogComponent],
                providers: [
                    PromotionService,
                    MenuService,
                    PromotionItemService
                ]
            })
            .overrideTemplate(PromotionItemDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PromotionItemDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PromotionItemService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new PromotionItem(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.promotionItem = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'promotionItemListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new PromotionItem();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.promotionItem = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'promotionItemListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
