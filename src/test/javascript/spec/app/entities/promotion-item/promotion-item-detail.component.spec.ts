/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { IchooseTestModule } from '../../../test.module';
import { PromotionItemDetailComponent } from '../../../../../../main/webapp/app/entities/promotion-item/promotion-item-detail.component';
import { PromotionItemService } from '../../../../../../main/webapp/app/entities/promotion-item/promotion-item.service';
import { PromotionItem } from '../../../../../../main/webapp/app/entities/promotion-item/promotion-item.model';

describe('Component Tests', () => {

    describe('PromotionItem Management Detail Component', () => {
        let comp: PromotionItemDetailComponent;
        let fixture: ComponentFixture<PromotionItemDetailComponent>;
        let service: PromotionItemService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [PromotionItemDetailComponent],
                providers: [
                    PromotionItemService
                ]
            })
            .overrideTemplate(PromotionItemDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PromotionItemDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PromotionItemService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new PromotionItem(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.promotionItem).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
