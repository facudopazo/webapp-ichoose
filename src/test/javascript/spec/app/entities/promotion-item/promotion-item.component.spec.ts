/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IchooseTestModule } from '../../../test.module';
import { PromotionItemComponent } from '../../../../../../main/webapp/app/entities/promotion-item/promotion-item.component';
import { PromotionItemService } from '../../../../../../main/webapp/app/entities/promotion-item/promotion-item.service';
import { PromotionItem } from '../../../../../../main/webapp/app/entities/promotion-item/promotion-item.model';

describe('Component Tests', () => {

    describe('PromotionItem Management Component', () => {
        let comp: PromotionItemComponent;
        let fixture: ComponentFixture<PromotionItemComponent>;
        let service: PromotionItemService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [PromotionItemComponent],
                providers: [
                    PromotionItemService
                ]
            })
            .overrideTemplate(PromotionItemComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PromotionItemComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PromotionItemService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new PromotionItem(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.promotionItems[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
