/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IchooseTestModule } from '../../../test.module';
import { ClientGroupComponent } from '../../../../../../main/webapp/app/entities/client-group/client-group.component';
import { ClientGroupService } from '../../../../../../main/webapp/app/entities/client-group/client-group.service';
import { ClientGroup } from '../../../../../../main/webapp/app/entities/client-group/client-group.model';

describe('Component Tests', () => {

    describe('ClientGroup Management Component', () => {
        let comp: ClientGroupComponent;
        let fixture: ComponentFixture<ClientGroupComponent>;
        let service: ClientGroupService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [ClientGroupComponent],
                providers: [
                    ClientGroupService
                ]
            })
            .overrideTemplate(ClientGroupComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ClientGroupComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ClientGroupService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new ClientGroup(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.clientGroups[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
