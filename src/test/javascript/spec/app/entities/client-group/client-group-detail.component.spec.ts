/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { IchooseTestModule } from '../../../test.module';
import { ClientGroupDetailComponent } from '../../../../../../main/webapp/app/entities/client-group/client-group-detail.component';
import { ClientGroupService } from '../../../../../../main/webapp/app/entities/client-group/client-group.service';
import { ClientGroup } from '../../../../../../main/webapp/app/entities/client-group/client-group.model';

describe('Component Tests', () => {

    describe('ClientGroup Management Detail Component', () => {
        let comp: ClientGroupDetailComponent;
        let fixture: ComponentFixture<ClientGroupDetailComponent>;
        let service: ClientGroupService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [ClientGroupDetailComponent],
                providers: [
                    ClientGroupService
                ]
            })
            .overrideTemplate(ClientGroupDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ClientGroupDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ClientGroupService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new ClientGroup(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.clientGroup).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
