
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IchooseTestModule } from '../../../test.module';
import { MenuDialogComponent } from '../../../../../../main/webapp/app/entities/menu/menu-dialog.component';
import { MenuService } from '../../../../../../main/webapp/app/entities/menu/menu.service';
import { Menu } from '../../../../../../main/webapp/app/entities/menu/menu.model';
import { MeasuringUnitService } from '../../../../../../main/webapp/app/entities/measuring-unit';
import { MenuClassificationService } from '../../../../../../main/webapp/app/entities/menu-classification';
import { RestaurantService } from '../../../../../../main/webapp/app/entities/restaurant';
import { PictureService } from '../../../../../../main/webapp/app/entities/picture';
import { MenuSubclassificationService } from '../../../../../../main/webapp/app/entities/menu-subclassification';

describe('Component Tests', () => {

    describe('Menu Management Dialog Component', () => {
        let comp: MenuDialogComponent;
        let fixture: ComponentFixture<MenuDialogComponent>;
        let service: MenuService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [MenuDialogComponent],
                providers: [
                    MeasuringUnitService,
                    MenuClassificationService,
                    RestaurantService,
                    PictureService,
                    MenuSubclassificationService,
                    MenuService
                ]
            })
            .overrideTemplate(MenuDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(MenuDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MenuService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Menu(123);
                        spyOn(service, 'update').and.returnValue(observableOf(new HttpResponse({body: entity})));
                        comp.menu = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'menuListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Menu();
                        spyOn(service, 'create').and.returnValue(observableOf(new HttpResponse({body: entity})));
                        comp.menu = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'menuListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
