/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { IchooseTestModule } from '../../../test.module';
import { PromotionShippingDetailComponent } from '../../../../../../main/webapp/app/entities/promotion-shipping/promotion-shipping-detail.component';
import { PromotionShippingService } from '../../../../../../main/webapp/app/entities/promotion-shipping/promotion-shipping.service';
import { PromotionShipping } from '../../../../../../main/webapp/app/entities/promotion-shipping/promotion-shipping.model';

describe('Component Tests', () => {

    describe('PromotionShipping Management Detail Component', () => {
        let comp: PromotionShippingDetailComponent;
        let fixture: ComponentFixture<PromotionShippingDetailComponent>;
        let service: PromotionShippingService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [PromotionShippingDetailComponent],
                providers: [
                    PromotionShippingService
                ]
            })
            .overrideTemplate(PromotionShippingDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PromotionShippingDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PromotionShippingService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new PromotionShipping(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.promotionShipping).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
