/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IchooseTestModule } from '../../../test.module';
import { PromotionShippingComponent } from '../../../../../../main/webapp/app/entities/promotion-shipping/promotion-shipping.component';
import { PromotionShippingService } from '../../../../../../main/webapp/app/entities/promotion-shipping/promotion-shipping.service';
import { PromotionShipping } from '../../../../../../main/webapp/app/entities/promotion-shipping/promotion-shipping.model';

describe('Component Tests', () => {

    describe('PromotionShipping Management Component', () => {
        let comp: PromotionShippingComponent;
        let fixture: ComponentFixture<PromotionShippingComponent>;
        let service: PromotionShippingService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [PromotionShippingComponent],
                providers: [
                    PromotionShippingService
                ]
            })
            .overrideTemplate(PromotionShippingComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PromotionShippingComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PromotionShippingService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new PromotionShipping(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.promotionShippings[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
