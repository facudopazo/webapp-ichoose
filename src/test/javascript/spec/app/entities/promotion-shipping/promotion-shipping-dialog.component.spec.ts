/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { IchooseTestModule } from '../../../test.module';
import { PromotionShippingDialogComponent } from '../../../../../../main/webapp/app/entities/promotion-shipping/promotion-shipping-dialog.component';
import { PromotionShippingService } from '../../../../../../main/webapp/app/entities/promotion-shipping/promotion-shipping.service';
import { PromotionShipping } from '../../../../../../main/webapp/app/entities/promotion-shipping/promotion-shipping.model';
import { PromotionService } from '../../../../../../main/webapp/app/entities/promotion';
import { ClientGroupService } from '../../../../../../main/webapp/app/entities/client-group';

describe('Component Tests', () => {

    describe('PromotionShipping Management Dialog Component', () => {
        let comp: PromotionShippingDialogComponent;
        let fixture: ComponentFixture<PromotionShippingDialogComponent>;
        let service: PromotionShippingService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [PromotionShippingDialogComponent],
                providers: [
                    PromotionService,
                    ClientGroupService,
                    PromotionShippingService
                ]
            })
            .overrideTemplate(PromotionShippingDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PromotionShippingDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PromotionShippingService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new PromotionShipping(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.promotionShipping = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'promotionShippingListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new PromotionShipping();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.promotionShipping = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'promotionShippingListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
