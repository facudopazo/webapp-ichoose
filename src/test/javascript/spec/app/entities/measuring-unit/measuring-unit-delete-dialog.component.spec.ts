
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IchooseTestModule } from '../../../test.module';
import { MeasuringUnitDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/measuring-unit/measuring-unit-delete-dialog.component';
import { MeasuringUnitService } from '../../../../../../main/webapp/app/entities/measuring-unit/measuring-unit.service';

describe('Component Tests', () => {

    describe('MeasuringUnit Management Delete Component', () => {
        let comp: MeasuringUnitDeleteDialogComponent;
        let fixture: ComponentFixture<MeasuringUnitDeleteDialogComponent>;
        let service: MeasuringUnitService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [MeasuringUnitDeleteDialogComponent],
                providers: [
                    MeasuringUnitService
                ]
            })
            .overrideTemplate(MeasuringUnitDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(MeasuringUnitDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MeasuringUnitService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(observableOf({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
