
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IchooseTestModule } from '../../../test.module';
import { MeasuringUnitComponent } from '../../../../../../main/webapp/app/entities/measuring-unit/measuring-unit.component';
import { MeasuringUnitService } from '../../../../../../main/webapp/app/entities/measuring-unit/measuring-unit.service';
import { MeasuringUnit } from '../../../../../../main/webapp/app/entities/measuring-unit/measuring-unit.model';

describe('Component Tests', () => {

    describe('MeasuringUnit Management Component', () => {
        let comp: MeasuringUnitComponent;
        let fixture: ComponentFixture<MeasuringUnitComponent>;
        let service: MeasuringUnitService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [MeasuringUnitComponent],
                providers: [
                    MeasuringUnitService
                ]
            })
            .overrideTemplate(MeasuringUnitComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(MeasuringUnitComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MeasuringUnitService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(observableOf(new HttpResponse({
                    body: [new MeasuringUnit(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.measuringUnits[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
