
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';

import { IchooseTestModule } from '../../../test.module';
import { MeasuringUnitDetailComponent } from '../../../../../../main/webapp/app/entities/measuring-unit/measuring-unit-detail.component';
import { MeasuringUnitService } from '../../../../../../main/webapp/app/entities/measuring-unit/measuring-unit.service';
import { MeasuringUnit } from '../../../../../../main/webapp/app/entities/measuring-unit/measuring-unit.model';

describe('Component Tests', () => {

    describe('MeasuringUnit Management Detail Component', () => {
        let comp: MeasuringUnitDetailComponent;
        let fixture: ComponentFixture<MeasuringUnitDetailComponent>;
        let service: MeasuringUnitService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [MeasuringUnitDetailComponent],
                providers: [
                    MeasuringUnitService
                ]
            })
            .overrideTemplate(MeasuringUnitDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(MeasuringUnitDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MeasuringUnitService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(observableOf(new HttpResponse({
                    body: new MeasuringUnit(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.measuringUnit).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
