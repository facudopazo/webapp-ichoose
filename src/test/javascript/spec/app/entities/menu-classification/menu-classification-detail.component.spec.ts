
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';

import { IchooseTestModule } from '../../../test.module';
import { MenuClassificationDetailComponent } from '../../../../../../main/webapp/app/entities/menu-classification/menu-classification-detail.component';
import { MenuClassificationService } from '../../../../../../main/webapp/app/entities/menu-classification/menu-classification.service';
import { MenuClassification } from '../../../../../../main/webapp/app/entities/menu-classification/menu-classification.model';

describe('Component Tests', () => {

    describe('MenuClassification Management Detail Component', () => {
        let comp: MenuClassificationDetailComponent;
        let fixture: ComponentFixture<MenuClassificationDetailComponent>;
        let service: MenuClassificationService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [MenuClassificationDetailComponent],
                providers: [
                    MenuClassificationService
                ]
            })
            .overrideTemplate(MenuClassificationDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(MenuClassificationDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MenuClassificationService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(observableOf(new HttpResponse({
                    body: new MenuClassification(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.menuClassification).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
