
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IchooseTestModule } from '../../../test.module';
import { MenuClassificationComponent } from '../../../../../../main/webapp/app/entities/menu-classification/menu-classification.component';
import { MenuClassificationService } from '../../../../../../main/webapp/app/entities/menu-classification/menu-classification.service';
import { MenuClassification } from '../../../../../../main/webapp/app/entities/menu-classification/menu-classification.model';

describe('Component Tests', () => {

    describe('MenuClassification Management Component', () => {
        let comp: MenuClassificationComponent;
        let fixture: ComponentFixture<MenuClassificationComponent>;
        let service: MenuClassificationService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IchooseTestModule],
                declarations: [MenuClassificationComponent],
                providers: [
                    MenuClassificationService
                ]
            })
            .overrideTemplate(MenuClassificationComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(MenuClassificationComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MenuClassificationService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(observableOf(new HttpResponse({
                    body: [new MenuClassification(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.menuClassifications[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
