import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
import 'rxjs/add/operator/take';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../../app/app.constants';
import { HttpClient, HttpResponse } from '@angular/common/http';

@Injectable()
export class MessagingService {
    messaging = firebase.messaging();
    currentMessage = new BehaviorSubject(null);
    private resourceUrl =  SERVER_API_URL + 'ichooseapi/api/firebase/fcm/token';
    constructor(
        private afAuth: AngularFireAuth,
        private http: HttpClient,
    ) {
        this.getPermission();
     }

    private updateToken(token) {
        const body = { fcmToken: token };
        this.http.put<any>(this.resourceUrl, body, { observe: 'response' }).subscribe( (result) => console.log(result), (err) => console.log(err));
    }

    getPermission() {
        this.messaging.requestPermission()
        .then (() => {
            console.log('Notification permission granted');
            return this.messaging.getToken();
        })
        .then ( (token) => {
            this.updateToken(token);
        })
        .catch ((err) => {
            console.log('Unable to get permission to notify', err);
        });

        this.messaging.onMessage((payload) => {
            console.log('Messing received ', payload);
            this.currentMessage.next(payload);
        });

    }

    receiveMessage(): BehaviorSubject<any> {
        return this.currentMessage;
    }
}
