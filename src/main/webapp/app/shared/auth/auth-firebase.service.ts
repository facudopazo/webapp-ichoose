import { AngularFireAuth } from 'angularfire2/auth';
import { Injectable } from '@angular/core';
import { AuthServerProvider } from './auth-jwt.service';
import { SERVER_API_URL } from '../../app.constants';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { of } from 'rxjs/observable/of';
import { timer } from 'rxjs/observable/timer';
import { mergeMap } from 'rxjs/operators';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';

@Injectable()
export class AuthFirebaseProvider {

  private accessToken: String;
  firebaseSub: Subscription;
  loggedInFirebase: boolean;
  refreshFirebaseSub: Subscription;

  constructor(
    private afAuth: AngularFireAuth,
    private $localStorage: LocalStorageService,
    private $sessionStorage: SessionStorageService,
    private http: HttpClient

  ) { }

  public getFirebaseToken() {
    // Prompt for login if no access token -> DISABLED

    const getToken$ = () => {
      return this.http
        .get(`${SERVER_API_URL}ichooseapi/api/firebase/auth/token`, {
          headers: new HttpHeaders().set('Authorization', `Bearer ${this.getToken()}`)
        });
    };
    this.firebaseSub = getToken$().subscribe(
      (res) => this._firebaseAuth(res['token']),
      (err) => console.error(`An error occurred fetching Firebase token: ${err.message}`)
    );
  }
  private _firebaseAuth(tokenObj) {
    console.log(tokenObj);
    this.afAuth.auth.signInWithCustomToken(tokenObj)
      .then((res) => {
        this.loggedInFirebase = true;
        // Schedule token renewal
        this.scheduleFirebaseRenewal();
        console.log('Successfully authenticated with Firebase!');
      })
      .catch((err) => {
        const errorCode = err.code;
        const errorMessage = err.message;
        console.error(`${errorCode} Could not log into Firebase: ${errorMessage}`);
        this.loggedInFirebase = false;
      });
  }

  scheduleFirebaseRenewal() {
    // If user isn't authenticated, check for Firebase subscription
    // and unsubscribe, then return (don't schedule renewal)
    if (!this.loggedInFirebase) {
      if (this.firebaseSub) {
        this.firebaseSub.unsubscribe();
      }
      return;
    }
    // Unsubscribe from previous expiration observable
    this.unscheduleFirebaseRenewal();
    // Create and subscribe to expiration observable
    // Custom Firebase tokens minted by Firebase
    // expire after 3600 seconds (1 hour)
    const expiresAt = new Date().getTime() + (3600 * 1000);
    const expiresIn$ = of(expiresAt)
      .pipe(
        mergeMap(
          (expires) => {
            const now = Date.now();
            // Use timer to track delay until expiration
            // to run the refresh at the proper time
            return timer(Math.max(1, expires - now));
          }
        )
      );

    this.refreshFirebaseSub = expiresIn$
      .subscribe(
        () => {
          console.log('Firebase token expired; fetching a new one');
          this.getFirebaseToken();
        }
      );
  }

  unscheduleFirebaseRenewal() {
    if (this.refreshFirebaseSub) {
      this.refreshFirebaseSub.unsubscribe();
    }
  }

  private getToken() {
    return this.$localStorage.retrieve('authenticationToken') || this.$sessionStorage.retrieve('authenticationToken');
  }
}
