import { HttpParams } from '@angular/common/http';
import { JhiDateUtils } from 'ng-jhipster';

export const createRequestOption = (req?: any): HttpParams => {
    let options: HttpParams = new HttpParams();
    const dateUtils: JhiDateUtils = new JhiDateUtils();
    if (req) {
        Object.keys(req).forEach((key) => {
            if (key !== 'sort' && req[key] != null && req[key] !== undefined ) {
                // Special treatment for dates
                if (typeof req[key].getMonth === 'function') {
                    req[key] = (req[key]).toISOString();
                }
                options = options.set(key, req[key]);
            }
        });
        if (req.sort) {
            req.sort.forEach((val) => {
                options = options.append('sort', val);
            });
        }
    }
    return options;
};
