import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { TaxBillAdditionalComponent } from './tax-bill-additional.component';
import { TaxBillAdditionalDetailComponent } from './tax-bill-additional-detail.component';
import { TaxBillAdditionalPopupComponent } from './tax-bill-additional-dialog.component';
import { TaxBillAdditionalDeletePopupComponent } from './tax-bill-additional-delete-dialog.component';

@Injectable()
export class TaxBillAdditionalResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const taxBillAdditionalRoute: Routes = [
    {
        path: 'tax-bill-additional',
        component: TaxBillAdditionalComponent,
        resolve: {
            'pagingParams': TaxBillAdditionalResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.taxBillAdditional.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tax-bill-additional/:id',
        component: TaxBillAdditionalDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.taxBillAdditional.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const taxBillAdditionalPopupRoute: Routes = [
    {
        path: 'tax-bill-additional-new',
        component: TaxBillAdditionalPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.taxBillAdditional.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'tax-bill-additional/:id/edit',
        component: TaxBillAdditionalPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.taxBillAdditional.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'tax-bill-additional/:id/delete',
        component: TaxBillAdditionalDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.taxBillAdditional.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
