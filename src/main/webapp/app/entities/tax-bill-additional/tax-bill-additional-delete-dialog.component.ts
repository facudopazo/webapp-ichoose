import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TaxBillAdditional } from './tax-bill-additional.model';
import { TaxBillAdditionalPopupService } from './tax-bill-additional-popup.service';
import { TaxBillAdditionalService } from './tax-bill-additional.service';

@Component({
    selector: 'jhi-tax-bill-additional-delete-dialog',
    templateUrl: './tax-bill-additional-delete-dialog.component.html'
})
export class TaxBillAdditionalDeleteDialogComponent {

    taxBillAdditional: TaxBillAdditional;

    constructor(
        private taxBillAdditionalService: TaxBillAdditionalService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.taxBillAdditionalService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'taxBillAdditionalListModification',
                content: 'Deleted an taxBillAdditional'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-tax-bill-additional-delete-popup',
    template: ''
})
export class TaxBillAdditionalDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private taxBillAdditionalPopupService: TaxBillAdditionalPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.taxBillAdditionalPopupService
                .open(TaxBillAdditionalDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
