import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IchooseSharedModule } from '../../shared';
import {
    TaxBillAdditionalService,
    TaxBillAdditionalPopupService,
    TaxBillAdditionalComponent,
    TaxBillAdditionalDetailComponent,
    TaxBillAdditionalDialogComponent,
    TaxBillAdditionalPopupComponent,
    TaxBillAdditionalDeletePopupComponent,
    TaxBillAdditionalDeleteDialogComponent,
    taxBillAdditionalRoute,
    taxBillAdditionalPopupRoute,
    TaxBillAdditionalResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...taxBillAdditionalRoute,
    ...taxBillAdditionalPopupRoute,
];

@NgModule({
    imports: [
        IchooseSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        TaxBillAdditionalComponent,
        TaxBillAdditionalDetailComponent,
        TaxBillAdditionalDialogComponent,
        TaxBillAdditionalDeleteDialogComponent,
        TaxBillAdditionalPopupComponent,
        TaxBillAdditionalDeletePopupComponent,
    ],
    entryComponents: [
        TaxBillAdditionalComponent,
        TaxBillAdditionalDialogComponent,
        TaxBillAdditionalPopupComponent,
        TaxBillAdditionalDeleteDialogComponent,
        TaxBillAdditionalDeletePopupComponent,
    ],
    providers: [
        TaxBillAdditionalService,
        TaxBillAdditionalPopupService,
        TaxBillAdditionalResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IchooseTaxBillAdditionalModule {}
