import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TaxBillAdditional } from './tax-bill-additional.model';
import { TaxBillAdditionalPopupService } from './tax-bill-additional-popup.service';
import { TaxBillAdditionalService } from './tax-bill-additional.service';
import { TaxBill, TaxBillService } from '../tax-bill';

@Component({
    selector: 'jhi-tax-bill-additional-dialog',
    templateUrl: './tax-bill-additional-dialog.component.html'
})
export class TaxBillAdditionalDialogComponent implements OnInit {

    taxBillAdditional: TaxBillAdditional;
    isSaving: boolean;

    taxbills: TaxBill[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private taxBillAdditionalService: TaxBillAdditionalService,
        private taxBillService: TaxBillService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.taxBillService.query()
            .subscribe((res: HttpResponse<TaxBill[]>) => { this.taxbills = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.taxBillAdditional.id !== undefined) {
            this.subscribeToSaveResponse(
                this.taxBillAdditionalService.update(this.taxBillAdditional));
        } else {
            this.subscribeToSaveResponse(
                this.taxBillAdditionalService.create(this.taxBillAdditional));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<TaxBillAdditional>>) {
        result.subscribe((res: HttpResponse<TaxBillAdditional>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: TaxBillAdditional) {
        this.eventManager.broadcast({ name: 'taxBillAdditionalListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackTaxBillById(index: number, item: TaxBill) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-tax-bill-additional-popup',
    template: ''
})
export class TaxBillAdditionalPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private taxBillAdditionalPopupService: TaxBillAdditionalPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.taxBillAdditionalPopupService
                    .open(TaxBillAdditionalDialogComponent as Component, params['id']);
            } else {
                this.taxBillAdditionalPopupService
                    .open(TaxBillAdditionalDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
