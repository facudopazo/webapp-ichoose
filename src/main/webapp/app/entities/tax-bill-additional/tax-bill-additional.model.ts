import { BaseEntity } from './../../shared';

export class TaxBillAdditional implements BaseEntity {
    constructor(
        public id?: number,
        public description?: string,
        public amount?: number,
        public iva?: number,
        public negative?: boolean,
        public taxBillId?: number,
    ) {
        this.negative = false;
    }
}
