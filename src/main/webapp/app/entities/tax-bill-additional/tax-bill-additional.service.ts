import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { TaxBillAdditional } from './tax-bill-additional.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<TaxBillAdditional>;

@Injectable()
export class TaxBillAdditionalService {

    private resourceUrl =  SERVER_API_URL + 'api/tax-bill-additionals';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/tax-bill-additionals';

    constructor(private http: HttpClient) { }

    create(taxBillAdditional: TaxBillAdditional): Observable<EntityResponseType> {
        const copy = this.convert(taxBillAdditional);
        return this.http.post<TaxBillAdditional>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(taxBillAdditional: TaxBillAdditional): Observable<EntityResponseType> {
        const copy = this.convert(taxBillAdditional);
        return this.http.put<TaxBillAdditional>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<TaxBillAdditional>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<TaxBillAdditional[]>> {
        const options = createRequestOption(req);
        return this.http.get<TaxBillAdditional[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<TaxBillAdditional[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<TaxBillAdditional[]>> {
        const options = createRequestOption(req);
        return this.http.get<TaxBillAdditional[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<TaxBillAdditional[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: TaxBillAdditional = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<TaxBillAdditional[]>): HttpResponse<TaxBillAdditional[]> {
        const jsonResponse: TaxBillAdditional[] = res.body;
        const body: TaxBillAdditional[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to TaxBillAdditional.
     */
    private convertItemFromServer(taxBillAdditional: TaxBillAdditional): TaxBillAdditional {
        const copy: TaxBillAdditional = Object.assign({}, taxBillAdditional);
        return copy;
    }

    /**
     * Convert a TaxBillAdditional to a JSON which can be sent to the server.
     */
    private convert(taxBillAdditional: TaxBillAdditional): TaxBillAdditional {
        const copy: TaxBillAdditional = Object.assign({}, taxBillAdditional);
        return copy;
    }
}
