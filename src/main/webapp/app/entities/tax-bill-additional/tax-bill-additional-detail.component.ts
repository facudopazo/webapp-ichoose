import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { TaxBillAdditional } from './tax-bill-additional.model';
import { TaxBillAdditionalService } from './tax-bill-additional.service';

@Component({
    selector: 'jhi-tax-bill-additional-detail',
    templateUrl: './tax-bill-additional-detail.component.html'
})
export class TaxBillAdditionalDetailComponent implements OnInit, OnDestroy {

    taxBillAdditional: TaxBillAdditional;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private taxBillAdditionalService: TaxBillAdditionalService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTaxBillAdditionals();
    }

    load(id) {
        this.taxBillAdditionalService.find(id)
            .subscribe((taxBillAdditionalResponse: HttpResponse<TaxBillAdditional>) => {
                this.taxBillAdditional = taxBillAdditionalResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTaxBillAdditionals() {
        this.eventSubscriber = this.eventManager.subscribe(
            'taxBillAdditionalListModification',
            (response) => this.load(this.taxBillAdditional.id)
        );
    }
}
