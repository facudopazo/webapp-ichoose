import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { TaxBillAdditional } from './tax-bill-additional.model';
import { TaxBillAdditionalService } from './tax-bill-additional.service';

@Injectable()
export class TaxBillAdditionalPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private taxBillAdditionalService: TaxBillAdditionalService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.taxBillAdditionalService.find(id)
                    .subscribe((taxBillAdditionalResponse: HttpResponse<TaxBillAdditional>) => {
                        const taxBillAdditional: TaxBillAdditional = taxBillAdditionalResponse.body;
                        this.ngbModalRef = this.taxBillAdditionalModalRef(component, taxBillAdditional);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.taxBillAdditionalModalRef(component, new TaxBillAdditional());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    taxBillAdditionalModalRef(component: Component, taxBillAdditional: TaxBillAdditional): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.taxBillAdditional = taxBillAdditional;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
