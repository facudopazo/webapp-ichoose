import { BaseEntity } from './../../shared';

export class Location implements BaseEntity {
    constructor(
        public id?: number,
        public latitude?: number,
        public longitude?: number,
        public streetNumber?: string,
        public route?: string,
        public postalCode?: string,
        public country?: string,
        public formattedAddress?: string,
    ) {
    }
}
