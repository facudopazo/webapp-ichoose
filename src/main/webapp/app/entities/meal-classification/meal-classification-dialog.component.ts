import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { MealClassification } from './meal-classification.model';
import { MealClassificationPopupService } from './meal-classification-popup.service';
import { MealClassificationService } from './meal-classification.service';

@Component({
    selector: 'jhi-meal-classification-dialog',
    templateUrl: './meal-classification-dialog.component.html'
})
export class MealClassificationDialogComponent implements OnInit {

    mealClassification: MealClassification;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private mealClassificationService: MealClassificationService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.mealClassification.id !== undefined) {
            this.subscribeToSaveResponse(
                this.mealClassificationService.update(this.mealClassification));
        } else {
            this.subscribeToSaveResponse(
                this.mealClassificationService.create(this.mealClassification));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<MealClassification>>) {
        result.subscribe((res: HttpResponse<MealClassification>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: MealClassification) {
        this.eventManager.broadcast({ name: 'mealClassificationListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-meal-classification-popup',
    template: ''
})
export class MealClassificationPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private mealClassificationPopupService: MealClassificationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.mealClassificationPopupService
                    .open(MealClassificationDialogComponent as Component, params['id']);
            } else {
                this.mealClassificationPopupService
                    .open(MealClassificationDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
