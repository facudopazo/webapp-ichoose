import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { MealClassification } from './meal-classification.model';
import { MealClassificationService } from './meal-classification.service';

@Component({
    selector: 'jhi-meal-classification-detail',
    templateUrl: './meal-classification-detail.component.html'
})
export class MealClassificationDetailComponent implements OnInit, OnDestroy {

    mealClassification: MealClassification;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private mealClassificationService: MealClassificationService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInMealClassifications();
    }

    load(id) {
        this.mealClassificationService.find(id)
            .subscribe((mealClassificationResponse: HttpResponse<MealClassification>) => {
                this.mealClassification = mealClassificationResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInMealClassifications() {
        this.eventSubscriber = this.eventManager.subscribe(
            'mealClassificationListModification',
            (response) => this.load(this.mealClassification.id)
        );
    }
}
