
import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SERVER_API_URL } from '../../app.constants';

import { MealClassification } from './meal-classification.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<MealClassification>;

@Injectable()
export class MealClassificationService {

    private resourceUrl =  SERVER_API_URL + 'ichooseapi/api/meal-classifications';
    private resourceSearchUrl = SERVER_API_URL + 'ichooseapi/api/_search/meal-classifications';

    constructor(private http: HttpClient) { }

    create(mealClassification: MealClassification): Observable<EntityResponseType> {
        const copy = this.convert(mealClassification);
        return this.http.post<MealClassification>(this.resourceUrl, copy, { observe: 'response' }).pipe(
            map((res: EntityResponseType) => this.convertResponse(res)));
    }

    update(mealClassification: MealClassification): Observable<EntityResponseType> {
        const copy = this.convert(mealClassification);
        return this.http.put<MealClassification>(this.resourceUrl, copy, { observe: 'response' }).pipe(
            map((res: EntityResponseType) => this.convertResponse(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<MealClassification>(`${this.resourceUrl}/${id}`, { observe: 'response'}).pipe(
            map((res: EntityResponseType) => this.convertResponse(res)));
    }

    query(req?: any): Observable<HttpResponse<MealClassification[]>> {
        const options = createRequestOption(req);
        return this.http.get<MealClassification[]>(this.resourceUrl, { params: options, observe: 'response' }).pipe(
            map((res: HttpResponse<MealClassification[]>) => this.convertArrayResponse(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<MealClassification[]>> {
        const options = createRequestOption(req);
        return this.http.get<MealClassification[]>(this.resourceSearchUrl, { params: options, observe: 'response' }).pipe(
            map((res: HttpResponse<MealClassification[]>) => this.convertArrayResponse(res)));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: MealClassification = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<MealClassification[]>): HttpResponse<MealClassification[]> {
        const jsonResponse: MealClassification[] = res.body;
        const body: MealClassification[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to MealClassification.
     */
    private convertItemFromServer(mealClassification: MealClassification): MealClassification {
        const copy: MealClassification = Object.assign({}, mealClassification);
        return copy;
    }

    /**
     * Convert a MealClassification to a JSON which can be sent to the server.
     */
    private convert(mealClassification: MealClassification): MealClassification {
        const copy: MealClassification = Object.assign({}, mealClassification);
        return copy;
    }
}
