import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { MealClassification } from './meal-classification.model';
import { MealClassificationPopupService } from './meal-classification-popup.service';
import { MealClassificationService } from './meal-classification.service';

@Component({
    selector: 'jhi-meal-classification-delete-dialog',
    templateUrl: './meal-classification-delete-dialog.component.html'
})
export class MealClassificationDeleteDialogComponent {

    mealClassification: MealClassification;

    constructor(
        private mealClassificationService: MealClassificationService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.mealClassificationService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'mealClassificationListModification',
                content: 'Deleted an mealClassification'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-meal-classification-delete-popup',
    template: ''
})
export class MealClassificationDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private mealClassificationPopupService: MealClassificationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.mealClassificationPopupService
                .open(MealClassificationDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
