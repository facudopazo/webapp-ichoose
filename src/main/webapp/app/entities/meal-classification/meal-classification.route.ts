import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { MealClassificationComponent } from './meal-classification.component';
import { MealClassificationDetailComponent } from './meal-classification-detail.component';
import { MealClassificationPopupComponent } from './meal-classification-dialog.component';
import { MealClassificationDeletePopupComponent } from './meal-classification-delete-dialog.component';

@Injectable()
export class MealClassificationResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const mealClassificationRoute: Routes = [
    {
        path: 'meal-classification',
        component: MealClassificationComponent,
        resolve: {
            'pagingParams': MealClassificationResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.mealClassification.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'meal-classification/:id',
        component: MealClassificationDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.mealClassification.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const mealClassificationPopupRoute: Routes = [
    {
        path: 'meal-classification-new',
        component: MealClassificationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.mealClassification.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'meal-classification/:id/edit',
        component: MealClassificationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.mealClassification.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'meal-classification/:id/delete',
        component: MealClassificationDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.mealClassification.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
