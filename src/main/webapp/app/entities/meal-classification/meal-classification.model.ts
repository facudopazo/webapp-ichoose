import { BaseEntity } from './../../shared';

export class MealClassification implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
    ) {
    }
}
