import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IchooseSharedModule } from '../../shared';
import {
    MealClassificationService,
    MealClassificationPopupService,
    MealClassificationComponent,
    MealClassificationDetailComponent,
    MealClassificationDialogComponent,
    MealClassificationPopupComponent,
    MealClassificationDeletePopupComponent,
    MealClassificationDeleteDialogComponent,
    mealClassificationRoute,
    mealClassificationPopupRoute,
    MealClassificationResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...mealClassificationRoute,
    ...mealClassificationPopupRoute,
];

@NgModule({
    imports: [
        IchooseSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        MealClassificationComponent,
        MealClassificationDetailComponent,
        MealClassificationDialogComponent,
        MealClassificationDeleteDialogComponent,
        MealClassificationPopupComponent,
        MealClassificationDeletePopupComponent,
    ],
    entryComponents: [
        MealClassificationComponent,
        MealClassificationDialogComponent,
        MealClassificationPopupComponent,
        MealClassificationDeleteDialogComponent,
        MealClassificationDeletePopupComponent,
    ],
    providers: [
        MealClassificationService,
        MealClassificationPopupService,
        MealClassificationResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IchooseMealClassificationModule {}
