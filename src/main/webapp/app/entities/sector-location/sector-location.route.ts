import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { SectorLocationComponent } from './sector-location.component';
import { SectorLocationDetailComponent } from './sector-location-detail.component';
import { SectorLocationPopupComponent } from './sector-location-dialog.component';
import { SectorLocationDeletePopupComponent } from './sector-location-delete-dialog.component';

@Injectable()
export class SectorLocationResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const sectorLocationRoute: Routes = [
    {
        path: 'sector-location',
        component: SectorLocationComponent,
        resolve: {
            'pagingParams': SectorLocationResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.sectorLocation.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'sector-location/:id',
        component: SectorLocationDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.sectorLocation.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const sectorLocationPopupRoute: Routes = [
    {
        path: 'sector-location-new',
        component: SectorLocationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.sectorLocation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'sector-location/:id/edit',
        component: SectorLocationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.sectorLocation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'sector-location/:id/delete',
        component: SectorLocationDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.sectorLocation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
