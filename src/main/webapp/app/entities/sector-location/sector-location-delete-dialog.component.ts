import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SectorLocation } from './sector-location.model';
import { SectorLocationPopupService } from './sector-location-popup.service';
import { SectorLocationService } from './sector-location.service';

@Component({
    selector: 'jhi-sector-location-delete-dialog',
    templateUrl: './sector-location-delete-dialog.component.html'
})
export class SectorLocationDeleteDialogComponent {

    sectorLocation: SectorLocation;

    constructor(
        private sectorLocationService: SectorLocationService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.sectorLocationService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'sectorLocationListModification',
                content: 'Deleted an sectorLocation'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-sector-location-delete-popup',
    template: ''
})
export class SectorLocationDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private sectorLocationPopupService: SectorLocationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.sectorLocationPopupService
                .open(SectorLocationDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
