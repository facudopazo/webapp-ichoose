import { BaseEntity } from './../../shared';

export class SectorLocation implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
    ) {
    }
}
