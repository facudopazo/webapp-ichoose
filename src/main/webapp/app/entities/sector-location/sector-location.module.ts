import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IchooseSharedModule } from '../../shared';
import {
    SectorLocationService,
    SectorLocationPopupService,
    SectorLocationComponent,
    SectorLocationDetailComponent,
    SectorLocationDialogComponent,
    SectorLocationPopupComponent,
    SectorLocationDeletePopupComponent,
    SectorLocationDeleteDialogComponent,
    sectorLocationRoute,
    sectorLocationPopupRoute,
    SectorLocationResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...sectorLocationRoute,
    ...sectorLocationPopupRoute,
];

@NgModule({
    imports: [
        IchooseSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        SectorLocationComponent,
        SectorLocationDetailComponent,
        SectorLocationDialogComponent,
        SectorLocationDeleteDialogComponent,
        SectorLocationPopupComponent,
        SectorLocationDeletePopupComponent,
    ],
    entryComponents: [
        SectorLocationComponent,
        SectorLocationDialogComponent,
        SectorLocationPopupComponent,
        SectorLocationDeleteDialogComponent,
        SectorLocationDeletePopupComponent,
    ],
    providers: [
        SectorLocationService,
        SectorLocationPopupService,
        SectorLocationResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IchooseSectorLocationModule {}
