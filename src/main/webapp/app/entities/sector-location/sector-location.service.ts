
import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SERVER_API_URL } from '../../app.constants';

import { SectorLocation } from './sector-location.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<SectorLocation>;

@Injectable()
export class SectorLocationService {

    private resourceUrl =  SERVER_API_URL + 'ichooseapi/api/sector-locations';
    private resourceSearchUrl = SERVER_API_URL + 'ichooseapi/api/_search/sector-locations';

    constructor(private http: HttpClient) { }

    create(sectorLocation: SectorLocation): Observable<EntityResponseType> {
        const copy = this.convert(sectorLocation);
        return this.http.post<SectorLocation>(this.resourceUrl, copy, { observe: 'response' }).pipe(
            map((res: EntityResponseType) => this.convertResponse(res)));
    }

    update(sectorLocation: SectorLocation): Observable<EntityResponseType> {
        const copy = this.convert(sectorLocation);
        return this.http.put<SectorLocation>(this.resourceUrl, copy, { observe: 'response' }).pipe(
            map((res: EntityResponseType) => this.convertResponse(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<SectorLocation>(`${this.resourceUrl}/${id}`, { observe: 'response'}).pipe(
            map((res: EntityResponseType) => this.convertResponse(res)));
    }

    query(req?: any): Observable<HttpResponse<SectorLocation[]>> {
        const options = createRequestOption(req);
        return this.http.get<SectorLocation[]>(this.resourceUrl, { params: options, observe: 'response' }).pipe(
            map((res: HttpResponse<SectorLocation[]>) => this.convertArrayResponse(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<SectorLocation[]>> {
        const options = createRequestOption(req);
        return this.http.get<SectorLocation[]>(this.resourceSearchUrl, { params: options, observe: 'response' }).pipe(
            map((res: HttpResponse<SectorLocation[]>) => this.convertArrayResponse(res)));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: SectorLocation = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<SectorLocation[]>): HttpResponse<SectorLocation[]> {
        const jsonResponse: SectorLocation[] = res.body;
        const body: SectorLocation[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to SectorLocation.
     */
    private convertItemFromServer(sectorLocation: SectorLocation): SectorLocation {
        const copy: SectorLocation = Object.assign({}, sectorLocation);
        return copy;
    }

    /**
     * Convert a SectorLocation to a JSON which can be sent to the server.
     */
    private convert(sectorLocation: SectorLocation): SectorLocation {
        const copy: SectorLocation = Object.assign({}, sectorLocation);
        return copy;
    }
}
