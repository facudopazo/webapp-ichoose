import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { SectorLocation } from './sector-location.model';
import { SectorLocationService } from './sector-location.service';

@Component({
    selector: 'jhi-sector-location-detail',
    templateUrl: './sector-location-detail.component.html'
})
export class SectorLocationDetailComponent implements OnInit, OnDestroy {

    sectorLocation: SectorLocation;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private sectorLocationService: SectorLocationService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSectorLocations();
    }

    load(id) {
        this.sectorLocationService.find(id)
            .subscribe((sectorLocationResponse: HttpResponse<SectorLocation>) => {
                this.sectorLocation = sectorLocationResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSectorLocations() {
        this.eventSubscriber = this.eventManager.subscribe(
            'sectorLocationListModification',
            (response) => this.load(this.sectorLocation.id)
        );
    }
}
