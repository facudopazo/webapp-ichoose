import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SectorLocation } from './sector-location.model';
import { SectorLocationPopupService } from './sector-location-popup.service';
import { SectorLocationService } from './sector-location.service';

@Component({
    selector: 'jhi-sector-location-dialog',
    templateUrl: './sector-location-dialog.component.html'
})
export class SectorLocationDialogComponent implements OnInit {

    sectorLocation: SectorLocation;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private sectorLocationService: SectorLocationService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.sectorLocation.id !== undefined) {
            this.subscribeToSaveResponse(
                this.sectorLocationService.update(this.sectorLocation));
        } else {
            this.subscribeToSaveResponse(
                this.sectorLocationService.create(this.sectorLocation));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<SectorLocation>>) {
        result.subscribe((res: HttpResponse<SectorLocation>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: SectorLocation) {
        this.eventManager.broadcast({ name: 'sectorLocationListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-sector-location-popup',
    template: ''
})
export class SectorLocationPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private sectorLocationPopupService: SectorLocationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.sectorLocationPopupService
                    .open(SectorLocationDialogComponent as Component, params['id']);
            } else {
                this.sectorLocationPopupService
                    .open(SectorLocationDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
