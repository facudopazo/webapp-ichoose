import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { IchooseLocationModule } from './location/location.module';
import { IchooseMealClassificationModule } from './meal-classification/meal-classification.module';
import { IchoosePictureModule } from './picture/picture.module';
import { IchooseRestaurantTypeModule } from './restaurant-type/restaurant-type.module';
import { IchooseRestaurantModule } from './restaurant/restaurant.module';
import { IchooseRoleModule } from './role/role.module';
import { IchooseSectorModule } from './sector/sector.module';
import { IchooseSectorLocationModule } from './sector-location/sector-location.module';
import { IchooseClientTableModule } from './client-table/client-table.module';
import { IchooseClientTableStatusModule } from './client-table-status/client-table-status.module';
import { IchooseMenuModule } from './menu/menu.module';
import { IchooseMenuClassificationModule } from './menu-classification/menu-classification.module';
import { IchooseMenuSubclassificationModule } from './menu-subclassification/menu-subclassification.module';
import { IchooseMeasuringUnitModule } from './measuring-unit/measuring-unit.module';
import { IchooseStayModule } from './stay/stay.module';
import { IchooseStayOrderModule } from './stay-order/stay-order.module';
import { IchooseBookingModule } from './booking/booking.module';
import { IchoosePromotionModule } from './promotion/promotion.module';
import { IchoosePromotionItemModule } from './promotion-item/promotion-item.module';
import { IchoosePromotionShippingModule } from './promotion-shipping/promotion-shipping.module';
import { IchooseClientGroupModule } from './client-group/client-group.module';
import { IchooseTaxBillModule } from './tax-bill/tax-bill.module';
import { IchooseTaxBillItemModule } from './tax-bill-item/tax-bill-item.module';
import { IchooseTaxBillAdditionalModule } from './tax-bill-additional/tax-bill-additional.module';
import { IchooseClientModule } from './client/client.module';
import { IchooseStayHistoryModule } from './stay-history/stay-history.module';
import { IchooseNationalityModule } from './nationality/nationality.module';
import { IchooseNotificationModule } from './notification/notification.module';
import { IchooseShiftModule } from './shift/shift.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        IchooseLocationModule,
        IchooseMealClassificationModule,
        IchoosePictureModule,
        IchooseRestaurantTypeModule,
        IchooseRestaurantModule,
        IchooseRoleModule,
        IchooseSectorModule,
        IchooseSectorLocationModule,
        IchooseClientTableModule,
        IchooseClientTableStatusModule,
        IchooseMenuModule,
        IchooseMenuClassificationModule,
        IchooseMenuSubclassificationModule,
        IchooseMeasuringUnitModule,
        IchooseStayModule,
        IchooseStayOrderModule,
        IchooseBookingModule,
        IchoosePromotionModule,
        IchoosePromotionItemModule,
        IchooseClientGroupModule,
        IchooseTaxBillModule,
        IchooseTaxBillItemModule,
        IchooseTaxBillAdditionalModule,
        IchoosePromotionShippingModule,
        IchooseClientModule,
        IchooseStayHistoryModule,
        IchooseNationalityModule,
        IchooseNotificationModule,
        IchooseShiftModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IchooseEntityModule {}
