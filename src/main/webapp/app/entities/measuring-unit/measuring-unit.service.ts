
import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SERVER_API_URL } from '../../app.constants';

import { MeasuringUnit } from './measuring-unit.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<MeasuringUnit>;

@Injectable()
export class MeasuringUnitService {

    private resourceUrl =  SERVER_API_URL + 'ichooseapi/api/measuring-units';
    private resourceSearchUrl = SERVER_API_URL + 'ichooseapi/api/_search/measuring-units';

    constructor(private http: HttpClient) { }

    create(measuringUnit: MeasuringUnit): Observable<EntityResponseType> {
        const copy = this.convert(measuringUnit);
        return this.http.post<MeasuringUnit>(this.resourceUrl, copy, { observe: 'response' }).pipe(
            map((res: EntityResponseType) => this.convertResponse(res)));
    }

    update(measuringUnit: MeasuringUnit): Observable<EntityResponseType> {
        const copy = this.convert(measuringUnit);
        return this.http.put<MeasuringUnit>(this.resourceUrl, copy, { observe: 'response' }).pipe(
            map((res: EntityResponseType) => this.convertResponse(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<MeasuringUnit>(`${this.resourceUrl}/${id}`, { observe: 'response'}).pipe(
            map((res: EntityResponseType) => this.convertResponse(res)));
    }

    query(req?: any): Observable<HttpResponse<MeasuringUnit[]>> {
        const options = createRequestOption(req);
        return this.http.get<MeasuringUnit[]>(this.resourceUrl, { params: options, observe: 'response' }).pipe(
            map((res: HttpResponse<MeasuringUnit[]>) => this.convertArrayResponse(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<MeasuringUnit[]>> {
        const options = createRequestOption(req);
        return this.http.get<MeasuringUnit[]>(this.resourceSearchUrl, { params: options, observe: 'response' }).pipe(
            map((res: HttpResponse<MeasuringUnit[]>) => this.convertArrayResponse(res)));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: MeasuringUnit = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<MeasuringUnit[]>): HttpResponse<MeasuringUnit[]> {
        const jsonResponse: MeasuringUnit[] = res.body;
        const body: MeasuringUnit[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to MeasuringUnit.
     */
    private convertItemFromServer(measuringUnit: MeasuringUnit): MeasuringUnit {
        const copy: MeasuringUnit = Object.assign({}, measuringUnit);
        return copy;
    }

    /**
     * Convert a MeasuringUnit to a JSON which can be sent to the server.
     */
    private convert(measuringUnit: MeasuringUnit): MeasuringUnit {
        const copy: MeasuringUnit = Object.assign({}, measuringUnit);
        return copy;
    }
}
