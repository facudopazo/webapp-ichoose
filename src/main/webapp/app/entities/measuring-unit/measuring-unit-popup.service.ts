import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { MeasuringUnit } from './measuring-unit.model';
import { MeasuringUnitService } from './measuring-unit.service';

@Injectable()
export class MeasuringUnitPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private measuringUnitService: MeasuringUnitService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.measuringUnitService.find(id)
                    .subscribe((measuringUnitResponse: HttpResponse<MeasuringUnit>) => {
                        const measuringUnit: MeasuringUnit = measuringUnitResponse.body;
                        this.ngbModalRef = this.measuringUnitModalRef(component, measuringUnit);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.measuringUnitModalRef(component, new MeasuringUnit());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    measuringUnitModalRef(component: Component, measuringUnit: MeasuringUnit): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.measuringUnit = measuringUnit;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
