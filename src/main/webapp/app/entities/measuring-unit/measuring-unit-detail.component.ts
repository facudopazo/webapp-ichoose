import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { MeasuringUnit } from './measuring-unit.model';
import { MeasuringUnitService } from './measuring-unit.service';

@Component({
    selector: 'jhi-measuring-unit-detail',
    templateUrl: './measuring-unit-detail.component.html'
})
export class MeasuringUnitDetailComponent implements OnInit, OnDestroy {

    measuringUnit: MeasuringUnit;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private measuringUnitService: MeasuringUnitService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInMeasuringUnits();
    }

    load(id) {
        this.measuringUnitService.find(id)
            .subscribe((measuringUnitResponse: HttpResponse<MeasuringUnit>) => {
                this.measuringUnit = measuringUnitResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInMeasuringUnits() {
        this.eventSubscriber = this.eventManager.subscribe(
            'measuringUnitListModification',
            (response) => this.load(this.measuringUnit.id)
        );
    }
}
