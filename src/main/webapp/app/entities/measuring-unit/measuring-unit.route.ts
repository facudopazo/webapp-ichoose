import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { MeasuringUnitComponent } from './measuring-unit.component';
import { MeasuringUnitDetailComponent } from './measuring-unit-detail.component';
import { MeasuringUnitPopupComponent } from './measuring-unit-dialog.component';
import { MeasuringUnitDeletePopupComponent } from './measuring-unit-delete-dialog.component';

@Injectable()
export class MeasuringUnitResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const measuringUnitRoute: Routes = [
    {
        path: 'measuring-unit',
        component: MeasuringUnitComponent,
        resolve: {
            'pagingParams': MeasuringUnitResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.measuringUnit.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'measuring-unit/:id',
        component: MeasuringUnitDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.measuringUnit.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const measuringUnitPopupRoute: Routes = [
    {
        path: 'measuring-unit-new',
        component: MeasuringUnitPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.measuringUnit.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'measuring-unit/:id/edit',
        component: MeasuringUnitPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.measuringUnit.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'measuring-unit/:id/delete',
        component: MeasuringUnitDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.measuringUnit.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
