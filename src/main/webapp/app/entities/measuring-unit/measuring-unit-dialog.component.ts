import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { MeasuringUnit } from './measuring-unit.model';
import { MeasuringUnitPopupService } from './measuring-unit-popup.service';
import { MeasuringUnitService } from './measuring-unit.service';

@Component({
    selector: 'jhi-measuring-unit-dialog',
    templateUrl: './measuring-unit-dialog.component.html'
})
export class MeasuringUnitDialogComponent implements OnInit {

    measuringUnit: MeasuringUnit;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private measuringUnitService: MeasuringUnitService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.measuringUnit.id !== undefined) {
            this.subscribeToSaveResponse(
                this.measuringUnitService.update(this.measuringUnit));
        } else {
            this.subscribeToSaveResponse(
                this.measuringUnitService.create(this.measuringUnit));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<MeasuringUnit>>) {
        result.subscribe((res: HttpResponse<MeasuringUnit>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: MeasuringUnit) {
        this.eventManager.broadcast({ name: 'measuringUnitListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-measuring-unit-popup',
    template: ''
})
export class MeasuringUnitPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private measuringUnitPopupService: MeasuringUnitPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.measuringUnitPopupService
                    .open(MeasuringUnitDialogComponent as Component, params['id']);
            } else {
                this.measuringUnitPopupService
                    .open(MeasuringUnitDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
