import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IchooseSharedModule } from '../../shared';
import {
    MeasuringUnitService,
    MeasuringUnitPopupService,
    MeasuringUnitComponent,
    MeasuringUnitDetailComponent,
    MeasuringUnitDialogComponent,
    MeasuringUnitPopupComponent,
    MeasuringUnitDeletePopupComponent,
    MeasuringUnitDeleteDialogComponent,
    measuringUnitRoute,
    measuringUnitPopupRoute,
    MeasuringUnitResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...measuringUnitRoute,
    ...measuringUnitPopupRoute,
];

@NgModule({
    imports: [
        IchooseSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        MeasuringUnitComponent,
        MeasuringUnitDetailComponent,
        MeasuringUnitDialogComponent,
        MeasuringUnitDeleteDialogComponent,
        MeasuringUnitPopupComponent,
        MeasuringUnitDeletePopupComponent,
    ],
    entryComponents: [
        MeasuringUnitComponent,
        MeasuringUnitDialogComponent,
        MeasuringUnitPopupComponent,
        MeasuringUnitDeleteDialogComponent,
        MeasuringUnitDeletePopupComponent,
    ],
    providers: [
        MeasuringUnitService,
        MeasuringUnitPopupService,
        MeasuringUnitResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IchooseMeasuringUnitModule {}
