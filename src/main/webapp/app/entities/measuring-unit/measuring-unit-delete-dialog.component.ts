import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { MeasuringUnit } from './measuring-unit.model';
import { MeasuringUnitPopupService } from './measuring-unit-popup.service';
import { MeasuringUnitService } from './measuring-unit.service';

@Component({
    selector: 'jhi-measuring-unit-delete-dialog',
    templateUrl: './measuring-unit-delete-dialog.component.html'
})
export class MeasuringUnitDeleteDialogComponent {

    measuringUnit: MeasuringUnit;

    constructor(
        private measuringUnitService: MeasuringUnitService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.measuringUnitService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'measuringUnitListModification',
                content: 'Deleted an measuringUnit'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-measuring-unit-delete-popup',
    template: ''
})
export class MeasuringUnitDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private measuringUnitPopupService: MeasuringUnitPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.measuringUnitPopupService
                .open(MeasuringUnitDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
