import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { PromotionShipping } from './promotion-shipping.model';
import { PromotionShippingService } from './promotion-shipping.service';

@Component({
    selector: 'jhi-promotion-shipping-detail',
    templateUrl: './promotion-shipping-detail.component.html'
})
export class PromotionShippingDetailComponent implements OnInit, OnDestroy {

    promotionShipping: PromotionShipping;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private promotionShippingService: PromotionShippingService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPromotionShippings();
    }

    load(id) {
        this.promotionShippingService.find(id)
            .subscribe((promotionShippingResponse: HttpResponse<PromotionShipping>) => {
                this.promotionShipping = promotionShippingResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPromotionShippings() {
        this.eventSubscriber = this.eventManager.subscribe(
            'promotionShippingListModification',
            (response) => this.load(this.promotionShipping.id)
        );
    }
}
