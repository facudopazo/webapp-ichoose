import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { PromotionShippingComponent } from './promotion-shipping.component';
import { PromotionShippingDetailComponent } from './promotion-shipping-detail.component';
import { PromotionShippingPopupComponent } from './promotion-shipping-dialog.component';
import { PromotionShippingDeletePopupComponent } from './promotion-shipping-delete-dialog.component';

@Injectable()
export class PromotionShippingResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const promotionShippingRoute: Routes = [
    {
        path: 'promotion-shipping',
        component: PromotionShippingComponent,
        resolve: {
            'pagingParams': PromotionShippingResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.promotionShipping.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'promotion-shipping/:id',
        component: PromotionShippingDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.promotionShipping.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const promotionShippingPopupRoute: Routes = [
    {
        path: 'promotion-shipping-new',
        component: PromotionShippingPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.promotionShipping.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'promotion-shipping/:id/edit',
        component: PromotionShippingPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.promotionShipping.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'promotion-shipping/:id/delete',
        component: PromotionShippingDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.promotionShipping.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
