import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IchooseSharedModule } from '../../shared';
import {
    PromotionShippingService,
    PromotionShippingPopupService,
    PromotionShippingComponent,
    PromotionShippingDetailComponent,
    PromotionShippingDialogComponent,
    PromotionShippingPopupComponent,
    PromotionShippingDeletePopupComponent,
    PromotionShippingDeleteDialogComponent,
    promotionShippingRoute,
    promotionShippingPopupRoute,
    PromotionShippingResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...promotionShippingRoute,
    ...promotionShippingPopupRoute,
];

@NgModule({
    imports: [
        IchooseSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        PromotionShippingComponent,
        PromotionShippingDetailComponent,
        PromotionShippingDialogComponent,
        PromotionShippingDeleteDialogComponent,
        PromotionShippingPopupComponent,
        PromotionShippingDeletePopupComponent,
    ],
    entryComponents: [
        PromotionShippingComponent,
        PromotionShippingDialogComponent,
        PromotionShippingPopupComponent,
        PromotionShippingDeleteDialogComponent,
        PromotionShippingDeletePopupComponent,
    ],
    providers: [
        PromotionShippingService,
        PromotionShippingPopupService,
        PromotionShippingResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IchoosePromotionShippingModule {}
