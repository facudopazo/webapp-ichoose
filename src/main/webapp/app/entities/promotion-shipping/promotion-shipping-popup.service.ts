import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { PromotionShipping } from './promotion-shipping.model';
import { PromotionShippingService } from './promotion-shipping.service';

@Injectable()
export class PromotionShippingPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private promotionShippingService: PromotionShippingService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.promotionShippingService.find(id)
                    .subscribe((promotionShippingResponse: HttpResponse<PromotionShipping>) => {
                        const promotionShipping: PromotionShipping = promotionShippingResponse.body;
                        promotionShipping.dateTimeShipping = this.datePipe
                            .transform(promotionShipping.dateTimeShipping, 'yyyy-MM-ddTHH:mm:ss');
                        this.ngbModalRef = this.promotionShippingModalRef(component, promotionShipping);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.promotionShippingModalRef(component, new PromotionShipping());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    promotionShippingModalRef(component: Component, promotionShipping: PromotionShipping): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.promotionShipping = promotionShipping;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
