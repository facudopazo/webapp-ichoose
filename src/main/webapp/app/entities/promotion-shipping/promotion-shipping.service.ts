import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { PromotionShipping } from './promotion-shipping.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<PromotionShipping>;

@Injectable()
export class PromotionShippingService {

    private resourceUrl =  SERVER_API_URL + 'ichooseapi/api/promotion-shippings';
    private resourceSearchUrl = SERVER_API_URL + 'ichooseapi/api/_search/promotion-shippings';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    send(promotionId: Number, groups: Number[]): Observable<any> {
        const resourceUrl =  SERVER_API_URL + 'ichooseapi/api/promotions';
        return this.http.post<any>(`${resourceUrl}/${promotionId}/shippings`, groups, { observe: 'response' });
    }
    create(promotionShipping: PromotionShipping): Observable<EntityResponseType> {
        const copy = this.convert(promotionShipping);
        return this.http.post<PromotionShipping>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(promotionShipping: PromotionShipping): Observable<EntityResponseType> {
        const copy = this.convert(promotionShipping);
        return this.http.put<PromotionShipping>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<PromotionShipping>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<PromotionShipping[]>> {
        const options = createRequestOption(req);
        return this.http.get<PromotionShipping[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<PromotionShipping[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<PromotionShipping[]>> {
        const options = createRequestOption(req);
        return this.http.get<PromotionShipping[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<PromotionShipping[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: PromotionShipping = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<PromotionShipping[]>): HttpResponse<PromotionShipping[]> {
        const jsonResponse: PromotionShipping[] = res.body;
        const body: PromotionShipping[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to PromotionShipping.
     */
    private convertItemFromServer(promotionShipping: PromotionShipping): PromotionShipping {
        const copy: PromotionShipping = Object.assign({}, promotionShipping);
        copy.dateTimeShipping = this.dateUtils
            .convertDateTimeFromServer(promotionShipping.dateTimeShipping);
        return copy;
    }

    /**
     * Convert a PromotionShipping to a JSON which can be sent to the server.
     */
    private convert(promotionShipping: PromotionShipping): PromotionShipping {
        const copy: PromotionShipping = Object.assign({}, promotionShipping);

        copy.dateTimeShipping = this.dateUtils.toDate(promotionShipping.dateTimeShipping);
        return copy;
    }
}
