import { BaseEntity } from './../../shared';
import { Promotion } from '../promotion';
import { ClientGroup } from '../client-group';

export class PromotionShipping implements BaseEntity {
    constructor(
        public id?: number,
        public promotion?: Promotion,
        public dateTimeShipping?: any,
        public group?: ClientGroup,
        public promotionId?: number,
        public groupId?: number,
    ) {
    }
}
