import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PromotionShipping } from './promotion-shipping.model';
import { PromotionShippingPopupService } from './promotion-shipping-popup.service';
import { PromotionShippingService } from './promotion-shipping.service';

@Component({
    selector: 'jhi-promotion-shipping-delete-dialog',
    templateUrl: './promotion-shipping-delete-dialog.component.html'
})
export class PromotionShippingDeleteDialogComponent {

    promotionShipping: PromotionShipping;

    constructor(
        private promotionShippingService: PromotionShippingService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.promotionShippingService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'promotionShippingListModification',
                content: 'Deleted an promotionShipping'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-promotion-shipping-delete-popup',
    template: ''
})
export class PromotionShippingDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private promotionShippingPopupService: PromotionShippingPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.promotionShippingPopupService
                .open(PromotionShippingDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
