import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PromotionShipping } from './promotion-shipping.model';
import { PromotionShippingPopupService } from './promotion-shipping-popup.service';
import { PromotionShippingService } from './promotion-shipping.service';
import { Promotion, PromotionService } from '../promotion';
import { ClientGroup, ClientGroupService } from '../client-group';

@Component({
    selector: 'jhi-promotion-shipping-dialog',
    templateUrl: './promotion-shipping-dialog.component.html'
})
export class PromotionShippingDialogComponent implements OnInit {

    promotionShipping: PromotionShipping;
    isSaving: boolean;

    promotions: Promotion[];

    clientgroups: ClientGroup[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private promotionShippingService: PromotionShippingService,
        private promotionService: PromotionService,
        private clientGroupService: ClientGroupService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.promotionService.query()
            .subscribe((res: HttpResponse<Promotion[]>) => { this.promotions = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.clientGroupService.query()
            .subscribe((res: HttpResponse<ClientGroup[]>) => { this.clientgroups = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.promotionShipping.id !== undefined) {
            this.subscribeToSaveResponse(
                this.promotionShippingService.update(this.promotionShipping));
        } else {
            this.subscribeToSaveResponse(
                this.promotionShippingService.create(this.promotionShipping));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<PromotionShipping>>) {
        result.subscribe((res: HttpResponse<PromotionShipping>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: PromotionShipping) {
        this.eventManager.broadcast({ name: 'promotionShippingListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackPromotionById(index: number, item: Promotion) {
        return item.id;
    }

    trackClientGroupById(index: number, item: ClientGroup) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-promotion-shipping-popup',
    template: ''
})
export class PromotionShippingPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private promotionShippingPopupService: PromotionShippingPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.promotionShippingPopupService
                    .open(PromotionShippingDialogComponent as Component, params['id']);
            } else {
                this.promotionShippingPopupService
                    .open(PromotionShippingDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
