import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { StayOrder } from './stay-order.model';
import { StayOrderService } from './stay-order.service';

@Injectable()
export class StayOrderPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private stayOrderService: StayOrderService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.stayOrderService.find(id)
                    .subscribe((stayOrderResponse: HttpResponse<StayOrder>) => {
                        const stayOrder: StayOrder = stayOrderResponse.body;
                        stayOrder.released = this.datePipe
                            .transform(stayOrder.released, 'yyyy-MM-ddTHH:mm:ss');
                        this.ngbModalRef = this.stayOrderModalRef(component, stayOrder);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.stayOrderModalRef(component, new StayOrder());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    stayOrderModalRef(component: Component, stayOrder: StayOrder): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.stayOrder = stayOrder;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
