import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { StayOrder } from './stay-order.model';
import { StayOrderService } from './stay-order.service';

@Component({
    selector: 'jhi-stay-order-detail',
    templateUrl: './stay-order-detail.component.html'
})
export class StayOrderDetailComponent implements OnInit, OnDestroy {

    stayOrder: StayOrder;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private stayOrderService: StayOrderService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInStayOrders();
    }

    load(id) {
        this.stayOrderService.find(id)
            .subscribe((stayOrderResponse: HttpResponse<StayOrder>) => {
                this.stayOrder = stayOrderResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInStayOrders() {
        this.eventSubscriber = this.eventManager.subscribe(
            'stayOrderListModification',
            (response) => this.load(this.stayOrder.id)
        );
    }
}
