import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { StayOrder } from './stay-order.model';
import { StayOrderPopupService } from './stay-order-popup.service';
import { StayOrderService } from './stay-order.service';
import { Stay, StayService } from '../stay';
import { Menu, MenuService } from '../menu';

@Component({
    selector: 'jhi-stay-order-dialog',
    templateUrl: './stay-order-dialog.component.html'
})
export class StayOrderDialogComponent implements OnInit {

    stayOrder: StayOrder;
    isSaving: boolean;

    stays: Stay[];

    menus: Menu[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private stayOrderService: StayOrderService,
        private stayService: StayService,
        private menuService: MenuService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.stayService.query()
            .subscribe((res: HttpResponse<Stay[]>) => { this.stays = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.menuService.query()
            .subscribe((res: HttpResponse<Menu[]>) => { this.menus = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.stayOrder.id !== undefined) {
            this.subscribeToSaveResponse(
                this.stayOrderService.update(this.stayOrder));
        } else {
            this.subscribeToSaveResponse(
                this.stayOrderService.create(this.stayOrder));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<StayOrder>>) {
        result.subscribe((res: HttpResponse<StayOrder>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: StayOrder) {
        this.eventManager.broadcast({ name: 'stayOrderListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackStayById(index: number, item: Stay) {
        return item.id;
    }

    trackMenuById(index: number, item: Menu) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-stay-order-popup',
    template: ''
})
export class StayOrderPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private stayOrderPopupService: StayOrderPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.stayOrderPopupService
                    .open(StayOrderDialogComponent as Component, params['id']);
            } else {
                this.stayOrderPopupService
                    .open(StayOrderDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
