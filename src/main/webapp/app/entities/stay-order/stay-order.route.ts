import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { StayOrderComponent } from './stay-order.component';
import { StayOrderDetailComponent } from './stay-order-detail.component';
import { StayOrderPopupComponent } from './stay-order-dialog.component';
import { StayOrderDeletePopupComponent } from './stay-order-delete-dialog.component';

@Injectable()
export class StayOrderResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const stayOrderRoute: Routes = [
    {
        path: 'stay-order',
        component: StayOrderComponent,
        resolve: {
            'pagingParams': StayOrderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.stayOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'stay-order/:id',
        component: StayOrderDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.stayOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const stayOrderPopupRoute: Routes = [
    {
        path: 'stay-order-new',
        component: StayOrderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.stayOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'stay-order/:id/edit',
        component: StayOrderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.stayOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'stay-order/:id/delete',
        component: StayOrderDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.stayOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
