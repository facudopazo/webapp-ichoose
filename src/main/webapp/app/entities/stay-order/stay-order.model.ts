import { BaseEntity } from './../../shared';
import { Menu } from './../menu';

export class StayOrder implements BaseEntity {
    constructor(
        public id?: number,
        public quantity?: number,
        public released?: any,
        public price?: number,
        public stayId?: number,
        public menuId?: number,
        public menu?: Menu,
        public available?: number,
    ) {
    }
}
