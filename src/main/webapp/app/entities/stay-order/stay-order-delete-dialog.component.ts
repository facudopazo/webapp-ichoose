import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { StayOrder } from './stay-order.model';
import { StayOrderPopupService } from './stay-order-popup.service';
import { StayOrderService } from './stay-order.service';

@Component({
    selector: 'jhi-stay-order-delete-dialog',
    templateUrl: './stay-order-delete-dialog.component.html'
})
export class StayOrderDeleteDialogComponent {

    stayOrder: StayOrder;

    constructor(
        private stayOrderService: StayOrderService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.stayOrderService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'stayOrderListModification',
                content: 'Deleted an stayOrder'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-stay-order-delete-popup',
    template: ''
})
export class StayOrderDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private stayOrderPopupService: StayOrderPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.stayOrderPopupService
                .open(StayOrderDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
