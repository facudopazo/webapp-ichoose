import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { StayOrder } from './stay-order.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<StayOrder>;

@Injectable()
export class StayOrderService {

    private resourceUrl =  SERVER_API_URL + 'ichooseapi/api/stay-orders';
    private resourceSearchUrl = SERVER_API_URL + 'ichooseapi/api/_search/stay-orders';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(stayOrder: StayOrder): Observable<EntityResponseType> {
        const copy = this.convert(stayOrder);
        return this.http.post<StayOrder>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(stayOrder: StayOrder): Observable<EntityResponseType> {
        const copy = this.convert(stayOrder);
        return this.http.put<StayOrder>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<StayOrder>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<StayOrder[]>> {
        const options = createRequestOption(req);
        return this.http.get<StayOrder[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<StayOrder[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<StayOrder[]>> {
        const options = createRequestOption(req);
        return this.http.get<StayOrder[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<StayOrder[]>) => this.convertArrayResponse(res));
    }

    markAsReleased(id: number): Observable<EntityResponseType> {
        return this.http.put<StayOrder>(`${this.resourceUrl}/${id}/released`, {}, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: StayOrder = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<StayOrder[]>): HttpResponse<StayOrder[]> {
        const jsonResponse: StayOrder[] = res.body;
        const body: StayOrder[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to StayOrder.
     */
    private convertItemFromServer(stayOrder: StayOrder): StayOrder {
        const copy: StayOrder = Object.assign({}, stayOrder);
        copy.released = this.dateUtils
            .convertDateTimeFromServer(stayOrder.released);
        return copy;
    }

    /**
     * Convert a StayOrder to a JSON which can be sent to the server.
     */
    private convert(stayOrder: StayOrder): StayOrder {
        const copy: StayOrder = Object.assign({}, stayOrder);

        copy.released = this.dateUtils.toDate(stayOrder.released);
        return copy;
    }
}
