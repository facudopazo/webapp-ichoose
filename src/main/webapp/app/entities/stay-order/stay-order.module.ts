import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IchooseSharedModule } from '../../shared';
import {
    StayOrderService,
    StayOrderPopupService,
    StayOrderComponent,
    StayOrderDetailComponent,
    StayOrderDialogComponent,
    StayOrderPopupComponent,
    StayOrderDeletePopupComponent,
    StayOrderDeleteDialogComponent,
    stayOrderRoute,
    stayOrderPopupRoute,
    StayOrderResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...stayOrderRoute,
    ...stayOrderPopupRoute,
];

@NgModule({
    imports: [
        IchooseSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        StayOrderComponent,
        StayOrderDetailComponent,
        StayOrderDialogComponent,
        StayOrderDeleteDialogComponent,
        StayOrderPopupComponent,
        StayOrderDeletePopupComponent,
    ],
    entryComponents: [
        StayOrderComponent,
        StayOrderDialogComponent,
        StayOrderPopupComponent,
        StayOrderDeleteDialogComponent,
        StayOrderDeletePopupComponent,
    ],
    providers: [
        StayOrderService,
        StayOrderPopupService,
        StayOrderResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IchooseStayOrderModule {}
