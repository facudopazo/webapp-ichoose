import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { MenuClassification } from './menu-classification.model';
import { MenuClassificationPopupService } from './menu-classification-popup.service';
import { MenuClassificationService } from './menu-classification.service';

@Component({
    selector: 'jhi-menu-classification-delete-dialog',
    templateUrl: './menu-classification-delete-dialog.component.html'
})
export class MenuClassificationDeleteDialogComponent {

    menuClassification: MenuClassification;

    constructor(
        private menuClassificationService: MenuClassificationService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.menuClassificationService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'menuClassificationListModification',
                content: 'Deleted an menuClassification'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-menu-classification-delete-popup',
    template: ''
})
export class MenuClassificationDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private menuClassificationPopupService: MenuClassificationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.menuClassificationPopupService
                .open(MenuClassificationDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
