import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { MenuClassificationComponent } from './menu-classification.component';
import { MenuClassificationDetailComponent } from './menu-classification-detail.component';
import { MenuClassificationPopupComponent } from './menu-classification-dialog.component';
import { MenuClassificationDeletePopupComponent } from './menu-classification-delete-dialog.component';

@Injectable()
export class MenuClassificationResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const menuClassificationRoute: Routes = [
    {
        path: 'menu-classification',
        component: MenuClassificationComponent,
        resolve: {
            'pagingParams': MenuClassificationResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.menuClassification.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'menu-classification/:id',
        component: MenuClassificationDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.menuClassification.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const menuClassificationPopupRoute: Routes = [
    {
        path: 'menu-classification-new',
        component: MenuClassificationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.menuClassification.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'menu-classification/:id/edit',
        component: MenuClassificationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.menuClassification.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'menu-classification/:id/delete',
        component: MenuClassificationDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.menuClassification.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
