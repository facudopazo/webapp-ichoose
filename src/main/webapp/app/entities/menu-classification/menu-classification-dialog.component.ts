import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { MenuClassification } from './menu-classification.model';
import { MenuClassificationPopupService } from './menu-classification-popup.service';
import { MenuClassificationService } from './menu-classification.service';
import { MenuSubclassification, MenuSubclassificationService } from '../menu-subclassification';

@Component({
    selector: 'jhi-menu-classification-dialog',
    templateUrl: './menu-classification-dialog.component.html'
})
export class MenuClassificationDialogComponent implements OnInit {

    menuClassification: MenuClassification;
    isSaving: boolean;

    menusubclassifications: MenuSubclassification[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private menuClassificationService: MenuClassificationService,
        private menuSubclassificationService: MenuSubclassificationService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.menuSubclassificationService.query()
            .subscribe((res: HttpResponse<MenuSubclassification[]>) => { this.menusubclassifications = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.menuClassification.id !== undefined) {
            this.subscribeToSaveResponse(
                this.menuClassificationService.update(this.menuClassification));
        } else {
            this.subscribeToSaveResponse(
                this.menuClassificationService.create(this.menuClassification));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<MenuClassification>>) {
        result.subscribe((res: HttpResponse<MenuClassification>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: MenuClassification) {
        this.eventManager.broadcast({ name: 'menuClassificationListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackMenuSubclassificationById(index: number, item: MenuSubclassification) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-menu-classification-popup',
    template: ''
})
export class MenuClassificationPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private menuClassificationPopupService: MenuClassificationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.menuClassificationPopupService
                    .open(MenuClassificationDialogComponent as Component, params['id']);
            } else {
                this.menuClassificationPopupService
                    .open(MenuClassificationDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
