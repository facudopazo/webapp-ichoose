import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { MenuClassification } from './menu-classification.model';
import { MenuClassificationService } from './menu-classification.service';

@Component({
    selector: 'jhi-menu-classification-detail',
    templateUrl: './menu-classification-detail.component.html'
})
export class MenuClassificationDetailComponent implements OnInit, OnDestroy {

    menuClassification: MenuClassification;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private menuClassificationService: MenuClassificationService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInMenuClassifications();
    }

    load(id) {
        this.menuClassificationService.find(id)
            .subscribe((menuClassificationResponse: HttpResponse<MenuClassification>) => {
                this.menuClassification = menuClassificationResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInMenuClassifications() {
        this.eventSubscriber = this.eventManager.subscribe(
            'menuClassificationListModification',
            (response) => this.load(this.menuClassification.id)
        );
    }
}
