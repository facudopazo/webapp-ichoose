import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IchooseSharedModule } from '../../shared';
import {
    MenuClassificationService,
    MenuClassificationPopupService,
    MenuClassificationComponent,
    MenuClassificationDetailComponent,
    MenuClassificationDialogComponent,
    MenuClassificationPopupComponent,
    MenuClassificationDeletePopupComponent,
    MenuClassificationDeleteDialogComponent,
    menuClassificationRoute,
    menuClassificationPopupRoute,
    MenuClassificationResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...menuClassificationRoute,
    ...menuClassificationPopupRoute,
];

@NgModule({
    imports: [
        IchooseSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        MenuClassificationComponent,
        MenuClassificationDetailComponent,
        MenuClassificationDialogComponent,
        MenuClassificationDeleteDialogComponent,
        MenuClassificationPopupComponent,
        MenuClassificationDeletePopupComponent,
    ],
    entryComponents: [
        MenuClassificationComponent,
        MenuClassificationDialogComponent,
        MenuClassificationPopupComponent,
        MenuClassificationDeleteDialogComponent,
        MenuClassificationDeletePopupComponent,
    ],
    providers: [
        MenuClassificationService,
        MenuClassificationPopupService,
        MenuClassificationResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IchooseMenuClassificationModule {}
