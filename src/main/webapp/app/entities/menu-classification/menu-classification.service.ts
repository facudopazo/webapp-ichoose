
import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SERVER_API_URL } from '../../app.constants';

import { MenuClassification } from './menu-classification.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<MenuClassification>;

@Injectable()
export class MenuClassificationService {

    private resourceUrl =  SERVER_API_URL + 'ichooseapi/api/menu-classifications';
    private resourceSearchUrl = SERVER_API_URL + 'ichoose/api/api/_search/menu-classifications';

    constructor(private http: HttpClient) { }

    create(menuClassification: MenuClassification): Observable<EntityResponseType> {
        const copy = this.convert(menuClassification);
        return this.http.post<MenuClassification>(this.resourceUrl, copy, { observe: 'response' }).pipe(
            map((res: EntityResponseType) => this.convertResponse(res)));
    }

    update(menuClassification: MenuClassification): Observable<EntityResponseType> {
        const copy = this.convert(menuClassification);
        return this.http.put<MenuClassification>(this.resourceUrl, copy, { observe: 'response' }).pipe(
            map((res: EntityResponseType) => this.convertResponse(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<MenuClassification>(`${this.resourceUrl}/${id}`, { observe: 'response'}).pipe(
            map((res: EntityResponseType) => this.convertResponse(res)));
    }

    query(req?: any): Observable<HttpResponse<MenuClassification[]>> {
        const options = createRequestOption(req);
        return this.http.get<MenuClassification[]>(this.resourceUrl, { params: options, observe: 'response' }).pipe(
            map((res: HttpResponse<MenuClassification[]>) => this.convertArrayResponse(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<MenuClassification[]>> {
        const options = createRequestOption(req);
        return this.http.get<MenuClassification[]>(this.resourceSearchUrl, { params: options, observe: 'response' }).pipe(
            map((res: HttpResponse<MenuClassification[]>) => this.convertArrayResponse(res)));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: MenuClassification = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<MenuClassification[]>): HttpResponse<MenuClassification[]> {
        const jsonResponse: MenuClassification[] = res.body;
        const body: MenuClassification[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to MenuClassification.
     */
    private convertItemFromServer(menuClassification: MenuClassification): MenuClassification {
        const copy: MenuClassification = Object.assign({}, menuClassification);
        return copy;
    }

    /**
     * Convert a MenuClassification to a JSON which can be sent to the server.
     */
    private convert(menuClassification: MenuClassification): MenuClassification {
        const copy: MenuClassification = Object.assign({}, menuClassification);
        return copy;
    }
}
