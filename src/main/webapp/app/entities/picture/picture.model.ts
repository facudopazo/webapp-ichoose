import { BaseEntity } from './../../shared';

export class Picture implements BaseEntity {
    constructor(
        public id?: number,
        public contentContentType?: string,
        public content?: any,
        public contentType?: string,
        public restaurantId?: number,
        public url?: string
    ) {
        this.content = '';
        this.contentType = '';
    }
}
