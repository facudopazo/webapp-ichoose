import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { PromotionItem } from './promotion-item.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<PromotionItem>;

@Injectable()
export class PromotionItemService {

    private resourceUrl =  SERVER_API_URL + 'api/promotion-items';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/promotion-items';

    constructor(private http: HttpClient) { }

    create(promotionItem: PromotionItem): Observable<EntityResponseType> {
        const copy = this.convert(promotionItem);
        return this.http.post<PromotionItem>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(promotionItem: PromotionItem): Observable<EntityResponseType> {
        const copy = this.convert(promotionItem);
        return this.http.put<PromotionItem>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<PromotionItem>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<PromotionItem[]>> {
        const options = createRequestOption(req);
        return this.http.get<PromotionItem[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<PromotionItem[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<PromotionItem[]>> {
        const options = createRequestOption(req);
        return this.http.get<PromotionItem[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<PromotionItem[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: PromotionItem = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<PromotionItem[]>): HttpResponse<PromotionItem[]> {
        const jsonResponse: PromotionItem[] = res.body;
        const body: PromotionItem[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to PromotionItem.
     */
    private convertItemFromServer(promotionItem: PromotionItem): PromotionItem {
        const copy: PromotionItem = Object.assign({}, promotionItem);
        return copy;
    }

    /**
     * Convert a PromotionItem to a JSON which can be sent to the server.
     */
    private convert(promotionItem: PromotionItem): PromotionItem {
        const copy: PromotionItem = Object.assign({}, promotionItem);
        return copy;
    }
}
