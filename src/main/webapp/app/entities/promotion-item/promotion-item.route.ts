import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { PromotionItemComponent } from './promotion-item.component';
import { PromotionItemDetailComponent } from './promotion-item-detail.component';
import { PromotionItemPopupComponent } from './promotion-item-dialog.component';
import { PromotionItemDeletePopupComponent } from './promotion-item-delete-dialog.component';

@Injectable()
export class PromotionItemResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const promotionItemRoute: Routes = [
    {
        path: 'promotion-item',
        component: PromotionItemComponent,
        resolve: {
            'pagingParams': PromotionItemResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.promotionItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'promotion-item/:id',
        component: PromotionItemDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.promotionItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const promotionItemPopupRoute: Routes = [
    {
        path: 'promotion-item-new',
        component: PromotionItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.promotionItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'promotion-item/:id/edit',
        component: PromotionItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.promotionItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'promotion-item/:id/delete',
        component: PromotionItemDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.promotionItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
