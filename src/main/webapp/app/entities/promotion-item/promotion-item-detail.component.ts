import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { PromotionItem } from './promotion-item.model';
import { PromotionItemService } from './promotion-item.service';

@Component({
    selector: 'jhi-promotion-item-detail',
    templateUrl: './promotion-item-detail.component.html'
})
export class PromotionItemDetailComponent implements OnInit, OnDestroy {

    promotionItem: PromotionItem;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private promotionItemService: PromotionItemService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPromotionItems();
    }

    load(id) {
        this.promotionItemService.find(id)
            .subscribe((promotionItemResponse: HttpResponse<PromotionItem>) => {
                this.promotionItem = promotionItemResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPromotionItems() {
        this.eventSubscriber = this.eventManager.subscribe(
            'promotionItemListModification',
            (response) => this.load(this.promotionItem.id)
        );
    }
}
