import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PromotionItem } from './promotion-item.model';
import { PromotionItemPopupService } from './promotion-item-popup.service';
import { PromotionItemService } from './promotion-item.service';
import { Promotion, PromotionService } from '../promotion';
import { Menu, MenuService } from '../menu';

@Component({
    selector: 'jhi-promotion-item-dialog',
    templateUrl: './promotion-item-dialog.component.html'
})
export class PromotionItemDialogComponent implements OnInit {

    promotionItem: PromotionItem;
    isSaving: boolean;

    promotions: Promotion[];

    menus: Menu[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private promotionItemService: PromotionItemService,
        private promotionService: PromotionService,
        private menuService: MenuService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.promotionService.query()
            .subscribe((res: HttpResponse<Promotion[]>) => { this.promotions = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.menuService.query()
            .subscribe((res: HttpResponse<Menu[]>) => { this.menus = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.promotionItem.id !== undefined) {
            this.subscribeToSaveResponse(
                this.promotionItemService.update(this.promotionItem));
        } else {
            this.subscribeToSaveResponse(
                this.promotionItemService.create(this.promotionItem));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<PromotionItem>>) {
        result.subscribe((res: HttpResponse<PromotionItem>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: PromotionItem) {
        this.eventManager.broadcast({ name: 'promotionItemListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackPromotionById(index: number, item: Promotion) {
        return item.id;
    }

    trackMenuById(index: number, item: Menu) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-promotion-item-popup',
    template: ''
})
export class PromotionItemPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private promotionItemPopupService: PromotionItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.promotionItemPopupService
                    .open(PromotionItemDialogComponent as Component, params['id']);
            } else {
                this.promotionItemPopupService
                    .open(PromotionItemDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
