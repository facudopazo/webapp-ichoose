import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IchooseSharedModule } from '../../shared';
import {
    PromotionItemService,
    PromotionItemPopupService,
    PromotionItemComponent,
    PromotionItemDetailComponent,
    PromotionItemDialogComponent,
    PromotionItemPopupComponent,
    PromotionItemDeletePopupComponent,
    PromotionItemDeleteDialogComponent,
    promotionItemRoute,
    promotionItemPopupRoute,
    PromotionItemResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...promotionItemRoute,
    ...promotionItemPopupRoute,
];

@NgModule({
    imports: [
        IchooseSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        PromotionItemComponent,
        PromotionItemDetailComponent,
        PromotionItemDialogComponent,
        PromotionItemDeleteDialogComponent,
        PromotionItemPopupComponent,
        PromotionItemDeletePopupComponent,
    ],
    entryComponents: [
        PromotionItemComponent,
        PromotionItemDialogComponent,
        PromotionItemPopupComponent,
        PromotionItemDeleteDialogComponent,
        PromotionItemDeletePopupComponent,
    ],
    providers: [
        PromotionItemService,
        PromotionItemPopupService,
        PromotionItemResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IchoosePromotionItemModule {}
