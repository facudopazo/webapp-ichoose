import { BaseEntity } from './../../shared';
import { Menu } from './../menu';

export class PromotionItem implements BaseEntity {
    constructor(
        public id?: number,
        public quantity?: number,
        public promotionId?: number,
        public menuId?: number,
        public menu?: Menu
    ) {
    }
}
