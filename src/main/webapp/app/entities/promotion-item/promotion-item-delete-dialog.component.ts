import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PromotionItem } from './promotion-item.model';
import { PromotionItemPopupService } from './promotion-item-popup.service';
import { PromotionItemService } from './promotion-item.service';

@Component({
    selector: 'jhi-promotion-item-delete-dialog',
    templateUrl: './promotion-item-delete-dialog.component.html'
})
export class PromotionItemDeleteDialogComponent {

    promotionItem: PromotionItem;

    constructor(
        private promotionItemService: PromotionItemService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.promotionItemService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'promotionItemListModification',
                content: 'Deleted an promotionItem'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-promotion-item-delete-popup',
    template: ''
})
export class PromotionItemDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private promotionItemPopupService: PromotionItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.promotionItemPopupService
                .open(PromotionItemDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
