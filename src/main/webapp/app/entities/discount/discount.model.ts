import { BaseEntity } from './../../shared';
import { Promotion } from '../promotion';
export enum DiscountType {
    PROMOTION
}

export class Discount implements BaseEntity {
    constructor(
        public id?: number,
        public description?: string,
        public amount?: number,
        public type?: DiscountType,
        public promotion?: Promotion,
    ) {

    }
}
