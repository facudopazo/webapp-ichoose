import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiEventManager } from 'ng-jhipster';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Promotion } from './promotion.model';
import { PromotionPopupService } from './promotion-popup.service';
import { PromotionService } from './promotion.service';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'jhi-promotion-finish-dialog',
    templateUrl: './promotion-finish-dialog.component.html',
    styleUrls: [
        'promotion.scss'
    ]
})
export class PromotionFinishDialogComponent {

    promotion: Promotion;
    isSaving: boolean;

    constructor(
        private promotionService: PromotionService,
        public activeModal: MatDialogRef<PromotionFinishPopupComponent>,
        private eventManager: JhiEventManager,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.promotion = data.promotion;
    }

    clear() {
        this.activeModal.close('cancel');
    }

    finishPromotion() {
        this.promotion.end = new Date();
        this.isSaving = true;
        this.subscribeToSaveResponse(
            this.promotionService.update(this.promotion));
            this.activeModal.close();
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Promotion>>) {
        result.subscribe((res: HttpResponse<Promotion>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Promotion) {
        this.eventManager.broadcast({ name: 'promotionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.close(result);
    }
    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-promotion-finish-popup',
    template: ''
})
export class PromotionFinishPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private promotionPopupService: PromotionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.promotionPopupService
                .open(PromotionFinishDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
