import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IchooseSharedModule } from '../../shared';
import { NgSelectModule } from '@ng-select/ng-select';
import {
    PromotionService,
    PromotionPopupService,
    PromotionComponent,
    PromotionDetailComponent,
    PromotionDialogComponent,
    PromotionPopupComponent,
    PromotionDeletePopupComponent,
    PromotionDeleteDialogComponent,
    PromotionFinishDialogComponent,
    PromotionFinishPopupComponent,
    PromotionReactivateDialogComponent,
    PromotionReactivatePopupComponent,
    PromotionSendPopupComponent,
    PromotionSendDialogComponent,
    promotionRoute,
    promotionPopupRoute,
    PromotionResolvePagingParams
} from './';

const ENTITY_STATES = [
    ...promotionRoute,
    ...promotionPopupRoute,
];

@NgModule({
    imports: [
        IchooseSharedModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        FormsModule,
        ReactiveFormsModule,
        NgSelectModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        PromotionComponent,
        PromotionDetailComponent,
        PromotionDialogComponent,
        PromotionDeleteDialogComponent,
        PromotionPopupComponent,
        PromotionFinishDialogComponent,
        PromotionFinishPopupComponent,
        PromotionReactivateDialogComponent,
        PromotionReactivatePopupComponent,
        PromotionSendPopupComponent,
        PromotionDeletePopupComponent,
        PromotionSendDialogComponent,
        PromotionSendPopupComponent,
    ],
    entryComponents: [
        PromotionComponent,
        PromotionDialogComponent,
        PromotionPopupComponent,
        PromotionDeleteDialogComponent,
        PromotionDeletePopupComponent,
        PromotionFinishPopupComponent,
        PromotionFinishDialogComponent,
        PromotionReactivateDialogComponent,
        PromotionReactivatePopupComponent,
        PromotionSendPopupComponent,
        PromotionSendDialogComponent,
        PromotionSendPopupComponent,
    ],
    providers: [
        PromotionService,
        PromotionPopupService,
        PromotionResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IchoosePromotionModule { }
