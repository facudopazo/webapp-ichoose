import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Promotion } from './promotion.model';
import { PromotionService } from './promotion.service';
import { MatTableDataSource } from '@angular/material';
import { PromotionItem, PromotionItemService } from './../promotion-item';

@Component({
    selector: 'jhi-promotion-detail',
    templateUrl: './promotion-detail.component.html',
    styleUrls: [
        'promotion.scss'
    ]
})
export class PromotionDetailComponent implements OnInit, OnDestroy {
    displayedColumns = ['Nombre', 'Cantidad'];

    promotion: Promotion = new Promotion;
    dataSource: any;
    amount: any;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private promotionService: PromotionService,
        private route: ActivatedRoute,
    ) {
        this.dataSource = new MatTableDataSource();
    }

    ngOnInit() {
         this.subscription = this.route.params.subscribe((params) => {
             this.load(params['id']);
         });
        this.registerChangeInPromotions();
    }

    calculatePromotionItemAmount(promotionItem: PromotionItem) {
        return promotionItem.menu.price * promotionItem.quantity;
       }
    reCalculatePromotionItemAmount(promotionAux: Promotion) {
        let total = 0;
        this.promotion.items.forEach( (promotion) => {
            total += this.calculatePromotionItemAmount(promotion);
        });
        this.amount = total;
    }

    load(id) {
        this.promotionService.find(id)
            .subscribe((promotionResponse: HttpResponse<Promotion>) => {
                this.promotion = promotionResponse.body;
                this.reCalculatePromotionItemAmount(this.promotion);
                this.dataSource.data = this.promotion.items;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPromotions() {
        this.eventSubscriber = this.eventManager.subscribe(
            'promotionListModification',
            (response) => this.load(this.promotion.id)
        );
    }
}
