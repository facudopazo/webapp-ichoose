import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { Promotion } from './promotion.model';
import { PromotionItem } from '../promotion-item';
import { PromotionPopupService } from './promotion-popup.service';
import { PromotionService } from './promotion.service';
import { Restaurant, RestaurantService } from '../restaurant';
import { PromotionShipping, PromotionShippingService } from '../promotion-shipping';
import { ClientGroup, ClientGroupService } from '../client-group';
import { Menu, MenuService } from '../menu';
import { MatTableDataSource } from '@angular/material';
import { map, startWith } from 'rxjs/operators';
import { truncate } from 'fs';
import { MatSnackBar } from '@angular/material';

@Component({
    selector: 'jhi-promotion-dialog',
    templateUrl: './promotion-send-dialog.component.html',
    styleUrls: [
        'promotion.scss'
    ]
})
export class PromotionSendDialogComponent implements OnInit {
    displayedColumns = [ 'Enviar', 'NombreGrupo'];
    displayedSends = [ 'NombreGrupo' , 'Estado'];

    promotion: Promotion;
    isSaving: boolean;
    dateCtrl: FormControl;
    menuCtrl: FormControl;
    nameCtrl: FormControl;
    finalPriceCtrl: FormControl;
    restaurants: Restaurant[];
    dataSource: any;
    dataSend: any;
    amount: number;
    currentRestaurant: Restaurant;
    count: any;
    clientgroups: any[];
    filteredMenus: Observable<any[]>;
    menu: Menu[];
    auxGroup: any[];
    closedGroupArray: any;
    selectedMenu: Menu;
    totalPrice: any;

    constructor(
        private jhiAlertService: JhiAlertService,
        private promotionService: PromotionService,
        private restaurantService: RestaurantService,
        private clientGroupService: ClientGroupService,
        private menuService:  MenuService,
        public activeModal: MatDialogRef<PromotionSendPopupComponent>,
        private eventManager: JhiEventManager,
        public snackBar: MatSnackBar,
        public promotionShippingService: PromotionShippingService,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.promotion = data.promotion;
        this.dataSource = new MatTableDataSource();
        this.dataSend = new MatTableDataSource;
         this.dataSource.filterPredicate = (data1, filter) => {
             const dataStr = data.group.name.trim() + data1.group.name.toLowerCase() ;
             return dataStr.indexOf(filter) !== -1;
            };
            this.dataSend.filterPredicate = (data2, filter) => {
                const dataStr = data.group.name.trim() + data2.group.name.toLowerCase() ;
                return dataStr.indexOf(filter) !== -1;
               };
    }

    ngOnInit() {
        this.isSaving = false;
        this.currentRestaurant = new Restaurant;
        this.restaurantService.getCurrent().subscribe((restaurant: Restaurant) => {
            this.currentRestaurant = restaurant;
            if (this.promotion.open) {
                this.clientGroupService.queryByRestaurant(this.currentRestaurant.id, {
                    page: 0,
                    size: 1000
                })
                .subscribe((res: HttpResponse<ClientGroup[]>) => {
                    this.clientgroups = res.body
                    .filter( (item: ClientGroup) =>
                        !this.promotion.shippings.some( (item2: PromotionShipping) => item2.group.id === item.id)
                        )
                    .map( (group: ClientGroup) => {
                            // tslint:disable-next-line:object-literal-shorthand
                            return { checked: false, group: group };
                            });
                        this.dataSource.data = this.clientgroups;
                        }, (res: HttpErrorResponse) => this.onError(res.message));
            } else {
                this.clientgroups = this.promotion.groups
                    .filter( (item: ClientGroup) =>
                        !this.promotion.shippings.some( (item2: PromotionShipping) => item2.group.id === item.id)
                    )
                    .map( (group: ClientGroup) => {
                            // tslint:disable-next-line:object-literal-shorthand
                            return { checked: false, group: group };
                        }
                    );
                this.dataSource.data = this.clientgroups;
            }

            this.dataSend.data = this.promotion.shippings;
                this.menuService.query()
                .subscribe((res: HttpResponse<Menu[]>) => { this.menu = res.body;
                    this.menu = res.body;
                });
            }
        );
        }

    clear() {
        this.activeModal.close('cancel');
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue;
        this.dataSend.filter = filterValue;
    }

    send() {
        const groupArray = this.clientgroups.filter( (x) => x.checked === true).map( (group) => {
            return group.group.id;
        });
        this.promotionShippingService.send(this.promotion.id, groupArray).subscribe(
            (res) => {
                const successfulShippingMessage = document.getElementById('successfulShippingMessage').textContent;
                this.snackBar.open(successfulShippingMessage, 'OK', {duration: 3000});
                this.activeModal.close('close');
            },
            (err) => {
                const failedShippingMessage = document.getElementById('failedShippingMessage').textContent;
                this.snackBar.open(failedShippingMessage, 'OK', {duration: 3000});
            });
    }

    markChecked() {
         this.count = this.clientgroups.filter( (x) => x.checked === true).length;
            if (this.count > 9) {
            const maxTen = document.getElementById('maxTen').textContent;
            this.snackBar.open(maxTen, 'Ok', {
                duration: 3000
              });
            }
        }

        private subscribeToSaveResponse(result: Observable<HttpResponse<Promotion>>) {
            result.subscribe((res: HttpResponse<Promotion>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Promotion) {
        this.eventManager.broadcast({ name: 'promotionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.close(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackRestaurantById(index: number, item: Restaurant) {
        return item.id;
    }

    trackClientGroupById(index: number, item: ClientGroup) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-promotion-send-popup',
    template: ''
})
export class PromotionSendPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private promotionPopupService: PromotionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.promotionPopupService
                    .open(PromotionSendDialogComponent as Component, params['id']);
            } else {
                this.promotionPopupService
                    .open(PromotionSendDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
