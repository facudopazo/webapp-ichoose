import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { Promotion } from './promotion.model';
import { PromotionItem } from '../promotion-item';
import { PromotionPopupService } from './promotion-popup.service';
import { PromotionService } from './promotion.service';
import { Restaurant, RestaurantService } from '../restaurant';
import { ClientGroup, ClientGroupService } from '../client-group';
import { Menu, MenuService } from '../menu';
import { MatTableDataSource } from '@angular/material';
import { map, startWith } from 'rxjs/operators';

@Component({
    selector: 'jhi-promotion-dialog',
    templateUrl: './promotion-dialog.component.html',
    styleUrls: [
        'promotion.scss'
    ]
})
export class PromotionDialogComponent implements OnInit {
    displayedColumns = ['Nombre', 'Cantidad', 'Precio unitario', 'Precio total', 'Eliminar'];

    promotion: Promotion;
    isSaving: boolean;
    dateCtrl: FormControl;
    menuCtrl: FormControl;
    nameCtrl: FormControl;
    finalPriceCtrl: FormControl;
    restaurants: Restaurant[];
    dataSource: any;
    quantity = 1;
    amount: number;
    currentRestaurant: Restaurant;

    clientgroups: ClientGroup[];
    filteredMenus: Observable<any[]>;
    menu: Menu[];
    selectedMenu: Menu;
    totalPrice: any;

    constructor(
        private jhiAlertService: JhiAlertService,
        private promotionService: PromotionService,
        private restaurantService: RestaurantService,
        private clientGroupService: ClientGroupService,
        private menuService:  MenuService,
        public activeModal: MatDialogRef<PromotionPopupComponent>,
        private eventManager: JhiEventManager,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.promotion = data.promotion;
        this.dataSource = new MatTableDataSource();
    }

    ngOnInit() {
        this.dateCtrl = new FormControl('', [
            Validators.required
          ]);
          this.menuCtrl = new FormControl('', [
            Validators.required
          ]);
          this.nameCtrl = new FormControl('', [
            Validators.required
          ]);
          this.finalPriceCtrl = new FormControl('', [
            Validators.required
          ]);
          this.restaurantService.getCurrent().subscribe((restaurant: Restaurant) => {
            this.currentRestaurant = restaurant;
              this.isSaving = false;
              this.restaurantService.query()
                  .subscribe((res: HttpResponse<Restaurant[]>) => { this.restaurants = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
              this.clientGroupService.queryByRestaurant(this.currentRestaurant.id)
                  .subscribe((res: HttpResponse<ClientGroup[]>) => { this.clientgroups = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
              this.menuService.queryByRestaurant(this.currentRestaurant.id)
                  .subscribe((res: HttpResponse<Menu[]>) => { this.menu = res.body;
                      this.menu = res.body;
                      this.filteredMenus = this.menuCtrl.valueChanges
                          .pipe(
                              startWith<string | Menu>(''),
                              map( (value) => typeof value === 'string' ? value : value.name),
                              map( (menu) => menu ? this.filtermenus(menu) : this.menu.slice())
                          );
                  }, (res: HttpErrorResponse) => this.onError(res.message));
          });
        this.dataSource.data = this.promotion.items;
        this.reCalculatePromotionItemAmount(this.promotion);
    }
    filtermenus(name: string) {
        return this.menu.filter((menu) =>
            menu.name.toLowerCase().indexOf(name.toLowerCase()) === 0);
        }
        displayFn(user?: Menu): string | undefined {
            return user ? user.name : undefined;
        }
        clear() {
        this.activeModal.close('cancel');
    }

    addMenuList() {
        const resultSearch: PromotionItem = this.promotion.items.find( (x: PromotionItem) => this.selectedMenu.id === x.menu.id);
        if (resultSearch !== undefined) {
            resultSearch.quantity = this.quantity;
        } else {
            const promotionItemArray: PromotionItem = new PromotionItem();
            promotionItemArray.quantity = this.quantity;
            promotionItemArray.menu = this.selectedMenu;
            this.promotion.items.push(promotionItemArray);
        }
        this.reCalculatePromotionItemAmount(this.promotion);
        this.dataSource.data = this.promotion.items;
        this.selectedMenu = new Menu;
        this.quantity = 1;
    }

    removeMenulist(promotionItem: PromotionItem) {
             const removedItem = this.promotion.items.indexOf(promotionItem);
             this.promotion.items.splice(removedItem, 1);
             this.reCalculatePromotionItemAmount(this.promotion);
             console.log(this.promotion.items);
             this.dataSource.data = this.promotion.items;
            }

    save() {
        this.isSaving = true;
        this.promotion.restaurantId = this.currentRestaurant.id;
        console.log(this.promotion.items);
        if (this.promotion.id !== undefined) {
            this.subscribeToSaveResponse(
                this.promotionService.update(this.promotion));
        } else {
            this.subscribeToSaveResponse(
                this.promotionService.create(this.promotion));
        }
    }
     calculatePromotionItemAmount(promotionItem: PromotionItem) {
         return promotionItem.menu.price * promotionItem.quantity;
        }
    reCalculatePromotionItemAmount(promotionAux: Promotion) {
        let total = 0;
        this.promotion.items.forEach( (promotion) => {
            total += this.calculatePromotionItemAmount(promotion);
        });
        this.amount = total;
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Promotion>>) {
        result.subscribe((res: HttpResponse<Promotion>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Promotion) {
        this.eventManager.broadcast({ name: 'promotionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.close(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackRestaurantById(index: number, item: Restaurant) {
        return item.id;
    }

    trackClientGroupById(index: number, item: ClientGroup) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-promotion-popup',
    template: ''
})
export class PromotionPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private promotionPopupService: PromotionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.promotionPopupService
                    .open(PromotionDialogComponent as Component, params['id']);
            } else {
                this.promotionPopupService
                    .open(PromotionDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
