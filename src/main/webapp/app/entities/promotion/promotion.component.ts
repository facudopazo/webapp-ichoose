import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';
import { MatTableDataSource } from '@angular/material';
import { Promotion } from './promotion.model';
import { PromotionService } from './promotion.service';
import { ITEMS_PER_PAGE, Principal } from '../../shared';
import { RestaurantService, Restaurant } from '../restaurant';

@Component({
    selector: 'jhi-promotion',
    templateUrl: './promotion.component.html',
    styleUrls: [
        'promotion.scss'
    ]
})
export class PromotionComponent implements OnInit, OnDestroy {
    displayedColumns = ['Nombre', 'Destinatarios', 'Estado', 'Opciones'];

currentAccount: any;
    promotions: Promotion[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    dataSource: any;
    currentRestaurant: Restaurant;

    constructor(
        private promotionService: PromotionService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private restaurantService: RestaurantService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.dataSource = new MatTableDataSource();
            this.previousPage = data.pagingParams.page;
            this.reverse = false;
            this.predicate = 'init';
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.promotionService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: HttpResponse<Promotion[]>) => this.onSuccess(res.body, res.headers),
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            return;
        }
        this.promotionService.queryByRestaurant(this.currentRestaurant.id, {
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
                (res: HttpResponse<Promotion[]>) => this.onSuccess(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/promotion'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    statusDate(promotion: Promotion) {
        const currentDate = new Date;
        if (promotion.init > currentDate) {
            return 'futura';
        }
        if (promotion.end < currentDate && promotion.end != null) {
            return 'finalizada';
        }
        if (promotion.init < currentDate && (promotion.end === null || promotion.end > currentDate) ) {
            return 'activa';
        }
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/promotion', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/promotion', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.restaurantService.getCurrent().subscribe( (restaurant: Restaurant) => { this.currentRestaurant = restaurant; this.loadAll(); });
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPromotions();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Promotion) {
        return item.id;
    }
    registerChangeInPromotions() {
        this.eventSubscriber = this.eventManager.subscribe('promotionListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.promotions = data;
        this.dataSource.data = this.promotions;
    }
    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
