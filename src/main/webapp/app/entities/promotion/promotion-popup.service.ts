import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { Promotion } from './promotion.model';
import { PromotionService } from './promotion.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
@Injectable()
export class PromotionPopupService {
    private ngbModalRef: MatDialogRef<Component>;

    constructor(
        private datePipe: DatePipe,
        private modalService: MatDialog,
        private router: Router,
        private promotionService: PromotionService

    ) {
        this.ngbModalRef = null;
    }
    open(component: Component, id?: number | any): Promise<MatDialogRef<Component>> {
            return new Promise<MatDialogRef<Component>>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.promotionService.find(id)
                    .subscribe((promotionResponse: HttpResponse<Promotion>) => {
                        const promotion: Promotion = promotionResponse.body;
                        this.ngbModalRef = this.promotionModalRef(component, promotion);
                        resolve(this.ngbModalRef);
                    });
            } else {
                setTimeout(() => {
                    this.ngbModalRef = this.promotionModalRef(component, new Promotion());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

            promotionModalRef(component: any, promotion: Promotion): MatDialogRef<Component> {
                    const modalRef = this.modalService.open(component, {height: 'auto',
                    width: '600px', data: {'promotion': promotion}});
                    modalRef.afterClosed().subscribe((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
