import { BaseEntity } from './../../shared';
import { PromotionItem } from './../promotion-item';
import { ClientGroup } from './../client-group';
import { PromotionShipping } from './../promotion-shipping';

export class Promotion implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public init?: any,
        public end?: any,
        public finalPrice?: number,
        public open?: boolean,
        public items?: BaseEntity[],
        public restaurantId?: number,
        public groups?: BaseEntity[],
        public shippings?: PromotionShipping[]
    ) {
        this.open = true;
        this.items = new Array<PromotionItem>();
        this.groups = new Array<ClientGroup>();
    }
}
