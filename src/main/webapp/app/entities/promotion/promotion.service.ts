import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Promotion } from './promotion.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Promotion>;

@Injectable()
export class PromotionService {

    private resourceUrl =  SERVER_API_URL + 'ichooseapi/api/promotions';
    private resourceSearchUrl = SERVER_API_URL + 'ichooseapi/api/_search/promotions';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(promotion: Promotion): Observable<EntityResponseType> {
        const copy = this.convert(promotion);
        return this.http.post<Promotion>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(promotion: Promotion): Observable<EntityResponseType> {
        const copy = this.convert(promotion);
        return this.http.put<Promotion>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Promotion>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Promotion[]>> {
        const options = createRequestOption(req);
        return this.http.get<Promotion[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Promotion[]>) => this.convertArrayResponse(res));
    }

    findActivePromotionsForClient(restaurantId: any, req?: any): Observable<HttpResponse<Promotion[]>> {
        const promotions = createRequestOption(req);
        const url = SERVER_API_URL + `ichooseapi/api/restaurants/${restaurantId}/promotions`;
        return this.http.get<Promotion[]>(this.resourceUrl, { params: promotions, observe: 'response' })
        .map((res: HttpResponse<Promotion[]>) => this.convertArrayResponse(res));
    }
    queryByRestaurant(restaurantId: number, req?: any): Observable<HttpResponse<Promotion[]>> {
        const options = createRequestOption(req);
        const url = SERVER_API_URL + 'ichooseapi/api/restaurants/' + restaurantId + '/promotions';
        return this.http.get<Promotion[]>(url, { params: options, observe: 'response' })
            .map((res: HttpResponse<Promotion[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<Promotion[]>> {
        const options = createRequestOption(req);
        return this.http.get<Promotion[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Promotion[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Promotion = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Promotion[]>): HttpResponse<Promotion[]> {
        const jsonResponse: Promotion[] = res.body;
        const body: Promotion[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Promotion.
     */
    private convertItemFromServer(promotion: Promotion): Promotion {
        const copy: Promotion = Object.assign({}, promotion);
        copy.init = this.dateUtils
            .convertDateTimeFromServer(promotion.init);
        copy.end = this.dateUtils
            .convertDateTimeFromServer(promotion.end);
        return copy;
    }

    /**
     * Convert a Promotion to a JSON which can be sent to the server.
     */
    private convert(promotion: Promotion): Promotion {
        const copy: Promotion = Object.assign({}, promotion);

        // copy.init = this.dateUtils.toDate(promotion.init);

        // copy.end = this.dateUtils.toDate(promotion.end);
        return copy;
    }
}
