
import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Booking } from './booking.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Booking>;

@Injectable()
export class BookingService {

    private resourceUrl =  SERVER_API_URL + 'ichooseapi/api/bookings';
    private resourceSearchUrl = SERVER_API_URL + 'ichooseapi/api/_search/bookings';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(booking: Booking): Observable<EntityResponseType> {
        const copy = this.convert(booking);
        return this.http.post<Booking>(this.resourceUrl, copy, { observe: 'response' }).pipe(
            map((res: EntityResponseType) => this.convertResponse(res)));
    }

    update(booking: Booking): Observable<EntityResponseType> {
        const copy = this.convert(booking);
        return this.http.put<Booking>(this.resourceUrl, copy, { observe: 'response' }).pipe(
            map((res: EntityResponseType) => this.convertResponse(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Booking>(`${this.resourceUrl}/${id}`, { observe: 'response'}).pipe(
            map((res: EntityResponseType) => this.convertResponse(res)));
    }

    query(req?: any): Observable<HttpResponse<Booking[]>> {
        const options = createRequestOption(req);
        return this.http.get<Booking[]>(this.resourceUrl, { params: options, observe: 'response' }).pipe(
            map((res: HttpResponse<Booking[]>) => this.convertArrayResponse(res)));
    }

    findByRestaurantId(restaurantId: any, req?: any ): Observable<HttpResponse<Booking[]>> {
        const options = createRequestOption(req);
        const resourceUrl =  `${SERVER_API_URL}ichooseapi/api/restaurants/${restaurantId}/bookings`;
        return this.http.get<Booking[]>(resourceUrl, {params: options, observe: 'response' })
            .map((res: HttpResponse<Booking[]>) => this.convertArrayResponse(res));
    }
    accept(bookingId: any): Observable<EntityResponseType> {
        const resourceUrl = `${SERVER_API_URL}ichooseapi/api/bookings/${bookingId}/accept`;
        return this.http.put<Booking>(resourceUrl, {}, { observe: 'response' }).pipe(
            map((res: EntityResponseType) => this.convertResponse(res)));
    }

    reject(bookingId: any): Observable<EntityResponseType> {
        const resourceUrl =  `${SERVER_API_URL}ichooseapi/api/bookings/${bookingId}/reject`;
        return this.http.put<Booking>(resourceUrl, {}, { observe: 'response' }).pipe(
            map((res: EntityResponseType) => this.convertResponse(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<Booking[]>> {
        const options = createRequestOption(req);
        return this.http.get<Booking[]>(this.resourceSearchUrl, { params: options, observe: 'response' }).pipe(
            map((res: HttpResponse<Booking[]>) => this.convertArrayResponse(res)));
    }

    findCurrentBookingsByRestaurantId(restaurantId: any, req?: any): Observable<HttpResponse<Booking[]>> {
        const options = createRequestOption(req);
        const resourceUrl =  `${SERVER_API_URL}ichooseapi/api/restaurants/${restaurantId}/bookings/current`;
        return this.http.get<Booking[]>(resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Booking[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Booking = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Booking[]>): HttpResponse<Booking[]> {
        const jsonResponse: Booking[] = res.body;
        const body: Booking[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Booking.
     */
    private convertItemFromServer(booking: Booking): Booking {
        const copy: Booking = Object.assign({}, booking);
        copy.dateTime = this.dateUtils
        .convertDateTimeFromServer(booking.dateTime);
        return copy;
    }

    /**
     * Convert a Booking to a JSON which can be sent to the server.
     */
    private convert(booking: Booking): Booking {
        const copy: Booking = Object.assign({}, booking);

        // copy.dateTime = this.dateUtils.toDate(booking.dateTime);
        return copy;
    }
}
