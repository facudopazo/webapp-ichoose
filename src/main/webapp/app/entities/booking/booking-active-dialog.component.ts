import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { JhiEventManager } from 'ng-jhipster';
import { Observable } from 'rxjs/Observable';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar } from '@angular/material';
import { Booking } from './booking.model';
import { BookingPopupService } from './booking-popup.service';
import { BookingService } from './booking.service';

@Component({
    selector: 'jhi-booking-active-dialog',
    templateUrl: './booking-active-dialog.component.html',
    styleUrls: [
        'booking.scss'
    ]
})
export class BookingActiveDialogComponent implements OnInit {

    booking: Booking;
    newStatus: string;
    isSaving: boolean;

    constructor(
        private bookingService: BookingService,
        public activeModal: MatDialogRef<BookingActivePopupComponent>,
        private eventManager: JhiEventManager,
        private route: ActivatedRoute,
        public snackBar: MatSnackBar,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.booking = data.booking;
    }

    ngOnInit() {
        this.route.queryParams
           .subscribe( (params) => {
               if (params.status !== undefined) {
                   this.newStatus = params.status;
                   console.log(this.newStatus);
               }
           }
       );
    }

    clear() {
        this.activeModal.close('cancel');
    }

    activeBooking() {
        this.isSaving = true;
        this.subscribeToSaveResponse(
            this.bookingService.accept(this.booking.id));
    }
    private subscribeToSaveResponse(result: Observable<HttpResponse<Booking>>) {
        this.eventManager.broadcast({ name: 'bookingListModification', content: 'OK'});
        result.subscribe((res: HttpResponse<Booking>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Booking) {
        this.eventManager.broadcast({ name: 'bookingListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.close(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-booking-status-popup',
    template: ''
})
export class BookingActivePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private bookingPopupService: BookingPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.bookingPopupService
                .open(BookingActiveDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
