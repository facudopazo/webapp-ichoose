import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormControl, ValidatorFn } from '@angular/forms';
import {FormBuilder, FormGroup, Validators, ReactiveFormsModule} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { FormGroupDirective, NgForm, AbstractControl } from '@angular/forms';
import {map, startWith} from 'rxjs/operators';

import { Booking } from './booking.model';
import { BookingPopupService } from './booking-popup.service';
import { BookingService } from './booking.service';
import { ClientTable, ClientTableService } from '../client-table';
import { PromiseObservable } from 'rxjs/observable/PromiseObservable';
import { RestaurantService, Restaurant } from '../restaurant';

@Component({
    selector: 'jhi-booking-dialog',
    templateUrl: './booking-dialog.component.html'
})
export class BookingDialogComponent implements OnInit {

    tableCtrl: FormControl;
    filteredTables: Observable<any[]>;

    currentRestaurant: Restaurant;
    booking: Booking;
    isSaving: boolean;
    dateCtrl: FormControl;
    peopleCtrl: FormControl;
    nameCtrl: FormControl;
    clienNumbCtrl: FormControl;
    matcher = new MyErrorStateMatcher();
    clienttables: ClientTable[];
    newIdTable: number;
    constructor(
        private restaurantService: RestaurantService,
        private jhiAlertService: JhiAlertService,
        private bookingService: BookingService,
        private clientTableService: ClientTableService,
        public activeModal: MatDialogRef<BookingPopupComponent>,
        private eventManager: JhiEventManager,
        private route: ActivatedRoute,
        @Inject(MAT_DIALOG_DATA) public data: any,
    ) {
        this.booking = data.booking;
        this.clienttables  = new Array <ClientTable>();
        this.tableCtrl = new FormControl();
    }

    ngOnInit() {
        this.isSaving = false;
        this.restaurantService.getCurrent().subscribe((restaurant: Restaurant) => {
                this.currentRestaurant = restaurant;
                this.clientTableService.findByRestaurantId(this.currentRestaurant.id, {page: 0, size: 10000})
                .subscribe((res: HttpResponse<ClientTable[]>) => { this.clienttables = res.body;
                    this.filteredTables = this.tableCtrl.valueChanges
                    .pipe(
                      startWith<string | ClientTable>(''),
                      map( (value) => typeof value === 'string' ? value : value.number),
                      map( (table) => table ? this.filtertables(table) : this.clienttables.slice())
                    );
                    if (this.booking.tableId != null) {
                        this.booking.table = this.clienttables.find((table) => table.id === this.booking.tableId);
                    }
                }, (res: HttpErrorResponse) => this.onError(res.message));
            }

        );

        this.dateCtrl = new FormControl('', [
            Validators.required
        ]);
        this.peopleCtrl = new FormControl('', [
            Validators.required
        ]);
        this.nameCtrl = new FormControl('', [
            Validators.required,
        ]);
        this.clienNumbCtrl = new FormControl('', [
            Validators.required
        ]);
        this.route.queryParams
           .subscribe( (params) => {
               if (params.tableId !== undefined) {
                this.booking.tableId = params.tableId;
               }
           }
       );
    }
    filtertables(name: string) {
         return this.clienttables.filter((table) =>
           table.number.toLowerCase().indexOf(name.toLowerCase()) === 0);
       }
       displayFn(user?: ClientTable): string | undefined {
        return user ? user.number : undefined;
      }
      clear() {
        this.activeModal.close('cancel');
    }

    save() {
        this.isSaving = true;
        this.booking['restaurantId'] = this.currentRestaurant ? this.currentRestaurant.id : null;
        // Ver esta linea
        this.booking.tableId = this.booking.table.id;
        if (this.booking.id !== undefined) {
            this.subscribeToSaveResponse(
                this.bookingService.update(this.booking));
        } else {
            this.subscribeToSaveResponse(
                this.bookingService.create(this.booking));
            }
        }
    private subscribeToSaveResponse(result: Observable<HttpResponse<Booking>>) {
        result.subscribe((res: HttpResponse<Booking>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Booking) {
        this.eventManager.broadcast({ name: 'bookingListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.close(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackClientTableById(index: number, item: ClientTable) {
        return item.id;
    }
}
@Component({
    selector: 'jhi-booking-popup',
    template: ''
})
export class BookingPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private bookingPopupService: BookingPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.bookingPopupService
                    .open(BookingDialogComponent as Component, params['id']);
            } else {
                this.bookingPopupService
                    .open(BookingDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
      const isSubmitted = form && form.submitted;
      return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}
