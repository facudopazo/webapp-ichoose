import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { IchooseSharedModule } from '../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import {
    BookingService,
    BookingPopupService,
    BookingComponent,
    BookingDetailComponent,
    BookingDialogComponent,
    BookingPopupComponent,
    BookingDeletePopupComponent,
    BookingDeleteDialogComponent,
    BookingStatusDialogComponent,
    BookingStatusPopupComponent,
    BookingDetailPopupComponent,
    BookingRejectedDialogComponent,
    BookingRejectedPopupComponent,
    BookingActiveDialogComponent,
    BookingActivePopupComponent,
    bookingRoute,
    bookingPopupRoute,
    BookingResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...bookingRoute,
    ...bookingPopupRoute,
];

@NgModule({
    imports: [
        IchooseSharedModule,
        FormsModule,
        ReactiveFormsModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        MatSnackBarModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        BookingComponent,
        BookingDetailComponent,
        BookingDialogComponent,
        BookingDeleteDialogComponent,
        BookingStatusDialogComponent,
        BookingStatusPopupComponent,
        BookingPopupComponent,
        BookingDeletePopupComponent,
        BookingDetailPopupComponent,
        BookingRejectedDialogComponent,
        BookingRejectedPopupComponent,
        BookingActiveDialogComponent,
        BookingActivePopupComponent,
    ],
    entryComponents: [
        BookingComponent,
        BookingDialogComponent,
        BookingPopupComponent,
        BookingDeleteDialogComponent,
        BookingStatusDialogComponent,
        BookingStatusPopupComponent,
        BookingDeletePopupComponent,
        BookingRejectedDialogComponent,
        BookingRejectedPopupComponent,
        BookingActiveDialogComponent,
        BookingActivePopupComponent,
    ],
    providers: [
        BookingService,
        BookingPopupService,
        BookingResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IchooseBookingModule {}
