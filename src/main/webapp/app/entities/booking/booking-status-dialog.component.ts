import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { JhiEventManager } from 'ng-jhipster';
import { Observable } from 'rxjs/Observable';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import {ClientTable, ClientTableService} from '../client-table';
import { Booking } from './booking.model';
import { BookingPopupService } from './booking-popup.service';
import { BookingService } from './booking.service';

@Component({
    selector: 'jhi-booking-status-dialog',
    templateUrl: './booking-status-dialog.component.html',
    styleUrls: [
        'booking.scss'
    ]
})
export class BookingStatusDialogComponent implements OnInit {

    booking: Booking;
    newStatus: string;
    isSaving: boolean;
    clientTable: ClientTable;

    constructor(
        private bookingService: BookingService,
        private clientTableService: ClientTableService,
        public activeModal: MatDialogRef<BookingStatusPopupComponent>,
        private eventManager: JhiEventManager,
        private route: ActivatedRoute,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.booking = data.booking;
    }

    ngOnInit() {
        this.route.queryParams
           .subscribe( (params) => {
               if (params.status !== undefined) {
                   this.newStatus = params.status;
                   console.log(this.newStatus);
               }
           }
       );
    }

    clear() {
        this.activeModal.close('cancel');
    }

    confirmChange() {
        this.isSaving = true;
        this.booking.status = this.newStatus;
        this.subscribeToSaveResponse(
            this.bookingService.update(this.booking));
            this.eventManager.broadcast({
                name: 'bookingListModification',
                content: 'Modified an booking'
            });
            this.activeModal.close(true);
        }
    private subscribeToSaveResponse(result: Observable<HttpResponse<Booking>>) {
        result.subscribe((res: HttpResponse<Booking>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Booking) {
        if ( result.tableId != null) {
            this.clientTableService.find(result.tableId).subscribe((res: HttpResponse<ClientTable>) =>
                this.freeTable(res.body), (res: HttpErrorResponse) => console.log(res));
        }
        this.eventManager.broadcast({ name: 'bookingListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.close(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
    private freeTable(table: ClientTable) {
        table.status = 'FREE';
        this.clientTableService.update(table).subscribe((res: HttpResponse<ClientTable>) => {
            console.log(res.body);
            this.eventManager.broadcast({ name: 'stayModification', content: 'OK'}); });
    }
}

@Component({
    selector: 'jhi-booking-status-popup',
    template: ''
})
export class BookingStatusPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private bookingPopupService: BookingPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.bookingPopupService
                .open(BookingStatusDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
