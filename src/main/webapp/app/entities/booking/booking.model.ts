import { BaseEntity } from './../../shared';
import { ClientTable } from '../client-table';
import { Client } from '../client';
import {StayOrder} from '../stay-order';
export enum BookingStatus {
    PENDING,
    ACTIVE,
    REJECTED,
    DISMISSED,
    FINALIZED,
}
export class Booking implements BaseEntity {
    constructor(
        public id?: number,
        public dateTime?: any,
        public amount?: number,
        public people?: number,
        public clientName?: string,
        public clientTelephoneNumber?: string,
        public comments?: string,
        public tableId?: number,
        public table?: ClientTable,
        public status?: any,
        public participants?: Client[],
        public orders?: StayOrder[],
    ) {
        this.status = 'ACTIVE';
        this.table = new ClientTable();
        this.amount = 0;
    }
}
