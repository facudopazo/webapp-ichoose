import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Booking } from './booking.model';
import { BookingService } from './booking.service';
import { BookingPopupService } from './booking-popup.service';
import { ClientTable } from '../client-table';
import {MatTableDataSource} from '@angular/material';
import {StayOrder} from '../stay-order';
import {Stay} from '../stay/stay.model';

@Component({
    selector: 'jhi-booking-detail',
    templateUrl: './booking-detail.component.html',
    styleUrls: [
        'booking.scss'
    ]
})
export class BookingDetailComponent implements OnInit, OnDestroy {
    displayedColumns = ['Nombre', 'Cantidad', 'Precio unitario', 'Precio total'];
    booking: Booking;
    private subscription: Subscription;
    private eventSubscriber: Subscription;
    dataSource: any;

    constructor(
        private eventManager: JhiEventManager,
        private bookingService: BookingService,
        private route: ActivatedRoute,
        public activeModal: MatDialogRef<BookingDetailComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.booking = data.booking;
        this.dataSource = new MatTableDataSource(this.booking.orders);
    }

    ngOnInit() {
        this.registerChangeInBookings();
    }

    load(id) {
        this.bookingService.find(id)
            .subscribe((bookingResponse: HttpResponse<Booking>) => {
                this.booking = bookingResponse.body;
                this.dataSource = new MatTableDataSource(this.booking.orders);
            });
    }
    previousState() {
        window.history.back();
    }
    getNumber(table: ClientTable) {
        if (table === null) {
            return false;
        } else {
            return true;
        }
    }
    clear() {
        this.activeModal.close('cancel');
    }

    ngOnDestroy() {
        // this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInBookings() {
        this.eventSubscriber = this.eventManager.subscribe(
            'bookingListModification',
            (response) => this.load(this.booking.id)
        );
    }
    reCalculateStayAmount(booking: Booking) {
        let total = 0;
        booking.orders.forEach( (order) => {
            total += this.calculateStayOrderAmount(order);
        });
        booking.amount = total;
    }

    calculateStayOrderAmount( stayOrder: StayOrder) {
        return stayOrder.quantity * stayOrder.price;
    }
}

@Component({
    selector: 'jhi-booking-detail-popup',
    template: ''
})
export class BookingDetailPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private bookingPopupService: BookingPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.bookingPopupService
                .open(BookingDetailComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
