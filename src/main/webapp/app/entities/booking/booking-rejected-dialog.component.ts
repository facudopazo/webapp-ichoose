import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { JhiEventManager } from 'ng-jhipster';
import { Observable } from 'rxjs/Observable';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Booking } from './booking.model';
import { BookingPopupService } from './booking-popup.service';
import { BookingService } from './booking.service';

@Component({
    selector: 'jhi-booking-rejected-dialog',
    templateUrl: './booking-rejected-dialog.component.html',
    styleUrls: [
        'booking.scss'
    ]
})
export class BookingRejectedDialogComponent implements OnInit {

    booking: Booking;
    newStatus: string;
    isSaving: boolean;

    constructor(
        private bookingService: BookingService,
        public activeModal: MatDialogRef<BookingRejectedPopupComponent>,
        private eventManager: JhiEventManager,
        private route: ActivatedRoute,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.booking = data.booking;
    }

    ngOnInit() {
        this.route.queryParams
           .subscribe( (params) => {
               if (params.status !== undefined) {
                   this.newStatus = params.status;
                   console.log(this.newStatus);
               }
           }
       );
    }

    clear() {
        this.activeModal.close('cancel');
    }

    RejectBooking() {
        this.isSaving = true;
        this.subscribeToSaveResponse(
            this.bookingService.reject(this.booking.id));
        }
    private subscribeToSaveResponse(result: Observable<HttpResponse<Booking>>) {
        console.log('por aca tambien');
        result.subscribe((res: HttpResponse<Booking>) => {
            console.log('y por aqui tambien');
            this.eventManager.broadcast({ name: 'bookingListModification', content: 'OK'});
            this.isSaving = false;
            this.activeModal.close(res.body);
        }, (res: HttpErrorResponse) => this.onSaveError(res));
    }

    private onSaveError(err) {
        console.log(err);
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-booking-status-popup',
    template: ''
})
export class BookingRejectedPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private bookingPopupService: BookingPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.bookingPopupService
                .open(BookingRejectedDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
