import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { merge, Observable, of as observableOf, Subscription } from 'rxjs';
import { catchError, map, startWith, switchMap} from 'rxjs/operators';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';
import { BookingStatus } from './booking.model';
import { Booking } from './booking.model';
import { BookingService } from './booking.service';
import { RestaurantService } from '../restaurant/restaurant.service';
import { ITEMS_PER_PAGE, Principal } from '../../shared';
import { MatPaginator, MatSort, MatTableDataSource, PageEvent } from '@angular/material';
import { ClientTable } from '../client-table';
import { Restaurant } from '../restaurant';

@Component({
    selector: 'jhi-booking',
    templateUrl: './booking.component.html'
})
export class BookingComponent implements OnInit, OnDestroy {

    displayedColumns = ['Número', 'Mesa', 'Cliente', 'Fecha', 'Estado', 'Opciones'];
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    bookingStatusArray: any[];
    bookingStatus: any;
    currentRestaurant: Restaurant;
    bookings: Booking[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    dataSource: any;

    resultsLength: any = '0';
    isLoadingResults = true;
    isRateLimitReached = false;

    constructor(
        private bookingService: BookingService,
        private restaurantService: RestaurantService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private route: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager
    ) {

    }

    ngOnInit() {
        this.bookingStatusArray = Object.keys(BookingStatus).filter((k) => typeof BookingStatus[k as any] === 'number');
        this.route.queryParams
            .subscribe((params) => {
                if (params.status !== undefined) {
                    this.bookingStatus = params.status;
                }
            });
        this.restaurantService.getCurrent().subscribe((restaurant: Restaurant) => {
            this.currentRestaurant = restaurant;
            this.loadAll();
        });
    }
    loadAll() {
        merge(this.paginator.page)
            .pipe(
                startWith({}),
                switchMap(() => {
                    this.isLoadingResults = true;
                    const options = {
                        page: this.paginator.pageIndex,
                        size: this.paginator.pageSize,
                        sort: ['dateTime,asc']
                    };

                    if ( this.bookingStatus) {
                        options ['status'] = this.bookingStatus;
                    }
                    return this.bookingService.findByRestaurantId(this.currentRestaurant.id, options);
                }),
                map((data) => {
                    // Flip flag to show that loading has finished.
                    this.isLoadingResults = false;
                    this.isRateLimitReached = false;
                    this.resultsLength = data.headers.get('X-Total-Count');
                    return data.body;
                }),
                catchError(() => {
                    this.isLoadingResults = false;
                    // Catch if the GitHub API has reached its rate limit. Return empty data.
                    this.isRateLimitReached = true;
                    return observableOf([]);
                })
            ).subscribe((data) => this.bookings = data);
    }

    getNumber(table: ClientTable) {
        if (table === null) {
            return false;
        } else {
            return true;
        }
    }
    onStatusChange() {
        this.paginator._pageIndex = 0;
        this.loadAll();
    }

    ngOnDestroy() {

    }

}
