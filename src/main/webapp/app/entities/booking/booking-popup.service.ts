import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { Booking } from './booking.model';
import { BookingService } from './booking.service';

@Injectable()
export class BookingPopupService {
    private ngbModalRef: MatDialogRef<Component>;

    constructor(
        private datePipe: DatePipe,
        private modalService: MatDialog,
        private router: Router,
        private bookingService: BookingService

    ) {
        this.ngbModalRef = null;
    }

        open(component: Component, id?: number | any): Promise<MatDialogRef<Component>> {
            return new Promise<MatDialogRef<Component>>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.bookingService.find(id)
                     .subscribe((bookingResponse: HttpResponse<Booking>) => {
                         const booking: Booking = bookingResponse.body;
                        //  booking.dateTime = this.datePipe
                            //  .transform(booking.dateTime, 'yyyy-MM-ddTHH:mm:ss');
                            // const booking: Booking = bookingResponse.body;
                        this.ngbModalRef = this.bookingModalRef(component, booking);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.bookingModalRef(component, new Booking());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    // bookingModalRef(component: Component, booking: Booking): NgbModalRef {
        // const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        // modalRef.componentInstance.booking = booking;
        // modalRef.result.then((result) => {
    bookingModalRef(component: any, booking: Booking): MatDialogRef<Component> {
        const modalRef = this.modalService.open(component, {height: 'Auto',
        width: '700px', data: {'booking': booking}});
        modalRef.afterClosed().subscribe((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
