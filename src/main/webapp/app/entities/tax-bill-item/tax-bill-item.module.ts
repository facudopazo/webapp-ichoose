import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IchooseSharedModule } from '../../shared';
import {
    TaxBillItemService,
    TaxBillItemPopupService,
    TaxBillItemComponent,
    TaxBillItemDetailComponent,
    TaxBillItemDialogComponent,
    TaxBillItemPopupComponent,
    TaxBillItemDeletePopupComponent,
    TaxBillItemDeleteDialogComponent,
    taxBillItemRoute,
    taxBillItemPopupRoute,
    TaxBillItemResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...taxBillItemRoute,
    ...taxBillItemPopupRoute,
];

@NgModule({
    imports: [
        IchooseSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        TaxBillItemComponent,
        TaxBillItemDetailComponent,
        TaxBillItemDialogComponent,
        TaxBillItemDeleteDialogComponent,
        TaxBillItemPopupComponent,
        TaxBillItemDeletePopupComponent,
    ],
    entryComponents: [
        TaxBillItemComponent,
        TaxBillItemDialogComponent,
        TaxBillItemPopupComponent,
        TaxBillItemDeleteDialogComponent,
        TaxBillItemDeletePopupComponent,
    ],
    providers: [
        TaxBillItemService,
        TaxBillItemPopupService,
        TaxBillItemResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IchooseTaxBillItemModule {}
