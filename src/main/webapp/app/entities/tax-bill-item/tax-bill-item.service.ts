import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { TaxBillItem } from './tax-bill-item.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<TaxBillItem>;

@Injectable()
export class TaxBillItemService {

    private resourceUrl =  SERVER_API_URL + 'api/tax-bill-items';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/tax-bill-items';

    constructor(private http: HttpClient) { }

    create(taxBillItem: TaxBillItem): Observable<EntityResponseType> {
        const copy = this.convert(taxBillItem);
        return this.http.post<TaxBillItem>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(taxBillItem: TaxBillItem): Observable<EntityResponseType> {
        const copy = this.convert(taxBillItem);
        return this.http.put<TaxBillItem>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<TaxBillItem>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<TaxBillItem[]>> {
        const options = createRequestOption(req);
        return this.http.get<TaxBillItem[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<TaxBillItem[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<TaxBillItem[]>> {
        const options = createRequestOption(req);
        return this.http.get<TaxBillItem[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<TaxBillItem[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: TaxBillItem = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<TaxBillItem[]>): HttpResponse<TaxBillItem[]> {
        const jsonResponse: TaxBillItem[] = res.body;
        const body: TaxBillItem[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to TaxBillItem.
     */
    private convertItemFromServer(taxBillItem: TaxBillItem): TaxBillItem {
        const copy: TaxBillItem = Object.assign({}, taxBillItem);
        return copy;
    }

    /**
     * Convert a TaxBillItem to a JSON which can be sent to the server.
     */
    private convert(taxBillItem: TaxBillItem): TaxBillItem {
        const copy: TaxBillItem = Object.assign({}, taxBillItem);
        return copy;
    }
}
