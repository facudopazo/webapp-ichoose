import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TaxBillItem } from './tax-bill-item.model';
import { TaxBillItemPopupService } from './tax-bill-item-popup.service';
import { TaxBillItemService } from './tax-bill-item.service';

@Component({
    selector: 'jhi-tax-bill-item-delete-dialog',
    templateUrl: './tax-bill-item-delete-dialog.component.html'
})
export class TaxBillItemDeleteDialogComponent {

    taxBillItem: TaxBillItem;

    constructor(
        private taxBillItemService: TaxBillItemService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.taxBillItemService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'taxBillItemListModification',
                content: 'Deleted an taxBillItem'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-tax-bill-item-delete-popup',
    template: ''
})
export class TaxBillItemDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private taxBillItemPopupService: TaxBillItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.taxBillItemPopupService
                .open(TaxBillItemDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
