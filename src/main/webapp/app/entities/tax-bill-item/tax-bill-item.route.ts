import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { TaxBillItemComponent } from './tax-bill-item.component';
import { TaxBillItemDetailComponent } from './tax-bill-item-detail.component';
import { TaxBillItemPopupComponent } from './tax-bill-item-dialog.component';
import { TaxBillItemDeletePopupComponent } from './tax-bill-item-delete-dialog.component';

@Injectable()
export class TaxBillItemResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const taxBillItemRoute: Routes = [
    {
        path: 'tax-bill-item',
        component: TaxBillItemComponent,
        resolve: {
            'pagingParams': TaxBillItemResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.taxBillItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tax-bill-item/:id',
        component: TaxBillItemDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.taxBillItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const taxBillItemPopupRoute: Routes = [
    {
        path: 'tax-bill-item-new',
        component: TaxBillItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.taxBillItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'tax-bill-item/:id/edit',
        component: TaxBillItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.taxBillItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'tax-bill-item/:id/delete',
        component: TaxBillItemDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.taxBillItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
