import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { TaxBillItem } from './tax-bill-item.model';
import { TaxBillItemService } from './tax-bill-item.service';

@Component({
    selector: 'jhi-tax-bill-item-detail',
    templateUrl: './tax-bill-item-detail.component.html'
})
export class TaxBillItemDetailComponent implements OnInit, OnDestroy {

    taxBillItem: TaxBillItem;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private taxBillItemService: TaxBillItemService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTaxBillItems();
    }

    load(id) {
        this.taxBillItemService.find(id)
            .subscribe((taxBillItemResponse: HttpResponse<TaxBillItem>) => {
                this.taxBillItem = taxBillItemResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTaxBillItems() {
        this.eventSubscriber = this.eventManager.subscribe(
            'taxBillItemListModification',
            (response) => this.load(this.taxBillItem.id)
        );
    }
}
