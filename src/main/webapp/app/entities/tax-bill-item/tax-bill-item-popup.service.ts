import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { TaxBillItem } from './tax-bill-item.model';
import { TaxBillItemService } from './tax-bill-item.service';

@Injectable()
export class TaxBillItemPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private taxBillItemService: TaxBillItemService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.taxBillItemService.find(id)
                    .subscribe((taxBillItemResponse: HttpResponse<TaxBillItem>) => {
                        const taxBillItem: TaxBillItem = taxBillItemResponse.body;
                        this.ngbModalRef = this.taxBillItemModalRef(component, taxBillItem);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.taxBillItemModalRef(component, new TaxBillItem());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    taxBillItemModalRef(component: Component, taxBillItem: TaxBillItem): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.taxBillItem = taxBillItem;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
