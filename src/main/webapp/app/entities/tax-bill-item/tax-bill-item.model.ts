import { BaseEntity } from './../../shared';

export class TaxBillItem implements BaseEntity {
    constructor(
        public id?: number,
        public aliciva?: number,
        public importe?: number,
        public ds?: string,
        public qty?: number,
        public stayId?: number,
        public taxBillId?: number,
    ) {
    }
}
