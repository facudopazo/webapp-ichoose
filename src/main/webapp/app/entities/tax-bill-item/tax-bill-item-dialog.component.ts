import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TaxBillItem } from './tax-bill-item.model';
import { TaxBillItemPopupService } from './tax-bill-item-popup.service';
import { TaxBillItemService } from './tax-bill-item.service';
import { Stay, StayService } from '../stay';
import { TaxBill, TaxBillService } from '../tax-bill';

@Component({
    selector: 'jhi-tax-bill-item-dialog',
    templateUrl: './tax-bill-item-dialog.component.html'
})
export class TaxBillItemDialogComponent implements OnInit {

    taxBillItem: TaxBillItem;
    isSaving: boolean;

    stays: Stay[];

    taxbills: TaxBill[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private taxBillItemService: TaxBillItemService,
        private stayService: StayService,
        private taxBillService: TaxBillService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.stayService.query()
            .subscribe((res: HttpResponse<Stay[]>) => { this.stays = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.taxBillService.query()
            .subscribe((res: HttpResponse<TaxBill[]>) => { this.taxbills = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.taxBillItem.id !== undefined) {
            this.subscribeToSaveResponse(
                this.taxBillItemService.update(this.taxBillItem));
        } else {
            this.subscribeToSaveResponse(
                this.taxBillItemService.create(this.taxBillItem));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<TaxBillItem>>) {
        result.subscribe((res: HttpResponse<TaxBillItem>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: TaxBillItem) {
        this.eventManager.broadcast({ name: 'taxBillItemListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackStayById(index: number, item: Stay) {
        return item.id;
    }

    trackTaxBillById(index: number, item: TaxBill) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-tax-bill-item-popup',
    template: ''
})
export class TaxBillItemPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private taxBillItemPopupService: TaxBillItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.taxBillItemPopupService
                    .open(TaxBillItemDialogComponent as Component, params['id']);
            } else {
                this.taxBillItemPopupService
                    .open(TaxBillItemDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
