import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { StayHistory } from './stay-history.model';
import { StayHistoryPopupService } from './stay-history-popup.service';
import { StayHistoryService } from './stay-history.service';
import { Restaurant, RestaurantService } from '../restaurant';
import { Client, ClientService } from '../client';

@Component({
    selector: 'jhi-stay-history-dialog',
    templateUrl: './stay-history-dialog.component.html',
    styleUrls: [
        'stay-history.scss'
    ]
})
export class StayHistoryDialogComponent implements OnInit {

    stayHistory: StayHistory;
    isSaving: boolean;

    restaurants: Restaurant[];

    clients: Client[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private stayHistoryService: StayHistoryService,
        private restaurantService: RestaurantService,
        private clientService: ClientService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.restaurantService.query()
            .subscribe((res: HttpResponse<Restaurant[]>) => { this.restaurants = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.clientService.query()
            .subscribe((res: HttpResponse<Client[]>) => { this.clients = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.stayHistory.id !== undefined) {
            this.subscribeToSaveResponse(
                this.stayHistoryService.update(this.stayHistory));
        } else {
            this.subscribeToSaveResponse(
                this.stayHistoryService.create(this.stayHistory));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<StayHistory>>) {
        result.subscribe((res: HttpResponse<StayHistory>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: StayHistory) {
        this.eventManager.broadcast({ name: 'stayHistoryListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackRestaurantById(index: number, item: Restaurant) {
        return item.id;
    }

    trackClientById(index: number, item: Client) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-stay-history-popup',
    template: ''
})
export class StayHistoryPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private stayHistoryPopupService: StayHistoryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.stayHistoryPopupService
                    .open(StayHistoryDialogComponent as Component, params['id']);
            } else {
                this.stayHistoryPopupService
                    .open(StayHistoryDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
