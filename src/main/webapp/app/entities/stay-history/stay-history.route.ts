import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { StayHistoryComponent } from './stay-history.component';
import { StayHistoryDetailComponent } from './stay-history-detail.component';
import { StayHistoryPopupComponent } from './stay-history-dialog.component';
import { StayHistoryDeletePopupComponent } from './stay-history-delete-dialog.component';

export const stayHistoryRoute: Routes = [
    {
        path: 'stay-history',
        component: StayHistoryComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.stayHistory.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'stay-history/:id',
        component: StayHistoryDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.stayHistory.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const stayHistoryPopupRoute: Routes = [
    {
        path: 'stay-history-new',
        component: StayHistoryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.stayHistory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'stay-history/:id/edit',
        component: StayHistoryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.stayHistory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'stay-history/:id/delete',
        component: StayHistoryDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.stayHistory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
