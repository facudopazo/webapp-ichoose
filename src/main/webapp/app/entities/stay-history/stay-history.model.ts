import { BaseEntity } from './../../shared';

export class StayHistory implements BaseEntity {
    constructor(
        public id?: number,
        public numberOfStays?: number,
        public averageAmount?: number,
        public lastStay?: any,
        public restaurantId?: number,
        public clientId?: number,
    ) {
    }
}
