import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { StayHistory } from './stay-history.model';
import { StayHistoryService } from './stay-history.service';

@Injectable()
export class StayHistoryPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private stayHistoryService: StayHistoryService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.stayHistoryService.find(id)
                    .subscribe((stayHistoryResponse: HttpResponse<StayHistory>) => {
                        const stayHistory: StayHistory = stayHistoryResponse.body;
                        stayHistory.lastStay = this.datePipe
                            .transform(stayHistory.lastStay, 'yyyy-MM-ddTHH:mm:ss');
                        this.ngbModalRef = this.stayHistoryModalRef(component, stayHistory);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.stayHistoryModalRef(component, new StayHistory());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    stayHistoryModalRef(component: Component, stayHistory: StayHistory): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.stayHistory = stayHistory;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
