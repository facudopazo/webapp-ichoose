import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { StayHistory } from './stay-history.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<StayHistory>;

@Injectable()
export class StayHistoryService {

    private resourceUrl =  SERVER_API_URL + 'ichooseapi/api/stay-histories';
    private resourceSearchUrl = SERVER_API_URL + 'ichooseapi/api/_search/stay-histories';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(stayHistory: StayHistory): Observable<EntityResponseType> {
        const copy = this.convert(stayHistory);
        return this.http.post<StayHistory>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(stayHistory: StayHistory): Observable<EntityResponseType> {
        const copy = this.convert(stayHistory);
        return this.http.put<StayHistory>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<StayHistory>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<StayHistory[]>> {
        const options = createRequestOption(req);
        return this.http.get<StayHistory[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<StayHistory[]>) => this.convertArrayResponse(res));
    }

    queryByRestaurant(restaurantId: Number, req?: any): Observable<HttpResponse<StayHistory[]>> {
        const options = createRequestOption(req);
        const url = SERVER_API_URL + 'ichooseapi/api/restaurants/' + restaurantId + '/stay-histories';
        return this.http.get<StayHistory[]>(url, { params: options, observe: 'response' })
            .map((res: HttpResponse<StayHistory[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<StayHistory[]>> {
        const options = createRequestOption(req);
        return this.http.get<StayHistory[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<StayHistory[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: StayHistory = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<StayHistory[]>): HttpResponse<StayHistory[]> {
        const jsonResponse: StayHistory[] = res.body;
        const body: StayHistory[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to StayHistory.
     */
    private convertItemFromServer(stayHistory: StayHistory): StayHistory {
        const copy: StayHistory = Object.assign({}, stayHistory);
        copy.lastStay = this.dateUtils
            .convertDateTimeFromServer(stayHistory.lastStay);
        return copy;
    }

    /**
     * Convert a StayHistory to a JSON which can be sent to the server.
     */
    private convert(stayHistory: StayHistory): StayHistory {
        const copy: StayHistory = Object.assign({}, stayHistory);

        copy.lastStay = this.dateUtils.toDate(stayHistory.lastStay);
        return copy;
    }
}
