import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { StayHistory } from './stay-history.model';
import { StayHistoryPopupService } from './stay-history-popup.service';
import { StayHistoryService } from './stay-history.service';

@Component({
    selector: 'jhi-stay-history-delete-dialog',
    templateUrl: './stay-history-delete-dialog.component.html',
    styleUrls: [
        'stay-history.scss'
    ]
})
export class StayHistoryDeleteDialogComponent {

    stayHistory: StayHistory;

    constructor(
        private stayHistoryService: StayHistoryService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.stayHistoryService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'stayHistoryListModification',
                content: 'Deleted an stayHistory'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-stay-history-delete-popup',
    template: ''
})
export class StayHistoryDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private stayHistoryPopupService: StayHistoryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.stayHistoryPopupService
                .open(StayHistoryDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
