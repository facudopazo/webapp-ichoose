import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { StayHistory } from './stay-history.model';
import { StayHistoryService } from './stay-history.service';

@Component({
    selector: 'jhi-stay-history-detail',
    templateUrl: './stay-history-detail.component.html',
    styleUrls: [
        'stay-history.scss'
    ]
})
export class StayHistoryDetailComponent implements OnInit, OnDestroy {

    stayHistory: StayHistory;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private stayHistoryService: StayHistoryService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInStayHistories();
    }

    load(id) {
        this.stayHistoryService.find(id)
            .subscribe((stayHistoryResponse: HttpResponse<StayHistory>) => {
                this.stayHistory = stayHistoryResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInStayHistories() {
        this.eventSubscriber = this.eventManager.subscribe(
            'stayHistoryListModification',
            (response) => this.load(this.stayHistory.id)
        );
    }
}
