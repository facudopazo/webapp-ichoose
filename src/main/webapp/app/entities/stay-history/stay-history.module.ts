import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { IchooseSharedModule } from '../../shared';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { NgSelectModule } from '@ng-select/ng-select';

import {
    StayHistoryService,
    StayHistoryPopupService,
    StayHistoryComponent,
    StayHistoryDetailComponent,
    StayHistoryDialogComponent,
    StayHistoryPopupComponent,
    StayHistoryDeletePopupComponent,
    StayHistoryDeleteDialogComponent,
    stayHistoryRoute,
    stayHistoryPopupRoute,
    ClientGroupPopupComponent
} from './';

const ENTITY_STATES = [
    ...stayHistoryRoute,
    ...stayHistoryPopupRoute,
];

@NgModule({
    imports: [
        IchooseSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        NgSelectModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
  ],
    declarations: [
        StayHistoryComponent,
        StayHistoryDetailComponent,
        StayHistoryDialogComponent,
        StayHistoryDeleteDialogComponent,
        StayHistoryPopupComponent,
        StayHistoryDeletePopupComponent,
        ClientGroupPopupComponent
    ],
    entryComponents: [
        StayHistoryComponent,
        StayHistoryDialogComponent,
        StayHistoryPopupComponent,
        StayHistoryDeleteDialogComponent,
        StayHistoryDeletePopupComponent,
        ClientGroupPopupComponent
    ],
    providers: [
        StayHistoryService,
        StayHistoryPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IchooseStayHistoryModule {}
