import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Client } from '../client/client.model';
import { StayHistory } from './stay-history.model';
import { Nationality, NationalityService } from '../nationality';
import { StayHistoryService } from './stay-history.service';
import { ITEMS_PER_PAGE, Principal } from '../../shared';
import { ClientGroupPopupService, ClientGroupDialogComponent } from '../client-group';

@Component({
    selector: 'jhi-stay-history',
    templateUrl: './stay-history.component.html',
    styleUrls: [
        'stay-history.scss'
    ]
})
export class StayHistoryComponent implements OnInit, OnDestroy {
    displayedColumns = ['picture', 'firstName', 'birthdate', 'lastStay', 'numberOfStays', 'nationality', 'averageAmount', 'Actions'];

    @ViewChild(MatSort) sortTable: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;

    stayHistories: StayHistory[];
    currentAccount: any;
    nationalities: Nationality[];
    eventSubscriber: Subscription;
    itemsPerPage: number;
    links: any;
    page: any;
    predicate: any;
    queryCount: any;
    reverse: any;
    dataSource: any;
    totalItems: number;
    currentSearch: string;
    clientsForGroup: any;
    currentRestaurantId: Number;

    minAverageAmount: Number;
    maxAverageAmount: Number;
    minNumberOfStays: Number;
    maxNumberOfStays: Number;
    isNewClient: boolean;
    minBirthdateDay: Number;
    maxBirthdateDay: Number;
    minBirthdateMonth: Number;
    maxBirthdateMonth: Number;
    rangeBirthdate: any;
    rangeStay: any;
    minLastStay: any;
    maxLastStay: any;
    nationalitiesFilter: Nationality[];
    isCheckAllSelected: Boolean = false;

    constructor(
        private stayHistoryService: StayHistoryService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private nationalityService: NationalityService,
        private parseLinks: JhiParseLinks,
        private activatedRoute: ActivatedRoute,
        private principal: Principal,
        private clientGroupPopupService: ClientGroupPopupService
    ) {
        this.stayHistories = [];
        this.dataSource = new MatTableDataSource();
        this.itemsPerPage = 10;
        this.page = 0;
        this.currentRestaurantId = Number.parseInt(sessionStorage.getItem('currentRestaurant'));
        this.clientsForGroup = new Map();
        this.links = {
            last: 0
        };
        this.predicate = 'id';
        this.reverse = true;
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        this.nationalityService.query()
            .subscribe((res: HttpResponse<Nationality[]>) => { this.nationalities = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    loadAll() {
        this.stayHistoryService.queryByRestaurant(this.currentRestaurantId, {
            query: this.currentSearch,
            page: this.page,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res: HttpResponse<StayHistory[]>) => this.onSuccess(res.body, res.headers),
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        return;
    }

    reset() {
        this.page = 0;
        this.stayHistories = [];
        this.loadAll();
    }

    loadPage(page) {
        this.page = page;
        this.loadAll();
    }

    clear() {
        this.stayHistories = [];
        this.links = {
            last: 0
        };
        this.page = 0;
        this.predicate = 'id';
        this.reverse = true;
        this.currentSearch = '';
        this.loadAll();
    }

    addClient(value) {
        if (!this.clientsForGroup.has('' + value.id)) {
            this.clientsForGroup.set('' + value.id, value);
        } else {
            this.clientsForGroup.delete('' + value.id);
        }
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.stayHistories = [];
        this.links = {
            last: 0
        };
        this.page = 0;
        this.predicate = '_score';
        this.reverse = false;
        this.currentSearch = query;
        this.loadAll();
    }

    cleanFilters() {
        this.rangeBirthdate = null;
        this.rangeStay = null;
        this.minAverageAmount = null;
        this.maxAverageAmount = null;
        this.minNumberOfStays = null;
        this.maxNumberOfStays = null;
        this.isNewClient = false;
        this.minBirthdateDay = null;
        this.maxBirthdateDay = null;
        this.minBirthdateMonth = null;
        this.maxBirthdateMonth = null;
        this.minLastStay = null;
        this.maxLastStay = null;
        this.nationalitiesFilter = [];
    }

    applyFilters() {
        if (this.rangeBirthdate && this.rangeBirthdate.length > 0) {
            this.minBirthdateDay = this.rangeBirthdate[0] ? this.rangeBirthdate[0].getDate() : null;
            this.maxBirthdateDay = this.rangeBirthdate[1] ? this.rangeBirthdate[1].getDate() : null;
            this.minBirthdateMonth = this.rangeBirthdate[0] ? this.rangeBirthdate[0].getMonth() + 1 : null;
            this.maxBirthdateMonth = this.rangeBirthdate[1] ? this.rangeBirthdate[1].getMonth() + 1 : null;
        }
        if (this.rangeStay && this.rangeStay.length > 0) {
            this.minLastStay = this.rangeStay[0] ? this.rangeStay[0] : null;
            this.maxLastStay = this.rangeStay[1] ? this.rangeStay[1] : null;
        }
        this.stayHistoryService.queryByRestaurant(this.currentRestaurantId, {
            query: this.currentSearch,
            page: this.page,
            size: this.itemsPerPage,
            sort: this.sort(),
            minAverageAmount: this.minAverageAmount,
            maxAverageAmount: this.maxAverageAmount,
            minNumberOfStays: this.minNumberOfStays,
            maxNumberOfStays: this.maxNumberOfStays,
            isNewClient: this.isNewClient ? this.isNewClient : false,
            minBirthdateDay: this.minBirthdateDay,
            maxBirthdateDay: this.maxBirthdateDay,
            minBirthdateMonth: this.minBirthdateMonth,
            maxBirthdateMonth: this.maxBirthdateMonth,
            minLastStay: this.minLastStay,
            maxLastStay: this.maxLastStay,
            nationalities: this.nationalitiesFilter
        }).subscribe(
            (res: HttpResponse<StayHistory[]>) => {
                this.stayHistories = res.body;
                this.isCheckAllSelected = false;
                this.links = this.parseLinks.parse(res.headers.get('link'));
                this.queryCount = this.totalItems;
                this.dataSource = new MatTableDataSource(this.stayHistories);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sortTable;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        return;
    }

    ngOnInit() {
        this.currentRestaurantId = Number.parseInt(sessionStorage.getItem('currentRestaurant'));
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInStayHistories();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    atLeastOneSelected() {
        return this.stayHistories.some((stayHistory: StayHistory) => (stayHistory['selected']));
    }

    trackId(index: number, item: StayHistory) {
        return item.id;
    }
    registerChangeInStayHistories() {
        this.eventSubscriber = this.eventManager.subscribe('stayHistoryListModification', (response) => this.reset());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    openModalGroup() {
        this.clientsForGroup = this.stayHistories.filter((stayHistory: StayHistory) => (stayHistory['selected'])).map((stayHistory: StayHistory) => ({id: stayHistory.clientId}));
        this.clientGroupPopupService
            .open(ClientGroupDialogComponent as Component, null, this.clientsForGroup);
    }

    checkAll(event: any) {
        this.stayHistories.forEach(
            (stayHistory: StayHistory) => (stayHistory['selected'] = this.isCheckAllSelected)
        );
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        for (let i = 0; i < data.length; i++) {
            data[i].selected = false;
            this.stayHistories.push(data[i]);
        }
        this.dataSource = new MatTableDataSource(this.stayHistories);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sortTable;
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-client-group-popup',
    template: ''
})
export class ClientGroupPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientGroupPopupService: ClientGroupPopupService
    ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.clientGroupPopupService
                    .open(ClientGroupDialogComponent as Component, params['id']);
            } else { }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
