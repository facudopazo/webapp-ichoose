import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { MenuSubclassificationComponent } from './menu-subclassification.component';
import { MenuSubclassificationDetailComponent } from './menu-subclassification-detail.component';
import { MenuSubclassificationPopupComponent } from './menu-subclassification-dialog.component';
import { MenuSubclassificationDeletePopupComponent } from './menu-subclassification-delete-dialog.component';

@Injectable()
export class MenuSubclassificationResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const menuSubclassificationRoute: Routes = [
    {
        path: 'menu-subclassification',
        component: MenuSubclassificationComponent,
        resolve: {
            'pagingParams': MenuSubclassificationResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.menuSubclassification.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'menu-subclassification/:id',
        component: MenuSubclassificationDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.menuSubclassification.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const menuSubclassificationPopupRoute: Routes = [
    {
        path: 'menu-subclassification-new',
        component: MenuSubclassificationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.menuSubclassification.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'menu-subclassification/:id/edit',
        component: MenuSubclassificationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.menuSubclassification.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'menu-subclassification/:id/delete',
        component: MenuSubclassificationDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.menuSubclassification.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
