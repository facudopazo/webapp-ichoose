import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { MenuSubclassification } from './menu-subclassification.model';
import { MenuSubclassificationService } from './menu-subclassification.service';

@Component({
    selector: 'jhi-menu-subclassification-detail',
    templateUrl: './menu-subclassification-detail.component.html'
})
export class MenuSubclassificationDetailComponent implements OnInit, OnDestroy {

    menuSubclassification: MenuSubclassification;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private menuSubclassificationService: MenuSubclassificationService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInMenuSubclassifications();
    }

    load(id) {
        this.menuSubclassificationService.find(id)
            .subscribe((menuSubclassificationResponse: HttpResponse<MenuSubclassification>) => {
                this.menuSubclassification = menuSubclassificationResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInMenuSubclassifications() {
        this.eventSubscriber = this.eventManager.subscribe(
            'menuSubclassificationListModification',
            (response) => this.load(this.menuSubclassification.id)
        );
    }
}
