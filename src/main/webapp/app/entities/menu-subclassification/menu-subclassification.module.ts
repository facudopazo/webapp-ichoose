import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IchooseSharedModule } from '../../shared';
import {
    MenuSubclassificationService,
    MenuSubclassificationPopupService,
    MenuSubclassificationComponent,
    MenuSubclassificationDetailComponent,
    MenuSubclassificationDialogComponent,
    MenuSubclassificationPopupComponent,
    MenuSubclassificationDeletePopupComponent,
    MenuSubclassificationDeleteDialogComponent,
    menuSubclassificationRoute,
    menuSubclassificationPopupRoute,
    MenuSubclassificationResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...menuSubclassificationRoute,
    ...menuSubclassificationPopupRoute,
];

@NgModule({
    imports: [
        IchooseSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        MenuSubclassificationComponent,
        MenuSubclassificationDetailComponent,
        MenuSubclassificationDialogComponent,
        MenuSubclassificationDeleteDialogComponent,
        MenuSubclassificationPopupComponent,
        MenuSubclassificationDeletePopupComponent,
    ],
    entryComponents: [
        MenuSubclassificationComponent,
        MenuSubclassificationDialogComponent,
        MenuSubclassificationPopupComponent,
        MenuSubclassificationDeleteDialogComponent,
        MenuSubclassificationDeletePopupComponent,
    ],
    providers: [
        MenuSubclassificationService,
        MenuSubclassificationPopupService,
        MenuSubclassificationResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IchooseMenuSubclassificationModule {}
