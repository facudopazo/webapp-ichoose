
import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SERVER_API_URL } from '../../app.constants';

import { MenuSubclassification } from './menu-subclassification.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<MenuSubclassification>;

@Injectable()
export class MenuSubclassificationService {

    private resourceUrl =  SERVER_API_URL + 'ichooseapi/api/menu-subclassifications';
    private resourceSearchUrl = SERVER_API_URL + 'ichooseapi/api/_search/menu-subclassifications';

    constructor(private http: HttpClient) { }

    create(menuSubclassification: MenuSubclassification): Observable<EntityResponseType> {
        const copy = this.convert(menuSubclassification);
        return this.http.post<MenuSubclassification>(this.resourceUrl, copy, { observe: 'response' }).pipe(
            map((res: EntityResponseType) => this.convertResponse(res)));
    }

    update(menuSubclassification: MenuSubclassification): Observable<EntityResponseType> {
        const copy = this.convert(menuSubclassification);
        return this.http.put<MenuSubclassification>(this.resourceUrl, copy, { observe: 'response' }).pipe(
            map((res: EntityResponseType) => this.convertResponse(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<MenuSubclassification>(`${this.resourceUrl}/${id}`, { observe: 'response'}).pipe(
            map((res: EntityResponseType) => this.convertResponse(res)));
    }

    query(req?: any): Observable<HttpResponse<MenuSubclassification[]>> {
        const options = createRequestOption(req);
        return this.http.get<MenuSubclassification[]>(this.resourceUrl, { params: options, observe: 'response' }).pipe(
            map((res: HttpResponse<MenuSubclassification[]>) => this.convertArrayResponse(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<MenuSubclassification[]>> {
        const options = createRequestOption(req);
        return this.http.get<MenuSubclassification[]>(this.resourceSearchUrl, { params: options, observe: 'response' }).pipe(
            map((res: HttpResponse<MenuSubclassification[]>) => this.convertArrayResponse(res)));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: MenuSubclassification = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<MenuSubclassification[]>): HttpResponse<MenuSubclassification[]> {
        const jsonResponse: MenuSubclassification[] = res.body;
        const body: MenuSubclassification[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to MenuSubclassification.
     */
    private convertItemFromServer(menuSubclassification: MenuSubclassification): MenuSubclassification {
        const copy: MenuSubclassification = Object.assign({}, menuSubclassification);
        return copy;
    }

    /**
     * Convert a MenuSubclassification to a JSON which can be sent to the server.
     */
    private convert(menuSubclassification: MenuSubclassification): MenuSubclassification {
        const copy: MenuSubclassification = Object.assign({}, menuSubclassification);
        return copy;
    }
}
