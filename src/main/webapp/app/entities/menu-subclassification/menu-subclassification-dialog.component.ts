import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { MenuSubclassification } from './menu-subclassification.model';
import { MenuSubclassificationPopupService } from './menu-subclassification-popup.service';
import { MenuSubclassificationService } from './menu-subclassification.service';

@Component({
    selector: 'jhi-menu-subclassification-dialog',
    templateUrl: './menu-subclassification-dialog.component.html'
})
export class MenuSubclassificationDialogComponent implements OnInit {

    menuSubclassification: MenuSubclassification;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private menuSubclassificationService: MenuSubclassificationService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.menuSubclassification.id !== undefined) {
            this.subscribeToSaveResponse(
                this.menuSubclassificationService.update(this.menuSubclassification));
        } else {
            this.subscribeToSaveResponse(
                this.menuSubclassificationService.create(this.menuSubclassification));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<MenuSubclassification>>) {
        result.subscribe((res: HttpResponse<MenuSubclassification>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: MenuSubclassification) {
        this.eventManager.broadcast({ name: 'menuSubclassificationListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-menu-subclassification-popup',
    template: ''
})
export class MenuSubclassificationPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private menuSubclassificationPopupService: MenuSubclassificationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.menuSubclassificationPopupService
                    .open(MenuSubclassificationDialogComponent as Component, params['id']);
            } else {
                this.menuSubclassificationPopupService
                    .open(MenuSubclassificationDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
