import { BaseEntity } from './../../shared';

export class MenuSubclassification implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
    ) {
    }
}
