import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { MenuSubclassification } from './menu-subclassification.model';
import { MenuSubclassificationService } from './menu-subclassification.service';

@Injectable()
export class MenuSubclassificationPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private menuSubclassificationService: MenuSubclassificationService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.menuSubclassificationService.find(id)
                    .subscribe((menuSubclassificationResponse: HttpResponse<MenuSubclassification>) => {
                        const menuSubclassification: MenuSubclassification = menuSubclassificationResponse.body;
                        this.ngbModalRef = this.menuSubclassificationModalRef(component, menuSubclassification);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.menuSubclassificationModalRef(component, new MenuSubclassification());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    menuSubclassificationModalRef(component: Component, menuSubclassification: MenuSubclassification): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.menuSubclassification = menuSubclassification;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
