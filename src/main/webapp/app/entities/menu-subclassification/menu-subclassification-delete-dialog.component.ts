import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { MenuSubclassification } from './menu-subclassification.model';
import { MenuSubclassificationPopupService } from './menu-subclassification-popup.service';
import { MenuSubclassificationService } from './menu-subclassification.service';

@Component({
    selector: 'jhi-menu-subclassification-delete-dialog',
    templateUrl: './menu-subclassification-delete-dialog.component.html'
})
export class MenuSubclassificationDeleteDialogComponent {

    menuSubclassification: MenuSubclassification;

    constructor(
        private menuSubclassificationService: MenuSubclassificationService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.menuSubclassificationService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'menuSubclassificationListModification',
                content: 'Deleted an menuSubclassification'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-menu-subclassification-delete-popup',
    template: ''
})
export class MenuSubclassificationDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private menuSubclassificationPopupService: MenuSubclassificationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.menuSubclassificationPopupService
                .open(MenuSubclassificationDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
