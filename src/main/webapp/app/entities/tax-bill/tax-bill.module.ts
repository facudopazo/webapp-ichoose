import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import {MatButtonModule} from '@angular/material/button';

import { IchooseSharedModule } from '../../shared';
import {
    TaxBillService,
    TaxBillPopupService,
    TaxBillComponent,
    TaxBillDetailComponent,
    TaxBillDialogComponent,
    TaxBillPopupComponent,
    TaxBillDeletePopupComponent,
    TaxBillDeleteDialogComponent,
    taxBillRoute,
    taxBillPopupRoute,
    TaxBillResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...taxBillRoute,
    ...taxBillPopupRoute,
];

@NgModule({
    imports: [
        IchooseSharedModule,
        MatButtonModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        TaxBillComponent,
        TaxBillDetailComponent,
        TaxBillDialogComponent,
        TaxBillDeleteDialogComponent,
        TaxBillPopupComponent,
        TaxBillDeletePopupComponent,
    ],
    entryComponents: [
        TaxBillComponent,
        TaxBillDialogComponent,
        TaxBillPopupComponent,
        TaxBillDeleteDialogComponent,
        TaxBillDeletePopupComponent,
    ],
    providers: [
        TaxBillService,
        TaxBillPopupService,
        TaxBillResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IchooseTaxBillModule {}
