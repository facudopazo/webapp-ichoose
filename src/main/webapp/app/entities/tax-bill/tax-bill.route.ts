import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { TaxBillComponent } from './tax-bill.component';
import { TaxBillDetailComponent } from './tax-bill-detail.component';
import { TaxBillPopupComponent } from './tax-bill-dialog.component';
import { TaxBillDeletePopupComponent } from './tax-bill-delete-dialog.component';

@Injectable()
export class TaxBillResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const taxBillRoute: Routes = [
    {
        path: 'tax-bill',
        component: TaxBillComponent,
        resolve: {
            'pagingParams': TaxBillResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.taxBill.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tax-bill/:id',
        component: TaxBillDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.taxBill.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const taxBillPopupRoute: Routes = [
    {
        path: 'tax-bill-new',
        component: TaxBillPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.taxBill.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'tax-bill/:id/edit',
        component: TaxBillPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.taxBill.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'tax-bill/:id/delete',
        component: TaxBillDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.taxBill.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
