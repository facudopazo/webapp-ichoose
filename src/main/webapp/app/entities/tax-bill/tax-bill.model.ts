import { BaseEntity } from './../../shared';
import { TaxBillItem } from '../tax-bill-item';
import { TaxBillAdditional } from '../tax-bill-additional';

export class TaxBill implements BaseEntity {
    constructor(
        public id?: number,
        public billDate?: any,
        public taxbillnumber?: string,
        public referencia?: string,
        public amount?: number,
        public stayId?: number,
        public domiciliocliente?: string,
        public nombrecliente?: string,
        public tipocbte?: any,
        public tiporesponsable?: any,
        public tipodoc?: any,
        public items?: TaxBillItem[],
        public additionals?: TaxBillAdditional[],
    ) {
        items = new Array <TaxBillItem>();
        additionals = new Array <TaxBillAdditional>();
    }
}
