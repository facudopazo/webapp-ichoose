import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { TaxBill } from './tax-bill.model';
import { TaxBillService } from './tax-bill.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Injectable()
export class TaxBillPopupService {
    private ngbModalRef: MatDialogRef<Component>;

    constructor(
        private router: Router,
        private modalService: MatDialog,
        private taxBillService: TaxBillService

    ) {
        this.ngbModalRef = null;
    }

    // open(component: Component, id?: number | any): Promise<NgbModalRef> {
    //     return new Promise<NgbModalRef>((resolve, reject) => {
        open(component: Component, id?: number | any): Promise<MatDialogRef<Component>> {
            return new Promise<MatDialogRef<Component>>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.taxBillService.find(id)
                    .subscribe((taxBillResponse: HttpResponse<TaxBill>) => {
                        // const taxBill: TaxBill = taxBillResponse.body;
                        // this.ngbModalRef = this.taxBillModalRef(component, taxBill);
                        const taxBill: TaxBill = taxBillResponse.body;
                        this.ngbModalRef = this.taxBillModalRef(component, taxBill);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.taxBillModalRef(component, new TaxBill());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }
            taxBillModalRef(component: any, taxBill: TaxBill): MatDialogRef<Component> {
            const modalRef = this.modalService.open(component, {height: 'auto',
            width: '700px', data: {'table': taxBill}});
            modalRef.afterClosed().subscribe((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
