import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TaxBill } from './tax-bill.model';
import { TaxBillPopupService } from './tax-bill-popup.service';
import { TaxBillService } from './tax-bill.service';

@Component({
    selector: 'jhi-tax-bill-delete-dialog',
    templateUrl: './tax-bill-delete-dialog.component.html'
})
export class TaxBillDeleteDialogComponent {

    taxBill: TaxBill;

    constructor(
        private taxBillService: TaxBillService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.taxBillService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'taxBillListModification',
                content: 'Deleted an taxBill'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-tax-bill-delete-popup',
    template: ''
})
export class TaxBillDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private taxBillPopupService: TaxBillPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.taxBillPopupService
                .open(TaxBillDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
