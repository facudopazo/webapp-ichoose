import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { JhiEventManager } from 'ng-jhipster';
import { MatTableDataSource } from '@angular/material';
import { TaxBill } from './tax-bill.model';
import { TaxBillPopupService } from './tax-bill-popup.service';
import { TaxBillService } from './tax-bill.service';
import { Stay, StayService } from '../stay';
import { StayOrder } from '../stay-order';
import { TaxBillItem } from '../tax-bill-item';
import { TaxBillAdditional } from '../tax-bill-additional';
import { TaxBillPrinterService } from '../tax-bill/tax-bill-printer.service';
import { ItemsDimensions } from '@ng-select/ng-select/ng-select/virtual-scroll.service';
import { MatSnackBar } from '@angular/material';

@Component({
    selector: 'jhi-tax-bill-dialog',
    templateUrl: './tax-bill-dialog.component.html'
})
export class TaxBillDialogComponent implements OnInit {
    displayedColumns = ['Cantidad', 'Nombre', 'Precio unitario', 'Precio total'];
    displayedColumnsPromotion = ['Descripcion', 'iva', , 'Descuento'];
    displayedColumnsBills = ['Fecha', 'Tipo', 'Numero', 'Monto', 'NotaCredito'];
    isPrinting: boolean;
    taxBill: TaxBill;
    taxBillArray: TaxBill[];
    itemElement: TaxBillItem;
    isSaving: boolean;
    tipoComprobantes: any[];
    tipoResponsables: any[];
    tipoDocumentos: any[];
    vali = true;
    total: any;
    tipoComprobanteModificado: any[];
    dataSource: any;
    dataSourceTax: any;
    dataSourceBills: any;
    stay: Stay;
    constructor(
        private taxBillService: TaxBillService,
        private eventManager: JhiEventManager,
        public activeModal: MatDialogRef<TaxBillPopupComponent>,
        private route: ActivatedRoute,
        private stayService: StayService,
        private taxBillPrinterService: TaxBillPrinterService,
        public snackBar: MatSnackBar,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.dataSource = new MatTableDataSource();
        this.dataSourceTax = new MatTableDataSource();
        this.dataSourceBills = new MatTableDataSource();
    }

    ngOnInit() {
        this.isSaving = false;
        this.isPrinting = false;
        this.taxBill = new TaxBill;
        this.taxBillArray = new Array<TaxBill>();
        this.tipoComprobantes = [{ value: 'T', name: 'Ticket' },
        { value: 'FA', name: 'Factura A' },
        { value: 'FB', name: 'Factura B' }, ];
        // { value: 'NCA', name: 'Nota de crédito A' },
        // { value: 'NCB', name: 'Nota de crédito B' }]

        this.tipoResponsables = [{ value: 'RESPONSABLE_INSCRIPTO', name: 'Responsable inscripto' },
        { value: 'RESPONSABLE_NO_INSCRIPTO', name: 'Responsable no inscripto' },
        { value: 'NO_RESPONSABLE', name: 'No resposable' },
        { value: 'EXENTO', name: 'Exento' },
        { value: 'CONSUMIDOR_FINAL', name: 'Consumidor final' },
        { value: 'NO_CATEGORIZADO', name: 'No categorizado' },
        { value: 'PEQUENIO_CONTRIBUYENTE_EVENTUAL', name: 'Pequeño contribuyente eventual' },
        { value: 'MONOTRIBUTISTA_SOCIAL', name: 'Monotributista social' },
        { value: 'PEQUENIO_CONTRIBUYENTE_EVENTUAL_SOCIAL', name: 'Pequeño contribuyente eventual social' }
        ];

        this.tipoDocumentos = [{ value: 'DNI', name: 'DNI' },
        { value: 'CUIT', name: 'CUIT' },
        { value: 'LIBRETA_ENROLAMIENTO', name: 'Libreta de enrolamiento' },
        { value: 'LIBRETA_CIVICA', name: 'Libreta cívica' },
        { value: 'CEDULA', name: 'Cedula' },
        { value: 'PASAPORTE', name: 'Pasaporte' },
        { value: 'SIN_CALIFICADOR', name: 'Sin calificador' },
        { value: 'MONOTRIBUTISTA_SOCIAL', name: 'Monotributista social' },
        { value: 'PEQUENIO_CONTRIBUYENTE_EVENTUAL_SOCIAL', name: 'Pequeño contribuyente eventual social' }
        ];

        this.taxBill.tipocbte = 'T';
        this.taxBill.tiporesponsable = 'CONSUMIDOR_FINAL';
        if (this.taxBill.tipocbte = 'T') {
            this.vali = false;
        }

        this.route.queryParams
            .subscribe((params) => {
                this.taxBillService.getTaxBillsByStayId(params.stayId, {
                    page: 0,
                    size: 1000,
                }).subscribe((taxbillResponse: HttpResponse<TaxBill[]>) => {
                    this.taxBillArray = taxbillResponse.body;
                    this.dataSourceBills = this.taxBillArray;
                    // this.isEnabledToPrintTaxBill();
                    console.log(this.dataSourceBills);
                });
            });

        this.route.queryParams
            .subscribe((params) => {
                this.stayService.find(params.stayId).subscribe((stayResponse: HttpResponse<Stay>) => {
                    this.taxBill = this.mapToTaxBill(stayResponse.body);
                    this.taxBill.tipocbte = 'T';
                    this.taxBill.tiporesponsable = 'CONSUMIDOR_FINAL';
                    this.showFunction();
                    if (this.taxBill.tipocbte = 'T') {
                        this.vali = false;
                    }
                    console.log(this.taxBill);
                });
            });
    }

    clear() {
        this.activeModal.close('cancel');
    }

    showFunction() {
        if (this.taxBill.tipocbte === 'T') {
            this.vali = false;
        } else {
            this.vali = true;
        }
    }

    save() {
        this.isSaving = true;
        this.isPrinting = true;
        console.log(this.taxBill);
        if (this.taxBill.id !== undefined) {
            this.subscribeToSaveResponse(
                this.taxBillService.update(this.taxBill));
        } else {
            this.subscribeToSaveResponse(
                this.taxBillService.create(this.taxBill));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<TaxBill>>) {
        result.subscribe((res: HttpResponse<TaxBill>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: TaxBill) {
        this.eventManager.broadcast({ name: 'taxBillListModification', content: 'OK' });
        this.isSaving = false;
        this.activeModal.close(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    mapToTaxBill(stay: Stay) {
        const newTaxbill = new TaxBill();
        newTaxbill.items = stay.orders.map((x) => {
            const item = new TaxBillItem;
            item.aliciva = 21;
            item.importe = x.menu.price;
            item.ds = x.menu.name;
            item.qty = x.quantity;
            return item;
        });
        newTaxbill.additionals = stay.discounts.map((x) => {
            const itemAdditional = new TaxBillAdditional;
            itemAdditional.amount = x.amount;
            itemAdditional.description = x.description;
            itemAdditional.iva = 21;
            itemAdditional.negative = false;
            return itemAdditional;
        });
        newTaxbill.domiciliocliente = this.taxBill.domiciliocliente;
        newTaxbill.nombrecliente = this.taxBill.nombrecliente;
        newTaxbill.stayId = stay.id;
        newTaxbill.amount = 0;
        newTaxbill.items.forEach((x: TaxBillItem) => {
            newTaxbill.amount = stay.amount;
        });
        this.dataSourceTax = newTaxbill.items;
        this.dataSource = newTaxbill.additionals;
        stay.taxBills.push(newTaxbill);
        console.log(newTaxbill);
        return newTaxbill;
    }

    printFiscalTicket(taxBill: TaxBill) {
        this.isPrinting = true;
        this.taxBillPrinterService.printTaxBill(this.taxBill)
            .subscribe((response) => {
                console.log('res => ', response);
                if (response.status === true) {
                    this.taxBill.taxbillnumber = response.taxbillnumber;
                    this.taxBill.billDate = new Date;
                    console.log(this.taxBill);
                    this.subscribeToSaveResponse(
                        this.taxBillService.create(this.taxBill));
                    this.activeModal.close(true);
                } else {
                    if (response.error) {
                        const snackBarRef = this.snackBar.open('Hubo un error al conectarse a la impresora fiscal.', '✕');
                        snackBarRef.afterDismissed().subscribe(() => {
                            this.vali = false;
                        });
                    }
                }
            });
    }

    hasCreditNote(taxBill: TaxBill) {
        return this.taxBillArray.some( (tb) => tb.referencia === taxBill.taxbillnumber);
    }

    isEnabledToPrintTaxBill() {
        return !this.taxBillArray.some( (tb) => ['FA', 'FB', 'T'].includes(tb.tipocbte) && !this.taxBillArray.some( (tbi) => tbi.referencia === tb.taxbillnumber));
    }

    mapToCreditNote(taxBill: TaxBill) {
        const newTaxbill = new TaxBill();
        if (taxBill.tipocbte === 'FA') {
            newTaxbill.tipocbte = 'NCA';
        } else {
            newTaxbill.tipocbte = 'NCB';
        }

        newTaxbill.items = new Array<TaxBill>();
        this.itemElement = {
            'aliciva': 21,
            'ds': 'Cancelación factura' + ' ' + taxBill.taxbillnumber,
            'importe': taxBill.amount,
            'qty': 1
        };

        newTaxbill.items.push(this.itemElement);
        newTaxbill.billDate = new Date();
        newTaxbill.referencia = taxBill.taxbillnumber;
        newTaxbill.domiciliocliente = taxBill.domiciliocliente;
        newTaxbill.nombrecliente = taxBill.nombrecliente;
        newTaxbill.stayId = taxBill.stayId;
        newTaxbill.amount = taxBill.amount;
        this.taxBillPrinterService.printCreditNote(newTaxbill)
            .subscribe( (response) => {
                console.log('res => ', response);
                if (response.status === true) {
                    newTaxbill.taxbillnumber = response.taxbillnumber;
                    this.subscribeToSaveResponse(
                        this.taxBillService.create(newTaxbill));
                }
            });
    }
}

@Component({
    selector: 'jhi-tax-bill-popup',
    template: ''
})
export class TaxBillPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private taxBillPopupService: TaxBillPopupService
    ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.taxBillPopupService
                    .open(TaxBillDialogComponent as Component, params['id']);
            } else {
                this.taxBillPopupService
                    .open(TaxBillDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
