import { Injectable } from '@angular/core';
import * as Rx from 'rxjs/Rx';
import { TaxBill } from './tax-bill.model';
import { TaxBillService } from './tax-bill.service';

import { Subject } from 'rxjs/Subject';
import { TaxBillItem } from '../tax-bill-item';
import { ItemsList } from '@ng-select/ng-select/ng-select/items-list';

const WEBSOCKET_URL = 'ws://localhost:12000/ws';

@Injectable()
export class TaxBillPrinterService {
    constructor(
    ) {
    }
    printTaxBill(taxBill: TaxBill): any {
        // ignore
        // const message =
        // {'printTicket':{'encabezado':{'tipo_cbte':'T'},'items':[{'alic_iva':21,'importe':0.01,'ds':'PEPSI','qty':1},{'alic_iva':21,'importe':0.12,'ds':'COCA','qty':2}]}
        // ,'printerName':'Hasar'};
        const message = this.convertForPrint(taxBill);

        const observable = Rx.Observable.create(
            (obs: Rx.Observer<any>) => {
                this.sendMessage(message)
                    .subscribe((res) => {
                        const response = JSON.parse(res.data);
                        let result = {};
                        if (response.rta) {
                            result = {
                                'taxbillnumber': response.rta[0].rta,
                                'status': true
                            };
                        } else {
                            result = {
                                'status': false
                            };
                        }
                        obs.next(result);
                    }, (error: MessageEvent) => {
                        obs.next({ 'status': false, 'error': error });
                    });
            });
        return observable;
    }

    printCreditNote(taxBill: TaxBill): any {
        // const message = {'printTicket':{'encabezado':{'tipo_cbte':'T'},'items':[{'alic_iva':21,'importe':0.01,'ds':'PEPSI','qty':1},
        // {'alic_iva':21,'importe':0.12,'ds':'COCA','qty':2}]},'printerName':'Hasar'};
        const message = this.convertNCForPrint(taxBill);

        const observable = Rx.Observable.create(
            (obs: Rx.Observer<any>) => {
                this.sendMessage(message)
                    .subscribe((res) => {
                        const response = JSON.parse(res.data);
                        console.log('prueba');
                        console.log(response);
                        console.log(response.rta);
                        let result = {};
                        if (response.rta) {
                            result = {
                                'taxbillnumber': response.rta[0].rta,
                                'status': true
                            };
                        } else {
                            result = {
                                'status': false
                            };

                        }
                        obs.next(result);
                    }, (error: MessageEvent) => {
                        obs.next({ 'status': false, 'error': error });
                    });
            });
        return observable;

    }

    doDialyClose(): any {
        const message = { 'dailyClose': 'X', 'printerName': 'Hasar' };
        this.sendMessage(message)
            .subscribe(
                (data) => { console.log(data); },
                (error) => { console.log(error); }
            );

    }
    /* printCreditNote(creditNote: CreditNote): any {

    }*/
    getStatus(): any {
        const message = { 'getAvaliablePrinters': '' };
        this.sendMessage(message)
            .subscribe((data) => {
                console.log(data);
            });
    }
    private sendMessage(message): Rx.Observable<any> {
        const ws = new WebSocket(WEBSOCKET_URL);
        const observableprint = Rx.Observable.create(
            (obs: Rx.Observer<any>) => {
                ws.onopen = (evt) => {
                    ws.send(JSON.stringify(message));
                };
                ws.onmessage = (data) => {
                    obs.next(data);
                    ws.close();
                };
                ws.onerror = obs.error.bind(obs);
                ws.onclose = obs.complete.bind(obs);
            });
        return observableprint;
    }
    private convertForPrint(taxBill: TaxBill): any {
        let itemsForPrint;
        itemsForPrint = taxBill.items.map((x) => {
            const item = {
                alic_iva: 0,
                importe: 0,
                ds: '-',
                qty: 0
            };
            item.alic_iva = x.aliciva,
                item.importe = x.importe,
                item.ds = x.ds,
                item.qty = x.qty;
            return item;
        });
        const response = {
            printTicket: {
                encabezado: {
                    tipo_cbte: taxBill.tipocbte,
                    referencia: taxBill.referencia
                },
                items: itemsForPrint
            },
            printerName: 'Hasar'
        };
        console.log(response);
        return response;
    }

    private convertNCForPrint(taxBill: TaxBill) {
        const response = this.convertForPrint(taxBill);
        response.printTicket.encabezado.referencia = taxBill.referencia;
        return response;
    }

    private convertTaxBillItem(taxBillItem: TaxBillItem): any {
        const response = {
            alic_iva: taxBillItem.aliciva,
            importe: taxBillItem.importe,
            ds: taxBillItem.ds,
            qty: taxBillItem.qty
        };
        return response;
    }
}
