import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { TaxBill } from './tax-bill.model';
import { TaxBillService } from './tax-bill.service';

@Component({
    selector: 'jhi-tax-bill-detail',
    templateUrl: './tax-bill-detail.component.html'
})
export class TaxBillDetailComponent implements OnInit, OnDestroy {

    taxBill: TaxBill;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private taxBillService: TaxBillService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTaxBills();
    }

    load(id) {
        this.taxBillService.find(id)
            .subscribe((taxBillResponse: HttpResponse<TaxBill>) => {
                this.taxBill = taxBillResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTaxBills() {
        this.eventSubscriber = this.eventManager.subscribe(
            'taxBillListModification',
            (response) => this.load(this.taxBill.id)
        );
    }
}
