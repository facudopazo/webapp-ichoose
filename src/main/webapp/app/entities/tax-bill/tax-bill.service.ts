import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { TaxBill } from './tax-bill.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<TaxBill>;

@Injectable()
export class TaxBillService {

    private resourceUrl =  SERVER_API_URL + 'ichooseapi/api/tax-bills';
    private resourceSearchUrl = SERVER_API_URL + 'ichooseapi/api/_search/tax-bills';

    constructor(private http: HttpClient) { }

    create(taxBill: TaxBill): Observable<EntityResponseType> {
        const copy = this.convert(taxBill);
        return this.http.post<TaxBill>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(taxBill: TaxBill): Observable<EntityResponseType> {
        const copy = this.convert(taxBill);
        return this.http.put<TaxBill>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<TaxBill>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<TaxBill[]>> {
        const options = createRequestOption(req);
        return this.http.get<TaxBill[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<TaxBill[]>) => this.convertArrayResponse(res));
    }

    getTaxBillsByStayId(stayId: number, req?: any): Observable<HttpResponse<TaxBill[]>> {
        const resourceUrl =  `${SERVER_API_URL}ichooseapi/api/stays/${stayId}/tax-bills`;
        return this.http.get<TaxBill[]>(resourceUrl, { observe: 'response' })
        .map((res: HttpResponse<TaxBill[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<TaxBill[]>> {
        const options = createRequestOption(req);
        return this.http.get<TaxBill[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<TaxBill[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: TaxBill = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<TaxBill[]>): HttpResponse<TaxBill[]> {
        const jsonResponse: TaxBill[] = res.body;
        const body: TaxBill[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to TaxBill.
     */
    private convertItemFromServer(taxBill: TaxBill): TaxBill {
        const copy: TaxBill = Object.assign({}, taxBill);
        return copy;
    }

    /**
     * Convert a TaxBill to a JSON which can be sent to the server.
     */
    private convert(taxBill: TaxBill): TaxBill {
        const copy: TaxBill = Object.assign({}, taxBill);
        return copy;
    }
}
