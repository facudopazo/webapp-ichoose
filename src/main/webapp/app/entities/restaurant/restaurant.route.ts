import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { RestaurantComponent } from './restaurant.component';
import { RestaurantDetailComponent } from './restaurant-detail.component';
import { RestaurantPopupComponent } from './restaurant-dialog.component';
import { RestaurantDialogComponent } from './restaurant-dialog.component';
import { RestaurantDeletePopupComponent } from './restaurant-delete-dialog.component';

@Injectable()
export class RestaurantResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const restaurantRoute: Routes = [
    {
        path: 'restaurant-new',
        component: RestaurantDialogComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.restaurant.home.title'
        },
        canActivate: [UserRouteAccessService],
    },
    {
        path: 'restaurant/:id/edit',
        component: RestaurantDialogComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.restaurant.home.title'
        },
        canActivate: [UserRouteAccessService],
    },
    {
        path: 'restaurant',
        component: RestaurantComponent,
        resolve: {
            'pagingParams': RestaurantResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.restaurant.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'restaurant/:id',
        component: RestaurantDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.restaurant.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const restaurantPopupRoute: Routes = [
        {
        path: 'restaurant/:id/delete',
        component: RestaurantDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.restaurant.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
