import { BaseEntity } from './../../shared';
import { Location } from '../location';
import { Picture } from '../Picture';

export class Restaurant implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public cuit?: string,
        public cbu?: string,
        public locationId?: number,
        public location?: Location,
        public pictures?: BaseEntity[],
        public logo?: Picture,
        public roles?: BaseEntity[],
        public restaurantTypeId?: number,
        public mealClassifications?: BaseEntity[],
    ) { this.pictures = new Array <Picture>();
        this.location = new Location();
    }
}
