import { Component, NgZone, OnInit, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable, Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';

import { Restaurant } from './restaurant.model';
import { RestaurantPopupService } from './restaurant-popup.service';
import { RestaurantService } from './restaurant.service';
import { Location, LocationService } from '../location';
import { RestaurantType, RestaurantTypeService } from '../restaurant-type';
import { MealClassification, MealClassificationService } from '../meal-classification';
import { FormControl } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MapsAPILoader } from '@agm/core';
import { MatSnackBar } from '@angular/material';
import { Picture } from '../picture';
import { JhiDataUtils } from 'ng-jhipster';
import { FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { AngularFireStorage } from 'angularfire2/storage';

@Component({
    selector: 'jhi-restaurant-dialog',
    templateUrl: './restaurant-dialog.component.html',
    styleUrls: [
        'restaurant.scss'
    ]

})

export class RestaurantDialogComponent implements OnInit {

    downloadURL: any;
    isLinear = true;
    title = 'Google Maps';
    lat = -31.6333;
    long = -60.7;
    searchControl: FormControl;
    public zoom: number;
    matcher = new MyErrorStateMatcher();
    // firstCtrlName = new FormControl('', [Validators.required]);
    ctrlName: FormControl;
    ctrlCuit: FormControl;

    @ViewChild('search')
    public searchElementRef: ElementRef;
    newArrayImage: any;
    newPicture: Picture;

    restaurant: Restaurant;
    isSaving: boolean;
    vali = false;
    private subscription: Subscription;
    locations: Location[];
    restauranttypes: RestaurantType[];

    mealclassifications: MealClassification[];

    constructor(
        private dataUtils: JhiDataUtils,
        private mapsAPILoader: MapsAPILoader,
        private ngZone: NgZone,
        private jhiAlertService: JhiAlertService,
        private restaurantService: RestaurantService,
        private locationService: LocationService,
        private restaurantTypeService: RestaurantTypeService,
        private mealClassificationService: MealClassificationService,
        private eventManager: JhiEventManager,
        private route: ActivatedRoute,
        private router: Router,
        private _formBuilder: FormBuilder,
        public snackBar: MatSnackBar,
        private storage: AngularFireStorage
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.restaurant = new Restaurant();
                this.load(params['id']);
            } else {
                this.restaurant = new Restaurant();
                for (let i = 0; i < 5; i++) {
                    this.restaurant.pictures.push(new Picture());
                }
                this.restaurant.logo = new Picture();
                this.restaurant.location = new Location();
                this.setCurrentPosition();
            }
        });
        this.ctrlName = new FormControl('', [
            Validators.required
        ]);
        this.ctrlCuit = new FormControl('', [
            Validators.required
        ]);
        this.searchControl = new FormControl('', [
            Validators.required
        ]);

        this.isSaving = false;
        this.restaurantTypeService.query()
            .subscribe((res: HttpResponse<RestaurantType[]>) => { this.restauranttypes = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.mealClassificationService.query()
            .subscribe((res: HttpResponse<MealClassification[]>) => { this.mealclassifications = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        // Google Maps
        this.searchControl = new FormControl();
        // Cargar lugares
        this.mapsAPILoader.load().then(() => {
            const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
                types: ['address']
            });
            autocomplete.addListener('place_changed', () => {
                this.ngZone.run(() => {
                    // get the place result
                    const place: google.maps.places.PlaceResult = autocomplete.getPlace();
                    // verify result
                    if (place.geometry === undefined || place.geometry === null || place.types.indexOf('street_address') === -1) {
                        const validAddress = document.getElementById('validAddress').textContent;
                        const snackBarRef = this.snackBar.open(validAddress, 'Ok');
                        snackBarRef.afterDismissed().subscribe(() => {
                            this.searchControl.reset();
                            this.vali = false;
                        });
                        return;
                    }
                    this.vali = true;
                    // set latitude, longitude and zoom
                    this.restaurant.location.latitude = place.geometry.location.lat();
                    this.restaurant.location.longitude = place.geometry.location.lng();
                    this.zoom = 15;
                });
            });
        });
    }

    resetControl() {
        this.vali = false;
        this.searchControl.setValue('');
        this.searchControl.reset();
    }
    previousState() {
        window.history.back();
    }
    private setCurrentPosition() {
        if ('geolocation' in navigator) {
            navigator.geolocation.getCurrentPosition((position) => {
                this.restaurant.location.latitude = position.coords.latitude;
                this.restaurant.location.longitude = position.coords.longitude;
                this.zoom = 15;
            });
        }
    }

    load(id) {
        this.restaurantService.find(id)
            .subscribe((restaurantResponse: HttpResponse<Restaurant>) => {
                this.restaurant = restaurantResponse.body;
                if (this.restaurant.logo == null) {
                    this.restaurant.logo = new Picture();
                }
                const aux: number = 5 - this.restaurant.pictures.length;
                for (let i = 0; i < aux; i++) {
                    this.restaurant.pictures.push(new Picture());
                }
                if (this.restaurant.location) {
                    this.searchControl.setValue(this.restaurant.location.formattedAddress);
                    this.zoom = 15;
                } else {
                    this.restaurant.location = new Location();
                    this.setCurrentPosition();
                }
                this.vali = true;
            });
    }
    clear() {
        this.router.navigate(['home']);
    }

    onKeydown(event) {
        event.preventDefault();
        event.stopPropagation();
    }

    save() {
        this.isSaving = true;
        this.restaurant.pictures = this.restaurant.pictures
            .filter((picture: Picture) => picture.url != null);
        if (this.restaurant.id !== undefined) {
            this.subscribeToSaveResponse(
                this.restaurantService.update(this.restaurant));
        } else {
            this.subscribeToSaveResponse(
                this.restaurantService.create(this.restaurant));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Restaurant>>) {
        result.subscribe((res: HttpResponse<Restaurant>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Restaurant) {
        this.eventManager.broadcast({ name: 'restaurantListModification', content: 'OK' });
        this.isSaving = false;
        this.restaurantService.changeCurrent(result)
            .then(() => this.router.navigate(['sector']))
            .catch((exc) => console.log(exc));
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackLocationById(index: number, item: Location) {
        return item.id;
    }

    trackRestaurantTypeById(index: number, item: RestaurantType) {
        return item.id;
    }

    trackMealClassificationById(index: number, item: MealClassification) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }

    onChooseLocation(event) {
        this.restaurant.location.latitude = event.coords.lat;
        this.restaurant.location.longitude = event.coords.lng;
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }
    addPicture(event, picture) {
        if (event.srcElement.value.length > 0) {
            const file = event.target.files[0];
            picture['path'] = 'restaurants/' + Math.random().toString(36).substring(5);
            const fileRef = this.storage.ref(picture['path']);
            const task = this.storage.upload(picture['path'], file);
            // observe percentage changes
            const uploadPercent = task.percentageChanges().subscribe(
                (value) => {
                    picture['percentage'] = value;
                }
            );

            // get notified when the download URL is available
            task.snapshotChanges().pipe(
                finalize(() => { fileRef.getDownloadURL().subscribe(
                    (url) => {
                        picture.url = url;
                    }
                );
             })
            )
           .subscribe();
        }
    }

    removPicture(item) {
        const indexArray = this.restaurant.pictures.indexOf(item);
        this.restaurant.pictures.splice(indexArray, 1);
    }

/*    ngOnDestroy() {
        this.restaurant.pictures.forEach(
            (picture: Picture) => {
                if(picture['path']){
                    this.storage.ref(picture['path']).delete();
                }
            }
        );
        if(this.restaurant.logo['path']){
            this.storage.ref(this.restaurant.logo['path']).delete();
        }
    }*/

}

@Component({
    selector: 'jhi-restaurant-popup',
    template: ''
})

export class RestaurantPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private restaurantPopupService: RestaurantPopupService
    ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.restaurantPopupService
                    .open(RestaurantDialogComponent as Component, params['id']);
            } else {
                this.restaurantPopupService
                    .open(RestaurantDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}
