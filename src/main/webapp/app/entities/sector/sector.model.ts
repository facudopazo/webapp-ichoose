import { BaseEntity } from './../../shared';
import { ClientTable } from '../client-table';

export class Sector implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public accesibility?: boolean,
        public deleteDate?: any,
        public restaurantId?: number,
        public tables?: any,
        public locationId?: number,
    ) {
        this.accesibility = false;
    }
}
