import {Injectable, Component, Inject} from '@angular/core';
import { Router } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { Sector } from './sector.model';
import { SectorService } from './sector.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Injectable()
export class SectorPopupService {
    private ngbModalRef: MatDialogRef<Component>;

    constructor(
        private datePipe: DatePipe,
        private modalService: MatDialog,
        private router: Router,
        private sectorService: SectorService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<MatDialogRef<Component>> {
        return new Promise<MatDialogRef<Component>>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }
            if (id) {
                this.sectorService.find(id)
                    .subscribe((sectorResponse: HttpResponse<Sector>) => {
                        const sector: Sector = sectorResponse.body;
                        this.ngbModalRef = this.sectorModalRef(component, sector);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.sectorModalRef(component, new Sector());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    sectorModalRef(component: any, sector: Sector): MatDialogRef<Component> {
        console.log(sector);
        const modalRef = this.modalService.open(component, {
            height: 'auto',
            width: '500px',
            data: {'sector': sector}
        });
        modalRef.afterClosed().subscribe((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
