import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { JhiEventManager } from 'ng-jhipster';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Sector } from './sector.model';
import { SectorPopupService } from './sector-popup.service';
import { SectorService } from './sector.service';

@Component({
    selector: 'jhi-sector-delete-dialog',
    templateUrl: './sector-delete-dialog.component.html'
})
export class SectorDeleteDialogComponent {

    sector: Sector;

    constructor(
        private sectorService: SectorService,
        public activeModal: MatDialogRef<SectorDeletePopupComponent>,
        private eventManager: JhiEventManager,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.sector = data.sector;
    }

    clear() {
        this.activeModal.close('cancel');
    }

    confirmDelete(id: number) {
        this.sectorService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'sectorListModification',
                content: 'Deleted an sector'
            });
            this.activeModal.close(true);
        });
    }
}

@Component({
    selector: 'jhi-sector-delete-popup',
    template: ''
})
export class SectorDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private sectorPopupService: SectorPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.sectorPopupService
                .open(SectorDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
