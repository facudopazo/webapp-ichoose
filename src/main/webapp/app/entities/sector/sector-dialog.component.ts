import {Component, OnInit, OnDestroy, Inject} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable ,  Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Sector } from './sector.model';
import { SectorPopupService } from './sector-popup.service';
import { SectorService } from './sector.service';
import { Restaurant, RestaurantService } from '../restaurant';
import { SectorLocation, SectorLocationService } from '../sector-location';

@Component({
    selector: 'jhi-sector-dialog',
    templateUrl: './sector-dialog.component.html'
})
export class SectorDialogComponent implements OnInit {

    sector: Sector;
    isSaving: boolean;
    nameFormControl: FormControl;
    sectorlocations: SectorLocation[];
    private subscription: Subscription;
    currentRestaurant: Restaurant;

    constructor(
        public activeModal: MatDialogRef<SectorPopupComponent>,
        private jhiAlertService: JhiAlertService,
        private sectorService: SectorService,
        private restaurantService: RestaurantService,
        private sectorLocationService: SectorLocationService,
        private eventManager: JhiEventManager,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private route: ActivatedRoute,
        private router: Router
    ) {
        this.sector = data.sector;
        console.log('data.sector es:');
        console.log(this.sector);
    }

    ngOnInit() {
        this.nameFormControl = new FormControl('', [
            Validators.required
        ]);
        this.isSaving = false;
        this.restaurantService.getCurrent()
            .subscribe((restaurant: Restaurant) => { this.currentRestaurant = restaurant; });
        this.sectorLocationService.query()
            .subscribe((res: HttpResponse<SectorLocation[]>) => { this.sectorlocations = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.close('cancel');
    }

    load(id) {
        this.sectorService.find(id)
            .subscribe((sectorResponse: HttpResponse<Sector>) => {
                this.sector = sectorResponse.body;
                console.log(this.sector);
            });
    }
    save() {
        this.isSaving = true;
        this.sector.restaurantId = this.currentRestaurant.id;
        if (this.sector.id !== undefined) {
            this.subscribeToSaveResponse(
                this.sectorService.update(this.sector));
        } else {
            this.subscribeToSaveResponse(
                this.sectorService.create(this.sector));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Sector>>) {
        result.subscribe((res: HttpResponse<Sector>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Sector) {
        this.eventManager.broadcast({ name: 'sectorListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.close(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackRestaurantById(index: number, item: Restaurant) {
        return item.id;
    }

    trackSectorLocationById(index: number, item: SectorLocation) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-sector-popup',
    template: ''
})
export class SectorPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private sectorPopupService: SectorPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.sectorPopupService
                    .open(SectorDialogComponent as Component, params['id']);
            } else {
                this.sectorPopupService
                    .open(SectorDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
