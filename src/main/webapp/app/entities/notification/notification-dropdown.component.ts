import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { Notification } from './notification.model';
import { NotificationService } from './notification.service';
import { ITEMS_PER_PAGE, Principal } from '../../shared';
import { MessagingService } from '../../shared/pushing/messaging.service';
import { RestaurantService, Restaurant } from '../../entities/restaurant';
import {MatSnackBar} from '@angular/material';

@Component({
    selector: 'jhi-notification-dropdown',
    templateUrl: './notification-dropdown.component.html',
    styleUrls: [
        'notification.scss'
    ]
})
export class NotificationDropdownComponent implements OnInit, OnDestroy {

    currentAccount: any;
    notifications: Notification[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    totalUnread: any;
    currentRestaurant: Restaurant;
    audio;

    constructor(
        private notificationService: NotificationService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private msgService: MessagingService,
        private restaurantService: RestaurantService,
        public snackBar: MatSnackBar
    ) {
        this.itemsPerPage = 7;
        this.totalUnread = 0;
        this.notifications = [];
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data && data.pagingParams && data.pagingParams.page ? data.pagingParams.page : 1;
            this.previousPage = this.page;
            this.reverse = false;
            this.predicate = '';
        });
    }

    loadAll(refresh?: boolean) {
        if (refresh) {
            this.page = 1;
            this.notifications = [];
        }
        if ( this.currentRestaurant && this.currentRestaurant != null) {
            this.notificationService.findByRestaurantId(this.currentRestaurant.id, {
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort()
            }).subscribe(
                (res: HttpResponse<Notification[]>) => this.onSuccess(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
        }
    }

    ngOnInit() {
        this.audio = new Audio('light.mp3');
        this.audio.load();
        this.restaurantService.getCurrent().subscribe( (restaurant: Restaurant) => {
            this.currentRestaurant = restaurant;
            this.loadAll(false);
        });
        this.msgService.receiveMessage().subscribe( (notification) => {
            this.audio.play();
            this.loadAll(true);
            this.eventManager.broadcast({ name: 'updateDashboard'});
            if ( notification != null ) {
                this.snackBar.open(notification.notification.title, 'OK', {duration: 3000}); }
            }
        );
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInNotifications();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Notification) {
        return item.id;
    }
    registerChangeInNotifications() {
        this.eventSubscriber = this.eventManager.subscribe('notificationListModification', (response) => this.loadAll(true));
    }

    markAsRead(notification, event: any): void {
        event.stopPropagation();
        notification.readDate = new Date();
        this.notificationService.markAsRead(notification.id);
        this.totalUnread--;
        this.eventManager.broadcast({ name: 'notificationsUnreadUpdate', content: this.totalUnread});
    }

    delete(notification, event: any, index: number): void {
        event.stopPropagation();
        this.notificationService.delete(notification.id).subscribe(
            (res) => {
                this.notifications.splice(index, 1);
                if (this.notifications.length < 7) {
                   this.loadAll(true);
                }
            }, (err) => {
                console.log(err);
            }
        );
    }

    seeMore(event) {
        event.stopPropagation();
        this.page++;
        this.loadAll(false);
    }

    private onSuccess(data, headers) {
        this.notifications = this.notifications.concat(data);
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.notificationService.getCountOfUnreadNotifications(this.currentRestaurant.id).subscribe(
            (res) => {
                this.totalUnread = res.body.unread;
                console.log(res);
                console.log(this.totalUnread);
                this.eventManager.broadcast({ name: 'notificationsUnreadUpdate', content: this.totalUnread});
            });
    }
    private onError(error) {
        console.log(error);
        this.jhiAlertService.error(error.message, null, null);
    }
    sort() {
        this.predicate = 'id';
        this.reverse = false;
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }
}
