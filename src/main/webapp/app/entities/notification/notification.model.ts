import { BaseEntity } from './../../shared';
import { Client } from '../client';

export enum NotificationType {
    NEW_BOOKING, BILL_REQUEST, STAY_STARTED_BY_CLIENT, ORDERS_CHANGED
}

export class Notification implements BaseEntity {
    constructor(
        public id?: number,
        public parameters?: any,
        public creationDate?: any,
        public readDate?: any,
        public type?: NotificationType,
        public restaurantId?: number,
        public clientId?: number,
        public client?: Client,
    ) {
    }
}
