import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Notification } from './notification.model';
import { createRequestOption } from '../../shared';
import { Client } from '../client';

export type EntityResponseType = HttpResponse<Notification>;

@Injectable()
export class NotificationService {

    private resourceUrl =  SERVER_API_URL + 'ichooseapi/api/notifications';
    private resourceSearchUrl = SERVER_API_URL + 'ichooseapi/api/_search/notifications';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(notification: Notification): Observable<EntityResponseType> {
        const copy = this.convert(notification);
        return this.http.post<Notification>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(notification: Notification): Observable<EntityResponseType> {
        const copy = this.convert(notification);
        return this.http.put<Notification>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    markAsRead(id: any) {
        this.http.put<any>(SERVER_API_URL + `ichooseapi/api/notifications/${id}/read`, { observe: 'response' })
            .subscribe((res) => console.log(res), (err) => console.log(err));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Notification>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    findByRestaurantId(restaurantId: number, req?: any) {
        const options = createRequestOption(req);
        const url = SERVER_API_URL + `ichooseapi/api/restaurants/${restaurantId}/notifications`;
        return this.http.get<Notification[]>(url, { params: options, observe: 'response' })
            .map((res: HttpResponse<Notification[]>) => this.convertArrayResponse(res));

    }

    query(req?: any): Observable<HttpResponse<Notification[]>> {
        const options = createRequestOption(req);
        return this.http.get<Notification[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Notification[]>) => this.convertArrayResponse(res));
    }

    delete(id: number) {
        return this.http.delete<any>(SERVER_API_URL + `ichooseapi/api/notifications/${id}`, { observe: 'response'});
    }

    getCountOfUnreadNotifications(restaurantId: number): Observable<any> {
        const url = SERVER_API_URL + `ichooseapi/api/restaurants/${restaurantId}/notifications/unread`;
        return this.http.get(url, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<Notification[]>> {
        const options = createRequestOption(req);
        return this.http.get<Notification[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Notification[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Notification = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Notification[]>): HttpResponse<Notification[]> {
        const jsonResponse: Notification[] = res.body;
        const body: Notification[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Notification.
     */
    private convertItemFromServer(notification: Notification): Notification {
        const copy: Notification = Object.assign({}, notification);
        copy.client = notification.client ? notification.client : new Client();
        try {
            copy.parameters = notification.parameters.split(',');
        } catch (exc) {
            console.log('El campo parámetro vino null');
        }
        copy.creationDate = this.dateUtils
            .convertDateTimeFromServer(notification.creationDate);
        copy.readDate = this.dateUtils
            .convertDateTimeFromServer(notification.readDate);
        try {
        if ( copy.parameters[1].includes('UTC') || copy.parameters[1].includes('Zulu')) {
            copy.parameters[1] = copy.parameters[1].replace('[UTC]', '');
            copy.parameters[1] = copy.parameters[1].replace('[Zulu]', '');
            copy.parameters[1] = this.dateUtils.convertDateTimeFromServer(copy.parameters[1]);
       }
        } catch (e) {
            console.log(e);
        }
        return copy;
    }

    /**
     * Convert a Notification to a JSON which can be sent to the server.
     */
    private convert(notification: Notification): Notification {
        const copy: Notification = Object.assign({}, notification);
        copy.parameters = notification.parameters.join(',');
        return copy;
    }
}
