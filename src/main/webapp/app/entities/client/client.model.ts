import { BaseEntity } from './../../shared';
import { Nationality } from '../nationality';
import { Picture } from '../picture';

export class Client implements BaseEntity {
    constructor(
        public id?: number,
        public telephoneNumber?: string,
        public birthdate?: any,
        public firstName?: string,
        public lastName?: string,
        public email?: string,
        public userId?: number,
        public locationId?: number,
        public pictureId?: number,
        public nationalityId?: number,
        public stayHistories?: BaseEntity[],
        public nationality?: Nationality,
        public picture?: Picture
    ) {
    }
}
