import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { HttpResponse } from '@angular/common/http';
import { ClientGroup } from './client-group.model';
import { Client } from '../client';
import { ClientGroupService } from './client-group.service';

@Injectable()
export class ClientGroupPopupService {
    private ngbModalRef: MatDialogRef<Component>;

    constructor(
        private router: Router,
        private modalService: MatDialog,
        private clientGroupService: ClientGroupService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any, people?: Client[] | any): Promise<MatDialogRef<Component>> {
        return new Promise<MatDialogRef<Component>>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }
            if (id) {
                this.clientGroupService.find(id)
                    .subscribe((clientGroupResponse: HttpResponse<ClientGroup>) => {
                        const clientGroup: ClientGroup = clientGroupResponse.body;
                        this.ngbModalRef = this.clientGroupModalRef(component, clientGroup);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const clientGroup = new ClientGroup();
                    clientGroup.people = [];
                    if (people) {
                        clientGroup.people = people;
                    }
                    this.ngbModalRef = this.clientGroupModalRef(component, clientGroup);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    clientGroupModalRef(component: any, clientGroup: ClientGroup): MatDialogRef<Component> {
        const modalRef = this.modalService.open(component, {
            height: 'Auto',
            width: '500px', data: { 'clientGroup': clientGroup }
        });
        modalRef.afterClosed().subscribe((result) => {
            this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
