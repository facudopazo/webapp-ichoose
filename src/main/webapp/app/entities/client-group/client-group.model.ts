import { BaseEntity } from './../../shared';
import { PromotionShipping } from './../promotion-shipping';
import { Client } from './../client';

export class ClientGroup implements BaseEntity {

    constructor(
        public id?: number,
        public name?: string,
        public promotionsShipping?: PromotionShipping,
        public clientId?: number,
        public restaurantId?: number,
        public people?: Client[],
    ) {

    }
}
