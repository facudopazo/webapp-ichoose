import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { ClientGroup } from './client-group.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<ClientGroup>;

@Injectable()
export class ClientGroupService {

    private resourceUrl =  SERVER_API_URL + 'ichooseapi/api/client-groups';
    private resourceSearchUrl = SERVER_API_URL + 'ichooseapi/api/_search/client-groups';

    constructor(private http: HttpClient) { }

    create(clientGroup: ClientGroup): Observable<EntityResponseType> {
        const copy = this.convert(clientGroup);
        return this.http.post<ClientGroup>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(clientGroup: ClientGroup): Observable<EntityResponseType> {
        const copy = this.convert(clientGroup);
        return this.http.put<ClientGroup>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ClientGroup>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ClientGroup[]>> {
        const options = createRequestOption(req);
        return this.http.get<ClientGroup[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ClientGroup[]>) => this.convertArrayResponse(res));
    }

    queryByRestaurant(restaurantId: number, req?: any): Observable<HttpResponse<ClientGroup[]>> {
        const options = createRequestOption(req);
        const url = SERVER_API_URL + 'ichooseapi/api/restaurants/' + restaurantId + '/client-groups';
        return this.http.get<ClientGroup[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ClientGroup[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<ClientGroup[]>> {
        const options = createRequestOption(req);
        return this.http.get<ClientGroup[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ClientGroup[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ClientGroup = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ClientGroup[]>): HttpResponse<ClientGroup[]> {
        const jsonResponse: ClientGroup[] = res.body;
        const body: ClientGroup[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ClientGroup.
     */
    private convertItemFromServer(clientGroup: ClientGroup): ClientGroup {
        const copy: ClientGroup = Object.assign({}, clientGroup);
        return copy;
    }

    /**
     * Convert a ClientGroup to a JSON which can be sent to the server.
     */
    private convert(clientGroup: ClientGroup): ClientGroup {
        const copy: ClientGroup = Object.assign({}, clientGroup);
        return copy;
    }
}
