import { Component, OnInit, OnDestroy, Inject} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { JhiEventManager } from 'ng-jhipster';

import { ClientGroup } from './client-group.model';
import { ClientGroupPopupService } from './client-group-popup.service';
import { ClientGroupService } from './client-group.service';
import { Restaurant, RestaurantService } from '../restaurant';

@Component({
    selector: 'jhi-client-group-dialog',
    templateUrl: './client-group-dialog.component.html'
})
export class ClientGroupDialogComponent implements OnInit {

    clientGroup: ClientGroup;
    isSaving: boolean;
    currentRestaurant: Restaurant;

    constructor(
        public activeModal: MatDialogRef<ClientGroupPopupComponent>,
        private clientGroupService: ClientGroupService,
        private eventManager: JhiEventManager,
        private restaurantService: RestaurantService,
        public router: Router,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.clientGroup = data.clientGroup;
    }

    ngOnInit() {
        this.isSaving = false;
        this.restaurantService.getCurrent().subscribe((restaurant: Restaurant) => this.currentRestaurant = restaurant);
    }

    clear() {
        this.activeModal.close('cancel');
    }

    save() {
        this.isSaving = true;
        this.clientGroup.restaurantId = this.currentRestaurant.id;
        if (this.clientGroup.id !== undefined) {
            this.subscribeToSaveResponse(
                this.clientGroupService.update(this.clientGroup));
        } else {
            this.subscribeToSaveResponse(
                this.clientGroupService.create(this.clientGroup));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ClientGroup>>) {
        result.subscribe((res: HttpResponse<ClientGroup>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ClientGroup) {
        this.eventManager.broadcast({ name: 'clientGroupListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.close('cancel');
        this.router.navigate(['../client-group', result.id ]);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-client-group-popup',
    template: ''
})
export class ClientGroupPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientGroupPopupService: ClientGroupPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.clientGroupPopupService
                    .open(ClientGroupDialogComponent as Component, params['id']);
            } else {
                this.clientGroupPopupService
                    .open(ClientGroupDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
