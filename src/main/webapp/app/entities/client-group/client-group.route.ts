import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { ClientGroupComponent } from './client-group.component';
import { ClientGroupDetailComponent } from './client-group-detail.component';
import { ClientGroupPopupComponent } from './client-group-dialog.component';
import { ClientGroupDeletePopupComponent } from './client-group-delete-dialog.component';

@Injectable()
export class ClientGroupResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const clientGroupRoute: Routes = [
    {
        path: 'client-group',
        component: ClientGroupComponent,
        resolve: {
            'pagingParams': ClientGroupResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.clientGroup.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'client-group/:id',
        component: ClientGroupDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.clientGroup.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const clientGroupPopupRoute: Routes = [
    {
        path: 'client-group-new',
        component: ClientGroupPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.clientGroup.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-group/:id/edit',
        component: ClientGroupPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.clientGroup.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-group/:id/delete',
        component: ClientGroupDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.clientGroup.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
