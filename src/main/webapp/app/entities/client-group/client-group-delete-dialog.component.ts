import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { JhiEventManager } from 'ng-jhipster';

import { ClientGroup } from './client-group.model';
import { ClientGroupPopupService } from './client-group-popup.service';
import { ClientGroupService } from './client-group.service';

@Component({
    selector: 'jhi-client-group-delete-dialog',
    templateUrl: './client-group-delete-dialog.component.html'
})
export class ClientGroupDeleteDialogComponent {

    clientGroup: ClientGroup;

    constructor(
        private clientGroupService: ClientGroupService,
        public activeModal: MatDialogRef<ClientGroupDeletePopupComponent>,
        private eventManager: JhiEventManager,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.clientGroup = data.clientGroup;
    }

    clear() {
        this.activeModal.close('cancel');
    }

    confirmDelete(id: number) {
        this.clientGroupService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'clientGroupListModification',
                content: 'Deleted a clientGroup'
            });
            this.activeModal.close(true);
        });
    }
}

@Component({
    selector: 'jhi-client-group-delete-popup',
    template: ''
})
export class ClientGroupDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientGroupPopupService: ClientGroupPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.clientGroupPopupService
                .open(ClientGroupDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
