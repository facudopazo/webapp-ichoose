import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IchooseSharedModule } from '../../shared';

import { FlexLayoutModule } from '@angular/flex-layout';
import {
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatDividerModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatStepperModule
  } from '@angular/material';

import {
    ClientGroupService,
    ClientGroupPopupService,
    ClientGroupComponent,
    ClientGroupDetailComponent,
    ClientGroupDialogComponent,
    ClientGroupPopupComponent,
    ClientGroupDeletePopupComponent,
    ClientGroupDeleteDialogComponent,
    clientGroupRoute,
    clientGroupPopupRoute,
    ClientGroupResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...clientGroupRoute,
    ...clientGroupPopupRoute,
];

@NgModule({
    imports: [
        IchooseSharedModule,
        MatInputModule,
        MatAutocompleteModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatCardModule,
        MatCheckboxModule,
        MatChipsModule,
        MatDatepickerModule,
        MatDialogModule,
        MatExpansionModule,
        MatGridListModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatNativeDateModule,
        MatPaginatorModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        MatRadioModule,
        MatRippleModule,
        MatSelectModule,
        MatSidenavModule,
        MatSliderModule,
        MatSlideToggleModule,
        MatSnackBarModule,
        MatDividerModule,
        MatSortModule,
        MatTableModule,
        MatTabsModule,
        MatToolbarModule,
        MatTooltipModule,
        MatStepperModule,
        FlexLayoutModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ClientGroupComponent,
        ClientGroupDetailComponent,
        ClientGroupDialogComponent,
        ClientGroupDeleteDialogComponent,
        ClientGroupPopupComponent,
        ClientGroupDeletePopupComponent,
    ],
    entryComponents: [
        ClientGroupComponent,
        ClientGroupDialogComponent,
        ClientGroupPopupComponent,
        ClientGroupDeleteDialogComponent,
        ClientGroupDeletePopupComponent,
    ],
    providers: [
        ClientGroupService,
        ClientGroupPopupService,
        ClientGroupResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IchooseClientGroupModule {}
