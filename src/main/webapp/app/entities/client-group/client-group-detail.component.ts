import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { Router } from '@angular/router';
import { ClientService, Client } from '../client';
import { ClientGroup } from './client-group.model';
import { ClientGroupService } from './client-group.service';
import { StayHistoryService, StayHistory } from '../stay-history';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';
import { Restaurant, RestaurantService } from '../restaurant';

@Component({
    selector: 'jhi-client-group-detail',
    templateUrl: './client-group-detail.component.html',
    styleUrls: [
        'client-group.scss'
    ]
})
export class ClientGroupDetailComponent implements OnInit, OnDestroy {

    currentRestaurant: Restaurant;
    clientGroup: ClientGroup;
    private subscription: Subscription;
    private eventSubscriber: Subscription;
    editMode: Boolean;
    clientsToAdd: Observable<any>;
    clients: Client[];
    clientCtrl: FormControl;
    selectedClient: Client;
    isSaving: Boolean;

    constructor(
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private clientGroupService: ClientGroupService,
        private clientService: ClientService,
        private stayHistoryService: StayHistoryService,
        private restaurantService: RestaurantService,
        private route: ActivatedRoute,
        public router: Router
    ) {
        this.clientCtrl = new FormControl('', [
            Validators.required
        ]);
        this.isSaving = false;
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInClientGroups();
        this.editMode = false;
        this.restaurantService.getCurrent().subscribe((restaurant: Restaurant) => {
            this.currentRestaurant = restaurant;
            this.stayHistoryService.queryByRestaurant(this.currentRestaurant.id, {size: 1000})
            .subscribe((res: HttpResponse<StayHistory[]>) => {
                this.clients = this.filterClientsNotInGroup(res.body.map((stayHistory: StayHistory) => (stayHistory['client'])));
                this.clientsToAdd = this.clientCtrl.valueChanges
                    .pipe(
                        startWith<string | Client>(''),
                        map((value) => typeof value === 'string' ? value : value.firstName),
                        map((client) => client ? this.filterClients(client) : this.clients.slice())
                    );
            }, (res: HttpErrorResponse) => this.onError(res.message));

        });
    }

    cancel() {
        this.router.navigate(['/client-group']);
    }

    agregarCliente() {
        if (this.selectedClient.id) {
            this.clientGroup.people.push(this.selectedClient);
            this.selectedClient = new Client;
            this.clients = this.filterClientsNotInGroup(this.clients);
        }
    }

    removePeople(client) {
        const clientsAux = this.clientGroup.people.map( (a) => a.id);
        if (clientsAux.indexOf(client.id) >= 0) {
            this.clientGroup.people.splice(clientsAux.indexOf(client.id), 1);
        }
    }

    displayFn(client?: Client): string | undefined {
        return client && client.firstName ? client.firstName + ' ' + client.lastName : client['login'];
    }

    filterClientsNotInGroup(allClients) {
        const keys = this.clientGroup.people.map( (a) => a.id);
        const list = [];
        allClients.forEach(function(client) {
            if (keys.indexOf(client.id) < 0) {
                list.push(client);
            }
        });
        return list;
    }

    filterClients(name: string) {
        const keys = Array.from(this.clientGroup.people.keys());
        return this.clients.filter((client) =>
            // filtra que no este dentro de los clientes del grupo, y que coincida con el nombre
            keys.indexOf(client.id) < 0 && client.firstName && client.firstName.toLowerCase().indexOf(name.toLowerCase()) === 0);
    }

    save() {
        this.isSaving = true;
        if (this.clientGroup.id !== undefined) {
            this.subscribeToSaveResponse(
                this.clientGroupService.update(this.clientGroup));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ClientGroup>>) {
        result.subscribe((res: HttpResponse<ClientGroup>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ClientGroup) {
        this.eventManager.broadcast({ name: 'clientGroupListModification', content: 'OK' });
        this.isSaving = false;
        this.router.navigate(['/client-group']);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    activateEdit() {
        this.editMode = true;
    }

    load(id) {
        this.clientGroupService.find(id)
            .subscribe((clientGroupResponse: HttpResponse<ClientGroup>) => {
                this.clientGroup = clientGroupResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInClientGroups() {
        this.eventSubscriber = this.eventManager.subscribe(
            'clientGroupListModification',
            (response) => this.load(this.clientGroup.id)
        );
    }
}
