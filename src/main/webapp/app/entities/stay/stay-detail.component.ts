import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { StayPopupService } from './stay-popup.service';
import { MatTableDataSource } from '@angular/material';
import { Stay } from './stay.model';
import { StayService } from './stay.service';
import { StayOrder, StayOrderService } from '../stay-order';

@Component({
    selector: 'jhi-stay-detail',
    templateUrl: './stay-detail.component.html',
    styleUrls: [
        'stay.scss'
    ]
})
export class StayDetailComponent implements OnInit, OnDestroy {
    displayedColumns = ['Nombre', 'Cantidad', 'Precio unitario', 'Precio total'];
    discountTable = ['Nombre', 'Descuento'];

    stay: Stay;
    dataSource: any;
    discountData: any;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private stayService: StayService,
        private route: ActivatedRoute,
        public activeModal: MatDialogRef<StayPopupDetailComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.stay = data.stay;
        this.dataSource = new MatTableDataSource();
        this.discountData = new MatTableDataSource();
    }

    ngOnInit() {

        this.reCalculateStayAmount(this.stay);
         this.dataSource.data = this.stay.orders;
         this.discountData.data = this.stay.discounts;
        this.registerChangeInStays();
    }

    clear() {
        this.activeModal.close('cancel');
    }

    load(id) {
        this.stayService.find(id)
            .subscribe((stayResponse: HttpResponse<Stay>) => {
                this.stay = stayResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    reCalculateStayAmount(stay: Stay) {
        let total = 0;
        stay.orders.forEach( (order) => {
            total += this.calculateStayOrderAmount(order);
        });
        stay.amount = total;
    }

    calculateStayOrderAmount( stayOrder: StayOrder) {
        return stayOrder.quantity * stayOrder.price;
    }

     ngOnDestroy() {
        //  this.subscription.unsubscribe();
         this.eventManager.destroy(this.eventSubscriber);
     }

    registerChangeInStays() {
        this.eventSubscriber = this.eventManager.subscribe(
            'stayListModification',
            (response) => this.load(this.stay.id)
        );
    }
}

@Component({
    selector: 'jhi-stay-popup',
    template: ''
})
export class StayPopupDetailComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private stayPopupService: StayPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.stayPopupService
                    .open(StayDetailComponent as Component, params['id']);
            } else {
                this.stayPopupService
                    .open(StayDetailComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
