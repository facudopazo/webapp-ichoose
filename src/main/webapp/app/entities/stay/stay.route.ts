import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { StayComponent } from './stay.component';
import { StayDetailComponent } from './stay-detail.component';
import { StayPopupDetailComponent } from './stay-detail.component';
import { StayPopupComponent } from './stay-dialog.component';
import { StayDeletePopupComponent } from './stay-delete-dialog.component';
import { StayOrdersPopupComponent } from './stay-orders-dialog.component';
import { StayFinishPopupComponent } from './stay-finish-dialog.component';
import { StayDashboardComponent } from './stay-dashboard.component';
import { TaxBillPopupComponent } from '../tax-bill';
import {StayBookingDialogPopupComponent} from './stay-booking-dialog.component';

@Injectable()
export class StayResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const stayRoute: Routes = [
    {
        path: 'stay',
        component: StayComponent,
        resolve: {
            'pagingParams': StayResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.stay.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'stay-dashboard',
        component: StayDashboardComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.stay.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const stayPopupRoute: Routes = [
    {
        path: 'stay-new',
        component: StayPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.stay.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
    path: 'stay/:id',
    component: StayPopupDetailComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'ichooseApp.stay.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
    },
    {
        path: 'stay/:id/edit',
        component: StayPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.stay.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'stay/:id/taxbill',
        component: TaxBillPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.stay.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'stay/:id/orders',
        component: StayOrdersPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.stay.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'stay/:id/finish',
        component: StayFinishPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.stay.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'stay/:id/delete',
        component: StayDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.stay.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'bookings/:bookingId/stay',
        component: StayBookingDialogPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.stay.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
