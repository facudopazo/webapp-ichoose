import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Stay } from './stay.model';
import { StayPopupService } from './stay-popup.service';
import { StayService } from './stay.service';

@Component({
    selector: 'jhi-stay-delete-dialog',
    templateUrl: './stay-delete-dialog.component.html',
    styleUrls: [
        'stay.scss'
    ]
})
export class StayDeleteDialogComponent {

    stay: Stay;

    constructor(
        private stayService: StayService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.stayService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'stayListModification',
                content: 'Deleted an stay'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-stay-delete-popup',
    template: ''
})
export class StayDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private stayPopupService: StayPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.stayPopupService
                .open(StayDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
