import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IchooseSharedModule } from '../../shared';
import {TaxBillPrinterService} from '../tax-bill/tax-bill-printer.service';
import {
    StayService,
    StayPopupService,
    StayComponent,
    StayDetailComponent,
    StayDialogComponent,
    StayOrdersDialogComponent,
    StayFinishDialogComponent,
    StayDeletePopupComponent,
    StayDeleteDialogComponent,
    StayPopupDetailComponent,
    StayPopupComponent,
    stayRoute,
    stayPopupRoute,
    StayResolvePagingParams,
    StayFinishPopupComponent,
    StayDashboardComponent,
    StayBookingDialogComponent,
    StayBookingDialogPopupComponent
} from './';
import { StayOrdersPopupComponent } from './stay-orders-dialog.component';
const ENTITY_STATES = [
    ...stayRoute,
    ...stayPopupRoute,
];

@NgModule({
    imports: [
        IchooseSharedModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(ENTITY_STATES),

    ],
    declarations: [
        StayComponent,
        StayDetailComponent,
        StayDialogComponent,
        StayDeleteDialogComponent,
        StayPopupDetailComponent,
        StayPopupComponent,
        StayDetailComponent,
        StayDeletePopupComponent,
        StayOrdersPopupComponent,
        StayOrdersDialogComponent,
        StayFinishDialogComponent,
        StayFinishPopupComponent,
        StayDashboardComponent,
        StayBookingDialogComponent,
        StayBookingDialogPopupComponent
    ],
    entryComponents: [
        StayComponent,
        StayDialogComponent,
        StayPopupDetailComponent,
        StayPopupComponent,
        StayDetailComponent,
        StayDeleteDialogComponent,
        StayDeletePopupComponent,
        StayOrdersPopupComponent,
        StayOrdersDialogComponent,
        StayFinishDialogComponent,
        StayFinishPopupComponent,
        StayDashboardComponent,
        StayBookingDialogComponent,
        StayBookingDialogPopupComponent
    ],
    providers: [
        StayService,
        StayPopupService,
        StayResolvePagingParams,
        TaxBillPrinterService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IchooseStayModule {}
