import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Sector } from '../sector/sector.model';
import { SectorService } from '../sector/sector.service';
import { ClientTableService } from '../client-table/client-table.service';
import { Restaurant, RestaurantService } from '../restaurant';

import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { ITEMS_PER_PAGE, Principal } from '../../shared';
import { ClientTable } from '../client-table';
import { StayService, Stay } from '.';
import { BookingService, Booking } from '../booking';
import { Subscription } from 'rxjs/Subscription';
import { ShiftService, Shift } from '../shift';

@Component({
    selector: 'jhi-stay-dashboard',
    templateUrl: './stay-dashboard.component.html',
    styleUrls: [
        'stay.scss'
    ]
})
export class StayDashboardComponent implements OnInit, OnDestroy {
    currentTable: ClientTable = null;
    currentBooking: Booking = null;
    currentStay: Stay = null;
    seconds: string;
    minutes: string;
    currentRestaurant: Restaurant;
    sectors: Sector[] = [];
    tables: ClientTable[] = [];
    activeStays: Stay[] = [];
    activeBookings: Booking[] = [];

    eventSubscribers: Subscription[] = new Array<Subscription>();
    intervalTimerUpdater: any;

    constructor(private sectorService: SectorService,
        private jhiAlertService: JhiAlertService,
        private restaurantService: RestaurantService,
        private clientTableService: ClientTableService,
        private stayService: StayService,
        private bookingService: BookingService,
        private eventManager: JhiEventManager,
        private shiftService: ShiftService
    ) { }

    /**
     * Busca el restaurante actual y luego carga los sectores, mesas e inicia
     * el actualizador de los segunderos de las estadías en curso.
     * Queda suscripto al observable del restaurante actual, por lo que se
     * ejecutará cada vez que se actualice el mismo.
     * También se suscribe a los eventos de actualización de estadías y reservas
     * y planifica el próximo reload de reservas activas.
     */
    ngOnInit() {
        this.restaurantService.getCurrent()
            .subscribe((restaurant: Restaurant) => {
                if (restaurant) {
                    this.currentRestaurant = restaurant;
                    this.loadSectors();
                    this.startTimerUpdate();
                }
            });

        this.subscribeToEvents();
        // this.scheduleReload();

    }

    /**
     * Buscar los sectores del restaurante actual y los configura a todos como visibles.
     */
    private loadSectors() {
        this.sectorService.queryByRestaurant(this.currentRestaurant.id).subscribe(
            (res: HttpResponse<Sector[]>) => {
                this.sectors = res.body;
                this.sectors.forEach((sector: Sector) => sector['visible'] = true);
                this.loadTables();
            }
        );
    }

    /**
     * Busca todas las mesas del restaurante actual y luego:
     * 1) Llama al método que agrupa las mesas por sector.
     * 2) Busca las estadías vigentes.
     * 3) Busca las reservas actuales.
     */
    private loadTables() {
        this.clientTableService.findByRestaurantId(this.currentRestaurant.id).subscribe(
            (res: HttpResponse<ClientTable[]>) => {
                this.tables = res.body;
                this.groupTablesBySector();
                if (this.currentTable) {
                    this.changeCurrentTable(
                        this.tables.find((table: ClientTable) => table.id === this.currentTable.id)
                    );
                }
                // this.loadStays();
                // this.loadBookings();
            }
        );

    }

    /**
     * Por cada sector busca las mesas y se las agrega al sector.
     */
    private groupTablesBySector() {
        this.sectors.forEach(
            (sector: Sector) =>
                sector.tables = this.tables.filter((table: ClientTable) => table.sectorId === sector.id)

        );
    }

    /**
     * Programa el nuevo reload de estadías y reservas, en base a la fecha y hora de cierre del turno actual.
     */
    private scheduleReload() {
        this.shiftService.query().subscribe(
            (res: HttpResponse<Shift[]>) => {
                const shifts: Shift[] = res.body;
                const today: Date = new Date();
                const timeToExpire: number = shifts[0].end.getTime() - today.getTime() + 30000; // margen de 30 seg
                setTimeout(() => {
                    this.scheduleReload();
                }, timeToExpire);
            }
        );
    }
    /** Se suscribe a los eventos de actualización/creación de estadías y reservas.
     * Por cada estadía o reserva actualizada/creada lo agrega/modifica en la lista
     * de reservas o estadías. También modifica la mesa asociada.
     */
    private subscribeToEvents() {
        let sub = this.eventManager.subscribe('bookingModification',
            (booking: Booking) => {
                console.log('new booking modification event', booking);
                this.loadTables();
            }
        );
        this.eventSubscribers.push(sub);
        sub = this.eventManager.subscribe('stayModification',
            (stay: Stay) => {
                this.loadTables();
            }
        );
        this.eventSubscribers.push(sub);
        sub = this.eventManager.subscribe('updateDashboard',
            (response) => {
                this.loadTables();
            });
        this.eventSubscribers.push(sub);
    }

    /**
     *
     * @param event el sector que se desea visualizar. Si es null, se muestran todos.
     */
    changeVisibilityOnSectors(event: any) {
        let sector: Sector = null;
        if (event) {
            sector = this.sectors[event.index - 1];
        }
        if (sector != null) {
            sector['visible'] = true;
            this.sectors.filter((sectorAux: Sector) => sectorAux.id !== sector.id).forEach(
                (sectorIf: Sector) => {
                    sectorIf['visible'] = false;
                }
            );
        } else { // mostrar todos
            this.sectors.forEach(
                (sectorElse: Sector) => {
                    sectorElse['visible'] = true;
                }
            );
        }
    }

    billRequest() {
        this.currentTable.status = 'BILL_REQUESTED';
        this.clientTableService.update(this.currentTable).subscribe((res ) => {
        }, (error: MessageEvent) => {
            console.log(error);
        });
    }

    getTables(sector) {
        return sector.tables;
    }

    /**
     * Retorna la cantidad de mesas de un sector que están en un determinado estado.
     * @param sector el sector al cual se le desea contar la cantidad de mesas.
     * @param status el estado que deben tener las mesas.
     */
    getNumberOfTablesWithStatus(sector: Sector, status: String): number {
        return sector.tables && typeof sector.tables === 'object' ? sector.tables.filter((table: ClientTable) => table.status === status).length : 0;
    }

    ngOnDestroy() {
        this.eventSubscribers.forEach(
            (sub: Subscription) => {
                sub.unsubscribe();
            }
        );
        if (this.intervalTimerUpdater) {
            clearInterval(this.intervalTimerUpdater);
        }
    }

    private onSuccess(data, headers) {
        this.sectors = data;
        this.sectors.forEach((sector: Sector) =>
            this.clientTableService.findBySectorId(sector.id).subscribe(
                (res: HttpResponse<Sector[]>) => { sector['tables'] = res.body; },
                (res: HttpErrorResponse) => this.onError(res.message)
            )
        );
    }
    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    changeCurrentTable(table: ClientTable) {
        const Options = {
            sort: ['dateTime,desc']
        };
        this.currentTable = null;
        this.currentStay = null;
        this.currentBooking = null;
        this.stopTimerUpdate();
        if (table.status === 'BUSY' || table.status === 'BILL_REQUESTED') { // Si la mesa está ocupada, buscamos la estadía asociada y arrancamos el timer
            this.stayService.findCurrentStayByTableId(table.id).subscribe(
                (res: HttpResponse<Stay>) => {
                    this.currentStay = res.body;
                    this.startTimerUpdate();
                    this.currentTable = table;
                }
            );
        }else if (table.status === 'IN_BOOKING') { // Si la mesa está reservada, buscamos la reserva asociada y arrancamos el timer
            this.bookingService.findCurrentBookingsByRestaurantId(this.currentRestaurant.id, Options).subscribe(
                (res: HttpResponse<Booking[]>) => {
                    this.activeBookings = res.body;
                    this.currentBooking = this.activeBookings.find((mesa: ClientTable) => table.id === this.currentTable.id);
                });
            this.startTimerUpdate();
            this.currentTable = table;
        }else {
            this.currentTable = table;
        }

    }

    /**
     * Actualizar el tiempo transcurrido de cada estadía.
     * Se ejecuta cada un segundo.
     */
    startTimerUpdate() {
        this.intervalTimerUpdater = setInterval(() => {
            const now: Date = new Date();
            const moment: Date = this.currentStay ? this.currentStay.init : this.currentBooking ? this.currentBooking.dateTime : null;
            this.minutes = moment ? ((now.getTime() - moment.getTime()) / 1000 / 60).toFixed(0) : '0';
            this.seconds = moment ? ((now.getTime() - moment.getTime()) / 1000 % 60).toFixed(0) : '0';
        }, 1000);
    }

    stopTimerUpdate() {
        clearInterval(this.intervalTimerUpdater);
    }
}
