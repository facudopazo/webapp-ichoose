import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Stay } from './stay.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Stay>;

@Injectable()
export class StayService {

    private resourceUrl =  SERVER_API_URL + 'ichooseapi/api/stays';
    private resourceSearchUrl = SERVER_API_URL + 'ichooseapi/api/_search/stays';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(stay: Stay): Observable<EntityResponseType> {
        const copy = this.convert(stay);
        return this.http.post<Stay>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }
    createByBookingId(bookingId: number): Observable<EntityResponseType> {
        const stay = new Stay;
        const copy = this.convert(stay);
        const resourceUrl = SERVER_API_URL + 'ichooseapi/api/bookings';
        return this.http.post<Stay>(`${resourceUrl}/${bookingId}/stay`, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }
    update(stay: Stay): Observable<EntityResponseType> {
        const copy = this.convert(stay);
        return this.http.put<Stay>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Stay>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Stay[]>> {
        const options = createRequestOption(req);
        return this.http.get<Stay[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Stay[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<Stay[]>> {
        const options = createRequestOption(req);
        return this.http.get<Stay[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Stay[]>) => this.convertArrayResponse(res));
    }

    findCurrentStaysByRestaurantId(restaurantId: any): Observable<HttpResponse<Stay[]>> {
        const resourceUrl =  `${SERVER_API_URL}ichooseapi/api/restaurants/${restaurantId}/stays/current`;
        // TODO sacar el 'this.' y dejar solo el 'resourceUrl' en el return
        return this.http.get<Stay[]>(resourceUrl, { observe: 'response' })
            .map((res: HttpResponse<Stay[]>) => this.convertArrayResponse(res));

    }
    findFinalizedStaysByRestaurantId(restaurantId: number , req?: any): Observable<HttpResponse<Stay[]>> {
        const resourceUrl =  `${SERVER_API_URL}ichooseapi/api/restaurants/${restaurantId}/stays/finalized`;
        const options = createRequestOption(req);
        return this.http.get<Stay[]>(resourceUrl, { observe: 'response' })
            .map((res: HttpResponse<Stay[]>) => this.convertArrayResponse(res));
    }

    findCurrentStayByTableId(tableId: number) {
        const resourceUrl = SERVER_API_URL + 'ichooseapi/api/client-tables/' + tableId + '/current-stay';
        return this.http.get<Stay>(resourceUrl, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    end(id: number): Observable<EntityResponseType> {
        const resourceUrl =  `${SERVER_API_URL}ichooseapi/api/stays/${id}/end`;
        return this.http.post<Stay>(resourceUrl, {}, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Stay = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Stay[]>): HttpResponse<Stay[]> {
        const jsonResponse: Stay[] = res.body;
        const body: Stay[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Stay.
     */
    private convertItemFromServer(stay: Stay): Stay {
        const copy: Stay = Object.assign({}, stay);
        copy.init = this.dateUtils
            .convertDateTimeFromServer(stay.init);
        copy.end = this.dateUtils
            .convertDateTimeFromServer(stay.end);
        return copy;
    }

    /**
     * Convert a Stay to a JSON which can be sent to the server.
     */
    private convert(stay: Stay): Stay {
        const copy: Stay = Object.assign({}, stay);

        // copy.init = this.dateUtils.toDate(stay.init);

        // copy.end = this.dateUtils.toDate(stay.end);
        return copy;
    }
}
