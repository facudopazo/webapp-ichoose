import { BaseEntity } from './../../shared';
import { ClientTable } from './../client-table';
import { Client } from './../client';
import { Menu } from './../menu';
import { StayOrder } from '../stay-order';
import { Discount } from './../discount';
import { TaxBill } from '../tax-bill';

export class Stay implements BaseEntity {
    constructor(
        public id?: number,
        public amount?: number,
        public people?: number,
        public init?: any,
        public end?: any,
        public discount?: number,
        public tableId?: number,
        public ownerId?: number,
        public restaurantId?: number,
        public table?: ClientTable,
        // public orders?: BaseEntity[],
        public orders?: StayOrder[],
        public discounts?: Discount[],
        public taxBills?: TaxBill[],
        public diners?: Client []
    ) {
        this.table = new ClientTable();
        this.init = new Date();
        this.amount = 0;
    }
}
