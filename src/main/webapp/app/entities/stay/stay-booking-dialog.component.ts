import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { Stay } from './stay.model';
import { Booking } from '../booking';
import { StayPopupService } from './stay-popup.service';
import { StayService } from './stay.service';
import { ClientTable, ClientTableService } from '../client-table';
import { JhiEventManager } from 'ng-jhipster';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

@Component({
    selector: 'jhi-stay-dialog',
    templateUrl: './stay-booking-dialog.component.html',
    styleUrls: [
        'stay.scss'
    ]
})
export class StayBookingDialogComponent implements OnInit {

    stay: Stay;
    isSaving: boolean;
    tableCtrl: FormControl;
    tableValidator: FormControl;
    peopleCtrl: FormControl;
    dateCtrl: FormControl;
    filteredTables: Observable<any[]>;
     booking: Booking;

    clienttables: ClientTable[];

    constructor(
       private stayService: StayService,
        public activeModal: MatDialogRef<StayBookingDialogPopupComponent>,
       private route: ActivatedRoute,
        private eventManager: JhiEventManager,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.stay = data.stay;
        this.clienttables = new Array<ClientTable>();
        this.tableCtrl = new FormControl();
    }

    ngOnInit() {
        this.route.queryParams
            .subscribe( ( params) => {
                this.booking = new Booking;
                this.booking.id = params.bookingId;
            } );
        }
    clear() {
        this.activeModal.close('cancel');
    }

    initStay() {
        this.isSaving = true;
        this.subscribeToSaveResponse(
            this.stayService.createByBookingId(this.booking.id));
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Stay>>) {
        result.subscribe((res: HttpResponse<Stay>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Stay) {
        this.eventManager.broadcast({ name: 'stayModification', content: result});
        this.isSaving = false;
        this.activeModal.close(result);
    }
    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-stay-popup',
    template: ''
})
export class StayBookingDialogPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private stayPopupService: StayPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.stayPopupService
                    .open(StayBookingDialogComponent as Component, params['id']);
            } else {
                this.stayPopupService
                    .open(StayBookingDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
