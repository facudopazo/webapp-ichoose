import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { map, startWith } from 'rxjs/operators';
import { ValidatorFn } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { Stay } from './stay.model';
import { StayPopupService } from './stay-popup.service';
import { StayService } from './stay.service';
import { PromotionService, Promotion } from '../promotion';
import { Discount, DiscountType } from './../discount';
import { PromotionItem } from './../promotion-item';
import { ClientTable, ClientTableService } from '../client-table';
import { Menu, MenuService } from '../menu';
import { DomSanitizer } from '@angular/platform-browser';
import { MatTableDataSource } from '@angular/material';
import 'rxjs/add/observable/interval';
import { StayOrderService, StayOrder } from '../stay-order';
import { RestaurantService, Restaurant } from '../restaurant';

@Component({
    selector: 'jhi-stay-dialog',
    templateUrl: './stay-orders-dialog.component.html',
    styleUrls: [
        'stay.scss'
    ]
})
export class StayOrdersDialogComponent implements OnInit {
    currentRestaurant: Restaurant;
    displayedColumns = ['Nombre', 'Cantidad', 'Precio unitario', 'Precio total', 'Released'];
    discountTable = ['Nombre', 'Descuento'];

    stay: Stay;
    isSaving: boolean;
    timer: any;
    initTime: any;
    min: any;
    sec: any;
    current: any;
    filteredMenus: Observable<any[]>;
    menuCtrl: FormControl;
    selectedMenu: Menu;
    dataSource: any;
    discountData: any;
    aux: any;
    vali: boolean;
    quantity = 1;
    clienttables: ClientTable[];
    menu: Menu[];
    promotions: Promotion[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private stayService: StayService,
        public activeModal: MatDialogRef<StayOrdersPopupComponent>,
        private clientTableService: ClientTableService,
        private stayOrderService: StayOrderService,
        private menuService: MenuService,
        private eventManager: JhiEventManager,
        private route: ActivatedRoute,
        private promotionService: PromotionService,
        private restaurantService: RestaurantService,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.stay = data.stay;
        this.dataSource = new MatTableDataSource();
        this.discountData = new MatTableDataSource();
        this.dataSource.data = this.stay.orders;
        this.discountData.data = this.stay.discounts;
    }

    ngOnInit() {
        this.isSaving = false;
        this.menuCtrl = new FormControl('', [
            Validators.required
        ]);
        this.promotions = new Array<Promotion>();
        this.clientTableService.query()
            .subscribe((res: HttpResponse<ClientTable[]>) => {
            this.clienttables = res.body;
                this.clienttables = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.initTime = this.stay.init;
        this.timer = Observable.interval(1000).subscribe((val) => {
        this.current = new Date(); const gap = this.current.getTime() - this.initTime.getTime();
            this.min = Math.trunc(gap / 1000 / 60);
            this.sec = Math.trunc((gap / 1000) % 60);
        });
        this.restaurantService.getCurrent().subscribe((restaurant: Restaurant) => {
            this.currentRestaurant = restaurant;
            this.menuService.queryByRestaurant(this.currentRestaurant.id)
            .subscribe((res: HttpResponse<Menu[]>) => {
                this.menu = res.body;
                this.filteredMenus = this.menuCtrl.valueChanges
                    .pipe(
                        startWith<string | Menu>(''),
                        map((value) => typeof value === 'string' ? value : value.name),
                        map((menu) => menu ? this.filtermenus(menu) : this.menu.slice())
                    );
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
            this.promotionService.findActivePromotionsForClient(this.currentRestaurant.id, {clientId: this.stay.ownerId, status: 'ACTIVE'})
                .subscribe((res: HttpResponse<Promotion[]>) => { this.promotions = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));

        });
    }
    filtermenus(name: string) {
        return this.menu.filter((menu) =>
            menu.name.toLowerCase().indexOf(name.toLowerCase()) === 0);
    }

    displayFn(user?: Menu): string | undefined {
        return user ? user.name : undefined;
    }

    clear() {
        this.activeModal.close('cancel');
    }
    addMenuList() {
        const stayOrder: StayOrder = new StayOrder();
        this.dataSource.data = this.stay.orders;
        this.discountData.data = this.stay.discounts;
        stayOrder.menuId = this.selectedMenu.id;
        stayOrder.stayId = this.stay.id;
        stayOrder.price = this.selectedMenu.price;
        stayOrder.quantity = this.quantity;
        this.vali = true;
        const aux = this.stayOrderService.create(stayOrder);
        aux.subscribe((res: HttpResponse<StayOrder>) => {
            this.stayService.find(this.stay.id).subscribe(
                (resStay: HttpResponse<Stay>) => {
                    console.log(this.stay);
                    this.stay = resStay.body;
                    this.dataSource.data = this.stay.orders;
                    this.discountData.data = this.stay.discounts;
                }
            );
/*              stayOrder = res.body;
            stayOrder.menu = this.selectedMenu;
            this.stay.orders.push(stayOrder);
            this.reCalculateStayAmount(this.stay);
            this.dataSource.data = this.stay.orders;
 */            this.selectedMenu = new Menu;
            this.quantity = 1;
        //    stayOrder.available = stayOrder.quantity;
         //   this.checkPromotions(this.stay);
            this.eventManager.broadcast({ name: 'stayModification'});
        });
    }
    checkPromotions(stay: Stay) {
        stay.discounts = new Array<Discount>();
        stay.orders.forEach((order: StayOrder) => {
            order.available = order.quantity;
        });
        const orders: StayOrder[] = stay.orders.map( (x) => Object.assign({}, x));
        // let item: PromotionItem;
        this.promotions.forEach( (promotion: Promotion) => {
            let flag = true;
            promotion.items.forEach((item: PromotionItem) => {
                const filterOrders = orders.filter((order) => order.menu.id === item.menu.id);
                const available = filterOrders.reduce((previousValue, currentItem) => {
                    return previousValue + currentItem.available;
                }, 0);
                if (available >= item.quantity) {
                    filterOrders.reduce((remainder, currentItem) => {
                        if (currentItem.available >= remainder) {
                            currentItem.available -= remainder; return 0;
                        } else {
                            const result = remainder - currentItem.available; currentItem.available = 0; return result; } }, item.quantity );
                    } else {
                    flag = false;
                }
            });
            if (flag) {
                stay.orders = orders;
                const discount: Discount = new Discount;
                discount.type = DiscountType.PROMOTION;
                discount.promotion = promotion;
                const totalPrice = this.reCalculatePromotionItemAmount(promotion);
                discount.amount = totalPrice  - promotion.finalPrice;
                discount.description = promotion.name + ' (' + Math.round (discount.amount / totalPrice * 100) + ')';
                // discount.description = promotion.name;
                this.stay.discounts.push(discount);
                this.stay.amount -= discount.amount;
                this.discountData.data = this.stay.discounts;
            }
        });
    }

    calculatePromotionItemAmount(promotionItem: PromotionItem) {
        return promotionItem.menu.price * promotionItem.quantity;
    }
    reCalculatePromotionItemAmount(promotionAux: Promotion) {
        let total = 0;
        promotionAux.items.forEach((promotion) => {
            total += this.calculatePromotionItemAmount(promotion);
        });
        return total;
    }

    calculateStayOrderAmount(stayOrder: StayOrder) {
        return stayOrder.quantity * stayOrder.price;
    }

    reCalculateStayAmount(stay: Stay) {
        let total = 0;
        stay.orders.forEach((order) => {
            total += this.calculateStayOrderAmount(order);
        });
        this.stay.amount = total;
    }

    markAsReleased(stayOrder: StayOrder) {
        const aux = this.stayOrderService.markAsReleased(stayOrder.id);
        aux.subscribe((res: HttpResponse<StayOrder>) => {
            stayOrder.released = new Date();
        // tslint:disable-next-line:no-unused-expression
        }), (res: HttpErrorResponse) => this.onError(stayOrder.released = null);
    }
    removeMenulist(stayOrder: StayOrder) {
        const aux = this.stayOrderService.delete(stayOrder.id);
        aux.subscribe((res) => {
            const removedOrder = this.stay.orders.indexOf(stayOrder);
            this.stay.orders.splice(removedOrder, 1);
            // this.reCalculateStayAmount(this.stay);
            // this.dataSource.data = this.stay.orders;
            // this.checkPromotions(this.stay);
            this.eventManager.broadcast({ name: 'stayModification'});
            this.stayService.find(this.stay.id).subscribe(
                (resStay: HttpResponse<Stay>) => {
                        console.log(this.stay);
                        this.stay = resStay.body;
                        this.dataSource.data = this.stay.orders;
                        this.discountData.data = this.stay.discounts;
                    }
                );
        });
    }

    save() {
        this.isSaving = true;
        if (this.stay.id !== undefined) {
            this.subscribeToSaveResponse(
                this.stayService.update(this.stay));
        } else {
            this.subscribeToSaveResponse(
                this.stayService.create(this.stay));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Stay>>) {
        result.subscribe((res: HttpResponse<Stay>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Stay) {
        this.eventManager.broadcast({ name: 'stayModification', content: result });
        this.isSaving = false;
        this.activeModal.close(result);
    }
    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackClientTableById(index: number, item: ClientTable) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-stay-popup',
    template: ''
})
export class StayOrdersPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private stayPopupService: StayPopupService
    ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.stayPopupService
                    .open(StayOrdersDialogComponent as Component, params['id']);
            } else {
                this.stayPopupService
                    .open(StayOrdersDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
