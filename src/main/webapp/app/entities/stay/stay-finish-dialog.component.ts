import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { JhiEventManager } from 'ng-jhipster';
import { Observable } from 'rxjs/Observable';
import { Stay } from './stay.model';
import { StayPopupService } from './stay-popup.service';
import { StayService } from './stay.service';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

@Component({
    selector: 'jhi-stay-finish-dialog',
    templateUrl: './stay-finish-dialog.component.html',
    styleUrls: [
        'stay.scss'
    ]
})
export class StayFinishDialogComponent {

    isSaving: boolean;
    stay: Stay;

    constructor(
        private stayService: StayService,
        private eventManager: JhiEventManager,
        public activeModal: MatDialogRef<StayFinishPopupComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.stay = data.stay;
    }

    finishStay() {
        this.isSaving = true;
        this.subscribeToSaveResponse(
            this.stayService.end(this.stay.id));
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Stay>>) {
        result.subscribe((res: HttpResponse<Stay>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Stay) {
        this.eventManager.broadcast({ name: 'stayModification', content: result});
        this.isSaving = false;
        this.activeModal.close(result);
    }
    private onSaveError() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.close('cancel');
    }
}

@Component({
    selector: 'jhi-stay-finish-popup',
    template: ''
})
export class StayFinishPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private stayPopupService: StayPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.stayPopupService
                .open(StayFinishDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
