import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { map, startWith } from 'rxjs/operators';
import { ValidatorFn } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { Stay } from './stay.model';
import { StayPopupService } from './stay-popup.service';
import { StayService } from './stay.service';
import { ClientTable, ClientTableService } from '../client-table';
import { ErrorStateMatcher } from '@angular/material/core';
import { RestaurantService, Restaurant } from '../restaurant';

@Component({
    selector: 'jhi-stay-dialog',
    templateUrl: './stay-dialog.component.html',
    styleUrls: [
        'stay.scss'
    ]
})
export class StayDialogComponent implements OnInit {

    currentRestaurant: Restaurant;
    stay: Stay;
    isSaving: boolean;
    tableCtrl: FormControl;
    tableValidator: FormControl;
    peopleCtrl: FormControl;
    dateCtrl: FormControl;
    filteredTables: Observable<any[]>;
    initFromTable: boolean;

    clienttables: ClientTable[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private stayService: StayService,
        public activeModal: MatDialogRef<StayPopupComponent>,
        private clientTableService: ClientTableService,
        private eventManager: JhiEventManager,
        private route: ActivatedRoute,
        private restaurantService: RestaurantService,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.stay = data.stay;
        this.clienttables = new Array<ClientTable>();
        this.tableCtrl = new FormControl();
    }

    ngOnInit() {
        this.peopleCtrl = new FormControl('', [
            Validators.required
        ]);
        this.dateCtrl = new FormControl('', [
            Validators.required
        ]);
        this.tableCtrl = new FormControl('', [
            Validators.required
        ]);
        this.isSaving = false;
        this.restaurantService.getCurrent().subscribe(
            (restaurant: Restaurant) => {
                this.currentRestaurant = restaurant;
                this.clientTableService.findByRestaurantId(this.currentRestaurant.id,
                    {
                        page: 0,
                        size: 1000,
                        sort: ['number,asc']
                    })
                    .subscribe((res: HttpResponse<ClientTable[]>) => {
                        this.clienttables = res.body;
                        this.clienttables = res.body;
                        this.filteredTables = this.tableCtrl.valueChanges
                            .pipe(
                                startWith<string | ClientTable>(''),
                                map((value) => typeof value === 'string' ? value : value.number),
                                map((table) => table ? this.filtertables(table) : this.clienttables.slice())
                            );
                        if (this.stay.tableId != null) {
                            this.stay.table = this.clienttables.find((table) => table.id === this.stay.tableId);
                        }
                        this.route.queryParams
                            .subscribe((params) => {
                                    if (params.tableId !== undefined) {
                                        try {
                                            // tslint:disable-next-line:no-construct
                                            this.initFromTable = true;
                                            this.stay.tableId = new Number(params.tableId).valueOf();
                                            this.stay.table = this.clienttables.find((table) => table.id === this.stay.tableId);
                                        } catch (exc) {
                                            console.log('No se pudo parsear query param');
                                        }
                                    }
                                }
                            );
                    }, (res: HttpErrorResponse) => this.onError(res.message));
            }
        );
    }
    filtertables(name: string) {
        return this.clienttables.filter((table) =>
            table.number.toLowerCase().indexOf(name.toLowerCase()) === 0);
    }
    displayFn(user?: ClientTable): string | undefined {
        return user ? user.number : undefined;
    }

    clear() {
        this.activeModal.close('cancel');
    }

    save() {
        this.isSaving = true;
        this.stay.restaurantId = this.currentRestaurant.id;
        this.stay.tableId = this.stay.table.id;
        if (this.stay.id !== undefined) {
            this.subscribeToSaveResponse(
                this.stayService.update(this.stay));
        } else {
            this.subscribeToSaveResponse(
                this.stayService.create(this.stay));
        }
    }
    private subscribeToSaveResponse(result: Observable<HttpResponse<Stay>>) {
        result.subscribe((res: HttpResponse<Stay>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(stay: Stay) {
        stay.table.status = 'BUSY';
        this.clientTableService.update(stay.table).subscribe(
            (res: HttpResponse<ClientTable>) => {
                stay.table = res.body;
                console.log('MANDO EVENTO');
                this.eventManager.broadcast({ name: 'stayModification', content: stay });
                this.isSaving = false;
                this.activeModal.close(stay);
            }
        );
    }
    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackClientTableById(index: number, item: ClientTable) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-stay-popup',
    template: ''
})
export class StayPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private stayPopupService: StayPopupService
    ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.stayPopupService
                    .open(StayDialogComponent as Component, params['id']);
            } else {
                this.stayPopupService
                    .open(StayDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}
