import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Stay } from './stay.model';
import { StayService } from './stay.service';
import { ITEMS_PER_PAGE, Principal } from '../../shared';
import * as Rx from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import {observable} from 'rxjs/index';
import { Restaurant } from '../restaurant';
import { RestaurantService } from '../restaurant';
import {TaxBillPrinterService} from '../tax-bill/tax-bill-printer.service';
import { TaxBill } from '../tax-bill';

const WEBSOCKET_URL = 'ws://localhost:12000/ws';

@Component({
    selector: 'jhi-stay',
    templateUrl: './stay.component.html',
    styleUrls: [
        'stay.scss'
    ],
    providers: [Subject]
})
export class StayComponent implements OnInit, OnDestroy {
    displayedColumns = [ 'table', 'people', 'init', 'end', 'amount', 'Actions' ];

    @ViewChild(MatSort) sortTable: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;

currentAccount: any;
    message;
    stays: Stay[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    dataSource: any;
    restaurant: Restaurant;

    constructor(
        private stayService: StayService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private subject: Rx.Subject<MessageEvent>,
        private restaurantService: RestaurantService,
        private taxBillPrinterService: TaxBillPrinterService
    ) {
        this.dataSource = new MatTableDataSource();
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
        this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.stayService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: HttpResponse<Stay[]>) => this.onSuccess(res.body, res.headers),
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            return;
        }
        this.stayService.findFinalizedStaysByRestaurantId(this.restaurant.id, {
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
                (res: HttpResponse<Stay[]>) => this.onSuccess(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/stay'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/stay', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/stay', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.restaurantService.getCurrent().subscribe(
            (restaurant: Restaurant) => { this.restaurant = restaurant;
                this.loadAll();
            });
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInStays();
        // PRUEBA ENVIO COMANDO DE STATUS
        // this.taxBillPrinterService.getStatus();
        // PRUEVA ENVIO DE TICKET
        // const taxBill = new TaxBill();
        // this.taxBillPrinterService.printTaxBill(taxBill);
        // PRUEBA ENVIO COMANDO DE DAIRLY CLOSE
        // this.taxBillPrinterService.doDialyClose();
    }
    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Stay) {
        return item.id;
    }
    registerChangeInStays() {
        this.eventSubscriber = this.eventManager.subscribe('stayListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.stays = data;
        this.dataSource = new MatTableDataSource(this.stays);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sortTable;
    }
    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
