import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { Stay } from './stay.model';
import { StayService } from './stay.service';

@Injectable()
export class StayPopupService {
    private ngbModalRef: MatDialogRef<Component>;

    constructor(
        private datePipe: DatePipe,
        private modalService: MatDialog,
        private router: Router,
        private stayService: StayService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<MatDialogRef<Component>> {
        return new Promise<MatDialogRef<Component>>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.stayService.find(id)
                    .subscribe((stayResponse: HttpResponse<Stay>) => {
                        const stay: Stay = stayResponse.body;
                        this.ngbModalRef = this.stayModalRef(component, stay);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.stayModalRef(component, new Stay());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }
    stayModalRef(component: any, stay: Stay): MatDialogRef<Component> {
                        const modalRef = this.modalService.open(component, {height: 'auto',
                        width: 'auto', data: {'stay': stay}});
                        modalRef.afterClosed().subscribe((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
