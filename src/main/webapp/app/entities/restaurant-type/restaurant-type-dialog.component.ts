import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { RestaurantType } from './restaurant-type.model';
import { RestaurantTypePopupService } from './restaurant-type-popup.service';
import { RestaurantTypeService } from './restaurant-type.service';

@Component({
    selector: 'jhi-restaurant-type-dialog',
    templateUrl: './restaurant-type-dialog.component.html'
})
export class RestaurantTypeDialogComponent implements OnInit {

    restaurantType: RestaurantType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private restaurantTypeService: RestaurantTypeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.restaurantType.id !== undefined) {
            this.subscribeToSaveResponse(
                this.restaurantTypeService.update(this.restaurantType));
        } else {
            this.subscribeToSaveResponse(
                this.restaurantTypeService.create(this.restaurantType));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<RestaurantType>>) {
        result.subscribe((res: HttpResponse<RestaurantType>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: RestaurantType) {
        this.eventManager.broadcast({ name: 'restaurantTypeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-restaurant-type-popup',
    template: ''
})
export class RestaurantTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private restaurantTypePopupService: RestaurantTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.restaurantTypePopupService
                    .open(RestaurantTypeDialogComponent as Component, params['id']);
            } else {
                this.restaurantTypePopupService
                    .open(RestaurantTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
