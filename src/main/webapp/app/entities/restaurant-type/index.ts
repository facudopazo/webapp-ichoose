export * from './restaurant-type.model';
export * from './restaurant-type-popup.service';
export * from './restaurant-type.service';
export * from './restaurant-type-dialog.component';
export * from './restaurant-type-delete-dialog.component';
export * from './restaurant-type-detail.component';
export * from './restaurant-type.component';
export * from './restaurant-type.route';
