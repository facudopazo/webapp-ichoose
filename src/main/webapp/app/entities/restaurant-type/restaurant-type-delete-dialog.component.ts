import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { RestaurantType } from './restaurant-type.model';
import { RestaurantTypePopupService } from './restaurant-type-popup.service';
import { RestaurantTypeService } from './restaurant-type.service';

@Component({
    selector: 'jhi-restaurant-type-delete-dialog',
    templateUrl: './restaurant-type-delete-dialog.component.html'
})
export class RestaurantTypeDeleteDialogComponent {

    restaurantType: RestaurantType;

    constructor(
        private restaurantTypeService: RestaurantTypeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.restaurantTypeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'restaurantTypeListModification',
                content: 'Deleted an restaurantType'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-restaurant-type-delete-popup',
    template: ''
})
export class RestaurantTypeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private restaurantTypePopupService: RestaurantTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.restaurantTypePopupService
                .open(RestaurantTypeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
