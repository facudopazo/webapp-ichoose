import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { RestaurantType } from './restaurant-type.model';
import { RestaurantTypeService } from './restaurant-type.service';

@Component({
    selector: 'jhi-restaurant-type-detail',
    templateUrl: './restaurant-type-detail.component.html'
})
export class RestaurantTypeDetailComponent implements OnInit, OnDestroy {

    restaurantType: RestaurantType;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private restaurantTypeService: RestaurantTypeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInRestaurantTypes();
    }

    load(id) {
        this.restaurantTypeService.find(id)
            .subscribe((restaurantTypeResponse: HttpResponse<RestaurantType>) => {
                this.restaurantType = restaurantTypeResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInRestaurantTypes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'restaurantTypeListModification',
            (response) => this.load(this.restaurantType.id)
        );
    }
}
