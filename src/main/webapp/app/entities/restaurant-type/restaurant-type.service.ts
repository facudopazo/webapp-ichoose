
import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SERVER_API_URL } from '../../app.constants';

import { RestaurantType } from './restaurant-type.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<RestaurantType>;

@Injectable()
export class RestaurantTypeService {

    private resourceUrl =  SERVER_API_URL + 'ichooseapi/api/restaurant-types';
    private resourceSearchUrl = SERVER_API_URL + 'ichooseapi/api/_search/restaurant-types';

    constructor(private http: HttpClient) { }

    create(restaurantType: RestaurantType): Observable<EntityResponseType> {
        const copy = this.convert(restaurantType);
        return this.http.post<RestaurantType>(this.resourceUrl, copy, { observe: 'response' }).pipe(
            map((res: EntityResponseType) => this.convertResponse(res)));
    }

    update(restaurantType: RestaurantType): Observable<EntityResponseType> {
        const copy = this.convert(restaurantType);
        return this.http.put<RestaurantType>(this.resourceUrl, copy, { observe: 'response' }).pipe(
            map((res: EntityResponseType) => this.convertResponse(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<RestaurantType>(`${this.resourceUrl}/${id}`, { observe: 'response'}).pipe(
            map((res: EntityResponseType) => this.convertResponse(res)));
    }

    query(req?: any): Observable<HttpResponse<RestaurantType[]>> {
        const options = createRequestOption(req);
        return this.http.get<RestaurantType[]>(this.resourceUrl, { params: options, observe: 'response' }).pipe(
            map((res: HttpResponse<RestaurantType[]>) => this.convertArrayResponse(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<RestaurantType[]>> {
        const options = createRequestOption(req);
        return this.http.get<RestaurantType[]>(this.resourceSearchUrl, { params: options, observe: 'response' }).pipe(
            map((res: HttpResponse<RestaurantType[]>) => this.convertArrayResponse(res)));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: RestaurantType = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<RestaurantType[]>): HttpResponse<RestaurantType[]> {
        const jsonResponse: RestaurantType[] = res.body;
        const body: RestaurantType[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to RestaurantType.
     */
    private convertItemFromServer(restaurantType: RestaurantType): RestaurantType {
        const copy: RestaurantType = Object.assign({}, restaurantType);
        return copy;
    }

    /**
     * Convert a RestaurantType to a JSON which can be sent to the server.
     */
    private convert(restaurantType: RestaurantType): RestaurantType {
        const copy: RestaurantType = Object.assign({}, restaurantType);
        return copy;
    }
}
