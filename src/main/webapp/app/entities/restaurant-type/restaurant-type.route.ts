import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { RestaurantTypeComponent } from './restaurant-type.component';
import { RestaurantTypeDetailComponent } from './restaurant-type-detail.component';
import { RestaurantTypePopupComponent } from './restaurant-type-dialog.component';
import { RestaurantTypeDeletePopupComponent } from './restaurant-type-delete-dialog.component';

@Injectable()
export class RestaurantTypeResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const restaurantTypeRoute: Routes = [
    {
        path: 'restaurant-type',
        component: RestaurantTypeComponent,
        resolve: {
            'pagingParams': RestaurantTypeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.restaurantType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'restaurant-type/:id',
        component: RestaurantTypeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.restaurantType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const restaurantTypePopupRoute: Routes = [
    {
        path: 'restaurant-type-new',
        component: RestaurantTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.restaurantType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'restaurant-type/:id/edit',
        component: RestaurantTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.restaurantType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'restaurant-type/:id/delete',
        component: RestaurantTypeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.restaurantType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
