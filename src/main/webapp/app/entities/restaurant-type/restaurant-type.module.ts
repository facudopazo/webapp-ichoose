import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IchooseSharedModule } from '../../shared';
import {
    RestaurantTypeService,
    RestaurantTypePopupService,
    RestaurantTypeComponent,
    RestaurantTypeDetailComponent,
    RestaurantTypeDialogComponent,
    RestaurantTypePopupComponent,
    RestaurantTypeDeletePopupComponent,
    RestaurantTypeDeleteDialogComponent,
    restaurantTypeRoute,
    restaurantTypePopupRoute,
    RestaurantTypeResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...restaurantTypeRoute,
    ...restaurantTypePopupRoute,
];

@NgModule({
    imports: [
        IchooseSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        RestaurantTypeComponent,
        RestaurantTypeDetailComponent,
        RestaurantTypeDialogComponent,
        RestaurantTypeDeleteDialogComponent,
        RestaurantTypePopupComponent,
        RestaurantTypeDeletePopupComponent,
    ],
    entryComponents: [
        RestaurantTypeComponent,
        RestaurantTypeDialogComponent,
        RestaurantTypePopupComponent,
        RestaurantTypeDeleteDialogComponent,
        RestaurantTypeDeletePopupComponent,
    ],
    providers: [
        RestaurantTypeService,
        RestaurantTypePopupService,
        RestaurantTypeResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IchooseRestaurantTypeModule {}
