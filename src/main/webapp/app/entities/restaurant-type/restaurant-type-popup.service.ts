import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { RestaurantType } from './restaurant-type.model';
import { RestaurantTypeService } from './restaurant-type.service';

@Injectable()
export class RestaurantTypePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private restaurantTypeService: RestaurantTypeService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.restaurantTypeService.find(id)
                    .subscribe((restaurantTypeResponse: HttpResponse<RestaurantType>) => {
                        const restaurantType: RestaurantType = restaurantTypeResponse.body;
                        this.ngbModalRef = this.restaurantTypeModalRef(component, restaurantType);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.restaurantTypeModalRef(component, new RestaurantType());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    restaurantTypeModalRef(component: Component, restaurantType: RestaurantType): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.restaurantType = restaurantType;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
