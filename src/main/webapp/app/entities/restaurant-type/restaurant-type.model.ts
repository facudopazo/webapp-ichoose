import { BaseEntity } from './../../shared';

export class RestaurantType implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
    ) {
    }
}
