import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Shift } from './shift.model';
import { createRequestOption } from '../../shared';
import { Http } from '@angular/http';

export type EntityResponseType = HttpResponse<Shift>;

@Injectable()
export class ShiftService {

    private resourceUrl =  SERVER_API_URL + 'api/shifts';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/shifts';

    constructor(private http: HttpClient) { }

    create(shift: Shift): Observable<EntityResponseType> {
        const copy = this.convert(shift);
        return this.http.post<Shift>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(shift: Shift): Observable<EntityResponseType> {
        const copy = this.convert(shift);
        return this.http.put<Shift>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Shift>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Shift[]>> {
        const options = createRequestOption(req);
        return new Observable((observer) => {
            const mock: Shift[] = new Array<Shift>();
            let start: Date = new Date();
            let end: Date = new Date();
            start.setHours(7);
            start.setMinutes(0);
            end.setHours(11);
            end.setMinutes(0);
            let mockItem: Shift = new Shift();
            mockItem.end = end;
            mockItem.start = start;
            mockItem.name = 'Desayuno';
            mock.push(mockItem);

            start = new Date();
            end = new Date();
            start.setHours(11);
            start.setMinutes(0);
            end.setHours(13);
            end.setMinutes(0);
            mockItem = new Shift();
            mockItem.end = end;
            mockItem.start = start;
            mockItem.name = 'Almuerzo';
            mock.push(mockItem);

            start = new Date();
            end = new Date();
            start.setHours(20);
            start.setMinutes(0);
            end.setHours(22);
            end.setMinutes(0);
            mockItem = new Shift();
            mockItem.end = end;
            mockItem.start = start;
            mockItem.name = 'Cena';
            mock.push(mockItem);

            const response: HttpResponse<Shift[]> = new HttpResponse<Shift[]>({body: mock});
            // observable execution
            observer.next(response);
            observer.complete();
        });

      //  return this.http.get<Shift[]>(this.resourceUrl, { params: options, observe: 'response' })
      //      .map((res: HttpResponse<Shift[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<Shift[]>> {
        const options = createRequestOption(req);
        return this.http.get<Shift[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Shift[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Shift = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Shift[]>): HttpResponse<Shift[]> {
        const jsonResponse: Shift[] = res.body;
        const body: Shift[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Shift.
     */
    private convertItemFromServer(shift: Shift): Shift {
        const copy: Shift = Object.assign({}, shift);
        return copy;
    }

    /**
     * Convert a Shift to a JSON which can be sent to the server.
     */
    private convert(shift: Shift): Shift {
        const copy: Shift = Object.assign({}, shift);
        return copy;
    }
}
