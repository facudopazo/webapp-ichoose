import { BaseEntity } from './../../shared';

export class Shift implements BaseEntity {
    constructor(
        public id?: number,
        public start?: Date,
        public end?: Date,
        public name?: string,
        public restaurantId?: number,
    ) {
    }
}
