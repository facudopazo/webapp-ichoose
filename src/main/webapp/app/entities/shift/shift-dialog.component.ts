import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Shift } from './shift.model';
import { ShiftPopupService } from './shift-popup.service';
import { ShiftService } from './shift.service';
import { Restaurant, RestaurantService } from '../restaurant';

@Component({
    selector: 'jhi-shift-dialog',
    templateUrl: './shift-dialog.component.html'
})
export class ShiftDialogComponent implements OnInit {

    shift: Shift;
    isSaving: boolean;

    restaurants: Restaurant[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private shiftService: ShiftService,
        private restaurantService: RestaurantService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.restaurantService.query()
            .subscribe((res: HttpResponse<Restaurant[]>) => { this.restaurants = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.shift.id !== undefined) {
            this.subscribeToSaveResponse(
                this.shiftService.update(this.shift));
        } else {
            this.subscribeToSaveResponse(
                this.shiftService.create(this.shift));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Shift>>) {
        result.subscribe((res: HttpResponse<Shift>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Shift) {
        this.eventManager.broadcast({ name: 'shiftListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackRestaurantById(index: number, item: Restaurant) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-shift-popup',
    template: ''
})
export class ShiftPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private shiftPopupService: ShiftPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.shiftPopupService
                    .open(ShiftDialogComponent as Component, params['id']);
            } else {
                this.shiftPopupService
                    .open(ShiftDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
