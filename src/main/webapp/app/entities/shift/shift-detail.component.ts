import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Shift } from './shift.model';
import { ShiftService } from './shift.service';

@Component({
    selector: 'jhi-shift-detail',
    templateUrl: './shift-detail.component.html'
})
export class ShiftDetailComponent implements OnInit, OnDestroy {

    shift: Shift;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private shiftService: ShiftService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInShifts();
    }

    load(id) {
        this.shiftService.find(id)
            .subscribe((shiftResponse: HttpResponse<Shift>) => {
                this.shift = shiftResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInShifts() {
        this.eventSubscriber = this.eventManager.subscribe(
            'shiftListModification',
            (response) => this.load(this.shift.id)
        );
    }
}
