import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IchooseSharedModule } from '../../shared';
import {
    ShiftService,
    ShiftPopupService,
    ShiftComponent,
    ShiftDetailComponent,
    ShiftDialogComponent,
    ShiftPopupComponent,
    ShiftDeletePopupComponent,
    ShiftDeleteDialogComponent,
    shiftRoute,
    shiftPopupRoute,
    ShiftResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...shiftRoute,
    ...shiftPopupRoute,
];

@NgModule({
    imports: [
        IchooseSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ShiftComponent,
        ShiftDetailComponent,
        ShiftDialogComponent,
        ShiftDeleteDialogComponent,
        ShiftPopupComponent,
        ShiftDeletePopupComponent,
    ],
    entryComponents: [
        ShiftComponent,
        ShiftDialogComponent,
        ShiftPopupComponent,
        ShiftDeleteDialogComponent,
        ShiftDeletePopupComponent,
    ],
    providers: [
        ShiftService,
        ShiftPopupService,
        ShiftResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IchooseShiftModule {}
