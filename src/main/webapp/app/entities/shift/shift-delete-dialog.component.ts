import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Shift } from './shift.model';
import { ShiftPopupService } from './shift-popup.service';
import { ShiftService } from './shift.service';

@Component({
    selector: 'jhi-shift-delete-dialog',
    templateUrl: './shift-delete-dialog.component.html'
})
export class ShiftDeleteDialogComponent {

    shift: Shift;

    constructor(
        private shiftService: ShiftService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.shiftService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'shiftListModification',
                content: 'Deleted an shift'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-shift-delete-popup',
    template: ''
})
export class ShiftDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private shiftPopupService: ShiftPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.shiftPopupService
                .open(ShiftDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
