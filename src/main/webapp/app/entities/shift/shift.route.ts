import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { ShiftComponent } from './shift.component';
import { ShiftDetailComponent } from './shift-detail.component';
import { ShiftPopupComponent } from './shift-dialog.component';
import { ShiftDeletePopupComponent } from './shift-delete-dialog.component';

@Injectable()
export class ShiftResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const shiftRoute: Routes = [
    {
        path: 'shift',
        component: ShiftComponent,
        resolve: {
            'pagingParams': ShiftResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.shift.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'shift/:id',
        component: ShiftDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.shift.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const shiftPopupRoute: Routes = [
    {
        path: 'shift-new',
        component: ShiftPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.shift.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'shift/:id/edit',
        component: ShiftPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.shift.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'shift/:id/delete',
        component: ShiftDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.shift.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
