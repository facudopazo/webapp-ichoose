import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ClientTableStatus } from './client-table-status.model';
import { ClientTableStatusPopupService } from './client-table-status-popup.service';
import { ClientTableStatusService } from './client-table-status.service';

@Component({
    selector: 'jhi-client-table-status-dialog',
    templateUrl: './client-table-status-dialog.component.html'
})
export class ClientTableStatusDialogComponent implements OnInit {

    clientTableStatus: ClientTableStatus;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private clientTableStatusService: ClientTableStatusService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.clientTableStatus.id !== undefined) {
            this.subscribeToSaveResponse(
                this.clientTableStatusService.update(this.clientTableStatus));
        } else {
            this.subscribeToSaveResponse(
                this.clientTableStatusService.create(this.clientTableStatus));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ClientTableStatus>>) {
        result.subscribe((res: HttpResponse<ClientTableStatus>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ClientTableStatus) {
        this.eventManager.broadcast({ name: 'clientTableStatusListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-client-table-status-popup',
    template: ''
})
export class ClientTableStatusPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientTableStatusPopupService: ClientTableStatusPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.clientTableStatusPopupService
                    .open(ClientTableStatusDialogComponent as Component, params['id']);
            } else {
                this.clientTableStatusPopupService
                    .open(ClientTableStatusDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
