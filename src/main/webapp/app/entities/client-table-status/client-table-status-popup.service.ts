import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { ClientTableStatus } from './client-table-status.model';
import { ClientTableStatusService } from './client-table-status.service';

@Injectable()
export class ClientTableStatusPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private clientTableStatusService: ClientTableStatusService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.clientTableStatusService.find(id)
                    .subscribe((clientTableStatusResponse: HttpResponse<ClientTableStatus>) => {
                        const clientTableStatus: ClientTableStatus = clientTableStatusResponse.body;
                        this.ngbModalRef = this.clientTableStatusModalRef(component, clientTableStatus);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.clientTableStatusModalRef(component, new ClientTableStatus());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    clientTableStatusModalRef(component: Component, clientTableStatus: ClientTableStatus): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.clientTableStatus = clientTableStatus;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
