import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { ClientTableStatusComponent } from './client-table-status.component';
import { ClientTableStatusDetailComponent } from './client-table-status-detail.component';
import { ClientTableStatusPopupComponent } from './client-table-status-dialog.component';
import { ClientTableStatusDeletePopupComponent } from './client-table-status-delete-dialog.component';

@Injectable()
export class ClientTableStatusResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const clientTableStatusRoute: Routes = [
    {
        path: 'client-table-status',
        component: ClientTableStatusComponent,
        resolve: {
            'pagingParams': ClientTableStatusResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.clientTableStatus.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'client-table-status/:id',
        component: ClientTableStatusDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.clientTableStatus.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const clientTableStatusPopupRoute: Routes = [
    {
        path: 'client-table-status-new',
        component: ClientTableStatusPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.clientTableStatus.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-table-status/:id/edit',
        component: ClientTableStatusPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.clientTableStatus.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-table-status/:id/delete',
        component: ClientTableStatusDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.clientTableStatus.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
