import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ClientTableStatus } from './client-table-status.model';
import { ClientTableStatusPopupService } from './client-table-status-popup.service';
import { ClientTableStatusService } from './client-table-status.service';

@Component({
    selector: 'jhi-client-table-status-delete-dialog',
    templateUrl: './client-table-status-delete-dialog.component.html'
})
export class ClientTableStatusDeleteDialogComponent {

    clientTableStatus: ClientTableStatus;

    constructor(
        private clientTableStatusService: ClientTableStatusService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.clientTableStatusService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'clientTableStatusListModification',
                content: 'Deleted an clientTableStatus'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-client-table-status-delete-popup',
    template: ''
})
export class ClientTableStatusDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientTableStatusPopupService: ClientTableStatusPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.clientTableStatusPopupService
                .open(ClientTableStatusDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
