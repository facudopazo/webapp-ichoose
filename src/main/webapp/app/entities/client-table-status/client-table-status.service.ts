
import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SERVER_API_URL } from '../../app.constants';

import { ClientTableStatus } from './client-table-status.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<ClientTableStatus>;

@Injectable()
export class ClientTableStatusService {

    private resourceUrl =  SERVER_API_URL + 'ichooseapi/api/client-table-statuses';
    private resourceSearchUrl = SERVER_API_URL + 'ichooseapi/api/_search/client-table-statuses';

    constructor(private http: HttpClient) { }

    create(clientTableStatus: ClientTableStatus): Observable<EntityResponseType> {
        const copy = this.convert(clientTableStatus);
        return this.http.post<ClientTableStatus>(this.resourceUrl, copy, { observe: 'response' }).pipe(
            map((res: EntityResponseType) => this.convertResponse(res)));
    }

    update(clientTableStatus: ClientTableStatus): Observable<EntityResponseType> {
        const copy = this.convert(clientTableStatus);
        return this.http.put<ClientTableStatus>(this.resourceUrl, copy, { observe: 'response' }).pipe(
            map((res: EntityResponseType) => this.convertResponse(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ClientTableStatus>(`${this.resourceUrl}/${id}`, { observe: 'response'}).pipe(
            map((res: EntityResponseType) => this.convertResponse(res)));
    }

    query(req?: any): Observable<HttpResponse<ClientTableStatus[]>> {
        const options = createRequestOption(req);
        return this.http.get<ClientTableStatus[]>(this.resourceUrl, { params: options, observe: 'response' }).pipe(
            map((res: HttpResponse<ClientTableStatus[]>) => this.convertArrayResponse(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<ClientTableStatus[]>> {
        const options = createRequestOption(req);
        return this.http.get<ClientTableStatus[]>(this.resourceSearchUrl, { params: options, observe: 'response' }).pipe(
            map((res: HttpResponse<ClientTableStatus[]>) => this.convertArrayResponse(res)));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ClientTableStatus = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ClientTableStatus[]>): HttpResponse<ClientTableStatus[]> {
        const jsonResponse: ClientTableStatus[] = res.body;
        const body: ClientTableStatus[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ClientTableStatus.
     */
    private convertItemFromServer(clientTableStatus: ClientTableStatus): ClientTableStatus {
        const copy: ClientTableStatus = Object.assign({}, clientTableStatus);
        return copy;
    }

    /**
     * Convert a ClientTableStatus to a JSON which can be sent to the server.
     */
    private convert(clientTableStatus: ClientTableStatus): ClientTableStatus {
        const copy: ClientTableStatus = Object.assign({}, clientTableStatus);
        return copy;
    }
}
