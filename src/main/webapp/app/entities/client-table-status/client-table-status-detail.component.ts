import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ClientTableStatus } from './client-table-status.model';
import { ClientTableStatusService } from './client-table-status.service';

@Component({
    selector: 'jhi-client-table-status-detail',
    templateUrl: './client-table-status-detail.component.html'
})
export class ClientTableStatusDetailComponent implements OnInit, OnDestroy {

    clientTableStatus: ClientTableStatus;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private clientTableStatusService: ClientTableStatusService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInClientTableStatuses();
    }

    load(id) {
        this.clientTableStatusService.find(id)
            .subscribe((clientTableStatusResponse: HttpResponse<ClientTableStatus>) => {
                this.clientTableStatus = clientTableStatusResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInClientTableStatuses() {
        this.eventSubscriber = this.eventManager.subscribe(
            'clientTableStatusListModification',
            (response) => this.load(this.clientTableStatus.id)
        );
    }
}
