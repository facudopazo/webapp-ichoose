import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IchooseSharedModule } from '../../shared';
import {
    ClientTableStatusService,
    ClientTableStatusPopupService,
    ClientTableStatusComponent,
    ClientTableStatusDetailComponent,
    ClientTableStatusDialogComponent,
    ClientTableStatusPopupComponent,
    ClientTableStatusDeletePopupComponent,
    ClientTableStatusDeleteDialogComponent,
    clientTableStatusRoute,
    clientTableStatusPopupRoute,
    ClientTableStatusResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...clientTableStatusRoute,
    ...clientTableStatusPopupRoute,
];

@NgModule({
    imports: [
        IchooseSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ClientTableStatusComponent,
        ClientTableStatusDetailComponent,
        ClientTableStatusDialogComponent,
        ClientTableStatusDeleteDialogComponent,
        ClientTableStatusPopupComponent,
        ClientTableStatusDeletePopupComponent,
    ],
    entryComponents: [
        ClientTableStatusComponent,
        ClientTableStatusDialogComponent,
        ClientTableStatusPopupComponent,
        ClientTableStatusDeleteDialogComponent,
        ClientTableStatusDeletePopupComponent,
    ],
    providers: [
        ClientTableStatusService,
        ClientTableStatusPopupService,
        ClientTableStatusResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IchooseClientTableStatusModule {}
