import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable , Subscription} from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { ClientTable } from './client-table.model';
import { Booking } from '../booking/booking.model';
import { ClientTablePopupService } from './client-table-popup.service';
import { ClientTableService } from './client-table.service';
import { BookingService } from '../booking/booking.service';
import { Sector, SectorService } from '../sector';
import { ClientTableStatus, ClientTableStatusService } from '../client-table-status';
import { RestaurantService, Restaurant } from '../restaurant';

@Component({
    selector: 'jhi-change-sector-dialog',
    templateUrl: './change-sector-dialog.component.html',
    styleUrls: [
        'client-table.scss'
    ]
})
export class ChangeSectorDialogComponent implements OnInit {

    table: ClientTable;
    isSaving: boolean;
    private subscription: Subscription;
    currentRestaurant: Restaurant;

    bookings: Booking[];
    sectors: Sector[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private clientTableService: ClientTableService,
        private bookingService: BookingService,
        private sectorService: SectorService,
        private clientTableStatusService: ClientTableStatusService,
        public activeModal: MatDialogRef<ChangeSectorPopupComponent>,
        private eventManager: JhiEventManager,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private route: ActivatedRoute,
        private router: Router,
        private restaurantService: RestaurantService
    ) {
        this.table = data.table;
    }

    ngOnInit() {
        this.isSaving = false;
        this.restaurantService.getCurrent().subscribe(
            (restaurant: Restaurant) => {
                this.currentRestaurant = restaurant;
                this.sectorService.queryByRestaurant(restaurant.id)
                .subscribe((res: HttpResponse<Sector[]>) => { this.sectors = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
            });
    }
    clear() {
        this.activeModal.close('cancel');
    }

    save() {
        this.subscribeToSaveResponse(
            this.clientTableService.update(this.table));
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ClientTable>>) {
        result.subscribe((res: HttpResponse<ClientTable>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ClientTable) {
        this.eventManager.broadcast({ name: 'clientTableListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.close(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackSectorById(index: number, item: Sector) {
        return item.id;
    }

    trackClientTableStatusById(index: number, item: ClientTableStatus) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-client-table-popup',
    template: ''
})
export class ChangeSectorPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientTablePopupService: ClientTablePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.clientTablePopupService
            .open(ChangeSectorDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
