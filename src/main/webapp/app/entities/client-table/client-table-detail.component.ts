import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ClientTable } from './client-table.model';
import { ClientTableService } from './client-table.service';

@Component({
    selector: 'jhi-client-table-detail',
    templateUrl: './client-table-detail.component.html',
    styleUrls: [
        'client-table.scss'
    ]
})
export class ClientTableDetailComponent implements OnInit, OnDestroy {

    clientTable: ClientTable;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private clientTableService: ClientTableService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInClientTables();
    }

    load(id) {
        this.clientTableService.find(id)
            .subscribe((clientTableResponse: HttpResponse<ClientTable>) => {
                this.clientTable = clientTableResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInClientTables() {
        this.eventSubscriber = this.eventManager.subscribe(
            'clientTableListModification',
            (response) => this.load(this.clientTable.id)
        );
    }
}
