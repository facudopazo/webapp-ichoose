import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { ClientTable } from './client-table.model';
import { ClientTableService } from './client-table.service';

@Injectable()
export class ClientTablePopupService {
    private ngbModalRef: MatDialogRef<Component>;

    constructor(
        private datePipe: DatePipe,
        private modalService: MatDialog,
        private router: Router,
        private clientTableService: ClientTableService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<MatDialogRef<Component>> {
        return new Promise<MatDialogRef<Component>>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.clientTableService.find(id)
                    .subscribe((clientTableResponse: HttpResponse<ClientTable>) => {
                        const table: ClientTable = clientTableResponse.body;
                        this.ngbModalRef = this.clientTableModalRef(component, table);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.clientTableModalRef(component, new ClientTable());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    clientTableModalRef(component: any, table: ClientTable): MatDialogRef<Component> {
        const modalRef = this.modalService.open(component, {height: 'auto',
        width: '600px', data: {'table': table}});
        modalRef.afterClosed().subscribe((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
