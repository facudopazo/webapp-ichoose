import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable , Subscription} from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { ClientTable } from './client-table.model';
import { ClientTablePopupService } from './client-table-popup.service';
import { ClientTableService } from './client-table.service';
import { Sector, SectorService } from '../sector';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ClientTableStatus, ClientTableStatusService } from '../client-table-status';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatSnackBar } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'jhi-client-table-dialog',
    templateUrl: './client-table-dialog.component.html',
    styleUrls: [
        'client-table.scss'
    ]
})
export class ClientTableDialogComponent implements OnInit {

    clientTable: ClientTable;
    isSaving: boolean;
    private subscription: Subscription;
    tableNum: FormControl;
    chairQuan: FormControl;
    status: FormControl;

    sectors: Sector[];
    statusOptions: any [] = [];

    constructor(
        private jhiAlertService: JhiAlertService,
        public activeModal: MatDialogRef<ClientTablePopupComponent>,
        private clientTableService: ClientTableService,
        private sectorService: SectorService,
        private clientTableStatusService: ClientTableStatusService,
        private eventManager: JhiEventManager,
        private translateService: TranslateService,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private route: ActivatedRoute,
        private router: Router,
        public snackBar: MatSnackBar
    ) {
        this.clientTable = data.table;
        this.statusOptions = [
            {
                key: 'FREE',
                value: translateService.instant('ichooseApp.clientTable.statusOption.FREE')
            }, {
                key: 'DISABLED',
                value: translateService.instant('ichooseApp.clientTable.statusOption.DISABLED')
            }, {
                key: 'IN_BOOKING',
                value: translateService.instant('ichooseApp.clientTable.statusOption.IN_BOOKING')
            }, {
                key: 'BUSY',
                value: translateService.instant('ichooseApp.clientTable.statusOption.BUSY')
            }, {
                key: 'BILL_REQUESTED',
                value: translateService.instant('ichooseApp.clientTable.statusOption.BILL_REQUESTED')
            }
        ];
    }

    ngOnInit() {
        this.tableNum = new FormControl('', [
            Validators.required,
          ]);
        this.chairQuan = new FormControl('', [
            Validators.required,
          ]);
        this.status = new FormControl('', [
            Validators.required,
          ]);

        this.isSaving = false;
        this.sectorService.query()
            .subscribe((res: HttpResponse<Sector[]>) => { this.sectors = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.route.queryParams
            .subscribe( (params) => {
                if (params.sectorId !== undefined) {
                    this.clientTable.sectorId = params.sectorId;
                }
            }
        );
        if (this.clientTable.id === undefined) {
            this.clientTable.status = 'FREE';
        }
    }

    clear() {
        this.activeModal.close('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.clientTable.id !== undefined) {
            this.subscribeToSaveResponse(
                this.clientTableService.update(this.clientTable));
        } else {
            this.subscribeToSaveResponse(
                this.clientTableService.create(this.clientTable));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ClientTable>>) {
        result.subscribe((res: HttpResponse<ClientTable>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => { this.onSaveError(res); });
    }

    private onSaveSuccess(result: ClientTable) {
        this.eventManager.broadcast({ name: 'clientTableListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.close(result);
    }

    private onSaveError(res) {
        if (res.error && res.error.message === 'NUMBER_IN_USE') {
            const tableNumber = document.getElementById('tableNumber').textContent;
            this.snackBar.open(tableNumber, 'OK', {duration: 3000});
        } else {
            const errorTable = document.getElementById('errorTable').textContent;
            this.snackBar.open(errorTable, 'OK', {duration: 3000});
        }
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackSectorById(index: number, item: Sector) {
        return item.id;
    }

    trackClientTableStatusById(index: number, item: ClientTableStatus) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-client-table-popup',
    template: ''
})
export class ClientTablePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientTablePopupService: ClientTablePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.clientTablePopupService
                    .open(ClientTableDialogComponent as Component, params['id']);
            } else {
                this.clientTablePopupService
                    .open(ClientTableDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
      const isSubmitted = form && form.submitted;
      return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
  }
