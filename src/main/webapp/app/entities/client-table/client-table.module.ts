import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { QRCodeModule } from 'angularx-qrcode';

import { IchooseSharedModule } from '../../shared';

import {
    ClientTableService,
    ClientTablePopupService,
    ChangeSectorDialogComponent,
    ChangeSectorPopupComponent,
    ClientTableComponent,
    ClientTableDetailComponent,
    ClientTableDialogComponent,
    ClientTablePopupComponent,
    ClientTableQrDialogComponent,
    ClientTableDeletePopupComponent,
    ClientTableDeleteDialogComponent,
    clientTableRoute,
    clientTablePopupRoute,
    ClientTableResolvePagingParams,
} from './';
import { ClientTableQrPopupComponent } from './client-table-qr-dialog.component';

const ENTITY_STATES = [
    ...clientTableRoute,
    ...clientTablePopupRoute,
];

@NgModule({
    imports: [
        IchooseSharedModule,
        QRCodeModule,
        RouterModule.forChild(ENTITY_STATES),
    ],
    declarations: [
        ClientTableComponent,
        ClientTableDetailComponent,
        ClientTableDialogComponent,
        ClientTableDeleteDialogComponent,
        ClientTablePopupComponent,
        ClientTableQrDialogComponent,
        ClientTableQrPopupComponent,
        ClientTableDeletePopupComponent,
        ChangeSectorDialogComponent,
        ChangeSectorPopupComponent
    ],
    entryComponents: [
        ClientTableComponent,
        ClientTableDialogComponent,
        ClientTablePopupComponent,
        ClientTableDeleteDialogComponent,
        ClientTableDeletePopupComponent,
        ClientTableQrDialogComponent,
        ClientTableQrPopupComponent,
        ChangeSectorDialogComponent,
        ChangeSectorPopupComponent
    ],
    providers: [
        ClientTableService,
        ClientTablePopupService,
        ClientTableResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IchooseClientTableModule {}
