import { BaseEntity } from './../../shared';

export class ClientTable implements BaseEntity {
    constructor(
        public id?: number,
        public number?: string,
        public chairs?: number,
        public modifiable?: boolean,
        public deleteDate?: any,
        public sectorId?: number,
        public status?: any,
    ) {
        this.modifiable = false;
        this.status = 'FREE';
    }
}
