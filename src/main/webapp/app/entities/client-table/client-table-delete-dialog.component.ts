import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { JhiEventManager } from 'ng-jhipster';

import { ClientTable } from './client-table.model';
import { ClientTablePopupService } from './client-table-popup.service';
import { ClientTableService } from './client-table.service';

@Component({
    selector: 'jhi-client-table-delete-dialog',
    templateUrl: './client-table-delete-dialog.component.html',
    styleUrls: [
        'client-table.scss'
    ]
})
export class ClientTableDeleteDialogComponent {

    table: ClientTable;

    constructor(
        private clientTableService: ClientTableService,
        public activeModal: MatDialogRef<ClientTableDeletePopupComponent>,
        private eventManager: JhiEventManager,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.table = data.table;
    }

    clear() {
        this.activeModal.close('cancel');
    }

    confirmDelete(id: number) {
        this.clientTableService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'clientTableListModification',
                content: 'Deleted an clientTable'
            });
            this.activeModal.close(true);
        });
    }
}

@Component({
    selector: 'jhi-client-table-delete-popup',
    template: ''
})
export class ClientTableDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientTablePopupService: ClientTablePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.clientTablePopupService
                .open(ClientTableDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
