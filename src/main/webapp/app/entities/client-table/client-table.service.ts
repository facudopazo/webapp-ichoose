
import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { ClientTable } from './client-table.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<ClientTable>;

@Injectable()
export class ClientTableService {

    private resourceUrl =  SERVER_API_URL + 'ichooseapi/api/client-tables';
    private resourceSearchUrl = SERVER_API_URL + 'ichooseapi/api/_search/client-tables';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(clientTable: ClientTable): Observable<EntityResponseType> {
        const copy = this.convert(clientTable);
        return this.http.post<ClientTable>(this.resourceUrl, copy, { observe: 'response' }).pipe(
            map((res: EntityResponseType) => this.convertResponse(res)));
    }

    update(clientTable: ClientTable): Observable<EntityResponseType> {
        const copy = this.convert(clientTable);
        return this.http.put<ClientTable>(this.resourceUrl, copy, { observe: 'response' }).pipe(
            map((res: EntityResponseType) => this.convertResponse(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ClientTable>(`${this.resourceUrl}/${id}`, { observe: 'response'}).pipe(
            map((res: EntityResponseType) => this.convertResponse(res)));
    }

    query(req?: any): Observable<HttpResponse<ClientTable[]>> {
        const options = createRequestOption(req);
        return this.http.get<ClientTable[]>(this.resourceUrl, { params: options, observe: 'response' }).pipe(
            map((res: HttpResponse<ClientTable[]>) => this.convertArrayResponse(res)));
    }

    findByRestaurantId(restaurantId: number, req?: any): Observable<HttpResponse<ClientTable[]>> {
        const options = createRequestOption(req);
        const url = SERVER_API_URL + 'ichooseapi/api/restaurants/' + restaurantId + '/client-tables';
        return this.http.get<ClientTable[]>(url, { params: options, observe: 'response' }).pipe(
            map((res: HttpResponse<ClientTable[]>) => this.convertArrayResponse(res)));
    }

    findBySectorId(sectorId: number, req?: any): Observable<HttpResponse<ClientTable[]>> {
        const options = createRequestOption(req);
        const url = SERVER_API_URL + 'ichooseapi/api/sectors/' + sectorId + '/client-tables';
        return this.http.get<ClientTable[]>(url, { params: options, observe: 'response' }).pipe(
            map((res: HttpResponse<ClientTable[]>) => this.convertArrayResponse(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<ClientTable[]>> {
        const options = createRequestOption(req);
        return this.http.get<ClientTable[]>(this.resourceSearchUrl, { params: options, observe: 'response' }).pipe(
            map((res: HttpResponse<ClientTable[]>) => this.convertArrayResponse(res)));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ClientTable = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ClientTable[]>): HttpResponse<ClientTable[]> {
        const jsonResponse: ClientTable[] = res.body;
        const body: ClientTable[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ClientTable.
     */
    private convertItemFromServer(clientTable: ClientTable): ClientTable {
        const copy: ClientTable = Object.assign({}, clientTable);
        copy.deleteDate = this.dateUtils
            .convertDateTimeFromServer(clientTable.deleteDate);
        return copy;
    }

    /**
     * Convert a ClientTable to a JSON which can be sent to the server.
     */
    private convert(clientTable: ClientTable): ClientTable {
        const copy: ClientTable = Object.assign({}, clientTable);

        copy.deleteDate = this.dateUtils.toDate(clientTable.deleteDate);
        return copy;
    }
}
