import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable , Subscription} from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { ClientTable } from './client-table.model';
import { ClientTablePopupService } from './client-table-popup.service';
import { ClientTableService } from './client-table.service';
import { Sector, SectorService } from '../sector';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ClientTableStatus, ClientTableStatusService } from '../client-table-status';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import { RestaurantService, Restaurant } from '../restaurant';
import {ErrorStateMatcher} from '@angular/material/core';

@Component({
    selector: 'jhi-client-table-dialog',
    templateUrl: './client-table-qr-dialog.component.html',
    styleUrls: [
        'client-table.scss'
    ]
})
export class ClientTableQrDialogComponent implements OnInit {

    clientTable: ClientTable;
    currentRestaurant: Restaurant;
    isSaving: boolean;
    private subscription: Subscription;
    tableNum: FormControl;
    chairQuan: FormControl;
    status: FormControl;

    sectors: Sector[];
   constructor(
        private restaurantService: RestaurantService,
        private jhiAlertService: JhiAlertService,
        public activeModal: MatDialogRef<ClientTableQrPopupComponent>,
        private clientTableService: ClientTableService,
        private sectorService: SectorService,
        private clientTableStatusService: ClientTableStatusService,
        private eventManager: JhiEventManager,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private route: ActivatedRoute,
        private router: Router,
    ) {
        this.clientTable = data.table;
        this.currentRestaurant = new Restaurant;
    }

    ngOnInit() {
        this.isSaving = false;
        this.restaurantService.getCurrent().subscribe((restaurant: Restaurant) => {
                this.currentRestaurant = restaurant;
            }
        );
    }

    qrCode() {
        return '{"tableId":'  + this.clientTable.id + ',' + '"restaurantId":' + this.currentRestaurant.id + '}';
    }

    print() {
        window.print();
    }

    clear() {
        this.activeModal.close('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.clientTable.id !== undefined) {
            this.subscribeToSaveResponse(
                this.clientTableService.update(this.clientTable));
        } else {
            this.subscribeToSaveResponse(
                this.clientTableService.create(this.clientTable));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ClientTable>>) {
        result.subscribe((res: HttpResponse<ClientTable>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ClientTable) {
        this.eventManager.broadcast({ name: 'clientTableListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.close(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackSectorById(index: number, item: Sector) {
        return item.id;
    }

    trackClientTableStatusById(index: number, item: ClientTableStatus) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-client-table-popup',
    template: ''
})
export class ClientTableQrPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientTablePopupService: ClientTablePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.clientTablePopupService
                    .open(ClientTableQrDialogComponent as Component, params['id']);
            } else {
                this.clientTablePopupService
                    .open(ClientTableQrDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
