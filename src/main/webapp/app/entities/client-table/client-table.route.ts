import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';
import { UserRouteAccessService } from '../../shared';
import { ClientTableComponent } from './client-table.component';
import { ClientTableDetailComponent } from './client-table-detail.component';
import { ClientTablePopupComponent } from './client-table-dialog.component';
import { ClientTableDialogComponent } from './client-table-dialog.component';
import { ChangeSectorDialogComponent } from './change-sector-dialog.component';
import { ChangeSectorPopupComponent } from './change-sector-dialog.component';
import { ClientTableQrPopupComponent} from './client-table-qr-dialog.component';
import { ClientTableDeletePopupComponent } from './client-table-delete-dialog.component';

@Injectable()
export class ClientTableResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const clientTableRoute: Routes = [
    {
        path: 'sector/:sectorId/client-table',
        component: ClientTableComponent,
        resolve: {
            'pagingParams': ClientTableResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.clientTable.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'client-table/:id',
        component: ClientTableDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.clientTable.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'client-table-new',
        component: ClientTablePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.clientTable.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-table/:id/edit',
        component: ClientTablePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.clientTable.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-table/:id/qr',
        component: ClientTableQrPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.clientTable.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-table/:id/change-sector',
        component: ChangeSectorPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.clientTable.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
export const clientTablePopupRoute: Routes = [
    {
        path: 'client-table/:id/delete',
        component: ClientTableDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.clientTable.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
