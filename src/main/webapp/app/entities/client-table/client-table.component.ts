import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ClientTable } from './client-table.model';
import { Sector, SectorService } from '../sector';
import { ClientTableService } from './client-table.service';
import { ITEMS_PER_PAGE, Principal } from '../../shared';

@Component({
    selector: 'jhi-client-table',
    templateUrl: './client-table.component.html',
    styleUrls: [
        'client-table.scss'
    ]
})
export class ClientTableComponent implements OnInit, OnDestroy {
    displayedColumns = [ 'number', 'chairs', 'modifiable', 'Actions' ];

    @ViewChild(MatSort) sortTable: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;

    currentAccount: any;
    clientTables: ClientTable[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    dataSource: any;
    sectorId: number;
    sectorName = '';

    constructor(
        private clientTableService: ClientTableService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private sectorService: SectorService
    ) {
        this.dataSource = new MatTableDataSource();
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.clientTableService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: HttpResponse<ClientTable[]>) => this.onSuccess(res.body, res.headers),
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            return;
        }

        this.clientTableService.findBySectorId(this.sectorId, {
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
                (res: HttpResponse<ClientTable[]>) => this.onSuccess(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/client-table'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/client-table', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        query = query.trim(); // Remove whitespace
        query = query.toLowerCase();
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/client-table', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.activatedRoute.params.subscribe((params) => {
            this.sectorId = params['sectorId'];

            this.sectorService.find(this.sectorId).subscribe(
                (res: HttpResponse<Sector>) => this.sectorName = res.body.name,
                (res: HttpErrorResponse) => this.onError(res.message)
            );
            this.loadAll();
        });
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInClientTables();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ClientTable) {
        return item.id;
    }
    registerChangeInClientTables() {
        this.eventSubscriber = this.eventManager.subscribe('clientTableListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.clientTables = data;
        this.dataSource = new MatTableDataSource(this.clientTables);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sortTable;
    }
    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    back() {
        this.router.navigate(['/sector']);
    }
}
