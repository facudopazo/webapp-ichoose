import { BaseEntity } from './../../shared';

export class Role implements BaseEntity {
    constructor(
        public id?: number,
        public type?: any,
        public login?: string,
        public restaurantId?: number,
    ) {
    }
}
