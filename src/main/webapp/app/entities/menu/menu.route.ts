import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { MenuComponent } from './menu.component';
import { MenuDetailComponent } from './menu-detail.component';
import { MenuDialogComponent } from './menu-dialog.component';
import { MenuDeletePopupComponent } from './menu-delete-dialog.component';

@Injectable()
export class MenuResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const menuRoute: Routes = [
    {
        path: 'menu',
        component: MenuComponent,
        resolve: {
            'pagingParams': MenuResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.menu.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'menu-new',
        component: MenuDialogComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.menu.home.title'
        },
        canActivate: [UserRouteAccessService],
    },
    {
        path: 'menu/:id/edit',
        component: MenuDialogComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.menu.home.title'
        },
        canActivate: [UserRouteAccessService],
    },

     {
        path: 'menu/:id',
        component: MenuDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.menu.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const menuPopupRoute: Routes = [

    {
        path: 'menu/:id/delete',
        component: MenuDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ichooseApp.menu.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
