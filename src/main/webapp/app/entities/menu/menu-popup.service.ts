import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { Menu } from './menu.model';
import { MenuService } from './menu.service';

@Injectable()
export class MenuPopupService {
    private ngbModalRef: MatDialogRef<Component>;

    constructor(
        private datePipe: DatePipe,
        private modalService: MatDialog,
        private router: Router,
        private menuService: MenuService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<MatDialogRef<Component>> {
        return new Promise<MatDialogRef<Component>>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.menuService.find(id)
                    .subscribe((menuResponse: HttpResponse<Menu>) => {
                        const menu: Menu = menuResponse.body;
                        this.ngbModalRef = this.menuModalRef(component, menu);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.menuModalRef(component, new Menu());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    menuModalRef(component: any, menu: Menu): MatDialogRef<Component> {
        const modalRef = this.modalService.open(component, {height: '250px',
        width: '600px', data: {'menu': menu}});
        modalRef.afterClosed().subscribe((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
