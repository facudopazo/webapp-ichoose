import { BaseEntity } from './../../shared';
import { Picture } from '../Picture';
import { MenuClassification } from '../menu-classification';

export class Menu implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public price?: number,
        public quantity?: number,
        public destination?: any,
        public disabled?: boolean,
        public deleteDate?: any,
        public measuringUnitId?: number,
        public classificationId?: number,
        public classification?: MenuClassification,
        public restaurantId?: number,
        public pictures?: BaseEntity[],
        public subclassifications?: BaseEntity[],
    ) {
        this.disabled = false;
        this.pictures = new Array <Picture>();
        this.classification =  new MenuClassification();
    }
}
