import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable ,  Subscription } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { finalize } from 'rxjs/operators';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { Menu } from './menu.model';
import { MenuPopupService } from './menu-popup.service';
import { MenuService } from './menu.service';
import { MeasuringUnit, MeasuringUnitService } from '../measuring-unit';
import { MenuClassification, MenuClassificationService } from '../menu-classification';
import { Restaurant, RestaurantService } from '../restaurant';
import { Picture, PictureService } from '../picture';
import { MenuSubclassification, MenuSubclassificationService } from '../menu-subclassification';
import { JhiDataUtils } from 'ng-jhipster';
import { FormGroup } from '@angular/forms/src/model';
import {AngularFireStorage} from 'angularfire2/storage';

@Component({
    selector: 'jhi-menu-dialog',
    templateUrl: './menu-dialog.component.html',
    styleUrls: [
        'menu.scss'
    ]
})
export class MenuDialogComponent implements OnInit {

    menu: Menu;
    isSaving: boolean;
    private subscription: Subscription;
    nameFormControl: FormControl;
    priceFormControl: FormControl;
    quantFormControl: FormControl;
    currentRestaurant: Restaurant;

    measuringunits: MeasuringUnit[];

    menuclassifications: MenuClassification[];

    restaurants: Restaurant[];

    pictures: Picture[];

    newPicture: Picture;

    menusubclassifications: MenuSubclassification[];

    constructor(
        // public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private jhiAlertService: JhiAlertService,
        private menuService: MenuService,
        private measuringUnitService: MeasuringUnitService,
        private menuClassificationService: MenuClassificationService,
        private restaurantService: RestaurantService,
        private pictureService: PictureService,
        private menuSubclassificationService: MenuSubclassificationService,
        private eventManager: JhiEventManager,
        private route: ActivatedRoute,
        private router: Router,
        private storage: AngularFireStorage
    ) {
    }

    ngOnInit() {
        this.nameFormControl = new FormControl('', [
            Validators.required
        ]);
        this.priceFormControl = new FormControl('', [
            Validators.required
        ]);
        this.quantFormControl = new FormControl('', [
            Validators.required
        ]);
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.menu = new Menu();
                this.load(params['id']);
            } else {
                this.menu = new Menu();
                for ( let i = 0 ; i < 5 ; i++ ) {
                    this.menu.pictures.push(new Picture());
                }
            }
        });

        this.isSaving = false;
        this.measuringUnitService.query()
            .subscribe((res: HttpResponse<MeasuringUnit[]>) => { this.measuringunits = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.menuClassificationService.query()
            .subscribe((res: HttpResponse<MenuClassification[]>) => { this.menuclassifications = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.restaurantService.query()
            .subscribe((res: HttpResponse<Restaurant[]>) => { this.restaurants = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.pictureService.query()
            .subscribe((res: HttpResponse<Picture[]>) => { this.pictures = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.menuSubclassificationService.query()
            .subscribe((res: HttpResponse<MenuSubclassification[]>) => { this.menusubclassifications = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.restaurantService.getCurrent()
            .subscribe((res: Restaurant) => { this.currentRestaurant = res; });
    }

    clear() {
        this.router.navigate(['menu']);
    }

    save() {
        this.isSaving = true;
        this.menu.quantity = 1;
        this.menu.pictures = this.menu.pictures
            .filter((picture: Picture) => picture.url != null);
        console.log(this.menu.pictures);
        this.menu.restaurantId = this.currentRestaurant.id;
        if (this.menu.id !== undefined) {
            this.subscribeToSaveResponse(
                this.menuService.update(this.menu));
        } else {
            this.subscribeToSaveResponse(
                this.menuService.create(this.menu));
                console.log(this.menu);
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Menu>>) {
        result.subscribe((res: HttpResponse<Menu>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Menu) {
        this.eventManager.broadcast({ name: 'menuListModification', content: 'OK'});
        this.isSaving = false;
        this.router.navigate(['menu']);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackMeasuringUnitById(index: number, item: MeasuringUnit) {
        return item.id;
    }

    trackMenuClassificationById(index: number, item: MenuClassification) {
        return item.id;
    }

    trackRestaurantById(index: number, item: Restaurant) {
        return item.id;
    }

    trackPictureById(index: number, item: Picture) {
        return item.id;
    }

    trackMenuSubclassificationById(index: number, item: MenuSubclassification) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }

    addPicture(event, picture) {
        if (event.srcElement.value.length > 0) {
            const file = event.target.files[0];
            picture['path'] = 'menus/' + Math.random().toString(36).substring(5);
            const fileRef = this.storage.ref(picture['path']);
            const task = this.storage.upload(picture['path'], file);
            // observe percentage changes
            const uploadPercent = task.percentageChanges().subscribe(
                (value) => {
                    picture['percentage'] = value;
                }
            );
            // get notified when the download URL is available
            task.snapshotChanges().pipe(
                finalize(() => { fileRef.getDownloadURL().subscribe(
                    (url) => {
                        picture.url = url;
                    }
                );
                })
            )
                .subscribe();
        }
    }

    removPicture(item) {
        const indexArray = this.menu.pictures.indexOf(item);
        this.menu.pictures.splice(indexArray, 1);
    }

    onClassificationChange() {
     if (this.menu.classificationId !== undefined) {
         this.menu.classification = this.menuclassifications.find( (cl)  => ( cl.id === this.menu.classificationId));
     }
    }
    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    load(id) {
        this.menuService.find(id)
            .subscribe((menuResponse: HttpResponse<Menu>) => {
                this.menu = menuResponse.body;
                const aux: number = 5 - this.menu.pictures.length;
                for (let i = 0; i < aux; i++) {
                    this.menu.pictures.push(new Picture());
                }
                console.log(this.menu);
                // this.onClassificationChange();
            });
    }
}
@Component({
    selector: 'jhi-menu-popup',
    template: ''
})
export class MenuPopupComponent implements OnInit, OnDestroy {

    downloadURL: any;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private menuPopupService: MenuPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.menuPopupService
                    .open(MenuDialogComponent as Component, params['id']);
            } else {
                this.menuPopupService
                    .open(MenuDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
      const isSubmitted = form && form.submitted;
      return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}
