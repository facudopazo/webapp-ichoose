import { Component, OnInit, AfterViewInit, Renderer, ElementRef } from '@angular/core';
import { JhiLanguageService } from 'ng-jhipster';
import { Router } from '@angular/router';

import { MatSnackBar } from '@angular/material';
import { Register } from './register.service';
import { LoginService } from '../../shared/login/login.service';
import {  EMAIL_ALREADY_USED_TYPE, LOGIN_ALREADY_USED_TYPE } from '../../shared';

@Component({
    selector: 'jhi-register',
    templateUrl: './register.component.html',
    styleUrls: [
        'register.scss'
    ]

})
export class RegisterComponent implements OnInit, AfterViewInit {
    hide = true;

    confirmPassword: string;
    doNotMatch: string;
    error: string;
    errorEmailExists: string;
    errorUserExists: string;
    registerAccount: any;
    success: boolean;

    constructor(
        private languageService: JhiLanguageService,
        private registerService: Register,
        private loginService: LoginService,
        private elementRef: ElementRef,
        private renderer: Renderer,
        public snackBar: MatSnackBar,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.success = false;
        this.registerAccount = {};
    }

    ngAfterViewInit() {
    //    this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#login'), 'focus', []);
    }

    register() {
        if (this.registerAccount.password !== this.confirmPassword) {
            this.doNotMatch = 'ERROR';
        } else {
            this.doNotMatch = null;
            this.error = null;
            this.errorUserExists = null;
            this.errorEmailExists = null;
            this.languageService.getCurrent().then((key) => {
                this.registerAccount.langKey = key;
                this.registerService.save(this.registerAccount).subscribe(() => {
                    this.loginService.login({
                        'username': this.registerAccount.login,
                        'password': this.registerAccount.password,
                        'rememberMe': false
                    }).then(() => {
                        this.router.navigate([''], { queryParams: { registeredRestaurantAdmin: ''} });
                    });
                    // this.success = true;
                }, (response) => this.processError(response));
            });
        }
    }

    openLogin() {
        this.router.navigate(['login']);
    }

    private processError(response) {
        this.success = null;
        if (response.status === 400 && response.error && response.error.errorKey === 'userexists' || response.error.errorKey === 'loginexists') {
            const nameInUse = document.getElementById('nameInUse').textContent;
            const snackBarRef = this.snackBar.open(nameInUse, '✕');
        } else if (response.status === 400 && response.error && response.error.errorKey === 'emailexists') {
            const emailInUse = document.getElementById('emailInUse').textContent;
            const snackBarRef = this.snackBar.open(emailInUse, '✕');
        } else {
            const errorSign = document.getElementById('errorSign').textContent;
            const snackBarRef = this.snackBar.open(errorSign, '✕');
        }
    }
}
