import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { JhiEventManager, JhiLanguageService } from 'ng-jhipster';
import { Subscription } from 'rxjs';
import { Client, ClientService } from '../../entities/client';
import { ProfileService } from '../profiles/profile.service';
import { JhiLanguageHelper, Principal, LoginService, AccountService } from '../../shared';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { RestaurantService, Restaurant } from '../../entities/restaurant';

import { VERSION } from '../../app.constants';

@Component({
    selector: 'jhi-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: [
        'navbar.scss'
    ]
})
export class NavbarComponent implements OnInit {
    inProduction: boolean;
    isNavbarCollapsed: boolean;
    languages: any[];
    swaggerEnabled: boolean;
    version: string;
    showSideNav: boolean;
    restaurants: Restaurant[];
    currentRestaurant: Restaurant = new Restaurant();
    eventSubscriber: Subscription;
    user: any;
    client: Client;
    unreadNotifications: any;

    constructor(
        private loginService: LoginService,
        private clientService: ClientService,
        private accountService: AccountService,
        private languageService: JhiLanguageService,
        private languageHelper: JhiLanguageHelper,
        private principal: Principal,
        private profileService: ProfileService,
        private router: Router,
        public dialog: MatDialog,
        public restaurantService: RestaurantService,
        private eventManager: JhiEventManager
    ) {
        this.version = VERSION ? 'v' + VERSION : '';
        this.isNavbarCollapsed = true;
        this.languages = [];
        this.showSideNav = false;
        this.unreadNotifications = 0;
    }

    editRestaurant(event, restaurantId) {
        event.stopPropagation();
        this.router.navigate(['restaurant/' + restaurantId + '/edit']);
    }

    ngOnInit() {
        this.eventSubscriber = this.eventManager.subscribe('notificationsUnreadUpdate', (response) => {
            this.unreadNotifications = response.content;
        });
        this.languageHelper.getAll().then((languages) => {
            this.languages = languages;
        });

        this.profileService.getProfileInfo().then((profileInfo) => {
            this.inProduction = profileInfo.inProduction;
            this.swaggerEnabled = profileInfo.swaggerEnabled;
        });

        this.principal.identity().then(
            (account) => {
                if (account != null) {
                    this.restaurantService.getCurrent().subscribe((restaurant: Restaurant) => {
                        this.currentRestaurant = restaurant;
                        sessionStorage.setItem('currentRestaurant', restaurant.id.toString());
                    });
                    this.loadRestaurants();
                    this.clientService.getClientByUserId(1).subscribe(
                        (client) => {
                            if (client) {
                                this.client = client.body;
                            } else {
                                this.client = null;
                            }
                        });
                } else {
                    this.client = null;
                }

            }
        );

        this.principal.getAuthenticationState().subscribe(
            (notUsedParameter) => {
                if (this.isAuthenticated()) {
                    this.restaurantService.getCurrent().subscribe((restaurant: Restaurant) => {
                        this.currentRestaurant = restaurant;
                        sessionStorage.setItem('currentRestaurant', restaurant.id.toString());
                    });
                    this.loadRestaurants();
                    this.clientService.getClientByUserId(1).subscribe(
                        (client) => {
                            if (client) {
                                this.client = client.body;
                            }
                        });
                } else {
                    this.client = null;
                }
            }
        );
        this.registerChangeInRestaurants();
        this.accountService.get().subscribe(
            (account: HttpResponse<Account>) => {
                this.user = account.body;
                this.clientService.getClientByUserId(1).subscribe(
                    (client) => {
                        if (client) {
                            this.client = client.body;
                        }
                    });
            });
    }

    changeLanguage(languageKey: string) {
        this.languageService.changeLanguage(languageKey);
    }

    collapseNavbar(sidebar) {
        if (sidebar && sidebar.opened) {
            sidebar.toggle();
        }
    }

    isAuthenticated() {
        const bool = this.principal.isAuthenticated();
        return bool;
    }

    login(sidebar) {
        this.collapseNavbar(sidebar);
        this.router.navigate(['login']);
    }

    logout(sidebar) {
        this.collapseNavbar(sidebar);
        this.loginService.logout();
        this.router.navigate(['']);
    }

    toggleNavbar() {
        this.isNavbarCollapsed = !this.isNavbarCollapsed;
    }

    getImageUrl() {
        return this.isAuthenticated() ? this.principal.getImageUrl() : null;
    }
/*

    openRestaurantOwnersDialog(sidebar): void {
        this.collapseNavbar(sidebar);
        this.dialog.open(RestaurantOwnersPopupComponent, {
            height: 'auto',
            width: '80vw',
            data: {}
        });
    }
*/

    changeCurrentRestaurant(restaurant: Restaurant) {
        this.restaurantService.changeCurrent(restaurant)
            .then(
                () => {
                    this.currentRestaurant = restaurant;
                },
                () => {
                    console.log('No se pudo cambiar restaurante actual.');
                }
            ).catch(
                () => {
                    console.log('No se pudo cambiar restaurante actual.');
                }
            );
    }

    showNoneRestaurantOption() {
        return !(this.restaurants !== undefined && this.restaurants.length > 0);
    }

    loadRestaurants() {
        this.restaurantService.query({ size: 999999 }).subscribe(
            (res: HttpResponse<Restaurant[]>) => {
                this.restaurants = res.body;
            }
        );
    }

    registerChangeInRestaurants() {
        this.eventSubscriber = this.eventManager.subscribe('restaurantListModification', (response) => this.loadRestaurants());
    }

}

/*@Component({
    selector: 'jhi-restaurant-owners-popup',
    templateUrl: 'restaurant-owners-popup.component.html',
    styleUrls: [
        'navbar.scss'
    ]
})*/
// export class RestaurantOwnersPopupComponent {
//
//     constructor(
//         public dialogRef: MatDialogRef<RestaurantOwnersPopupComponent>,
//         @Inject(MAT_DIALOG_DATA) public data: any) { }
//
//     onNoClick(): void {
//         this.dialogRef.close();
//     }
// }
