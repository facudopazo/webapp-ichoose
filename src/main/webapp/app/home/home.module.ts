import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IchooseSharedModule } from '../shared';

import { HOME_ROUTE, HomeComponent, WelcomePopupComponent } from './';

@NgModule({
    imports: [
        IchooseSharedModule,
        RouterModule.forChild([ HOME_ROUTE ]),
        FormsModule,
        ReactiveFormsModule,
    ],
    declarations: [
        HomeComponent,
        WelcomePopupComponent
    ],
    entryComponents: [
        WelcomePopupComponent
    ],
    providers: [
        WelcomePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IchooseHomeModule {}
