import { Component, OnInit, Inject } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';
import { ActivatedRoute, Router } from '@angular/router';

import { Account, User, Principal, UserService } from '../shared';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: [
        'home.scss'
    ]

})
export class HomeComponent implements OnInit {
    account: Account;
    user: User;

    constructor(
        private principal: Principal,
        private eventManager: JhiEventManager,
        private router: Router,
        private route: ActivatedRoute,
        public dialog: MatDialog,
        private userService: UserService
    ) {
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.account = account;
            if (this.account) {
                this.userService.find(this.account.login).subscribe((response) => {
                   this.user = response.body;
                });
            }
        });
        this.route.queryParams
            .subscribe((params) => {
                if (params.registeredRestaurantAdmin !== undefined) {
                    this.showRegisteredRestaurantAdminPopUp();
                }
            }
        );
        this.registerAuthenticationSuccess();
    }

    showRegisteredRestaurantAdminPopUp() {
        this.dialog.open(WelcomePopupComponent, {
            height: '300px',
            width: '500px',
            data: {}
        });
    }

    registerAuthenticationSuccess() {
        this.eventManager.subscribe('authenticationSuccess', (message) => {
            this.principal.identity().then((account) => {
                this.account = account;
            });
        });
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        this.router.navigate(['login']);
    }
}

@Component({
    selector: 'jhi-welcome-popup',
    templateUrl: 'welcome-popup.component.html',
    styleUrls: [
        'home.scss'
    ]
})
export class WelcomePopupComponent {

    constructor(
        public dialogRef: MatDialogRef<WelcomePopupComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private router: Router) { }

    onNoClick(): void {
        this.dialogRef.close();
    }
}
